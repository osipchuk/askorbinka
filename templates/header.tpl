{* Smarty *}
<!DOCTYPE html>
<html lang="{$app->getLang()}">
<head>
 <meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <title>{$metaTags->get_title()}</title>
 <meta name="Description" content="{$metaTags->get_description()}">
 <meta name="Keywords" content="{$metaTags->get_keywords()}">
 <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
 <link rel="icon" href="/favicon.ico" type="image/x-icon">
 
 <!-- Bootstrap -->
 <link href="/bootstrap/css/bootstrap.css" rel="stylesheet">
 <link href="/css/owl.carousel.css" rel="stylesheet">
 <link href="/css/owl.theme.css" rel="stylesheet">
 <link href="/js/file_uploader/fileuploader.css" rel="stylesheet">
 <link href="/css/style.css" rel="stylesheet">
 <link href="/css/media-queries.css" rel="stylesheet">
 <link href="/css/fonts.css" rel="stylesheet">
 <link href="/css/print.css" rel="stylesheet">
 <link href="/js/jquery-ui-1.10.1.custom/themes/base/jquery.ui.base.css" rel="stylesheet">
 <link href="/js/jquery-ui-1.10.1.custom/themes/base/jquery.ui.datepicker.css" rel="stylesheet">
 
 <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
 <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
 <!--[if lt IE 9]>
 <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
 <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
 <![endif]-->
 <script src="/js/jquery-1.10.2.min.js"></script>
 <script src="/bootstrap/js/bootstrap.min.js"></script>
 <script src="/js/scripts.js"></script>
 <script src="/js/order.js"></script>
 <script src="/js/cart.js"></script>
 <script src="/js/filters.js"></script>
 <script src="/js/profile.js"></script>
 <script src="/js/owl.carousel.min.js"></script>
 <script src="/js/jquery-ui-1.10.1.custom/js/jquery-ui-1.10.1.custom.min.js"></script>
 <script src="/js/jquery-ui-1.10.1.custom/datepicker-ru.js"></script>
 <script src="/js/file_uploader/fileuploader.js"></script>
 <script src="/js/jquery.mask.min.js"></script>
 {include file="default/fancybox-init.tpl"}

 <link href="/js/fancybox2/helpers/jquery.fancybox-buttons.css" rel="stylesheet">
 <script src="/js/fancybox2/helpers/jquery.fancybox-buttons.js"></script>
 <script>
  var you_must_fill_all_the_fields = "{$words->_('you_must_fill_all_the_fields')|escape}", 
		  cart_no_goods = "{$words->cart_no_goods('Ваша корзина пуста')|escape}",
		  cart_delete_item_confirm = "{$words->cart_delete_item_confirm('Вы действительно хотите удалить товар из корзины?')|escape}",
		  cart_good_is_in_cart = "{$words->goods_item_is_in_cart|escape}",
		  cart_good_add_to_cart = "{$words->goods_add_to_cart|escape}",
		  logout_url = "{'/ajax/auth_ajax/logout'|url}",
		  ships = [],
		  avatar_upload_button = "{$words->avatar_upload_button('Изменить аватар')|escape}",
		  avatar_upload_url = "{'/ajax/edit_profile/upload_avatar'|url}",
          
		  generate_code_disabled = {ldelim}2: "{$words->generate_code_disabled_center('Пожалуйста, выберите товар')|escape}",
		                                   3: "{$words->generate_code_disabled_right('Пожалуйста, сохраните карточку кода')|escape}"{rdelim};
 </script>
</head>
<body class="{if $app->isStart()}start{else}main{/if}-page">
{$appHelper->get_global('analytics')}