{* Smarty *}
{assign var="cart" value=$controller->get_cart()}
<div class="container">
 <div class="cart">
  {$controller->depart_item.caption|print_caption}
  <div class="cart-section">
   {if $controller->items}
	<table class="table table-cart" id="cart-table">
	 <tr>
	  <th colspan="2">{$words->cart_th_caption('Товар')}</th>
	  <th class="hidden-xs">{$words->cart_th_count('Количество')}</th>
	  <th>{$words->cart_th_price('Стоимость')}</th>
	  <th></th>
	 </tr>
	 {foreach from=$controller->items item="item"}
	  {assign var="good" value=$item.good}
	  <tr data-id="{$good.id}">
	   <td class="image">{if $good.image}<img class="hidden-xs" src="/admin/image.php?file=project_images/{$good.image}&amp;width=125&amp;height=90" alt="{$good.caption|escape}">{/if}</td>
	   <td class="caption">
		<a href="{$good.url}">{$good.category} {$good.caption}</a>
		<div class="code">{$words->cart_code('Код товара:')} {$good.code}</div>
	   </td>
	   <td class="count hidden-xs">
		<div class="input-group input-group-lg">
      <span class="input-group-btn">
        <button class="btn btn-info more" data-count-change="1" type="button">Less</button>
      </span>
		 <input type="text" class="form-control change-count" value="{$item.count}">
	  <span class="input-group-btn">
        <button class="btn btn-info less" data-count-change="-1" type="button">More</button>
      </span>
		</div>
	   </td>
	   <td class="price">
		<span>{$item.price|price_format}</span>{$words->currency}
	   </td>
	   <td class="operations">
		<a class="delete" href="#"><span class="btn btn-sm btn-info"></span>{$words->cart_delete('Удалить')}</a>
	   </td>
	  </tr>
	 {/foreach}
	 {if $cart->getFreeShip()}
	  <tr>
	   <td class="image"></td>
	   <td class="caption">{$words->cart_free_ship('Бесплатная доставка')}</td>
	   <td class="count hidden-xs">
		<div class="input-group input-group-lg">
		 <input type="text" class="form-control change-count" value="1" readonly>
		</div>
	   </td>
	   <td class="price">-</td>
	   <td class="operations">-</td>
	  </tr>
	 {/if}
	</table>
	<output class="output">
	 {$words->cart_output('Итого:')} <span data-cart-price="1">{$controller->cart_summary.price}</span> {$words->currency}
	</output>
	<footer class="block-footer">
	 <a class="btn btn-primary btn-lg btn-back" href="javascript:history.back()">{$words->cart_back('Вернуться к покупкам')}</a>
	 <a class="btn btn-warning btn-lg btn-make-order" href="{$url_maker->d_module_url('order')}">{$words->cart_order_href('Оформить заказ')}</a>
	</footer>
   {else}
	{$words->cart_no_goods('К сожалению, Ваша корзина пуста')}
   {/if}
   
  </div>
 </div>
</div>

{include file="manual-blocks-list.tpl"}