{* Smarty *}
<div class="catalog">
 {if !$controller->hideFilters}
  {include file="catalog-filters.tpl"}
 {/if}

 <div class="catalog-list-comparison">
  <div class="container">
   <a class="close" href="#"></a>
   <div id="popup-table-comparison"></div>
  </div>
 </div>
 
 <div class="grey">
  <div class="container">
   <ul class="catalog-list row-5">
	{foreach from=$controller->items item="item"}
	 <li class="col-xs-4-4 col-sm-2-4 col-md-1-4 col-lg-1-5">
	  {include file="catalog-list-item.tpl" full_footer=1}
	 </li>
	{/foreach}
   </ul>
  </div>
  {include file="pagination.tpl"}
 </div>
</div>

{include file="best-sellers-and-last-goods-list.tpl"}

{include file="manual-blocks-list.tpl"}