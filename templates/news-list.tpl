{* Smarty *}
{assign var="filters" value=$controller->filters}
<div class="container">
 <div class="news-list">
  <header class="block-header">
   <form class="form-inline form-large" action="#">
	<div class="form-group">
	 <label for="sort-button">{$words->news_header_date_caption('Сортировать новости')}</label>
	 <div class="btn-group calendar">
	  <button id="sort-button" type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
	   {if $filters->getFilter('dateBegin') || $filters->getFilter('dateEnd')}
		{$filters->getFilter('dateBegin')|default:'...'}  -  {$filters->getFilter('dateEnd')|default:'...'}
	   {else}
		{$words->news_header_date_button('по дате')}
	   {/if}
       <span class="caret"></span>
	  </button>
	  <div class="dropdown-menu">
	   <div class="row">
		{include file="news-and-actions-items/filters-calendar.tpl"}
	   </div>
	  </div>
	 </div>
	 <div class="btn-group">
	  <button id="sort-button" type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
	   {assign var="category_caption" value=$filters->getCategoryCaption()}
	   {if $category_caption}
		{$category_caption}
	   {else}
		{$words->news_header_category_button('по категории')}
	   {/if}
	   <span class="caret"></span>
	  </button>
	  <ul class="dropdown-menu" role="menu">
	   {foreach from=$filters->getCategories() item="filter_item"}
		{include file="news-and-actions-items/top-filter-category-item.tpl" item=$filter_item filters=$filters}
	   {/foreach}
	  </ul>
	 </div>
	</div>
	<div class="form-group">
	 <label for="count-button">{$words->news_header_limit_caption('Выводить по:')}</label>
	 <div class="btn-group">
	  <button id="count-button" type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
	   {$filters->getFilter('limit')} <span class="caret"></span>
	  </button>
	  <ul class="dropdown-menu" role="menu">
	   {foreach from=$filters->getLimits() item="limit"}
	    <li><a href="?{$filters->getParamsStr('limit', $limit)}">{$limit}</a></li>
	   {/foreach}
	  </ul>
	 </div>
	</div>
   </form>
  </header>
  
  {foreach from=$controller->data item="item"}
   {include file="news-list-item.tpl"}
  {/foreach}

  {include file="pagination.tpl"}
 </div>
</div>

{include file="best-sellers-and-last-goods-list.tpl"}

{include file="manual-blocks-list.tpl"}