{* Smarty *}
{assign var="account" value=$controller->account}
<div class="profile">
 <div class="container">
  <div class="profile-header">
   {if $controller->profile_message}
	<div class="alert alert-warning" role="alert">{$controller->profile_message}</div>
   {/if}
   <div class="row">
	<div class="col-xs-4 col-md-2">
	 <div class="avatar">
	  <img src="{$controller->clients_model->account_avatar($account)}" alt="{$controller->clients_model->account_fio($account)|escape}">
	 </div>
	</div>
	<div class="col-xs-8 col-md-4 col-lg-3 col-md-push-6 col-lg-push-7">
	 <a href="{$url_maker->d_module_url('profile')|cat:"/history"|url}">
	 <div class="bonuses-summary">
	  <div class="well well-lg well-primary">
	   <div class="row">
		<div class="col-xs-5">{$words->profile_bonuses_summary('На вашем балансе бонусов:')}</div>
		<div class="col-xs-3"><div class="ico"></div></div>
		<div class="col-xs-4"><div class="bonuses">{$account.bonuses}</div></div>
	   </div>
	  </div>
	 </div>
	 </a>
	</div>
	<div class="col-sm-10 col-md-6 col-lg-7 col-md-pull-4 col-lg-pull-3">
	 <div class="company-describe">
	  {$controller->clients_model->account_fio($account)|print_caption}
	  {*<div class="describe">Описание компании в несколько слов</div>*}
	 </div>
	</div>
   </div>
   {include file="profile/simple-nav.tpl"}
   {if $account.type == 'pr'}{include file="profile/pro-nav.tpl" nav=$controller->pro_nav}{/if}
  </div>
 </div>
 {include file="profile/"|cat:$controller->profileTemplate}

</div>

{include file="best-sellers-and-last-goods-list.tpl"}

{include file="manual-blocks-list.tpl"}