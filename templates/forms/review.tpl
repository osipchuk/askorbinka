{* Smarty *}
{if !$controller->account}{assign var="registered" value=0}{else}{assign var="registered" value=1}{/if}
{assign var="account" value=$controller->account}
<form class="review-add{if !$registered} unregistered{/if}" action="{'ajax/reviews_post'|url}" role="form" data-ajax-form="">
 <header class="block-header">
  <div class="row">
   {if $registered}
	<div class="col-xs-1"><div class="avatar"><img src="{$controller->clients_model->account_avatar($account)}"  alt="{$controller->clients_model->account_fio($account)|escape}"></div></div>
	<div class="col-xs-11">
	 <div class="h4 arial fio">{$controller->clients_model->account_fio($account)}</div>
	 <h3 class="h1">{$words->reviews_form_caption('написать отзыв')}</h3>
	</div>
   {else}
	<div class="col-lg-3">
	 <h3 class="h1">{$words->reviews_form_caption('написать отзыв')}</h3>
	</div>
	<div class="col-lg-9">
	 <div class="register-block">
	  <span class="message">{$words->reviews_form_you_should_auth('Отзывы могут оставлять только зарегистрированные пользователи')}</span>
	  <a class="btn btn-info" href="{$url_maker->d_module_url('profile')|cat:'/register'|url}">{$words->register_link_caption('Регистрация')}</a>
	  <a class="btn btn-info" href="{$url_maker->d_module_url('auth')}">{$words->auth_link_caption('Войти')}</a>
	 </div>
	</div>
   {/if}
  </div>
 </header>
 <div class="form-group">
  <label class="sr-only" for="text">{$words->reviews_form_text_label('Отзыв')}</label>
  <textarea class="form-control" name="text" id="text"></textarea>
  <input type="hidden" name="good" value="{$good}">
  <input type="hidden" name="rating" id="rating" value="0">
 </div>
 <footer class="block-footer">
  <div class="row">
   {if $registered}
	<div class="col-xs-6">
	 <div class="h2">{$words->reviews_form_rate_label('поставить оценку')}</div>
	 <div class="rating rating-editable">
	  <span class="stars">{section name="stars" loop=5}<span class="star"></span>{/section}</span>
	 </div>
	</div>
   {/if}
   <div class="{if !$registered}col-sm-offset-6 {/if}col-sm-6 text-right">
	<div class="submit-block">
	 <input type="submit" class="btn btn-primary" value="{$words->reviews_form_submit('Отправить')}">
	</div>
   </div>
  </div>
 </footer>
</form>