{* Smarty *}
<form class="form-horizontal register" action="{'ajax/profile_ajax/bonuses_transaction_request'|url}" role="form" data-ajax-form="">
 <div class="form-group">
  <label for="amount" class="col-sm-4 control-label">{$words->bonuses_transaction_form_amount('Количество бонусов')} <sup>*</sup></label>
  <div class="col-sm-8"><input type="text" class="form-control input-lg" id="amount" name="amount" value="{$account.bonuses}" required></div>
 </div>
 <div class="form-group">
  <label for="card" class="col-sm-4 control-label">{$words->bonuses_transaction_form_card('Номер карты для вывода')} <sup>*</sup></label>
  <div class="col-sm-8"><input type="text" class="form-control input-lg" id="card" name="card" value="{$account.credit_card}" required></div>
 </div>

 <div class="form-group">
  <div class="col-sm-8 col-sm-offset-4">
   <footer class="block-footer">
	<div class="note">{$words->edit_form_note('<sup>*</sup> Поля, обязательные для заполнения')}</div>
	<div class="buttons">
	 <a href="#" class="btn btn-primary btn-block btn-lg btn-ok" data-submit-buttton="">{$words->bonuses_transaction_form_submit('Вывести')}</a>
	 <input type="submit">
	</div>
   </footer>
  </div>
 </div>
 
</form>