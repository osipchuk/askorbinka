{* Smarty *}
<form class="search-form" role="form" action="{$url_maker->d_module_url('search')|url}" method="get">
 <div class="visible-md text-right">
  <span class="input-group-btn">
   <input type="button" class="btn btn-search" value="Search" onclick="$(this).parents('form').find('.inputs').removeClass('visible-lg');">
  </span>
 </div>
 <div class="hidden-md inputs">
  <div class="input-group">
   <input name="search" type="text" class="form-control" value="{$controller->searchQuery|escape}" required>
   <span class="input-group-btn">
    <input type="submit" class="btn btn-search" value="{$words->search_submit('Поиск')|escape}">
   </span>
  </div>
 </div>
</form>