{* Smarty *}
{assign var="account" value=$controller->account}
<div class="form-group">
 <label for="name" class="col-sm-4 control-label">{$words->profile_form_name('Имя')}<sup>*</sup></label>
 <div class="col-sm-8"><input type="text" class="form-control input-lg" id="name" name="name" value="{$account.name}" {if $account}readonly{/if} required></div>
</div>
<div class="form-group">
 <label for="surname" class="col-sm-4 control-label">{$words->profile_form_surname('Фамилия')}<sup>*</sup></label>
 <div class="col-sm-8"><input type="text" class="form-control input-lg" id="surname" name="surname" value="{$account.surname}" {if $account}readonly{/if} required></div>
</div>
<div class="form-group">
 <label for="city" class="col-sm-4 control-label">{$words->profile_form_city('Город')}<sup>*</sup></label>
 <div class="col-sm-8"><input type="text" class="form-control input-lg" id="city" name="city" value="{$account.city}" {if $account}readonly{/if} required></div>
</div>
<div class="form-group">
 <label for="tel" class="col-sm-4 control-label">{$words->profile_form_tel('Телефон')}<sup>*</sup></label>
 <div class="col-sm-8"><input type="tel" class="form-control input-lg" id="tel" name="tel" value="{$account.tel}" {if $account}readonly{/if} data-phone-number pattern="\+380 [0-9]{ldelim}2{rdelim} [0-9]{ldelim}7{rdelim}" required></div>
</div>
<div class="form-group">
 <label for="email" class="col-sm-4 control-label">{$words->profile_form_email('Email')}<sup>*</sup></label>
 <div class="col-sm-8"><input type="email" class="form-control input-lg" id="email" name="email" value="{$account.email}" {if $account}readonly{/if} required></div>
</div>