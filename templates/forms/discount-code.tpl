{* Smarty *}
<form class="form form-horizontal" action="{'ajax/discount_codes?mode=save'}" data-ajax-form="" data-ajax-form-response="save_discount_code_response" data-generate-form="1">
 <div class="form-group code-form-group">
  <div class="col-xs-12">
   <a class="btn btn-primary btn-block btn-lg btn-key" href="#" data-generate-code-button="1">{$words->profile_pro_form_generate_code_button('Сгенерировать код')}</a>
   <input type="submit" class="hidden">
   <input type="hidden" name="good" value="{$controller->good_id}">
   <input type="hidden" name="code_id" id="code-id" value="0">
   <input type="hidden" name="send_controller" value="email">
  </div>
 </div>

 <div class="form-group code-form-group">
  <label class="col-sm-3 control-label" for="code">{$words->profile_pro_form_code('Код')}</label>
  <div class="col-sm-9"><input name="code" type="text" class="form-control input-lg" id="code" value="" data-discount-code="1" required></div>
 </div>

 <div class="form-group">
  <label class="col-sm-3 control-label" for="fio">{$words->profile_pro_form_fio('Кому')}</label>
  <div class="col-sm-9"><input name="fio" type="text" class="form-control input-lg" id="fio" required></div>
 </div>
 <div class="form-group">
  <label class="col-sm-3 control-label" for="city">{$words->profile_pro_form_city('Город')}</label>
  <div class="col-sm-9"><input name="city" type="text" class="form-control input-lg" id="city" required></div>
 </div>
 <div class="form-group">
  <label class="col-sm-3 control-label" for="email">{$words->profile_pro_form_email('Email')}</label>
  <div class="col-sm-9"><input name="email" type="email" class="form-control input-lg" id="email" required></div>
 </div>
 <div class="form-group">
  <label class="col-sm-3 control-label" for="tel">{$words->profile_pro_form_tel('Телефон')}</label>
  <div class="col-sm-9"><input name="tel" type="tel" class="form-control input-lg" id="tel"></div>
 </div>
 <div class="form-group">
  <label class="col-sm-3 control-label" for="date">{$words->profile_pro_form_date('Дата')}</label>
  <div class="col-sm-7"><input type="text" class="form-control input-lg" id="date" value="{''|normal_date}"><div class="calendar-show"></div></div>
 </div>

 <div class="form-group">
  <div class="col-sm-offset-3 col-sm-9">
   <div class="checkbox">
	<label>
	 <input type="checkbox" name="show_alts" value="1" data-code-discount-form-change-visibility="alt" checked> {$words->profile_pro_form_alt_avail('Даль альтернативу')}
	</label>
   </div>
   <div class="checkbox">
	<label>
	 <input type="checkbox" name="show_description" value="1" data-code-discount-form-change-visibility="describe" checked> {$words->profile_pro_form_show_describe('Показать описание товара')}
	</label>
   </div>
  </div>
 </div>
</form>