{* Smarty *}
<div class="container">
 <div class="row">
  <div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-2">
   <form class="form-horizontal auth" action="{'ajax/auth_ajax/login'|url}" role="form" data-ajax-form="" data-ajax-form-response="auth_response">
	<header class="block-header">
	 <div class="form-group">
	  <label class="col-sm-4 control-label">{$words->register_social_auth_caption('Войти через')}</label>
	  <div class="col-sm-8">
	   {include file="auth-social.tpl"}
	  </div>
	 </div>
	</header>
	<div class="form-group">
	 <label for="email" class="col-sm-4 control-label">{$words->auth_form_email('Email')}</label>
	 <div class="col-sm-8">
	  <input name="email" type="email" class="form-control input-lg" id="email" required>
	 </div>
	</div>
	<div class="form-group">
	 <label for="password" class="col-sm-4 control-label">{$words->auth_form_pass('Пароль')}</label>
	 <div class="col-sm-8">
	  <input name="password" type="password" class="form-control input-lg" id="password" required>
	 </div>
	</div>
	<div class="form-group">
	 <div class="col-sm-offset-4 col-sm-8">
	  <div class="checkbox">
	   <label class="no-after">
		<input type="checkbox" name="remember" value="1"> {$words->auth_form_remember('Запомнить')}
	   </label>
	  </div>
	 </div>
	</div>
	<div class="form-group">
	 <div class="col-sm-offset-4 col-sm-8">
	  <button type="submit" class="btn btn-lg btn-primary">{$words->auth_form_submit('Войти')}</button>
	  <button type="submit" class="btn btn-lg btn-info" onclick="location.href='{$url_maker->d_module_url('profile')|cat:'/register'|url}'; return false;">{$words->auth_form_register('Зарегистрироваться')}</button>
	 </div>
	</div>
   </form>
  </div>
 </div>
</div>