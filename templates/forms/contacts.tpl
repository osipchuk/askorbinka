{* Smarty *}
<form action="{'ajax/contacts_post'|url}" method="post" role="form" data-ajax-form="">
 <div>
  <label for="fio">{$words->contacts_form_fio('Name')}</label>
  <input type="text" name="fio" id="fio" required>
 </div>
 <div>
  <label for="email">{$words->contacts_form_email('Email')}</label>
  <input type="email" name="email" id="email" required>
 </div>
 <div>
  <textarea name="text" id="message" required></textarea>
 </div>
 <div>
  <label for="email">{$words->contacts_form_captcha('Secure code')}</label>
  <input class="captcha" type="text" name="captcha" id="captcha" required>
  <div class="captcha-img"><img src="/captcha?width=120&amp;height=33&amp;color=51af2f&amp;bg_color=fff&amp;captcha=contacts" width="120" height="33" alt="{$words->contacts_form_captcha|escape}"></div>
 </div>
 <div>
  <input type="submit" value="{$words->contacts_form_submit('Оформить покупку')|escape}">
 </div>
</form>