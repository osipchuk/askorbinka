{* Smarty *}
{assign var="item" value=$controller->item}
<div class="catalog-item">
 <div class="container">
  <div class="row">
   <div class="col-md-6 col-md-push-6 col-lg-5 col-lg-push-4">
	<section class="good-info">
	 <header class="block-header">
      {if $controller->brand}
       <a href="{$controller->brand.url|url}" target="_blank">
        <img class="brand-logo" src="/admin/image.php?file=project_images/{$item.brand_image}&amp;width=300&amp;height=55" alt="{$item.brand_caption|escape}">
       </a>
      {/if}
	  <h1 class="caption">{$words->goods_model('Модель:')} {$controller->goods_model->goodModel($item)}</h1>
	  <h2 class="h1 subcaption">{$item.category}</h2>
	 </header>
	 <div class="buy-info">
	  <div class="row">
	   <div class="col-sm-6">
		<div class="prices">
		 {if $item.price > 0}
		  {if $item.action_price > 0}
		   <div class="price old">
			<div class="row">
			 <div class="col-xs-5 col-sm-3"><label>{$words->goods_discount('Скидка:')}</label></div>
			 <div class="col-xs-7 col-sm-8 col-sm-offset-1"><strong>{$controller->goods_model->getDiscountInPercentages($item)}</strong> %</div>
			</div>
		   </div>
		   <div class="price">
			<div class="row">
			 <div class="col-xs-5 col-sm-3"><label>{$words->goods_price_new('Новая цена:')}</label></div>
			 <div class="col-xs-7 col-sm-8 col-sm-offset-1"><div class="price-wrapper"><strong>{$item.action_price|price_format}</strong> {$words->currency('грн')}</div></div>
			</div>
		   </div>
		  {else}
		   <div class="price simple">
			<div class="row">
			 <div class="col-xs-5 col-sm-3"><label>{$words->goods_price('Цена:')}</label></div>
			 <div class="col-xs-7 col-sm-8 col-sm-offset-1"><div class="price-wrapper"><strong>{$item.price|price_format}</strong> {$words->currency('грн')}</div></div>
			</div>
		   </div>
		  {/if}
		 {/if}
		 
		</div>
	   </div>
	   <div class="col-sm-4">
		{if $item.price !=0 and $item.avail}
		 <a class="btn btn-primary btn-block btn-lg" href="#" data-add-to-cart="{$item.id}" data-buy-one-good="1">{$words->goods_buy('Купить')}</a>
		 {include file="catalog-add-to-cart-button.tpl" item=$item btn_class="btn-warning btn-lg"}
		{/if}
	   </div>
	  </div>
	  <div class="in-stock">
	   <div class="row">
		<div class="col-xs-8 col-sm-6">{$words->goods_in_stock_caption('Наличие на складе')}</div>
		<div class="col-xs-4 label-col"><span class="label {if $item.avail}label-warning{else}label-danger{/if}">{$words->_w('goods_in_stock_', $item.avail)}</span></div>
	   </div>
	  </div>
	 </div>

	 <dl class="dl-horizontal">
	  {if $item.ship}
	   <dt>{$words->goods_ship_caption('Условия доставки:')}</dt>
	   <dd>{$item.ship}</dd>
	  {/if}
	  {if $item.pay}
	   <dt>{$words->goods_pay_caption('Способы оплаты:')}</dt>
	   <dd>{$item.pay}</dd>
	  {/if}
	  {if $item.back}
	   <dt>{$words->goods_back_caption('Возврат:')}</dt>
	   <dd>{$item.back}</dd>
	  {/if}
	  {if $item.warranty}
	   <dt>{$words->goods_warranty_caption('Гарантия:')}</dt>
	   <dd>{$item.warranty}</dd>
	  {/if}
	 </dl>
	 
	</section>
   </div>
   <div class="col-md-6 col-md-pull-6 col-lg-4 col-lg-pull-5">
	<section class="photos">
	 {if $controller->actions}
	  <div class="action-labels">
	   {foreach from=$controller->actions item="action"}
		{include file="news-and-actions-items/label.tpl" item=$action show_href=1}
	   {/foreach}
	  </div>
	 {/if}
	 {if $item.image}
	  <div class="main-photo">
	   <a class="show_photo" data-fancybox-group="good-photos" href="/admin/project_images/{$item.image}" title="{$item.caption|escape}">
	    <img data-main-good-image="{$item.id}" src="/admin/image.php?file=project_images/{$item.image}&amp;width=620&amp;height=285" alt="{$item.caption|escape}">
	   </a>
	  </div>
	 {/if}
	 
	 {if $controller->photos|@count > 1}
	  <div class="small-photos">
	   <div class="row">
		{foreach name="photos" from=$controller->photos item="photo"}
		 <div class="col-xs-4">
		  <a class="show_photo item{if $smarty.foreach.photos.first} active{/if}" data-fancybox-group="good-photos" href="/admin/project_images/{$photo.image}" title="{$photo.caption|escape}">
		   <img src="/admin/image.php?file=project_images/{$photo.image}&amp;width=92&amp;height=85" alt="{$photo.caption|escape}">
		  </a>
		 </div>
		{/foreach}
	   </div>
	  </div>
	 {/if}
	 
	</section>
   </div>
   <div class="col-md-12 col-lg-3">
	<div class="ml-10">
	 {include file="catalog-item-programs.tpl" data=$controller->programs}
	 {if $controller->videos}
	 <section class="video-reviews">
	  <h3 class="h4">{$words->good_videos_caption('Видео-обзор товара')}</h3>
	  <div class="item">
	   {foreach from=$controller->videos item="video"}
		{if $video.video_id}
		 <div class="img"><a class="various fancybox.iframe" title="{$item.caption|escape}" href="http://www.youtube.com/embed/{$video.video_id}?autoplay=1"><img src="{$video.video_id|youtube_video_preview:$video.image}" alt="{$item.caption|escape}"></a></div>
		{/if}
	   {/foreach}
	   <h4>{$words->catalog_videos_text_caption('Как осуществить покупку')}</h4>
	   <p>{$words->catalog_videos_text_text('Найти и заказать именно то, что Вы хотите очень просто!')}</p>
       {*
	   <footer class="block-footer">
		<a class="btn btn-info" href="{$controller->video_more_link.href|url}">{$controller->video_more_link.caption}</a>
	   </footer>
       *}
	  </div>
	 </section>
	 {/if}
	 <nav class="catalog-info-nav">
	  <ul class="nav nav-pills nav-justified">
	   {foreach from=$controller->video_links item="link"}
		<li class="{$link.mode}"><a href="{$link.href|url}"><span class="img"></span>{$link.caption}</a></li>
	   {/foreach}
	  </ul>
	 </nav>
	</div>
   </div>
  </div>
 </div>

 {if $controller->active_tab}
 {assign var="comparison_properties" value=$controller->comparison->get_properties()}
 <section class="good-tabs">
  <div class="container">
   <!-- Nav tabs -->
   <ul class="nav nav-tabs auto-width" role="tablist">
	{foreach name="tabs" from=$controller->tabs item="tab"}
	 <li{if $smarty.foreach.tabs.first} class="active"{/if}><a href="#tab-{$tab.id}" role="tab" data-toggle="tab">{$tab.caption}</a></li>
	{/foreach}
	{if $comparison_properties}
	 <li{if $controller->active_tab=='comparison'} class="active"{/if}><a href="#tab-compare" role="tab" data-toggle="tab">{$words->good_tabs_compare_caption('Сравнение')}</a></li>
	{/if}
	{if !$item.disable_reviews}
	 <li{if $controller->active_tab=='reviews'} class="active"{/if}><a href="#tab-reviews" role="tab" data-toggle="tab">{$words->good_tabs_reviews_caption('Отзывы')}</a></li>
	{/if}
   </ul>
  </div>

  <div class="grey">
   <div class="container">
	<!-- Tab panes -->
	<div class="tab-content">
	 {foreach name="tabs" from=$controller->tabs item="tab"}
	  {assign var="technologies" value=$controller->tabs_model->get_tech_images($tab.id)}
	  {assign var="files" value=$controller->tabs_model->get_files($tab.id)}
	  
	  <section class="tab-pane{if $smarty.foreach.tabs.first} active{/if}" id="tab-{$tab.id}">
	   {if $technologies}
		<div class="row">
		 <div class="col-md-7 right-border">{$tab.text|put_content}</div>
		 <div class="col-md-5 left-border">
		  {if $technologies}
		   <div class="technologies">
			<div class="row">
			 {foreach from=$technologies item="technology"}{strip}
			  <div class="col-sm-6">
			   {if $technology.image}
			    <a class="item show_lg_photo" data-fancybox-group="tab-{$tab.id}" title="{$technology.caption|escape}" href="/admin/project_images/{$technology.image}">
			   {else}
				 <span class="item">
			   {/if}
				<span class="img"><img src="/admin/image.php?file=project_images/{$technology.image_small}&amp;width=150&amp;height=80" alt="{$technology.caption|escape}"></span>
				<span class="caption"><div class="row"><div class="col-lg-10 col-lg-offset-1">{$technology.caption}</div></div></span>
			   {if $technology.image}
				</a>
			   {else}
			    </span>
			   {/if}
			  </div>
			 {/strip}{/foreach}
			</div>
		   </div>
		  {/if}
		 </div>
		</div>
	   {else}
		{$tab.text|put_content}
	   {/if}
	   {if $files}
		<div class="files">
		 <div class="row">
		  <div class="col-xs-6 col-sm-6 col-md-5 col-lg-4">
		   {foreach from=$files item="file"}
			<a class="btn btn-info btn-block" href="{"/download-file/"|cat:$file.id|url}">{if $file.caption}{$file.caption}{else}{$file.file_filename}{/if}</a>
		   {/foreach}
		  </div>
		 </div>
		</div>
	   {/if}
	  </section>
	 {/foreach}
	 
	 {if $comparison_properties}
	 <section class="tab-pane{if $controller->active_tab=='comparison'} active{/if}" id="tab-compare">
	  {assign var="brands" value=$controller->comparison->get_brands()}
	  <form class="catalog-filters" action="" method="get">
	   <h4 class="arial">{$words->goods_compare_caption('Сравнительные характеристики')}</h4>
	   
	   {if $brands|@count > 1}
	   <div class="brands">
		<div class="row">
		 <div class="col-md-2">
		  <label>{$words->goods_compare_other_brands_label('Сравнить товары других брендов:')}</label>
		 </div>
		 <div class="col-md-7">
		  <div class="logos">
		   <div class="row">
			{foreach name="visible_brands" from=$brands item="brand"}
			 {if $smarty.foreach.visible_brands.index < 4}
			  <div class="col-xs-6 col-sm-3">
			  {include file="catalog-filters-brand-item.tpl" brand=$brand comparison=1}
			  </div>
			 {/if}
			{/foreach}
		   </div>
		  </div>
		 </div>
		 <div class="col-md-3">
		  <div class="links">
		   <div class="row-5">
			{if $brands|@count > 4}
			<div class="col-lg-1-5 col-xs-1-4">
			 <a class="btn btn-primary btn-block dropdown-toggle" href="#" data-toggle="dropdown">Show all</a>
			 <div class="dropdown-menu" role="menu">
			  <div class="all-brands">
			   <div class="row-5">
				{foreach name="visible_brands" from=$brands item="brand"}
				 {if $smarty.foreach.visible_brands.index >= 4}
				  <div class="col-xs-2-4 col-md-1-4 col-lg-1-5">
				   {include file="catalog-filters-brand-item.tpl" brand=$brand comparison=1}
				  </div>
				 {/if}
				{/foreach}
			   </div>
			  </div>
			 </div>
			</div>
			{/if}
			<div class="{if $brands|@count > 4}col-lg-4-5 col-xs-3-4{else}col-xs-4-4{/if}"><a class="btn btn-default btn-block" href="#" data-filter-all-brands="1">{$words->goods_compare_all_brands_href('Все производители')}</a></div>
		   </div>
		  </div>
		 </div>
		</div>
	   </div>
	   {/if}
	  </form>

	  {include file="catalog-comparison-table.tpl"}
	  
	 </section>
	 {/if}
	 
	 {if !$item.disable_reviews}
	  <section class="tab-pane{if $controller->active_tab=='reviews'} active{/if}" id="tab-reviews">
	   <div class="reviews-list">
		{if $controller->reviews or $smarty.get|@count}
		<header class="block-header">
		 <form class="form-inline" action="#">
		  <div class="form-group">
		   <label for="sort-button">{$words->good_reviews_header_date_caption('Сортировать отзывы')}</label>
		   <div class="btn-group calendar">
			<button id="sort-button" type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
			 {if $controller->filters->getFilter('dateBegin') || $controller->filters->getFilter('dateEnd')}
			  {$controller->filters->getFilter('dateBegin')|default:'...'}  -  {$controller->filters->getFilter('dateEnd')|default:'...'}
			 {else}
			  {$words->good_reviews_header_date_button('по дате')}
			 {/if}
			 <span class="caret"></span>
			</button>
			<div class="dropdown-menu">
			 <div class="row">
			  {include file="news-and-actions-items/filters-calendar.tpl"}
			 </div>
			</div>
		   </div>
		  </div>
		 </form>
		</header>
		{/if}

		{foreach from=$controller->reviews item="review"}
		 {include file="reviews-item.tpl" item=$review good="1"}
		{/foreach}

		{include file="pagination.tpl"}

		{include file="forms/review.tpl" good=$item.id}
	   </div>
	  </section>
	 {/if}
	</div>
   </div>
  </div>
 </section>
 {/if}
</div>

{include file="best-sellers-and-last-goods-list.tpl" show_analogs=$item.id}

{include file="manual-blocks-list.tpl"}