{* Smarty *}
{assign var="item" value=$controller->item}
<div class="container">
 <div class="news-item">
  <header class="block-header">
   <form class="form-inline form-large" action="#">
	<div class="form-group">
	 <a class="btn btn-primary btn-back" href="{$url_maker->d_id_url($item.external_parent)|url}">{$words->news_all_items_href('Все новости')}</a>
	</div>
   </form>
  </header>
  
  {$item.caption|print_caption}
  {$item.text|put_content}
 
  {include file="catalog-links.tpl" model=$controller->model item=$item}
 </div>
</div>

{if $controller->related_items}
<nav class="other-news">
 <div class="grey">
  <div class="container">
   <div class="page-header with-h-rows">
	<h2 class="h1">{$words->news_other_news('новости по теме')}</h2>
   </div>
   <div class="row">
	{foreach name="other_news" from=$controller->related_items item="related_item"}{strip}
	 <div class="col-sm-6 col-md-4">
	  <article class="item">
	   <div class="row">
		<div class="col-xs-4">
		 <div class="image">{if $related_item.image}<img src="/admin/image.php?file=project_images/{$related_item.image}&amp;width=110&amp;height=85" alt="{$related_item.caption|escape}">{/if}</div>
		</div>
		<div class="col-xs-8">
		 <h3 class="h1 list-item-caption">{$related_item.caption}</h3>
		 <div class="text">{$related_item.preview}</div>
		 {if $related_item.date != 0}
		  {if $related_item.date != 0}<time class="date" datetime="{$related_item.date}"><span class="date-caption">{$words->pub_date_caption('Дата публикации:')} </span>{$related_item.date|normal_date}<span class="date-caption">{$words->pub_date_year_short('г.')}</span></time>{/if}
		 {/if}
		 <footer class="block-footer">
		  <a class="btn btn-default" href="{$related_item.url|url}">{$words->more}</a>
		 </footer>
		</div>
	   </div>
	  </article>
	 </div>
	{/strip}{/foreach}
   </div>
  </div>
 </div>
</nav>
{/if}

{include file="best-sellers-and-last-goods-list.tpl"}

{include file="manual-blocks-list.tpl"}