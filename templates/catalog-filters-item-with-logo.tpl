{* Smarty *}
{assign var="active" value=$controller->filters->is_filter_item_checked($item.id)}
<input type="checkbox" name="filter[{$item.external_parent}][]" value="{$item.id}"{if $active} checked{/if}>
<a class="item" href="#" data-filter="{$item.id}">{if $item.image}<img src="/admin/image.php?file=project_images/{$item.image}&amp;width=130&amp;height=40{if !$active}&amp;grey=1{/if}" alt="{$item.caption|escape}">{/if}</a>