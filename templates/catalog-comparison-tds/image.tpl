{* Smarty *}
<div class="img">
 {foreach from=$item.images item="image"}
  {if $image.id == $item.value}
   {$image.caption}
   {if $image.image_small}
	{if $image.image}<a href="/admin/project_images/{$image.image}" data-fancybox-group="technologies" class="show_lg_photo" title="{$image.caption|escape}">{/if}
	<img src="/admin/image.php?file=project_images/{$image.image_small}&amp;width=170&amp;height=95" alt="{$image.caption|escape}">
	{if $image.image}</a>{/if}
   {/if}
  {/if}
 {/foreach}
</div>