{* Smarty *}
<div class="container">
 <div class="brands-list height-100">
  <div class="row">
   {foreach from=$controller->data item="item"}
	{assign var="links" value=$controller->model->get_links($item.id)}
	{assign var="brand_url" value=$url_maker->make_item_url($item, $controller->model)|url}
	<div class="col-sm-6 col-md-4 col-lg-3">
	 <article class="item">
	  <a href="{$brand_url}">
	   <div class="image">{if $item.image_large}<img src="/admin/image.php?file=project_images/{$item.image_large}&amp;width=240&amp;height=156" alt="{$item.caption|escape}">{/if}</div>
	   <div class="logo">{if $item.image}<img src="/admin/image.php?file=project_images/{$item.image}&amp;width=130&amp;height=40" alt="{$item.caption|escape}">{/if}</div>
	  </a>
	  {if $item.country_caption}
	   <div class="country"{if $item.country_image} style="background-image: url('/admin/project_images/{$item.country_image}');"{/if}>{$item.country_caption}</div>
	  {/if}
	  <h2 class="h6 category-caption">{if $item.good_category}{$item.good_category}{else}{$words->brands_good_category_default('Медицинская техника:')}{/if}</h2>
	  {if $links}
	   <ul>
		{foreach from=$links item="link"}
		 <li><a href="{$link.url|url}">{$link.caption}</a></li>
		{/foreach}
	   </ul>
	  {/if}
	  <footer class="item-footer">
	   <a class="btn btn-default btn-block" href="{$brand_url}">{$words->brands_more('Подробно о бренде')}</a>
	   {if $item.url}<a class="btn btn-default btn-block" href="{$item.url}" target="_blank">{$words->brands_official_site_button('Официальный сайт бренда')}</a>{/if}
	   <a class="btn btn-default btn-block" href="{$url_maker->d_module_url('brand_technologies')|cat:"/"|cat:$item.static|url}">{$words->brands_technologies_button('Технологии')}</a>
	  </footer>
	 </article>
	</div>
   {/foreach}
  </div>
  
  {include file="pagination.tpl"}
 </div>
</div>
