{* Smarty *}
<nav class="navbar navbar-default main-nav" role="navigation">
 <div class="navbar-header">
  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#main-menu-navbar-collapse">
   <span class="sr-only">{$words->header_nav_toggle('Toggle navigation')}</span>
   <span class="icon-bar"></span>
   <span class="icon-bar"></span>
   <span class="icon-bar"></span>
  </button>
 </div>
 <div class="collapse navbar-collapse" id="main-menu-navbar-collapse">
  <ul class="nav nav-pills nav-justified">
   {foreach from=$main_nav item="item"}{strip}
	{if $item.module == 'profile' and !$controller->account}
	 {assign var="caption" value=$words->main_nav_auth('Вход')}
	{else}
	 {assign var="caption" value=$item.caption}
	{/if}
	{assign var="url" value=$item.url}
	{if $item.sub}{assign var="url" value=$url|cat:"/"|cat:$item.sub.0.static}{/if}
	<li{if $item.module == 'cart'} class="cart"{/if}><a{if $app->getModesArray(0) == $item.static} class="active"{/if} href="{$url|url}">{$caption}{if $item.module == 'cart'} (<span data-cart-count="1">{$controller->cart_summary.count}</span>){/if}</a></li>
   {/strip}{/foreach}
  </ul>
 </div>
</nav>