{* Smarty *}
<section class="container">
 <div class="news-and-actions-list start">
  <div class="page-header with-h-rows">
   <h2 class="h1">{$words->start_news_and_action_caption('Новинки и акции')}</h2>
  </div>
  {include file="news-and-actions-list-data.tpl" actions=$controller->news_and_actions.actions news=$controller->news_and_actions.news}
 </div>
</section>


{include file="best-sellers-and-last-goods-list.tpl"}

{include file="manual-blocks-list.tpl"}

<div class="container">
 <section class="seo-text">
  <h1>{$controller->depart_item.caption}</h1>
  <div class="content">
   {$controller->depart_item.text}
  </div>
 </section>
</div>


