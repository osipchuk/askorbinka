{* Smarty *}
<nav class="right-nav">
 <ul class="nav nav-pills nav-justified">
  {foreach from=$appHelper->get_global('right_nav') item="nav_item"}
   {assign var="url" value=$url_maker->d_module_url($nav_item)}
   <li class="{$nav_item|replace:'_':'-'}">
	<a href="{if $url}{$url|url}{else}#{/if}">
	 <span class="image"></span>
	 <span class="caption-wrap">
	  <span class="caption">{$words->_w('right_nav_', $nav_item)}</span>
	 </span>
	</a>
   </li>
  {/foreach}
 </ul>
 <div class="social-nav visible-lg">
  {include file="social-nav.tpl"}
 </div>
</nav>