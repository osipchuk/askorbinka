{* Smarty *}
<div class="container">
 <section class="manual-blocks-list">
  <div class="row">
   {foreach name="manual_blocks" from=$controller->manual_blocks item="item"}
	<div class="{cycle values="col-sm-8 col-md-6,col-xs-6 col-sm-4 col-md-3,col-xs-6 col-sm-4 col-md-3,col-xs-6 col-sm-4 col-md-3,col-xs-6 col-sm-4 col-md-3,col-sm-8 col-md-6"}">
	 {if $item.href}<a href="{$item.href|url}">{/if}
	  <article class="item">
	   <img src="/admin/project_images/{$item.image}" alt="{$item.caption|escape}">
	   <div class="text">
		<h3 class="h4 arial">{$item.caption}</h3>
		<p>{$item.text}</p>
	   </div>
	  </article>
	  {if $item.href}</a>{/if}
	</div>
   {/foreach}
  </div>
 </section>
</div>
