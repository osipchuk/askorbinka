{* Smarty *}
<div class="container">
 

 {$controller->depart_item.caption|print_caption}
 <div class="page-services">
  <div class="row">
   <div class="col-xs-12">
	<div class="help-with-tel">
	 {$words->distribution_header('По любым вопросам сервисного и гарантийного обслуживания обращайтесь по телефону горячей линии:<br /><span>0 800 600 100</span> (бесплатно с мобильных и стационарных телефонов на территории Украины)')}
	</div>
   </div>
  </div>
  {if $words->distribution_header_note('На этой странице Вы можете найти интересующие Вас адреса наших представительств.')}
   <div class="row">
	<div class="col-xs-12">
	 <header class="block-header">{$words->distribution_header_note}</header>
	</div>
   </div>
  {/if}
  <div class="row map">
   <div class="hidden-lg hidden-md col-sm-12 col-xs-12">
	<div class="form-group">
	 <label for="city">{$words->distribution_city('Город')}</label>
	 <div class="form-control dropdown">
	  <button class="btn btn-default dropdown-toggle" type="button" id="city" data-toggle="dropdown">
	   <span class="title"></span><span class="caret"></span>
	  </button>
	  <ul class="dropdown-menu" role="menu" aria-labelledby="city"></ul>
	 </div>
	</div>
   </div>
   <div class="col-md-9 hidden-sm hidden-xs">
	<div class="map-wrapper">
	 <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="730px" height="520px" viewBox="0 0 478.022 336.25" xml:space="preserve">
	  {include file="distribution-regions.tpl"}
	  {foreach from=$controller->cities item="item"}
	   <g class="city" data-region="{$item.region}" data-city="{$item.id}">
		<circle cx="{$item.cx}" cy="{$item.cy}" r="{$item.r}" />
		<text{if $item.transform} transform="{$item.transform}"{/if} font-size="{$item.font_size}">{$item.caption}</text>
	   </g>
	  {/foreach}
	 </svg>
	</div>
   </div>
   <div class="col-md-3 col-sm-12 col-xs-12">
	<div class="description">
	 <div class="city h1"></div>
	 <div class="service service-center">
	  <div class="type"><div class="icon"></div><div class="title">{$words->distribution_type_service('сервисный центр')}<span class="nearest-city"> ({$words->distribution_other_city('в г.')} <span class="nearest-city-name">-</span>)</span></div></div>
	  <div class="contacts">
	   <span class="contact address"><div class="icon">{$words->distribution_address_title('Адрес')}</div><div class="title"></div></span>
	   <span class="contact tel"><div class="icon">{$words->distribution_tel_title('Телефон')}</div><div class="title"></div></span>
	   <a class="contact map various fancybox.iframe" href="#">
		<div class="icon"></div><div class="title">{$words->distribution_google_map_href('Посмотреть карту проезда Google')}</div>
	   </a>
	  </div>
	 </div>
	 <div class="service regional-office">
	  <div class="type"><div class="icon"></div><div class="title">{$words->distribution_type_office('региональное отделение')}<span class="nearest-city"> ({$words->distribution_other_city('в г.')} <span class="nearest-city-name">-</span>)</span></div></div>
	  <div class="contacts">
	   <span class="contact address"><div class="icon">{$words->distribution_address_title('Адрес')}</div><div class="title"></div></span>
	   <span class="contact tel"><div class="icon">{$words->distribution_tel_title('Телефон')}</div><div class="title"></div></span>
	   <a class="contact map various fancybox.iframe" href="#">
		<div class="icon"></div><div class="title">{$words->distribution_google_map_href('Посмотреть карту проезда Google')}</div>
	   </a>
	  </div>
	 </div>
	</div>
   </div>
  </div>
  <div class="row help">
   <div class="col-xs-12">
	{$words->distribution_footer_note('<span> примечание:</span> если в области нет РО, то она закреплена за ближайшим РО. Например, в Черниговской области нет РО, она закреплена за Киевским РО.')}
   </div>
  </div>
 </div>

 <script src="/js/distributions.js"></script>
 <script>
  var city = {$controller->addresses};
 </script>
</div>