{* Smarty *}
{if $item.price != 0 and $item.avail}
 {if !$btn_class}{assign var="btn_class" value="btn-primary"}{/if}

 {if $controller->cart->is_item_in_cart($item.id)}
  <a class="btn {$btn_class} btn-block active" href="#" data-add-to-cart="{$item.id}">{$words->goods_item_is_in_cart('В корзине')}</a>
 {else}
  <a class="btn {$btn_class} btn-block" href="#" data-add-to-cart="{$item.id}">{$words->goods_add_to_cart('В корзину')}</a>
 {/if}
{/if}
