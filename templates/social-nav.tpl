{* Smarty *}
<ul class="list-unstyled">
 {foreach from=$appHelper->get_global('social_links') key="social" item="link"}
  {if $link}
   <li class="{$social}"><a href="{$link}" target="_blank">{$social}</a></li>
  {/if}
 {/foreach}
</ul>