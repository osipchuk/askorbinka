{* Smarty *}
<section class="good-discount-card{if $used} used{/if}" id="discount-card">
 <div class="good-discount-card-inner">
  <header class="block-header">
   <h2 class="h1">{$words->discount_card_caption('НАЗВАНИЕ ИНТЕРНЕТ-МАГАЗИНА')}</h2>
   <p class="h4 arial text-center">{$words->discount_card_sub_caption('Интернет-магазин медицинской техники и товаров для здоровья')}</p>
  </header>
  <div class="discount-info">
   <div class="row">
	<div class="col-md-6">
	 <dl class="dl-horizontal">
	  <dt>{$words->discount_card_code('Код скидки:')}</dt><dd data-discount-code="1">{$code}</dd>
	  <dt>{$words->discount_card_good('Рекомендованный товар:')}</dt><dd>{$good.category} {$good.caption}</dd>
	  <dt>{$words->discount_card_price('Цена:')}</dt><dd>{$price_without_discount|price_format} {$words->currency}</dd>
	  <dt>{$words->discount_card_discount('Ваша скидка:')}</dt><dd>{$good.code_discount}%</dd>
	  <dt>{$words->discount_card_price_with_discount('Цена для Вас (с учетом скидки):')}</dt><dd>{$price|price_format} {$words->currency}</dd>
	 </dl>
	</div>
	<div class="col-md-6">
	 <dl class="dl-horizontal second-column">
	  <dt>{$words->discount_card_shop('Интернет-магазин:')}</dt><dd><a href="{''|url:'':1}">{$appHelper->get_global('base')}</a></dd>
	  <dt>{$words->discount_card_tel_caption('Информационная линия:')}</dt><dd>{$words->discount_card_tel('0 800 600 100 (бесплатные звонки по Украине со стационарных и мобильных телефонов с 9:00 до 17:00 по будням)')}</dd>
	 </dl>
	</div>
   </div>
  </div>
  
  <div class="catalog-item">
   <div class="row">
	<div class="col-md-5">
	 <div class="left-column">
	  <span class="label label-danger label-action">{$words->discount_card_discount_label('Скидка')} <span class="big">{$good.code_discount}</span><span class="small">%</span><span class="label-image"><img src="/img/action-image.png" width="87" height="78" alt=""></span></span>
      {if $good.image}
       <div class="photos">
        <div class="main-photo"><img src="/admin/image.php?file=project_images/{$good.image}&amp;width=415&amp;height=400" alt="{$good.caption|escape}"></div>
       </div>
      {/if}
	  {if $show_describe}
	   <div class="technologies" data-code-discount-form-visibility="describe">
		<div class="row">
		 {foreach from=$technologies item="technology"}{strip}
		  <div class="col-xs-6 col-sm-4 col-md-6">
           <div class="item">
		   {*<a class="item show_lg_photo" data-fancybox-group="technologies" title="{$technology.caption|escape}" href="/admin/project_images/{$technology.image}">*}
			<span class="img"><img src="/admin/image.php?file=project_images/{$technology.image_small}&amp;width=190&amp;height=100" alt="{$technology.caption|escape}"></span>
			<span class="caption"><span class="row"><span class="col-lg-10 col-lg-offset-1">{$technology.caption}</span></span></span>
		   {*</a>*}
           </div>
		  </div>
         {/strip}{/foreach}
		</div>
	   </div>
	  {/if}
	 </div>
	</div>
	<div class="col-md-7">
	 <div class="right-column">
	  <div class="good-info">
	   <h1>{$good.category} {$good.caption}</h1>
	   <div class="discount-prices">
		<div class="row">
		 <div class="col-sm-6"><div class="price">{$words->discount_card_old_price('Старая цена:')} <strong>{$price_without_discount|price_format}</strong> {$words->currency}</div></div>
		 <div class="col-sm-6"><div class="price new-price">{$words->discount_card_new_price('Новая цена:')} <strong>{$price|price_format}</strong> {$words->currency}</div></div>
		</div>
	   </div>
	   {if $show_describe}
	    <div data-code-discount-form-visibility="describe">
		 {$describe|put_content}
	    </div>
	   {/if}
	  </div>
	 </div>
	</div>
   </div>
  </div>

  {if $show_alts and $analogs}
  <section class="goods-list" data-code-discount-form-visibility="alt">
   <h2 class="h1">{$words->discount_card_alts('Альтернативы')}</h2>
	 <ul class="catalog-list row-5">
	  {foreach from=$analogs item="item"}
	   <li class="col-xs-4-4 col-sm-2-4 col-md-1-4">
		{include file="catalog-list-item.tpl" item=$item with_full_prices="1"}
	   </li>
	  {/foreach}
	 </ul>
  </section>
  {/if}
  
 </div>
</section>