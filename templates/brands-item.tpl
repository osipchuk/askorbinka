{* Smarty *}
{assign var="item" value=$controller->item}
<div class="container">
 <div class="brands-item">
  <header class="block-header">
   <a class="btn btn-primary btn-back" href="{$url_maker->d_id_url($item.external_parent)|url}">{$words->brands_all_button('Все бренды')}</a>
  </header>
  <div class="row">
   <div class="col-sm-3">
	<div class="images">
	 <div class="logo">{if $item.image}<img src="/admin/image.php?file=project_images/{$item.image}&amp;width=130&amp;height=40" alt="{$item.caption|escape}">{/if}</div>
	 <div class="image">{if $item.image_large}<img src="/admin/image.php?file=project_images/{$item.image_large}&amp;width=240&amp;height=156" alt="{$item.caption|escape}">{/if}</div>
	</div>
   </div>
   <div class="col-sm-9">
	<div class="row">
	 <div class="col-lg-11">
	  <div class="text">
	   {if $item.country_caption}
		<div class="country"{if $item.country_image} style="background-image: url('/admin/project_images/{$item.country_image}');"{/if}>{$item.country_caption}</div>
	   {/if}
	   {$item.text|put_content}
	   <footer class="block-footer">
		<div class="row">
		 {if $item.url}
		  <div class="col-xs-6 col-md-4"><a class="btn btn-default btn-block" href="{$item.url|url}" target="_blank">{$words->brands_official_site_button('Официальный сайт бренда')}</a></div>
		 {/if}
		 <div class="col-xs-6 col-md-4"><a class="btn btn-default btn-block" href="{$url_maker->d_module_url('brand_technologies')|cat:"/"|cat:$item.static|url}">{$words->brands_technologies_button('Технологии')}</a></div>
		</div>
	   </footer>
	  </div>
	 </div>
	</div>
   </div>
  </div>

  {include file="catalog-links.tpl" model=$controller->model item=$item}
  
 </div>
</div>

{include file="best-sellers-and-last-goods-list.tpl"}

{include file="manual-blocks-list.tpl"}