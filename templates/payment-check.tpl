{* Smarty *}
{assign var="summary" value=$controller->order_processor->get_cart_summary()}
<!DOCTYPE HTML>
<html>
<head>
 <meta charset="utf-8">
 <title>Рахунок-фактура на оплату товарів</title>
 <style>
  {literal}
  body, html {padding:5px; margin:0;}
  body {font-family:Arial, Tahoma, sans-serif; font-size:12px; background-color:#fff; line-height:15px; color:#000;}
  table {border-collapse:collapse;}
  td {vertical-align:top; padding:0px;}
  a {text-decoration:none; color:#C57001;}
  a:hover {text-decoration:underline;}
  img {border:none;}

  .main_table {width:650px;}
  .main_table table {margin-top:20px; border:solid #000000 1px; width:100%;}
  .main_table table td, .main_table table th {border:solid #000000 1px; padding:1px; text-align:center;}
  .main_table table th {background-color:#E0E0E0; font-weight:bold; padding: 2px 1px 2px 1px;}
  {/literal}
 </style>
</head>
<body>
<table class="main_table">
 <tr>
  <td style="font-weight:bold; text-decotation:underline; width:100px;">Постачальник</td>
  <td>{$controller->seller}</td>
 </tr>
 <tr>
  <td style="font-weight:bold; text-decotation:underline; padding-top:10px;">Одержувач</td>
  <td style="padding-top:10px;">Той самий</td>
 </tr>
 <tr>
  <td style="font-weight:bold; text-decotation:underline; padding-top:30px;">Платник</td>
  <td style="padding-top:30px;">{$controller->order_processor->get_field('surnname')} {$controller->order_processor->get_field('name')}</td>
 </tr>
 <tr>
  <td colspan="2" style="padding-top:20px; font-weight:bold; text-align:center;">
   Рахунок-фактура №{$controller->order_id}<br />
   від {''|ukr_date}
  </td>
 </tr>
 <tr>
  <td colspan="2">
   <table>
    <tr>
     <th style="width:6%">№</th>
     <th>Назва</th>
     <th style="width:17%">Кількість</th>
     <th style="width:17%">Ціна</th>
     <th style="width:17%">Сума</th>
    </tr>
    <tr>
	{foreach name="items" from=$controller->order_processor->get_goods() item="item"}
	 <tr>
	 <td>{$smarty.foreach.items.iteration}</td>
	 <td style="text-align: left;">{$item.good.caption}</td>
	 <td>{$item.count}</td>
	 <td>{$controller->goods_model->price($item.good)|price_format}</td>
	 <td>{$item.count*$controller->goods_model->price($item.good)}</td>
	 </tr>
	{/foreach}
    </tr>
    <tr>
     <td colspan="4" style="text-align:right;">Сума:</td>
     <td>{$summary.price} грн</td>
    </tr>
   </table>
  </td>
 </tr>
 <tr>
  <td colspan="2" style="padding-top:20px;">Всього на суму:<br />
   <div style="font-weight:bold; padding:3px 0px 3px 0px;">{$summary.price|num2str}</div>
  </td>
 </tr>
 <tr>
  <td colspan="2" style="text-align:right; padding-top:20px;">Виписав (ла):&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; _______________</td>
 </tr>
 <tr>
  <td colspan="2" style="text-align:right; padding-top:20px;">Рахунок дійсний до сплати до {"+5 days"|date_format:"%d.%m.%Y"}</td>
 </tr>
</table>
</body>
</html>