{* Smarty *}
{if $model and $item}
 {assign var="catalog_links" value=$controller->catalog_links_model->get_structured_links_of_item($model, $item.id)}
 {if $catalog_links}
  <nav class="catalog-links">
   {foreach name="l_sections" from=$catalog_links key="section" item="links"}
	{if $links|@count > 3}{assign var="col" value="3"}{else}{assign var="col" value="4"}{/if}
	<section class="section">
	 <h2 class="h1">{$section}</h2>
	 <div class="items">
	  <div class="row">
	   {foreach name="links" from=$links item="link"}
		<article class="item col-sm-6 col-lg-{$col}">
		 <div class="row">
		  <div class="col-xs-2 col-sm-4">
		   <div class="image">{if $link.image}<img src="/admin/project_images/{$link.image}" alt="{$link.caption|escape}">{/if}</div>
		  </div>
		  <div class="col-xs-10 col-sm-8">
		   <h3 class="h6">{$link.caption}</h3>
		   <a class="btn btn-default" href="{$link.href|url}">{$words->catalog_links_more('Посмотреть')}</a>
		  </div>
		 </div>
		</article>
	   {/foreach}
	  </div>
	 </div>
	</section>
   {/foreach}
  </nav>
 {/if}
{/if}