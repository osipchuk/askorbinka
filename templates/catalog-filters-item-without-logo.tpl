{* Smarty *}
{assign var="active" value=$controller->filters->is_filter_item_checked($item.id)}
<li{if $active} class="active"{/if}>
 <input type="checkbox" name="filter[{$item.external_parent}][]" value="{$item.id}"{if $active} checked{/if}>
 <a href="#" data-filter="{$item.id}">{$item.caption}</a>
</li>