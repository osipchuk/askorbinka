{* Smarty *}
{assign var="video_template" value="videos/"|cat:$controller->video_template}

<div class="videos-page">
 <div class="container">
  <!-- Nav tabs -->
  <ul class="nav nav-tabs">
   {foreach from=$controller->video_depart.sub item="item"}
	<li{if $item.id == $controller->depart_item.id} class="active"{/if}><a href="{$item.url|url}">{$item.caption}</a></li>
   {/foreach}
  </ul>
 </div>

 <div class="grey">
  <div class="container">
   {include file=$video_template}
   {include file="pagination.tpl"}
  </div>
 </div>
</div>

{include file="best-sellers-and-last-goods-list.tpl"}

{include file="manual-blocks-list.tpl"}
