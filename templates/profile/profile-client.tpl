{* Smarty *}
<div class="container">
 <div class="row">
  <div class="col-lg-6">
   <div class="client-info">
	<table class="table table-bordered">
	 <tr>
	  <th>{$words->client_info_fio('Ф.И.О.')}</th>
	  <td>{$controller->clients_model->account_fio($account)}</td>
	 </tr>
	 <tr>
	  <th>{$words->client_info_city('Город')}</th>
	  <td>{$account.city}</td>
	 </tr>
	 <tr>
	  <th>{$words->client_info_tel('Телефон')}</th>
	  <td>{$account.tel}</td>
	 </tr>
	 <tr>
	  <th>{$words->client_info_email('Email')}</th>
	  <td>{$account.email}</td>
	 </tr>
	</table>
   </div>
  </div>
 </div>
</div>