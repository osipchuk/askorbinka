{* Smarty *}
<section class="sold-goods">
 <div class="grey">
  <div class="container">
   <h2 class="h1">{$words->profile_pro_statistic_caption('статистика')}</h2>
   <div class="table-responsive">
	<table class="table">
	 <thead>
	 <tr>
	  <th class="caption">{$words->profile_pro_statistic_th_fio('ФИО покупателя')}</th>
	  <th>{$words->profile_pro_statistic_th_code_date('Дата выдачи кода')}</th>
	  <th>{$words->profile_pro_statistic_th_code('Код')}</th>
	  <th>{$words->profile_pro_statistic_th_good('Товар')}</th>
	  <th>{$words->profile_pro_statistic_th_price('Стоимость')}</th>
	  <th>{$words->profile_pro_statistic_th_order_date('Дата покупки')}</th>
	  <th>{$words->profile_pro_statistic_th_bonuses('Вам начислено бонусов')}</th>
	 </tr>
	 </thead>
	 <tbody>
	 {foreach name="statistic" from=$controller->statistic item="item"}
	  <tr{if $smarty.foreach.statistic.iteration > 3} class="hidden"{/if}>
	   <td class="caption">{$item.fio}</td>
	   <td>{$item.add_time|normal_date}</td>
	   <td>{$item.code}</td>
	   <td>{foreach from=$item.goods item="good"}
		 <p>{$good}</p>
	   {/foreach}</td>
	   <td>{$item.order_price|price_format} {$words->currency}</td>
	   <td>{$item.order_date|normal_date}</td>
	   <td>{$item.bonuses}</td>
	  </tr>
	 {/foreach}
	 </tbody>
	</table>
   </div>
   {if $controller->statistic|@count > 3}
	<footer class="block-footer">
	 <a class="btn btn-default btn-arrow-double-bottom" data-show-all-statistic="1" href="#">{$words->profile_pro_statistic_show_all_button('Посмотреть всю статистику')}</a>
	</footer>
   {/if}
  </div>
 </div>
</section>

<section class="format-code" id="format-code">
 <div class="container">
  <h2 class="h1">{$words->profile_pro_new_code_caption('Отправить новый код')}</h2>
  <div class="row main-row">
   <div class="col-sm-7 col-md-5">
	<div class="column column-left">
	 <div class="label label-warning label-lg">1</div>
	 <div class="categories">
	  <h3 class="h2">{$words->profile_pro_categories_caption('Категория')}</h3>
	  <table class="filters without-images">
	   <tr>
		{foreach name="categories" from=$controller->filter_categories item="item"}
		 <td{if $item.id == $controller->category_id} class="active"{/if}>
		  <a href="{$controller->base_href|url}?category={$item.id}#format-code">
		   {$item.caption}
		  </a>
		 </td>
		 {cycle name="categories" values='<td class="separator"></td>,</tr><tr>'}
		{/foreach}
	   </tr>
	  </table>

	  {if $controller->filter_subcategories}
	   <h3 class="h2">{$words->profile_pro_subcategories_caption('Подкатегория')}</h3>
	   <table class="filters">
		<tr>
		 {foreach name="subcategories" from=$controller->filter_subcategories item="item"}
		  <td{if $item.id == $controller->subcategory_id} class="active"{/if}>
		   <a href="{$controller->base_href|url}?category={$controller->category_id}&amp;subcategory={$item.id}#format-code">
			<span class="img"{if $item.image_filter} style="background-image: url('/admin/project_images/{$item.image_filter}');"{/if}></span>
			<span class="img-active"{if $item.image_filter_active} style="background-image: url('/admin/project_images/{$item.image_filter_active}');"{/if}></span>
			{$item.caption}
		   </a>
		  </td>
		  {cycle name="subcategories" values='<td class="separator"></td>,</tr><tr>'}
		 {/foreach}
		</tr>
	   </table>
	  {/if}

	  {if $controller->filter_brands}
	   <h3 class="h2">{$words->profile_pro_brand_caption('производитель')}</h3>
	   <table class="filters brands">
		<tr>
		 {foreach name="brands" from=$controller->filter_brands item="item"}
		  <td{if $item.id == $controller->brand_id} class="active"{/if}>
		   <a href="{$controller->base_href|url}?category={$controller->category_id}{if $controller->subcategory_id}&amp;subcategory={$controller->subcategory_id}{/if}&amp;brand={$item.id}#format-code" title="{$item.caption|escape}">
			{if $item.image}<img src="/admin/image.php?file=project_images/{$item.image}&amp;width=130&amp;height=40" alt="{$item.caption|escape}">{/if}
		   </a>
		  </td>
		  {cycle name="brands" values='<td class="separator"></td>,</tr><tr>'}
		 {/foreach}
		</tr>
	   </table>
	  {/if}
	 </div>
	</div>
   </div>

   <div class="col-sm-5 col-md-4">
	<div class="column{if !$controller->good_id} disabled{/if}" id="generate-code">
	 <div class="label label-warning label-lg">2</div>
	 {include file="forms/discount-code.tpl"}
	</div>
   </div>
   <div class="col-sm-7 col-md-3">
	<div class="column column-right{if !$controller->code_id} disabled{/if}">
	 <div class="label label-warning label-lg">3</div>
	 <a class="btn btn-primary btn-block btn-email" data-send-controller="email" href="#">{$words->profile_pro_send_code_by_email('Отправить на Email')}</a>
	 <a class="btn btn-primary btn-block btn-sms" data-send-controller="sms" href="#">{$words->profile_pro_send_code_by_sns('Отправить на SMS')}</a>
	 <a class="btn btn-primary btn-block btn-print" data-send-controller="print" href="#">{$words->profile_pro_print_code('Распечатать')}</a>
	</div>
   </div>
  </div>
 </div>
</section>

<section class="goods-list">
 <div class="grey">
  <div class="container">
   <ul class="catalog-list row-5">
	{foreach from=$controller->items item="item"}
	 <li class="col-xs-4-4 col-sm-2-4 col-md-1-4 col-lg-1-5">
	  {include file="catalog-list-item.tpl" with_full_prices="1" add_discount_make_href="1"}
	 </li>
	{/foreach}
   </ul>
   
   {include file="pagination.tpl"}
   
  </div>
 </div>
</section>

{if $controller->discountCard}
 <div class="container">
  {$controller->discountCard}
 </div>
{/if}
