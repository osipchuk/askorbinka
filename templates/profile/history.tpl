{* Smarty *}
<div class="sold-goods">
 <div class="grey">
  <div class="container">
   <h1>{$controller->caption}</h1>
   {if $controller->data}
	<div class="table-responsive">
	 <table class="table">
	  <thead>
	  <tr>
	   <th>{$words->profile_history_id('ID транзакции')}</th>
	   <th>{$words->profile_history_order_id('ID заказа')}</th>
	   <th>{$words->profile_history_date('Дата операции')}</th>
	   <th>{$words->profile_history_sum('Сумма')}</th>
	   <th>{$words->profile_history_amount('Бонусный счет после применения операции')}</th>
	   <th>{$words->profile_history_comment('Комментарий')}</th>
	  </tr>
	  </thead>
	  <tbody>
	  {foreach from=$controller->data item="item"}
	   <tr class="{if $item.value > 0}positive{else}negative{/if}">
		<td>{$item.id}</td>
		<td>{$item.order_id|default:"-"}</td>
		<td>{$item.add_time|normal_date:true}</td>
		<td>{$item.value}</td>
		<td>{$item.ballance_after}</td>
		<td>{$item.comment|default:"-"}</td>
	   </tr>
	  {/foreach}
	  </tbody>
	 </table>
	</div>
	{include file="pagination.tpl"}
   {else}
	<p>{$words->profile_pro_statistic_empty('У Вас нет истории бонусного счета')}</p>
   {/if}
  </div>
 </div>
</div>

