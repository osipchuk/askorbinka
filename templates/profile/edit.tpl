{* Smarty *}
<div class="container" data-scroll-content="without-animation">
 <div class="row">
  <div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-2">
   <form class="form-horizontal register" action="{'ajax/edit_profile'|url}" data-ajax-form="" data-ajax-form-response="edit_profile_response">
	{foreach from=$controller->fields key="field" item="item"}
	 <div class="form-group">
	  <label for="{$field}" class="col-sm-4 control-label">{$words->_w('edit_profile_fields_', $field)}{if $item.required} <sup>*</sup>{/if}</label>
	  <div class="col-sm-8">
	   {if $item.type == 'avatar'}
		<div class="avatar-block">
		 <img src="{$controller->clients_model->account_avatar($account)}" alt="" id="avatar-image">
		 <input type="hidden" name="image" id="avatar-filename" value="">
		 <div class="uploader"><div id="avatar-file-uploader"><noscript><p>Please enable JavaScript to use file uploader.</p></noscript></div></div>
		 {if $account[$field]}
		  <input type="checkbox" name="delete_image" id="delete-image" data-delete-avatar>
		  <label for="delete-image">{$words->edit_profile_fields_image_delete('Удалить')}</label>
		 {/if}
		</div>
	   {else}
		<input type="{if $item.type}{$item.type}{else}text{/if}" class="form-control input-lg" name="{$field}" id="{$field}"
			   value="{$account[$field]|escape}"{if $item.required} required{/if}{if in_array($field, $controller->readOnlyFields)} readonly{/if}
                {if $item.type == 'tel'} data-phone-number pattern="\+380 [0-9]{ldelim}2{rdelim} [0-9]{ldelim}7{rdelim}"{/if}>
	   {/if}
	  </div>
	 </div>
	{/foreach}
	<div class="form-group">
	 <div class="col-sm-8 col-sm-offset-4">
	  <footer class="block-footer">
	   <div class="note">{$words->edit_form_note('<sup>*</sup> Поля, обязательные для заполнения')}</div>
	   <div class="buttons">
		<a href="#" class="btn btn-primary btn-block btn-lg btn-ok" data-submit-buttton="">{$words->edit_profile_form_submit('Сохранить изменения')}</a>
		<input type="submit">
	   </div>
	  </footer>
	 </div>
	</div>
   </form>
  </div>
 </div>
</div>