{* Smarty *}
<nav class="profile-simple-nav">
 <ul class="nav nav-tabs">
  {foreach from=$controller->profile_nav key="static" item="caption"}
   {if $static == 'default'}{assign var="static" value=""}{/if}
   <li{if $static == $controller->get_static()} class="active"{/if}><a href="{$controller->profile_nav_base_url}{if $static}/{$static}{/if}">{$caption}</a></li>
  {/foreach}
  <li><a href="#" data-logout="">{$words->logout_href('Выйти')}</a></li>
 </ul>
</nav>