{* Smarty *}
<div class="grey" data-scroll-content>
 <div class="container">
  <div class="text-page">
   <h1>{$controller->caption}</h1>
   {$controller->text|put_content}
   {if $controller->show_form}
	<aside class="transaction-form">
	 <div class="row">
	  <div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-2">
	   {include file="forms/bonuses-transaction.tpl"}
	  </div>
	 </div>
	</aside>
   {/if}
  </div>
 </div>
</div>