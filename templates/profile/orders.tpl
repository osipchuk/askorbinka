{* Smarty *}
<div class="orders">
 <div class="grey">
  <div class="container">
   <h1>{$controller->caption}</h1>
   {if $controller->data}
	<div class="table-responsive">
	 <table class="table">
	  <thead>
	  <tr>
	   <th>{$words->profile_orders_id('ID заказа')}</th>
	   <th>{$words->profile_orders_date('Дата заказа')}</th>
	   <th>{$words->profile_orders_sum('Стоимость')}</th>
	   <th>{$words->profile_orders_status('Статус')}</th>
	   <th>{$words->profile_orders_goods('Товары')}</th>
	  </tr>
	  </thead>
	  <tbody>
	  {foreach from=$controller->data item="item"}
	   {assign var="t" value=$controller->cart->load_from_db($item.id)}
	   {assign var="summary" value=$controller->cart->get_summary()}
	   
	   <tr>
		<td>{$item.id}</td>
		<td>{$item.add_time|normal_date:true}</td>
		<td>{$summary.price} {$words->currency}</td>
		<td>{$controller->statuses[$item.status]}</td>
		<td class="text-left">
		 {foreach from=$controller->cart->get_goods() item="good"}
		  <div><a href="{$good.good.url|url}" target="_blank">{$good.good.caption}</a></div>
		 {/foreach}
		</td>
	   </tr>
	  {/foreach}
	  </tbody>
	 </table>
	</div>
	{include file="pagination.tpl"}
   {else}
	<p>{$words->profile_orders_empty('К сожалению, Вы не совершали заказов')}</p>
   {/if}
  </div>
 </div>
</div>

