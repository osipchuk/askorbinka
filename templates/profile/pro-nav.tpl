{* Smarty *}
<nav class="profile-nav">
 <ul class="nav nav-pills nav-justified">
  {foreach from=$nav key="type" item="item"}
   {if $type != 'video'}
	<li class="{$type}">
	 <a href="{$controller->profile_nav_base_url}/{$item}">
	  <span class="img"></span>
	  <span class="caption">{$words->_w('profile_pro_nav_', $type)}</span>
	 </a>
	</li>
   {else}
	<li class="{$type}">
	 <a class="various fancybox.iframe" href="http://www.youtube.com/embed/{$item}?autoplay=1">
	  <span class="caption">{$words->_w('profile_pro_nav_', $type)}</span>
	  <img src="/img/good-review.jpg" width="208" height="127" alt="">
	 </a>
	</li>
   {/if}
  {/foreach}
 </ul>
</nav>