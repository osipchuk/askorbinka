{* Smarty *}
<div class="container">
 {$controller->depart_item.caption|print_caption}
 <div class="order">
  {if !$controller->account}
   <div class="auth-href-block text-center">
	{$words->do_you_have_account('Уже есть учетная запись?')|put_content}
   </div>
  {/if}
  <div class="row">
   <div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-2">
	<form class="form-horizontal" role="form" action="{'ajax/cart_ajax/order'|url}" method="post" data-cart-form="">
	 {if !$controller->account}
	  <header class="block-header">
	   <div class="form-group">
		<label class="col-sm-4 control-label">{$words->register_social_caption('Зарегистрироваться через')}</label>
		<div class="col-sm-8">
		 {include file="auth-social.tpl"}
		</div>
	   </div>
	  </header>
	 {/if}

	 {include file="forms/profile-form-part-main.tpl"}
	 
	 <div class="form-group">
	  <label for="address" class="col-sm-4 control-label">{$words->order_form_ship_address('Адрес доставки')}<sup>*</sup></label>
	  <div class="col-sm-8"><input type="text" class="form-control input-lg" id="address" name="address" required></div>
	 </div>
	 <div class="form-group">
	  <label for="ship" class="col-sm-4 control-label">{$words->order_form_ship('Способ доставки')}</label>
	  <div class="col-sm-8">
	   <select class="form-control input-lg" id="ship" name="ship" data-ship="1">
		{foreach from=$controller->ships item="ship"}
		 <option value="{$ship.id}">{$ship.caption}</option>
		{/foreach}
	   </select>
	   <div data-ship-describe="1" class="data-ship-describe"></div>
	  </div>
	 </div>
	 {*
	 <div class="form-group">
	  <label for="ship-coast" class="col-sm-4 control-label">{$words->order_form_ship_coast('Стоимость доставки')}</label>
	  <div class="col-sm-8"><input type="text" class="form-control input-lg" id="ship-coast" name="ship-coast" placeholder="________________ {$words->currency|escape}"></div>
	 </div>
     *}
	 <div class="form-group">
	  <label for="pay" class="col-sm-4 control-label">{$words->order_form_pay('Способ оплаты')}</label>
	  <div class="col-sm-8">
	   <select class="form-control input-lg" id="pay" name="pay">
		{foreach from=$controller->pay_systems item="system"}
         {assign var="pay_system_caption" value=$words->_w('pay_system_', $system)}
         {if $pay_system_caption}
		  <option value="{$system}">{$pay_system_caption}</option>
         {/if}
		{/foreach}
	   </select>
	  </div>
	 </div>
	 <div class="form-group">
	  <label for="discount-code" class="col-sm-4 control-label">{$words->order_form_discount_code('Скидочный код')}</label>
	  <div class="col-sm-8">
	   <div class="input-group input-group-lg">
		<input type="hidden" id="discount-code-id" name="discount_code_id">
		<input type="text" class="form-control input-lg discount-code-input" id="discount-code" value="">
         <span class="input-group-btn">
          <button class="btn btn-default" type="button" data-apply-discount-code="">{$words->order_form_discount_apply('Применить')}</button>
         </span>
	    </div>
	  </div>
	 </div>
	 <div class="form-group" data-discount-info="1">
	  <label for="discount-info" class="col-sm-4 control-label">{$words->order_form_discount_info('Ваша скидка по коду')}</label>
	  <div class="col-sm-8"><span class="form-control input-lg"><span id="discount-info">0</span> {$words->currency}</span></div>
	 </div>
	 <div class="form-group">
	  <label for="bonuses-count" class="col-sm-4 control-label">{$words->order_form_bonuses_count('У Вас на счету')}</label>
	  <div class="col-sm-8"><input type="text" class="form-control input-lg" id="bonuses-count" name="bonuses-count" value="{$controller->account.bonuses|intval} {$words->order_form_bonuses_count_after('бонусов')}" readonly></div>
	 </div>
	 <div class="form-group">
	  <label for="pay-with-bonuses" class="col-sm-4 control-label">{$words->order_form_pay_with_bonuses('Оплата бонусами')}</label>
	  <div class="col-sm-8">
	   <select class="form-control input-lg" id="pay-with-bonuses" name="pay_with_bonuses"{if $controller->account.bonuses == 0 or !$controller->possible_to_pay_with_bonuses} disabled{/if}>
		<option value="0">{$words->bool_caption_0('Нет')}</option>
		<option value="1">{$words->bool_caption_1('Да')}</option>
	   </select>
	  </div>
	 </div>
	 {*
	 <div class="form-group" data-new-post-only="1">
	  <label for="ship-date" class="col-sm-4 control-label">{$words->order_form_ship_date('Дата доставки')}</label>
	  <div class="col-sm-8"><input type="text" class="form-control input-lg" id="ship-date" name="ship-date" value="20 / 03 / 14"><span class="calendar-show"></span></div>
	 </div>
     *}
	 
	 <div class="form-group">
	  <label for="" class="col-sm-4 control-label">{$words->order_form_reviever('Получатель')}</label>
	  <div class="col-sm-8">
	   <div class="select-receiver">
		<div class="row">
		 <div class="col-xs-3"><div class="first-btn"><a class="btn btn-default btn-block active" href="#" data-other-receiver="0">{$words->order_form_reciever_you('Вы')}</a></div></div>
		 <div class="col-xs-9"><a class="btn btn-default btn-block" href="#" data-other-receiver="1">{$words->order_form_not_you('Третье лицо')}</a></div>
		</div>
	   </div>
	   <input type="hidden" name="other-receiver" id="other-receiver" value="0">
	  </div>
	 </div>
	 
	 <div class="other-receiver">
	  <div class="form-group">
	   <label for="receiver-name" class="col-sm-4 control-label">{$words->order_form_reciever_name('Имя получателя')}</label>
	   <div class="col-sm-8"><input type="text" class="form-control input-lg" id="receiver-name" name="receiver-name"></div>
	  </div>
	  <div class="form-group">
	   <label for="receiver-surname" class="col-sm-4 control-label">{$words->order_form_reciever_surname('Фамилия получателя')}</label>
	   <div class="col-sm-8"><input type="text" class="form-control input-lg" id="receiver-surname" name="receiver-surname"></div>
	  </div>
	  <div class="form-group">
	   <label for="receiver-tel" class="col-sm-4 control-label">{$words->order_form_reciever_tel('Телефон получателя')}</label>
	   <div class="col-sm-8"><input type="tel" class="form-control input-lg" id="receiver-tel" name="receiver-tel"></div>
	  </div>
	  <div class="form-group">
	   <label for="receiver-email" class="col-sm-4 control-label">{$words->order_form_reciever_email('Email получателя')}</label>
	   <div class="col-sm-8"><input type="email" class="form-control input-lg" id="receiver-email" name="receiver-email"></div>
	  </div>
	 </div>
	 

	 <div class="form-group">
	  <label for="note" class="col-sm-4 control-label">{$words->order_form_note('Добавить сообщение к заказу')}</label>
	  <div class="col-sm-8"><textarea class="form-control input-lg" id="note" name="note"></textarea></div>
	 </div>
	 {if !$controller->account}
	  {php}
	   $this->assign('password_fields', array('password', 'password_repeat'))
	  {/php}
	  {foreach from=$password_fields item="field"}
	   <div class="form-group">
		<label for="{$field}" class="col-sm-4 control-label">{$words->_w('register_fields_', $field)}<sup>*</sup></label>
		<div class="col-sm-8"><input type="password" class="form-control input-lg" name="{$field}" id="{$field}" required></div>
	   </div>
	  {/foreach}
	 {/if}
	 
	 <footer class="form-footer">
	  <div class="form-group">
	   <div class="col-sm-8 col-lg-offset-4"><input class="btn btn-primary btn-block btn-lg" type="submit" value="{$words->order_form_submit('Оформить заказ')}"></div>
	  </div>
	 </footer>
	 
	</form>
   </div>
  </div>
 </div>
</div>
<script>
 var ships = {$controller->ships|@json};
</script>
