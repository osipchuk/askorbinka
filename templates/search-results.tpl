{* Smarty *}
{if $controller->items}
 <div class="container">
  {$controller->depart_item.caption|print_caption}
 </div>
 {include file="catalog-list.tpl"}
{else}
 <div class="container">
  {$controller->depart_item.caption|print_caption}
  {$words->search_no_results('К сожалению, по Вашемо запросу ничего не найдено')}
 </div>
{/if}
  