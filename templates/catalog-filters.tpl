{* Smarty *}
{assign var="filters" value=$controller->filters}
<div class="container">
 <form class="catalog-filters" action="" method="get">
  <h2 class="h1">{$words->catalog_filters_caption('Выберите один или несколько критериев, удовлетворяющие Вашему запросу')}</h2>
  {if $controller->filter_brands}
   <div class="row-5">
	<div class="filters-with-logo clearfix">
	 <label class="col-lg-1-5">{$words->catalog_filters_brands_label('Выбрать по производителю:')}</label>
	 <div class="col-lg-4-5">
	  <div class="row">
	   <div class="col-xs-10 col-sm-11">
		<div class="row">
		 {foreach name="visible_brands" from=$controller->filter_brands item="brand"}
		  {if $smarty.foreach.visible_brands.index < 4}
		   <div class="col-xs-6 col-sm-3">
			{include file="catalog-filters-brand-item.tpl" brand=$brand}
		   </div>
		  {/if}
		 {/foreach}
		</div>
	   </div>
	   {if $controller->filter_brands|@count > 4}
		<div class="col-xs-2 col-sm-1 text-right">
		 <a class="btn btn-primary dropdown-toggle" href="#" data-toggle="dropdown">Show all</a>
		 <div class="dropdown-menu" role="menu">
		  <div class="all-filters-with-logo">
		   <div class="row-5">
			{foreach name="visible_brands" from=$controller->filter_brands item="brand"}
			 {if $smarty.foreach.visible_brands.index >= 4}
			  <div class="col-xs-2-4 col-md-1-4 col-lg-1-5">
			   {include file="catalog-filters-brand-item.tpl" brand=$brand}
			  </div>
			 {/if}
			{/foreach}
		   </div>
		  </div>
		 </div>
		</div>
	   {/if}
	  </div>
	 </div>
	</div>
   </div>
  {/if}
  
  {if $controller->filter_categories}
   <div class="row-5">
	<label class="col-md-1-4 col-lg-1-5">{$words->catalog_filters_category_label('Выбрать по типу товара')}</label>
	<div class="col-md-3-4 col-lg-4-5">
	 <div class="categories">
	  <ul class="nav nav-tabs">
	   {foreach from=$controller->filter_categories item="category"}
		{assign var="active" value=$controller->filters->is_category_checked($category.id)}
		{if $active}{assign var="image" value=$category.image_filter_active}{else}{assign var="image" value=$category.image_filter}{/if}
		
		<li{if $active} class="active"{/if}>
		 <a href="{$category.url|url}{$controller->filters->getSelectedBrandsForUrl()}">{if $image}<img src="/admin/project_images/{$image}" alt="{$category.caption|escape}">{/if}{$category.caption}</a>
		</li>
	   {/foreach}
	  </ul>
	 </div>
	</div>
   </div>
  {/if}
  
  {foreach from=$filters->getAppendFilters(true) item="filter"}
   <div class="row-5">
	<label class="col-md-1-4 col-lg-1-5">{$filter.caption}</label>
	<div class="col-md-3-4 col-lg-4-5">
	 {if $filter.type == 'n'}
	  <div class="categories">
	   <ul class="nav nav-tabs">
		{foreach from=$filter.items item="item"}
		 {include file="catalog-filters-item-without-logo.tpl" item=$item}
		{/foreach}
	   </ul>
	  </div>
	 {else}
	  <div class="filters-with-logo">
	   <div class="row">
		<div class="col-xs-10 col-sm-11">
		 <div class="row">
		  {foreach name="visible_filter_items" from=$filter.items item="item"}
		   {if $smarty.foreach.visible_filter_items.index < 4}
			<div class="col-xs-6 col-sm-3">
			 {include file="catalog-filters-item-with-logo.tpl" item=$item}
			</div>
		   {/if}
		  {/foreach}
		 </div>
		</div>
		{if $filter.items|@count > 4}
		 <div class="col-xs-2 col-sm-1 text-right">
		  <a class="btn btn-primary dropdown-toggle" href="#" data-toggle="dropdown">Show all</a>
		  <div class="dropdown-menu" role="menu">
		   <div class="all-filters-with-logo">
			<div class="row-5">
			 {foreach name="invisible_filter_items" from=$filter.items item="item"}
			  {if $smarty.foreach.invisible_filter_items.index >= 4}
			   <div class="col-xs-2-4 col-md-1-4 col-lg-1-5">
				{include file="catalog-filters-item-with-logo.tpl" item=$item}
			   </div>
			  {/if}
			 {/foreach}
			</div>
		   </div>
		  </div>
		 </div>
		{/if}
	   </div>
	  </div>
	 {/if}
	</div>
   </div>
  {/foreach}
 </form>
</div>
