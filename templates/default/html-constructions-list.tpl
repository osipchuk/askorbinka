{* Smarty *}

<div class="container">
 {$controller->depart_item.caption|print_caption}
 
 <div class="html-constructions">
  {foreach from=$controller->items item="contruction"}
   <article class="item">
    <h2 class="h2">{$contruction.caption}</h2>
    {$contruction.html|put_content}
   </article>
  {/foreach}
 </div>
</div>
