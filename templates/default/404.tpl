{* Smarty *}
{"HTTP/1.1 404 Not Found"|header}
<!DOCTYPE html>
<html>
<head>
 <title>Ошибка 404 — документ не найден</title>
 <meta charset="UTF-8">
</head>
<body>
<h1>Документ не найден</h1>
<p>Запрашиваемый адрес http://{$smarty.server.SERVER_NAME}{$smarty.server.REQUEST_URI} не найден на сервере.</p>
</body>
</html> 