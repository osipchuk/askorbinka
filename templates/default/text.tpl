{* Smarty *}
<div class="text">
 {$controller->caption|print_caption}
 {$controller->text|put_content}
 {if $controller->contacts}
  <section class="form-section contacts">
   <h2 class="caption magenta">{$words->_('contacts_form_caption')}</h2>
   <div class="row">
    {include file="forms/contacts.tpl"}
   </div>
  </section>
 {/if}
</div>