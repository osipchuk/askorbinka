{* Smarty *}
{assign var="departs_caption" value=$app->get_departs_caption()}
{if $departs_caption->get_path_array()}
 <ol class="breadcrumb">
  <li><a href="{''|url}">{$words->bread_crubms_start}</a></li>
  {foreach name="bread_crumbs" from=$departs_caption->get_path_array() key="static" item="caption"}
   {strip}
    {if $smarty.foreach.bread_crumbs.last}
     <li class="active">{$caption}</li>
    {else}
     <li><a href="{$static|url}">{$caption}</a></li>
    {/if}
   {/strip}
  {/foreach}
 </ol>
{/if}