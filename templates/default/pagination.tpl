{* Smarty *}
{assign var="pagination" value=$app->pagination()}
{*page = {$pagination->get_page()}; pages_count = {$pagination->get_pages_count()}*}

{if $pagination->get_pages_count() > 1}
 <nav class="pagination-nav">
  <ul class="pagination pagination-sm">
   <li class="previous"><a href="{$pagination->first_page_url()|url}"><span class="glyphicon glyphicon-chevron-left"></span></a></li>
   {section name="pagination" loop=$pagination->get_pages_count()}
    <li{if $smarty.section.pagination.index == $pagination->get_page()} class="active"{/if}>
     <a href="{$pagination->page_url($smarty.section.pagination.index)}">{$smarty.section.pagination.iteration}</a>
    </li>
   {/section}
   <li class="next"><a href="{$pagination->last_page_url()|url}"><span class="glyphicon glyphicon-chevron-right"></span></a></li>
  </ul>
 </nav>
{/if}