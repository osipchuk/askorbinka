{* Smarty *}
{$item.caption|print_caption}
{if $item.search_result}
<nav class="search-results">
 {foreach from=$item.search_result item="result"}
  <article>
   <h4 class="h"><a href="{$result.href|url}">{$result.caption}</a></h4>
   <div class="describe">{$result.describe|strip_tags|truncate:400}</div>
   <footer><a href="{$result.href|url}">{$words->_('more')}</a></footer>
  </article>
 {/foreach}
</nav>
{else}
 <p>{$words->_('search_no_results')}</p>
{/if}