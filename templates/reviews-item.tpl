{* Smarty *}
<article class="item">
 <div class="row">
  <div class="col-lg-1 col-sm-2 avatar">
   <img src="{$controller->clients_model->account_avatar($item)}" alt="{$item.author_fio|escape}">
   {*{if $item.author_image}<img src="/admin/image.php?file={$item.author_image}&amp;width=81&amp;height=81" alt="{$item.author_fio|escape}">{/if}*}
  </div>
  <div class="col-lg-11 col-sm-10 review-content">
   <header class="item-header">
	<h2 class="h4 arial fio">{$item.author_fio}</h2>
	{include file="datetime-item.tpl" date=$item.date}
	<div class="rating">{$words->reviews_rate_caption('Оценка пользователя:')} <span class="stars">{section name="stars" loop=$item.rating}<span class="star{if $smarty.section.stars.index < $item} active{/if}"></span>{/section}</span></div>
   </header>
   <div>
	{if $good}
	 <h3 class="h4 arial">{$words->good_reviews_item_caption('Отзыв о товаре:')}</h3>
	{else}
	 <h3 class="h6">{$words->reviews_item_caption('Отзыв о магазине:')}</h3>
	{/if}
	{$item.text|put_user_content}
   </div>
  </div>
 </div>
 {if $item.reply}
  <footer class="item-footer">
   <div class="row">
	<div class="col-lg-2 col-md-3 col-sm-4 avatar"><img src="/img/reviews-admin-avatar.jpg" width="81" height="81" alt=""></div>
	<div class="col-lg-10 col-md-9 col-sm-8 review-content">
	 <div class="h4 arial fio">{$words->reviews_admin_fio('Патриция Плюшкина')}</div>
	 {include file="datetime-item.tpl" date=$item.reply_date}
	 <div>
	  <h3 class="{if $good}h4 arial{else}h6{/if} reply-caption">{$words->reviews_admin_reply_caption('Ответ администратора сайта:')}</h3>
	  {$item.reply|put_content}
	 </div>
	</div>
   </div>
  </footer>
 {/if}
</article>