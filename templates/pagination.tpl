{* Smarty *}
{assign var="pagination" value=$app->pagination()}

{if $pagination->get_pages_count() > 1}
 <nav class="pagination-nav">
  <ul class="pagination">
   <li class="previous"><a href="{$pagination->first_page_url()|url}"><span class="prev"></span>{$words->pagination_first('Назад')} </a></li>
   {section name="pagination" loop=$pagination->get_pages_count()}
	 {*
	 {if $smarty.section.pagination.index == 16}
	  <li class="dots"><span>...</span></li>
	 {else}
	  *}
	  <li class="{if $smarty.section.pagination.index == $pagination->get_page()} active{/if}">
	   <a href="{$pagination->page_url($smarty.section.pagination.index)}">{$smarty.section.pagination.iteration}</a>
	  </li>
	 {*
	 {/if}
     *}
   {/section}
   <li class="next"><a href="{$pagination->last_page_url()|url}">{$words->pagination_last('Вперед')} <span class="next"></span></a></li>
  </ul>
 </nav>
{/if}