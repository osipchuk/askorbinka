{* Smarty *}
{assign var="item" value=$controller->item}
{assign var="goods" value=$controller->goods}
<div class="container">
 <div class="news-and-actions-item">
  <header class="block-header">
   <form class="form-inline form-large" action="#">
	<div class="form-group">
	 <a class="btn btn-primary btn-back" href="{$url_maker->d_id_url($item.external_parent)|url}">{$words->news_and_actions_all_items_href('Все новинки и акции')}</a>
	</div>
   </form>
  </header>

  {$item.caption|print_caption}
  {include file="news-and-actions-items/dates.tpl" item=$item}
  {$item.text|put_content}
 </div>
</div>

{if $goods}
<div class="grey">
 <section class="container">
  <ul class="catalog-list row-5">
   {foreach from=$goods item="item"}
	<li class="col-xs-4-4 col-sm-2-4 col-md-1-4 col-lg-1-5">
	 {include file="catalog-list-item.tpl"}
	</li>
   {/foreach}
  </ul>
 </section>
 </div>
{/if}


{include file="best-sellers-and-last-goods-list.tpl"}

{include file="manual-blocks-list.tpl"}