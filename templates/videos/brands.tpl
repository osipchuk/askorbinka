{* Smarty *}
{assign var="filters" value=$controller->filters}
{assign var="brand_info" value=$controller->brand_info}
<header class="block-header">
 <form class="form-inline form-large" action="#">
  <div class="row">
   <div class="col-lg-7">
	<div class="form-group">
	 <label for="sort-button">{$words->videos_header_brand_caption('Сортировать:')}</label>
	 <div class="btn-group">
	  <button id="sort-button" type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
	   {assign var="category_caption" value=$filters->getCategoryCaption()}
	   {if $category_caption}
		{$category_caption}
	   {else}
		{$words->videos_header_brand_button('по брендам')}
	   {/if}
	   <span class="caret"></span>
	  </button>
	  <ul class="dropdown-menu" role="menu">
	   {foreach from=$filters->getCategories() item="item"}
		<li><a href="?{$filters->getParamsStr('category', $item.id)}">{$item.caption}</a></li>
	   {/foreach}
	  </ul>
	 </div>
	</div>
	<div class="form-group">
	 <label for="count-button">{$words->videos_header_limit_caption('Выводить по:')}</label>
	 <div class="btn-group sm">
	  <button id="count-button" type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
	   {$filters->getFilter('limit')} <span class="caret"></span>
	  </button>
	  <ul class="dropdown-menu" role="menu">
	   {foreach from=$filters->getLimits() item="limit"}
		<li><a href="?{$filters->getParamsStr('limit', $limit)}">{$limit}</a></li>
	   {/foreach}
	  </ul>
	 </div>
	</div>
   </div>
   <div class="col-lg-5">
	<div class="header-links">
	 {if $brand_info}
	  <a class="btn btn-default" href="{$url_maker->make_item_url($brand_info, $controller->cat_model)|url}">{$words->videos_brand_url('Подробно о бренде')} {$brand_info.caption}</a>
	  {if $brand_info.url}<a class="btn btn-default" href="{$brand_info.url}" target="_blank">{$words->videos_brand_url_official('Официальный сайт бренда')}</a>{/if}
	 {/if}
	</div>
   </div>
  </div>
  
 </form>
</header>

<div class="items-list brand-videos-list">
 {if $brand_info and $brand_info.image}
  <div class="page-header with-h-rows">
   <h2 class="h1"><img src="/admin/image.php?file=project_images/{$brand_info.image}&amp;width=130&amp;height=80" alt="{$brand_info.caption|escape}"></h2>
  </div>
 {/if}
 <div class="row">
  {foreach from=$controller->data item="item"}{strip}
   <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 column">
	<article class="item">
	 <a class="img various fancybox.iframe" href="http://www.youtube.com/embed/{$item.video_id}?autoplay=1" title="{$item.caption|escape}">
	  <img src="{$item.video_id|youtube_video_preview:$item.image}" alt="{$item.caption|escape}">
      <h2 class="h5 caption arial">{$item.caption}</h2>
	 </a>
	</article>
   </div>
  {/strip}{foreachelse}{include file="videos/no-items.tpl"}{/foreach}
 </div>
</div>

{if $controller->brand_categories}
<nav class="brand-categories">
 <h3 class="h4 arial">{$controller->brand_categories_caption}</h3>
 <div class="row-5">
  {foreach from=$controller->brand_categories item="item"}
   <div class="col-lg-1-5">
	<a class="btn btn-default btn-block" href="{$item.url|url}?filter_brands[]={$brand_info.static}">
	 {if $item.image}<img src="/admin/project_images/{$item.image}" alt="{$item.caption|escape}">{/if}
	 {$item.caption}
	</a>
   </div>
  {/foreach}
 </div>
</nav>
{/if}