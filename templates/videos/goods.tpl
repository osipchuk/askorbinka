{* Smarty *}
{assign var="filters" value=$controller->filters}
<header class="block-header">
 <form class="form-inline form-large" action="#">
  <div class="form-group">
   <label for="sort-button">{$words->videos_header_gods_caption('Сортировать:')}</label>
   <div class="btn-group">
	<button id="sort-button" type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
	 {assign var="brand_caption" value=$filters->getBrandCaption()}
	 {if $brand_caption}
	  {$brand_caption}
	 {else}
	  {$words->videos_header_brand_button('по брендам')}
	 {/if}
	 <span class="caret"></span>
	</button>
	<ul class="dropdown-menu" role="menu">
	 {foreach from=$filters->getBrands() item="item"}
	  <li><a href="?{$filters->getParamsStr('brand', $item.id)}">{$item.caption}</a></li>
	 {/foreach}
	</ul>
   </div>
   <div class="btn-group">
	<button id="sort-button" type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
	 {assign var="category_caption" value=$filters->getCategoryCaption()}
	 {if $category_caption}
	  {$category_caption}
	 {else}
	  {$words->videos_header_category_button('по категории')}
	 {/if}
	 <span class="caret"></span>
	</button>
	<ul class="dropdown-menu" role="menu">
	 {foreach from=$filters->getCategories() item="item"}
	  <li><a href="?{$filters->getParamsStr('category', $item.id)}">{$item.caption}</a></li>
	 {/foreach}
	</ul>
   </div>
  </div>
  <div class="form-group">
   <label for="count-button">{$words->videos_header_limit_caption('Выводить по:')}</label>
   <div class="btn-group sm">
	<button id="count-button" type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
	 {$filters->getFilter('limit')} <span class="caret"></span>
	</button>
	<ul class="dropdown-menu" role="menu">
	 {foreach from=$filters->getLimits() item="limit"}
	  <li><a href="?{$filters->getParamsStr('limit', $limit)}">{$limit}</a></li>
	 {/foreach}
	</ul>
   </div>
  </div>
 </form>
</header>

<div class="items-list goods-list">
 <div class="row">
  {foreach from=$controller->data item="item"}{strip}
   {assign var="url" value=$item.url|url}
   <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 column">
	<article class="item">
	 <a class="img various fancybox.iframe" href="http://www.youtube.com/embed/{$item.video_id}?autoplay=1" title="{$item.caption|escape}">
	  <img src="{$item.video_id|youtube_video_preview:$item.image}" alt="{$item.caption|escape}">
	 </a>
	 <h2 class="h5 caption arial">{$item.caption}</h2>
	 <footer class="item-footer">
	  <div class="row">
	   <div class="col-xs-4 col-sm-5"><div class="more"><a class="btn btn-default btn-block" href="{$url}">{$words->videos_good_more('Подробно')}</a></div></div>
	   <div class="col-xs-4 col-sm-4"><div class="reviews"><a class="btn btn-default btn-block" href="{$url}#tab-reviews">{$words->videos_good_reviews('Отзывы')}</a></div></div>
	   {if $item.price !=0 and $item.avail}
		<div class="col-xs-4 col-sm-3">
		 <div class="buy">
		  <a class="btn btn-primary btn-block" href="#" data-add-to-cart="{$item.id}" data-buy-one-good="1">{$words->videos_good_buy('Купить')}</a>
		 </div>
		</div>
	   {/if}
	  </div>
	 </footer>
	</article>
   </div>
  {/strip}{foreachelse}{include file="videos/no-items.tpl"}{/foreach}
 </div>
</div>