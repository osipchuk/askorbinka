{* Smarty *}
{assign var="filters" value=$controller->filters}
<header class="block-header">
 <form class="form-inline form-large" action="#">
  <div class="form-group">
   <label for="count-button">{$words->videos_header_limit_caption('Выводить по:')}</label>
   <div class="btn-group sm">
	<button id="count-button" type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
	 {$filters->getFilter('limit')} <span class="caret"></span>
	</button>
	<ul class="dropdown-menu" role="menu">
	 {foreach from=$filters->getLimits() item="limit"}
	  <li><a href="?{$filters->getParamsStr('limit', $limit)}">{$limit}</a></li>
	 {/foreach}
	</ul>
   </div>
  </div>
 </form>
</header>

<div class="items-list simple-videos-list">
 <div class="row">
  {foreach from=$controller->data item="item"}{strip}
   <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 column">
	<article class="item">
	 <a class="img various fancybox.iframe" href="http://www.youtube.com/embed/{$item.video_id}?autoplay=1" title="{$item.caption|escape}">
	  <img src="{$item.video_id|youtube_video_preview:$item.image}" alt="{$item.caption|escape}">
	 </a>
	 <h2 class="h5 caption arial">{$item.caption}</h2>
	</article>
   </div>
  {/strip}{foreachelse}{include file="videos/no-items.tpl"}{/foreach}
 </div>
</div>

<footer class="block-footer">
 <a class="btn btn-info" href="{$url_maker->d_module_url('reviews')|url}">{$words->videos_reviews_link('Отзывы об интернет-магазине')}</a>
</footer>