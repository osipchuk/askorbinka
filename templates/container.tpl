{* Smarty *}
{include file="header.tpl"}
<header class="main-header">
 <div class="top">
  <div class="container">
   <div class="col-logo">
	<a class="logo" href="{''|url}"><img src="/img/logo.png" width="164" height="192" alt="{$appHelper->get_global(title)|escape}"></a>
   </div>
   <div class="col-near-logo">
	<div class="row">
	 <div class="col-lg-9 col-md-11">
	  {include file="main-nav.tpl"}
	 </div>
	 <div class="col-lg-3 col-md-1">
	  {include file="forms/search-form.tpl"}
	 </div>
	</div>
   </div>
  </div>
 </div>
 <div class="container">
  <div class="col-near-logo">
   {include file="categories-nav/nav.tpl"}
  </div>
 </div>
 <div class="container">
  {include file="right-nav.tpl"}
 </div>
 {if $app->isStart()}
  {include file="slider.tpl"}
 {else}
  {if !$controller->hide_bread}{include file="bread-crumbs.tpl"}{/if}
 {/if}
</header>
<main>
 <section class="container popup-message-container">
  <div class="popup-message" data-block-id="cart-message">
   <a class="close" data-close="" href="#"></a>
   <div class="message" data-id="message">Message</div>
   <footer class="footer">
	<a class="btn btn-primary" href="{$url_maker->d_module_url('cart')|url}">{$words->cart_message_go_to_cart('перейти в корзину')}</a>
	<a class="btn btn-info" data-close="" href="#">{$words->cart_message_continue('продолжить')}</a>
   </footer>
  </div>
 </section>
 {include file=$app->getTemplate()}
</main>
<footer class="main-footer">
 <div class="container">
  <nav class="footer-nav">
   <div class="row">
	<div class="col-xs-12">
	 {include file="categories-nav/nav.tpl" footer="1"}
	</div>
   </div>
  </nav>
 </div>
 <div class="bottom">
  <div class="container">
   <div class="row">
	<div class="col-sm-3 col-md-3 col-lg-5">
	 <div class="copy">
	  <a class="logo" href="{''|url}"><img src="/img/footer-logo.png" width="130" height="18" alt="{$appHelper->get_global(title)|escape}"></a>
	  <small>{$words->footer_small('Копирайт Копирайт Копирайт')}</small>
	 </div>
	 <nav class="social-nav visible-lg">
	  {include file="social-nav.tpl"}
	 </nav>
	</div>
	<div class="hidden-xs col-sm-9 col-md-9 col-lg-7">
	 {include file="main-nav.tpl"}
	</div>
   </div>
   <div class="row">
    <div class="col-xs-12">
     <aside class="seo">
      {$words->footer_seo_text('Доставка во все города Украины: Киев, днепропетровск, Харьков, Ровно, Львов, Полтава')}
     </aside>
    </div>
   </div>
   
  </div>
 </div>
</footer>
{include file="footer.tpl"}