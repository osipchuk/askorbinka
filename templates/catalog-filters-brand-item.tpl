{* Smarty *}
{if $comparison}
 {assign var="active" value=$controller->comparison->is_brand_checked($brand.id)}
{else}
 {assign var="active" value=$controller->filters->is_brand_checked($brand.id)}
{/if}
<input type="checkbox" name="filter_brands[]" value="{$brand.static}"{if $active} checked{/if}>
<a class="item" href="#" data-filter-brand="{$brand.static}">{if $brand.image}<img src="/admin/image.php?file=project_images/{$brand.image}&amp;width=130&amp;height=40{if !$active}&amp;grey=1{/if}" alt="{$brand.caption|escape}">{/if}</a>