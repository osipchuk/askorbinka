{* Smarty *}
{assign var="filters" value=$controller->filters}
<div class="container">
 <div class="reviews-list">
  <header class="block-header">
   <form class="form-inline form-large" action="#">
	<div class="form-group">
	 <label for="sort-button">{$words->reviews_header_date_caption('Сортировать отзывы')}</label>
	 <div class="btn-group calendar">
	  <button id="sort-button" type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
	   {if $filters->getFilter('dateBegin') || $filters->getFilter('dateEnd')}
		{$filters->getFilter('dateBegin')|default:'...'}  -  {$filters->getFilter('dateEnd')|default:'...'}
	   {else}
		{$words->reviews_header_date_button('по дате')}
	   {/if}
	   <span class="caret"></span>
	  </button>
	  <div class="dropdown-menu">
	   <div class="row">
		{include file="news-and-actions-items/filters-calendar.tpl"}
	   </div>
	  </div>
	 </div>
	</div>
	<div class="form-group">
	 <label for="count-button">{$words->reviews_header_limit_caption('Выводить по:')}</label>
	 <div class="btn-group">
	  <button id="count-button" type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
	   {$filters->getFilter('limit')} <span class="caret"></span>
	  </button>
	  <ul class="dropdown-menu" role="menu">
	   {foreach from=$filters->getLimits() item="limit"}
		<li><a href="?{$filters->getParamsStr('limit', $limit)}">{$limit}</a></li>
	   {/foreach}
	  </ul>
	 </div>
	</div>
   </form>
  </header>

  {foreach from=$controller->data item="item"}
   {include file="reviews-item.tpl" item=$item}
  {/foreach}
  
  {include file="pagination.tpl"}
  
  {include file="forms/review.tpl"}
 </div>
</div>

{include file="best-sellers-and-last-goods-list.tpl"}

{include file="manual-blocks-list.tpl"}
