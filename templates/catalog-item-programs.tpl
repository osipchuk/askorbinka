{* Smarty *}
{if $data}
 <section class="program-icons">
  {if $data|@count == 1}
   {assign var="item" value=$data.0}
   <a href="{$item.url|url}" title="{$item.caption|escape}">
    <img src="/admin/image.php?file=project_images/{$item.good_image}&amp;width=260&amp;height=400" alt="{$item.caption|escape}">
   </a>
  {else}
   <div class="row">
    {foreach from=$data item="item"}{strip}
     <div class="col-xs-6">
      <a href="{$item.url|url}" title="{$item.caption|escape}">
       <img src="/admin/image.php?file=project_images/{$item.good_image}&amp;width=130&amp;height=200" alt="{$item.caption|escape}">
      </a>
     </div>
    {/strip}{/foreach}
   </div>
  {/if}
 </section>
{/if}
