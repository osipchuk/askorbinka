{* Smarty *}
<div class="container">
 <div class="news-list">
  {foreach from=$controller->data item="item"}
   <article class="item">
    <div class="row">
     <div class="col-xs-2">
      <div class="image">{if $item.image}<img src="/admin/image.php?file=project_images/{$item.image}&amp;width=140&amp;height=110" alt="{$item.caption|escape}">{/if}</div>
     </div>
     <div class="col-xs-8">
      <div class="text">
       <h2 class="h1 list-item-caption">{$item.caption}</h2>
       {$item.preview}
       {include file="datetime-item.tpl" date=$item.date}
      </div>
     </div>
     <div class="col-xs-2 text-right">
      <a class="btn btn-default btn-arrow-right" href="{$item.url|url}">{$words->more('Читать далее')}</a>
     </div>
    </div>
   </article>
  {/foreach}

  {include file="pagination.tpl"}
 </div>
</div>

{include file="best-sellers-and-last-goods-list.tpl"}

{include file="manual-blocks-list.tpl"}