{* Smarty *}
{assign var="item" value=$controller->item}
<div class="container">
 <div class="news-item">
  {$item.caption|print_caption}
  {$item.text|put_content}
 </div>
</div>

<div class="grey">
 <div class="container">
  <section class="action-goods">
   <div class="page-header">
    <h2 class="h1">{$words->programs_program_goods('Акционные товары')}</h2>
   </div>
   {if $controller->goods}
    <ul class="catalog-list row-5">
     {foreach from=$controller->goods item="good"}
      <li class="col-xs-4-4 col-sm-2-4 col-md-1-4 col-lg-1-5">
       {include file="catalog-list-item.tpl" item=$good}
      </li>
     {/foreach}
    </ul>
    {include file="pagination.tpl"}
   {else}
    {$controller->message}
   {/if}
  </section>
 </div>
</div>


{include file="best-sellers-and-last-goods-list.tpl"}

{include file="manual-blocks-list.tpl"}