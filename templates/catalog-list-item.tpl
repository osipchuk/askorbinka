{* Smarty *}
{assign var="caption" value=$item.category|cat:" "|cat:$item.caption}
{if !$items_actions}
 {assign var="items_actions" value=$controller->items_actions}
{/if}
<div class="item{if $with_full_prices} with-full-prices{/if}{if $add_discount_make_href and $item.id == $controller->good_id} active{/if}"
		{if $add_discount_make_href} data-discount-make-href="{$controller->filters->discountMakeHref($item)|url}#generate-code"{/if}>
 {if $items_actions[$item.id]}
  <div class="action-labels">
   {foreach from=$items_actions[$item.id] item="action"}
	{include file="news-and-actions-items/label-small.tpl" item=$action show_href=1}
   {/foreach}
  </div>
 {/if}
 {if !$with_full_prices}<a href="{$item.url|url}">{/if}
  <div class="img">
   {if $item.image}<img data-good-image="{$item.id}" src="/admin/image.php?file=project_images/{$item.image}&amp;width=190&amp;height=160" alt="{$caption|escape}">{/if}
  </div>
  <h3 class="h6"><span>{$caption|strip_tags}</span></h3>
 {if !$with_full_prices}</a>{/if}
 <div class="prices">
  {if $item.price > 0}
   {if $with_full_prices}
	<dl class="dl-horizontal">
	 <dt>{$words->goods_price}</dt><dd><strong>{$item.price|price_format}</strong> {$words->currency}</dd>
	 <dt>{$words->goods_discount('Скидка:')}</dt><dd><strong>{$item.code_discount}</strong> %</dd>
	 <dt>{$words->goods_price_with_discount('Цена со скидкой:')}</dt><dd><strong class="discount">{$controller->goods_model->price($item, true)|price_format}</strong> {$words->currency}</dd>
	 <dt>{$words->goods_bonuses('Бонусов:')}</dt><dd><strong>{$controller->discount_code_modifier->codeBonusesCount($item)}</strong></dd>
	</dl>
   {else}
	{if $item.action_price > 0}
	 <div>{$words->goods_price_new('Новая цена:')} <strong>{$item.action_price|price_format}</strong> {$words->currency('грн')}</div>
	 <div class="old">{$words->goods_price_old('Старая цена:')} <strong>{$item.price|price_format}</strong> {$words->currency('грн')}</div>
	{else}
	 <div class="simple">{$words->goods_price('Цена:')} <strong>{$item.price|price_format}</strong> {$words->currency('грн')}</div>
	{/if}
   {/if}
  {/if}
  
 </div>
 <footer class="block-footer">{strip}
   {if $full_footer}
	{if $controller->categories_model->check_good_item_can_be_compared_in_category($controller->category, $item)}{assign var="show_comparison" value=1}{/if}
	{if $show_comparison and $item.price and $item.avail}
	 <div class="item-btn"><a class="btn btn-default btn-block{if $controller->comparison->is_good_selected($item.id)} active{/if}" href="#" data-comparison="{$item.id}">{$words->goods_compare('Сравнить')}</a></div>
	 <div class="item-btn">{include file="catalog-add-to-cart-button.tpl" item=$item}</div>
	{else}
	 {if $show_comparison}
	  <a class="btn btn-default btn-block{if $controller->comparison->is_good_selected($item.id)} active{/if}" href="#" data-comparison="{$item.id}">{$words->goods_compare('Сравнить')}</a>
	 {else}
	  {include file="catalog-add-to-cart-button.tpl" item=$item}
	 {/if}
	{/if}
	<a class="btn btn-default btn-block" href="{$item.url|url}">{$words->goods_more('Подробно')}</a>
   {else}
	<div class="item-btn"><a class="btn btn-default btn-block" href="{$item.url|url}">{$words->goods_more('Подробно')}</a></div>
	<div class="item-btn">{include file="catalog-add-to-cart-button.tpl" item=$item}</div>
   {/if}
  {/strip}</footer>
</div>