{* Smarty *}
<ul id="links">
 {foreach name="articles_nav" from=$controller->articles_nav item="item"}
  <li>{if !$smarty.foreach.articles_nav.first}<span>|</span>{/if}<a href="{$item.url}" title="{$item.caption|escape}">{$item.caption}</a></li>
 {/foreach}
</ul>

<div id="accordion" class="panel-group">
 {foreach name="articles" from=$controller->data item="item"}
  {assign var="index" value=$smarty.foreach.articles.iteration}
  <div class="panel panel-default">
   <div class="panel-heading">
	<h2 class="panel-title{if $index == 1} in{/if}">
	 <a data-toggle="collapse" data-parent="#accordion" href="#collapse-{$index}" title="{$item.caption|escape}">{$item.caption}<div class="arrow"></div></a>
	</h2>
   </div>
   <div id="collapse-{$index}" class="panel-collapse collapse{if $index == 1} in{/if}">
	<div class="panel-body">
	 {if $item.image}
	  <div class="photo"><img src="/admin/image.php?file=project_images/{$item.image}&amp;width=300&amp;height=185" alt="{$item.caption|escape}"></div>
	 {/if}
	 <div class="description{if $item.image} with-image{/if}">{$item.text|put_content}</div>
	</div>
   </div>
  </div>
 {/foreach}
</div>