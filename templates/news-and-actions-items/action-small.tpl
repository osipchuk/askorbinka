{* Smarty *}
<article class="item item-small item-action">
 {include file="news-and-actions-items/label.tpl" item=$item}
 <div class="row">
  <div class="col-xs-6">
   <div class="image">
	{if $item.image}<img src="/admin/project_images/{$item.image}" alt="{$item.caption|escape}">{/if}
   </div>
  </div>
  <div class="col-xs-6">
   {include file="news-and-actions-items/dates.tpl" item=$item}
   <h3 class="h6">{$item.caption}</h3>
   <footer class="image-footer">{$item.preview}</footer>
  </div>
 </div>
 <div class="hidden"><a data-more="1" href="{$item.url|url}">{$words->more}</a></div>
</article>