{* Smarty *}
<div class="col-sm-6">
 <div class="h5 arial">{$words->filters_calendar_from('С')}</div>
 <div id="filter-from"></div>
</div>
<div class="col-sm-6">
 <div class="h5 arial">{$words->filters_calendar_to('По')}</div>
 <div id="filter-to"></div>
</div>
<script>
 var active_dates = {$controller->active_dates|@json_encode}, 
	 filter_params = {$controller->filters->getFilters()|@json_encode};
</script>