{* Smarty *}
{if $item.date_begin != 0 and $item.date_end != 0}
 <div class="dates">{$words->news_and_actions_dates_caption('Период проведения акции с')} {$item.date_begin|normal_date}{$words->pub_date_year_short} {$words->news_and_actions_dates_to('по')} {$item.date_end|normal_date}{$words->pub_date_year_short}</div>
{/if}