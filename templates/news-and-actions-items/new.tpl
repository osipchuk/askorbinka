{* Smarty *}
<article class="item item-new">
 {if $item}
  {include file="news-and-actions-items/label.tpl" item=$item}
  <div class="image">
   {if $item.image}<img src="/admin/project_images/{$item.image}" alt="{$item.caption|escape}">{/if}
  </div>
  <h3 class="h6">{$item.caption}</h3>
  <footer class="image-footer">{$item.preview}</footer>
 {/if}
 <div class="hidden"><a data-more="1" href="{$item.url|url}">{$words->more}</a></div>
</article>