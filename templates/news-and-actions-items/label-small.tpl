{* Smarty *}
{if $item.image_label_small}
 {if $show_href}<a href="{$item.url|url}" target="_blank" title="{$item.caption|escape}">{/if}
  <span class="label-image"><img src="/admin/project_images/{$item.image_label_small}" alt="{$item.label_text|escape}"></span>
 {if $show_href}</a>{/if}
{/if}
