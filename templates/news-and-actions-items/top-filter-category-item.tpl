{* Smarty *}
<li><a{if $item.level} style="padding-left: {$item.level*20+20}px;"{/if} href="?{$filters->getParamsStr('category', $item.id)}">{$item.caption}</a></li>
{foreach from=$item.sub item="sub_item"}
 {include file="news-and-actions-items/top-filter-category-item.tpl" item=$sub_item filters=$filters}
{/foreach}