{* Smarty *}
{if $item.action or ($item.new and $item.image_label)}
 {if $show_href}<a href="{$item.url|url}" target="_blank" title="{$item.caption|escape}">{/if}
 {if $item.action}
  <span class="label label-danger label-action{if $item.double_label} label-double{/if}">{$item.label_text}{if $item.image_label}<span class="label-image"><img src="/admin/project_images/{$item.image_label}" alt="{$item.label_text|escape}"></span>{/if}</span>
 {else}
  <span class="label-image"><img src="/admin/project_images/{$item.image_label}" alt="{$item.label_text|escape}"></span>
 {/if}
 {if $show_href}</a>{/if}
{/if}


