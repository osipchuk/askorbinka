{* Smarty *}
{if $date != 0}
 <time class="date" datetime="{$date}"><span class="date-caption">{$words->pub_date_caption('Дата публикации:')} </span>{$date|normal_date} <span class="date-caption">{$words->pub_date_year_short('г.')}</span></time>
{/if}
