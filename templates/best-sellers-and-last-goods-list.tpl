{* Smarty *}
{assign var="watched_goods" value=$controller->watched_goods->get_goods()}
{if $show_analogs}
 {assign var="block_2_goods" value=$controller->goods_model->get_analogs($show_analogs)}
{/if}
{if !$block_2_goods}
 {assign var="block_2_goods" value=$controller->goods_model->get_bestsellers()}
 {assign var="show_analogs" value=0}
{/if}
{assign var="watched_goods_actions" value=$controller->news_and_actions_goods->get_goods_array_actions($watched_goods, true)}
{assign var="block_2_actions" value=$controller->news_and_actions_goods->get_goods_array_actions($block_2_goods, true)}

<section class="goods-list">
 <div class="container">
  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
   {if $watched_goods}
	<li class="active"><a href="#watched" role="tab" data-toggle="tab">{$words->good_tabs_watched_caption('Просмотренные товары')}</a></li>
   {/if}
   <li{if !$watched_goods} class="active"{/if}>
	{if $show_analogs}
	 <a href="#analogs" role="tab" data-toggle="tab">{$words->good_tabs_analogs_caption('Аналоги')}</a>
	{else}
	 <a href="#best-sellers" role="tab" data-toggle="tab">{$words->good_tabs_bestsellers_caption('Бестселлеры')}</a>
	{/if}
   </li>
  </ul>
 </div>

 <div class="grey">
  <div class="container">
   <!-- Tab panes -->
   <div class="tab-content">
	{if $watched_goods}
	 <div class="tab-pane active" id="watched">
	  <div class="catalog-list row-5">
	   <section class="owl-carousel goods-scroll">
		{foreach from=$watched_goods item="item"}
		 <div>
		  {include file="catalog-list-item.tpl" item=$item items_actions=$watched_goods_actions}
		 </div>
		{/foreach}
	   </section>
	  </div>
	 </div>
	{/if}
	<div class="tab-pane{if !$watched_goods} active{/if}" id="{if $show_analogs}analogs{else}best-sellers{/if}">
	 <div class="catalog-list row-5">
	  <section class="owl-carousel goods-scroll">
	   {foreach from=$block_2_goods item="item"}
		<div>
		 {include file="catalog-list-item.tpl" item=$item items_actions=$block_2_actions}
		</div>
	   {/foreach}
	  </section>
	 </div>
	</div>
   </div>
  </div>
 </div>
</section>