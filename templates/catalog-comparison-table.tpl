{* Smarty *}
{assign var="comparison" value=$controller->comparison}
<div class="table-responsive">
 <table class="table table-comparison">
  <thead>
  <tr>
   <th{if $list} colspan="2"{/if}>{$words->comparison_model_th('Модель')}</th>
   {foreach from=$comparison->get_properties() item="property"}
	<th>{$property.caption}</th>
   {/foreach}
   <th>{$words->comparison_price_th('Цена, грн')}</th>
  </tr>
  </thead>
  <tbody>
  {foreach from=$comparison->get_data($list) item="item"}
   <tr class="item">
	{if $list}
	 <td class="delete">
	  <a{if $controller->comparison->is_good_selected($item.id)} class="active"{/if} href="#" data-comparison="{$item.id}">delete</a>
	 </td>
	{/if}
	<td class="caption">
	 <a href="{$item.url|url}" target="_blank">
	  {if $item.image}
	   <div class="img"><img data-good-image="{$item.id}" src="/admin/image.php?file=project_images/{$item.image}&amp;width=55&amp;height=50" alt="{$item.caption|escape}"></div>
	  {/if}
	  {$item.caption}
	 </a>
	</td>
	{foreach from=$item.params item="param"}
	 <td>
	  {if !$param.value}
	   <div class="bool">-</div>
	  {else}
	   {include file="catalog-comparison-tds/"|cat:$param.type|cat:".tpl" item=$param}</td>
	  {/if}
	{/foreach}
	<td>{$controller->goods_model->price($item)}</td>
	{if $list}<td class="buttons">
	 <a class="btn btn-default btn-block" href="{$item.url|url}">{$words->goods_more}</a>
	 {include file="catalog-add-to-cart-button.tpl" item=$item}
	</td>{/if}
   </tr>
  {/foreach}
  </tbody>
 </table>
</div>
