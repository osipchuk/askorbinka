{* Smarty *}
{if $controller->slider}
 {assign var="settings" value=$appHelper->get_global('global_settings')}
 <div class="slider"{if $settings.slider_background} style="{$settings.slider_background|escape}"{/if}>
  <div class="container">
   <section id="carousel" class="carousel slide" data-ride="carousel" data-interval="6000">
	<div class="carousel-inner">
	 {foreach name="slider" from=$controller->slider item="item"}
	  <div class="item{if $smarty.foreach.slider.first} active{/if}">
		{if $item.href}<a href="{$item.href|url}" title="{$item.caption|escape}">{/if}
		 <img src="/admin/project_images/{$item.image}" alt="{$item.caption|escape}" title="{$item.caption|escape}">
		{if $item.href}</a>{/if}
	  </div>
	 {/foreach}
	</div>
	
 
	<!-- Controls -->
	<a class="left carousel-control" href="#carousel" role="button" data-slide="prev"></a>
	<a class="right carousel-control" href="#carousel" role="button" data-slide="next"></a>
 
	<!-- Indicators -->
	<ol class="carousel-indicators">{strip}
	  {foreach name="slider" from=$controller->slider item="item"}
	   <li data-target="#carousel" data-slide-to="{$smarty.foreach.slider.index}"{if $smarty.foreach.slider.first} class="active"{/if}></li>
	  {/foreach}
	 {/strip}</ol>
  </div>
</div>
{/if}
