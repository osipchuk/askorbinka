{* Smarty *}
{assign var="pro" value=$controller->pro}
<div class="register">
 <div class="container">
  {$controller->title|print_caption}

  <div class="row">
   <div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-2">
	<form class="form-horizontal" action="{'ajax/register'|url}" data-ajax-form="">
	 <input type="hidden" name="pro" value="{$pro}">
	 {if !$pro}
	  <header class="block-header">
	   <div class="form-group">
		<label class="col-sm-4 control-label">{$words->register_social_auth_caption('Войти через')}</label>
		<div class="col-sm-8">
		 {include file="auth-social.tpl"}
		</div>
	   </div>
	  </header>
	 {/if}
	 
	 {foreach from=$controller->fields key="field" item="item"}
	  <div class="form-group">
	   <label for="{$field}" class="col-sm-4 control-label">{$words->_w('register_fields_', $field)}{if $item.required} <sup>*</sup>{/if}</label>
	   <div class="col-sm-8">
        <input type="{if $item.type}{$item.type}{else}text{/if}" class="form-control input-lg" name="{$field}" id="{$field}"{if $item.required} required{/if}
                {if $item.type == 'tel'} data-phone-number value="+380" pattern="\+380 [0-9]{ldelim}2{rdelim} [0-9]{ldelim}7{rdelim}"{/if}>
       </div>
	  </div>
	 {/foreach}
	 <div class="form-group">
	  <div class="col-sm-8 col-sm-offset-4">
	   <footer class="block-footer">
		<div class="note">{$words->register_form_note('<sup>*</sup> Поля, обязательные для заполнения')}</div>
		{if $controller->delivery_categories}
		 <h2 class="h1 text-center">{$words->register_form_subscribe_caption('получать уведомления')}</h2>
		 {foreach from=$controller->delivery_categories item="item"}
		  <div class="checkbox">
		   <label>
			<input type="checkbox" name="delivery_categories[]" value="{$item.id}" checked> {$item.caption}
		   </label>
		  </div>
		 {/foreach}
		{/if}
		<div class="buttons">
		 <a href="#" class="btn btn-primary btn-block btn-lg btn-ok" data-submit-buttton="">{$words->register_form_submit('Зарегистрироваться')}</a>
		 <a href="{$url_maker->d_module_url('profile')|url}" class="btn btn-primary btn-block btn-lg btn-profile">{$words->register_form_profile_button('Личный кабинет')}</a>
		 <input type="submit">
		</div>
	   </footer> 
	  </div>
	 </div>
	</form>
   </div>
  </div>
 </div>
</div>