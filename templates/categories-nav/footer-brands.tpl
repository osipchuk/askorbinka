{* Smarty *}
<div class="footer-brands">
 <strong>{$words->categories_nav_brands_caption('Производители:')}</strong>
 <ul>
  {foreach from=$controller->categories_model->get_category_brands($parent.id) item="brand"}
   <li><a href="{$item.url|url}?brand={$brand.static}">{$brand.caption}</a></li>
  {/foreach}
 </ul>
</div>