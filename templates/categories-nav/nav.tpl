{* Smarty *}
<nav class="categories-nav">
 <ul class="nav nav-pills nav-justified">
  {if $footer}
   <li class="register-li">
	<a class="btn-register professional" href="{$url_maker->d_module_url('profile')|cat:"/register-pro"|url}">{$words->footer_register_prof('регистрация для профи')}</a>
   </li>
  {/if}
  
  {foreach from=$controller->categories item="item"}
   <li{if $item.sub} class="dropdown"{/if}>
	<a href="{$item.url|url}"{if $item.sub} class="dropdown-toggle" data-toggle="dropdown"{/if}>
	 <span class="img">{if $item.image}<img src="/admin/project_images/{$item.image}" alt="{$item.caption|escape}">{/if}</span>
	 <span class="caption">{$item.caption}</span>
	</a>
	{include file="categories-nav/subnav.tpl" item=$item}
   </li>
  {/foreach}
 </ul>
</nav>