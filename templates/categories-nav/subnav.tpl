{* Smarty *}
{if $item.sub}
 {assign var="large" value=$controller->categories_model->is_nav_item_large($item)}
 <div class="dropdown-menu{if $large} large{/if}" role="menu"{if $item.image_subnav} style="background-image: url('/admin/project_images/{$item.image_subnav}');"{/if}>
  {if $large}
   {foreach from=$item.sub item=subcategory}
	{if $subcategory.sub}
	 {include file="categories-nav/subnav-section.tpl" item=$subcategory subitem=$subcategory}
	{/if}
   {/foreach}

   <div class="brands-section">
	{foreach name="brand_sections" from=$item.sub item=subcategory}
	 <div class="brands{if $smarty.foreach.brand_sections.last} last{/if}">
	  <strong>{$words->categories_nav_brands_caption('Производители:')}</strong>
	  <ul>
	   {foreach from=$controller->categories_model->get_category_brands($subcategory.id) item="brand"}
		{include file="categories-nav/brand-item.tpl" category=$subcategory brand=$brand}
	   {/foreach}
	  </ul>
	 </div>
	{/foreach}
	{include file="categories-nav/footer-brands.tpl" parent=$item}
   </div>
  {else}
   {include file="categories-nav/subnav-section.tpl" item=$item subitem=$subitem}
   <div class="brands-section">
	<div class="brands">
	 <strong>{$words->categories_nav_brands_caption('Производители:')}</strong>
	 <ul>
	  {foreach from=$controller->categories_model->get_category_brands($item.id) item="brand"}
	   {include file="categories-nav/brand-item.tpl" category=$item brand=$brand}
	  {/foreach}
	 </ul>
	</div>
	{include file="categories-nav/footer-brands.tpl" parent=$item}
   </div>
  {/if}
 </div>
{/if}