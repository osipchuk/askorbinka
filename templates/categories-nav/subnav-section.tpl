{* Smarty *}
<div class="section">
 <h4 class="h1"><a href="{$item.url|url}">{$item.caption}</a></h4>
 <ul class="subnav-ul">
  {foreach from=$item.sub item="subitem"}
   <li><a href="{$subitem.url|url}"><span class="small-img">{if $subitem.image}<img src="/admin/project_images/{$subitem.image}" alt="{$subitem.caption|escape}">{/if}</span>{$subitem.caption}</a></li>
  {/foreach}
 </ul>
</div>