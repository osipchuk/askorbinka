{* Smarty *}
<div class="brands-technologies">
 <div class="container">
  <header class="block-header">
   <nav class="catalog-filters">
	<div class="brands">
	 <div class="row">
	  {foreach from=$controller->brands item="item"}
	   {if $item.image}
		<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
		 <a class="item" href="{$controller->base_url|cat:"/"|cat:$item.static|url}">
		  <img src="/admin/image.php?file=project_images/{$item.image}&amp;width=130&amp;height=40{if $item.id != $controller->item.id}&amp;grey=1{/if}" alt="{$item.caption|escape}">
		 </a>
		</div>
	   {/if}
	  {/foreach}
	 </div>
	</div>
	<div class="categories">
	 <ul class="nav nav-tabs">
	  {foreach from=$controller->brand_categories item="item"}
	   {if $item.id == $controller->category.id}
		{assign var="active" value=1}
		{assign var="image" value=$item.image_filter_active}
	   {else}
		{assign var="active" value=0}
		{assign var="image" value=$item.image_filter}
		{/if}
	   <li{if $active} class="active"{/if}>
		<a href="{$controller->base_url|cat:"/"|cat:$controller->item.static|url}?category={$item.id}">
		 {if $image}<img src="/admin/project_images/{$image}" alt="{$item.caption|escape}">{/if}{$item.caption}
		</a>
	   </li>
	  {/foreach}
	 </ul>
	</div>
   </nav>
  </header>
 </div>
 
 <div class="grey">
  <div class="container">
   <div class="page-header with-h-rows">
	<h2 class="h1 normal">{$words->technologies_page_header('характеристики. преимущества. выгоды')}</h2>
	<h1>{$controller->category.caption} {$controller->item.caption}</h1>
   </div>
   
   <div class="technologies-list height-100">
	<div class="row">
	 {foreach from=$controller->technologies item="item"}
	  <div class="col-xs-6 col-sm-4 col-md-3">
	   <article class="item">
		<div class="image">
		 {if $item.image_small}
		  <a class="show_lg_photo" data-fancybox-group="technologies-images" href="/admin/project_images/{$item.image}">
		   <img src="/admin/image.php?file=project_images/{$item.image_small}&amp;width=210&amp;height=96" alt="{$item.caption|escape}">
		  </a>
		 {/if}
		</div>
		<h3 class="h6">{$item.caption}</h3>
		<footer class="item-footer">
		 {if $item.image}
		  <a class="btn btn-default btn-block show_lg_photo" data-fancybox-group="technologies" href="/admin/project_images/{$item.image}">{$words->technologoes_more('Подробнее')}</a>
		 {/if}
		 <a class="btn btn-default btn-block" href="{$controller->base_url|cat:"/"|cat:$controller->item.static|url}?category={$controller->category.id}&amp;tech={$item.id}">
		  {$controller->category.caption} {$controller->item.caption} {$words->technologies_goods_with_current_tech('c данной технологией')}
		 </a>
		</footer>
	   </article>
	  </div>
	 {/foreach}
	</div>
   </div>
  </div>
 </div>
 
 {if $controller->goods}
  <section class="goods" data-scroll-content>
   <div class="container">
	<div class="page-header with-h-rows">
	 <h2 class="h1">{$controller->category.caption} {$controller->item.caption}</h2>
	</div>
   </div>
   <div class="grey">
	<div class="container">
	 <div class="row">
	  <ul class="catalog-list row-5">
	   {foreach from=$controller->goods item="item"}{strip}
		<li class="col-xs-4-4 col-sm-2-4 col-md-1-4 col-lg-1-5">
		 {include file="catalog-list-item.tpl" item=$item}
		</li>
	   {/strip}{/foreach}
	  </ul>
	 </div>
	</div>
   </div>
  </section>
 {/if}
</div>


{include file="best-sellers-and-last-goods-list.tpl"}

{include file="manual-blocks-list.tpl"}