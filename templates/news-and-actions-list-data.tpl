{* Smarty *}
<div class="row-5">
 {assign var="news_output" value="0"}
 {section name="actions" loop=$actions|@count}
  {assign var="item_type" value=$smarty.section.actions.iteration%6}
  {if $item_type == 3 or $item_type == 4}
   <div class="col-lg-1-5 visible-lg">
	{include file="news-and-actions-items/new.tpl" item=$news.$news_output}
   </div>
   {assign var="news_output" value=$news_output+1}
  {else}
   <div class="col-md-2-4 col-lg-2-5">
	{if $item_type == 0 or $item_type == 1}
	 {include file="news-and-actions-items/action-large.tpl" item=$actions[$smarty.section.actions.index]}
	{else}
	 {section name="small_actions" loop=2}
	  {assign var="index" value=$smarty.section.actions.index+$smarty.section.small_actions.index}
	  {if $item_type == 5}{assign var="index" value=$index-1}{/if}
	  {include file="news-and-actions-items/action-small.tpl" item=$actions.$index}
	 {/section}
	{/if}
   </div>
  {/if}
 {/section}
 {if $news_output < $news|@count}
  {foreach name=news from=$news item="item"}
   {if $smarty.foreach.news.iteration > $news_output}
	<div class="col-lg-1-5 visible-lg">
	 {include file="news-and-actions-items/new.tpl" item=$item}
	</div>
   {/if}
  {/foreach}
 {/if}
</div>