<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 17.12.14
 */

/**
 * Класс для работы с индексом покрытия
 */
class programsCoverageIndex
{
    const TABLE_NAME = 'program_cover_index';

    /**
     * @var programsCoverage
     */
    private $coverage;

    /**
     * @var array элемент акции
     */
    private $action;

    /**
     * @param programsCoverage $coverage
     */
    public function __construct(programsCoverage $coverage)
    {
        $this->coverage = $coverage;
        $this->setActionId($coverage->getActionId());
    }

    /**
     * Устанавливает
     * @param int|null $actionId
     * @return $this
     * @throws Exception если акция не найдена
     */
    public function setActionId($actionId)
    {
        $this->action = $this->getAction($actionId);
        $this->coverage->setActionId($actionId);

        return $this;
    }

    /**
     * Возвращает массив всех товаров, которые попадают под акцию
     * @param int $itemsOnPage количество товаров на странице
     * @return array
     */
    public function getGoods($itemsOnPage = 999999)
    {
        $goodsModel = $this->coverage->getModel('goodsModel');
        $goodsModel->ignore_page_num(false);
        $itemIds = array_keys($this->getIndexData());
        if (empty($itemIds)) {
            return [];
        }
        
        return $goodsModel->index_by_field($goodsModel->get_all("`{$goodsModel->get_table_name()}`.`id` in (" . implode(', ', $itemIds) . ")", $itemsOnPage));
    }

    /**
     * Обновляет индекс акции
     * @return bool
     * @throws Exception в случае ошибки
     */
    public function updateIndex()
    {
        return $this->rebuildIndex() and $this->rebuildBonuses() and $this->rebuildPrices();
    }

    /**
     * Проверяет, нужно ли для акции перестраивать индекс акционных цен
     * @param array $action
     * @return bool
     */
    public function needRebuildPrices(array $action)
    {
        return $action['bonus_type'] == 5;
    }

    
    
    
    
    
    
    /**
     * Возвращает акцию
     * @param int $actionId
     * @returns array
     * @throws Exception в случае ошибки
     */
    private function getAction($actionId)
    {
        if (!is_null($actionId) and $actionId > 0) {
            $model = $this->coverage->getModel('model');
            $item = $model->get_item("`{$model->get_table_name()}`.`id`='" . (int)$actionId . "'");
        } else {
            $item = null;
        }

        if (empty($item)) {
            throw new Exception('program is not found');
        }

        return $item;
    }

    /**
     * Врзвращает индекс программы
     * @return array
     */
    private function getIndexData()
    {
        return a_model::index_by_field(mysql_data_assoc("select * from `" . self::TABLE_NAME . "` where `program`='{$this->action['id']}'"), 'good');
    }

    /**
     * Перестраиват индекс
     * @return bool
     * @throws Exception
     */
    private function rebuildIndex()
    {
        $action = $this->action;
        
        if ($action['type'] == 3 and !$action['bonuses_applied']) {
            $justForActualPrograms = false;
        } else {
            $justForActualPrograms = true;
        }

        $indexDataTableName = self::TABLE_NAME;
        $goods = $this->coverage->getAllGoods($justForActualPrograms);
        $indexData = $this->getIndexData();

        foreach ($goods as $good) {
            if (!array_key_exists($good['id'], $indexData)) {
                mysql_insert($indexDataTableName, "`program`='{$action['id']}', `good`='{$good['id']}'");
                if (mysql_error()) throw new Exception('unknown error');
            }
        }

        foreach ($indexData as $indexItem) {
            if (!array_key_exists($indexItem['good'], $goods)) {
                mysql_delete($indexDataTableName, "`id`='{$indexItem['id']}'");
                if (mysql_error()) throw new Exception('unknown error');
            }
        }

        return true;
    }

    /**
     * Проверяет, нужно ли для акции перестраивать индекс бонусов
     * @return bool
     */
    private function needRebuildBonuses()
    {
        return $this->action['bonus_type'] == 1;
    }

    /**
     * Перестраивает ииндекс бонусов
     * @return bool
     * @throws Exception в случае ошибки
     */
    private function rebuildBonuses()
    {
        if (!$this->needRebuildBonuses()) {
            return true;
        }

        $action = $this->action;
        $coverage = $this->coverage;
        $bonusCoverageTypes = [
            'brand' => a_model::index_by_field(mysql_data_assoc("select * from `" . ($coverage::BRANDS_DATA_TABLE) . "` where `program`='{$action['id']}'"), 'brand'),
            'external_parent' => a_model::index_by_field(mysql_data_assoc("select * from `" . ($coverage::CATEGORIES_DATA_TABLE) . "` where `program`='{$action['id']}'"), 'category'),
            'id' => a_model::index_by_field(mysql_data_assoc("select * from `" . ($coverage::GOODS_DATA_TABLE) . "` where `program`='{$action['id']}'"), 'good'),
        ];
        
        $indexData = $this->getIndexData();
        $goods = $this->getGoods();
        
        /** @var categories $categoriesModel */
        $categoriesModel = $this->coverage->getModel('categoriesModel');
        $categoriesStructure = $categoriesModel->get_structure();
        
        foreach ($goods as $good) {
            $bonus = $action['base_bonus'];
            
            foreach ($bonusCoverageTypes as $field => $data) {
                $key = $good[$field];
                $value = 0;
                
                if (array_key_exists($key, $data)) {
                    $value = $data[$key]['bonus'];
                } 
                
                if ($field == 'external_parent') {
                    $category = ['parent' => $good[$field]];
                    while ($category = $categoriesModel->find_item_in_structure_by_id($categoriesStructure, $category['parent'])) {
                        if (array_key_exists($category['id'], $data) and $v = $data[$category['id']]['bonus'] and $value < $v) {
                            $value = $v;
                        }
                    }
                }

                if ($bonus < $value) {
                    $bonus = $value;
                }
            }
            
            if ($bonus != $indexData[$good['id']]['bonus']) {
                mysql_update(self::TABLE_NAME, "`bonus`='$bonus'", "`program`='{$action['id']}' and `good`='{$good['id']}'");
                
                if (mysql_error()) {
                    throw new Exception('unknown error');
                }
            }
        }
        
        return true;
    }
    
    /**
     * Перестраивает индекс цен
     * @return bool
     */
    private function rebuildPrices()
    {
        if (!$this->needRebuildPrices($this->action)) {
            return true;
        }
        
        $priceUpdater = new programPricesUpdater($this->coverage);
        return $priceUpdater->update();
    }
}