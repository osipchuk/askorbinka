<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 23.12.14
 */

/**
 * Акционная цена товара
 */
class goodsActionPriceBonusStrategy extends aBonusStrategy {

    /**
     * @inheritdoc
     */
    public function apply()
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    public function disApply()
    {
        return parent::disApply();
    }
}