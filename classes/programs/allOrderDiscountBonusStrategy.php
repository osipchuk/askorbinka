<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 20.12.14
 */

/**
 * Применение скидки к всем товарам из корзины
 */
class allOrderDiscountBonusStrategy extends aBonusStrategy {

    /**
     * Применяет стратегию
     * @return bool флаг применения стратегии
     */
    public function apply()
    {
        $program = $this->getModifier()->getProgram();
        
        if ($program['discount'] <= 0) {
            return false;
        }
        
        $cart = $this->getModifier()->getCart();
        $items = $cart->get_goods();
        
        foreach ($items as $item) {
            $price = $item['price'] - ($item['price'] * $program['discount'] / 100);
            $cart->setItemPriceWithDiscount($item['good']['id'], $price);
        }
        
        return true;
    }
}