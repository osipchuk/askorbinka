<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 17.12.14
 */

/**
 * Класс для обновления акционных цен товаров
 */
class programPricesUpdater {

    /**
     * @var programsCoverage
     */
    private $coverage;
    
    /**
     * @param programsCoverage $coverage
     */
    public function __construct(programsCoverage $coverage)
    {
        $this->coverage = $coverage;
    }

    /**
     * Обновляет акционные цены товаров
     * @return bool
     * @throws Exception в случае ошибки
     */
    public function update()
    {
        $coverage = $this->coverage;
        $programsModel = $coverage->getModel('model');
        $goodsModel = $coverage->getModel('goodsModel');
        $goodsTable = $goodsModel->get_table_name();
        
        /** @var array $programs */
        $programs = $programsModel->getActual();
        $data = [];
        
        foreach ($programs as $line) {
            if ($coverage->getCoverageIndexModel()->needRebuildPrices($line)) {
                $coverage->getCoverageIndexModel()->setActionId($line['id']);
                $goods = $coverage->getAllGoodsFromIndex();
                foreach ($goods as $good) {
                    if (!isset($data[$good['id']])) {
                        $data[$good['id']] = [
                            'base_price' => (float) $good['price'],
                            'discount' => 0,
                        ];
                    }
                    
                    $data[$good['id']]['discount'] += $line['discount'];
                }
            }
        }
        
        mysql_update($goodsTable, "`action_price`='0'", "`action_price`<>'0' and `manual_action`='0'");
        if (mysql_error()) {
            throw new Exception('unknown error');
        }
        
        foreach ($data as $itemId => $item) {
            $actionPrice = $item['base_price'] - ($item['base_price'] * $item['discount'] / 100);
            
            mysql_update($goodsTable, "`action_price`='$actionPrice', `manual_action`='0'", "`id`='$itemId'");
            if (mysql_error()) {
                throw new Exception('unknown error');
            }
        }
        
        return true;
    }
}