<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 20.12.14
 */

/**
 * Бесплатный товар из каталога товаров
 */
class freeGoodBonusStrategy extends aBonusStrategy {

    /**
     * Применяет стратегию
     * @return bool флаг применения стратегии
     */
    public function apply()
    {
        $modifier = $this->getModifier();
        $good = $this->getFreeGood();
        if (empty($good)) {
            return false;
        }
        
        $cart = $modifier->getCart();
        if (!$cart->is_item_in_cart($good['id'])) {
            $cart->add_item($good['id'], 1, $modifier->getFreeGoodPrice());
        } else {
            $cart->setItemPriceWithDiscount($good['id'], $modifier->getFreeGoodPrice());
        }
        
        return true;
    }

    /**
     * @inheritdoc
     */
    public function disApply()
    {
        $good = $this->getFreeGood();
        if (empty($good)) {
            return false;
        }
        
        $modifier = $this->getModifier();
        $cart = $modifier->getCart();
        
        if ($cart->is_item_in_cart($good['id'])) {
            $cart->remove_item($good['id']);
        }
        
        return true;
    }
    
    

    /**
     * Возвращает акционный товар
     * @return array|null
     * @throws Exception
     */
    private function getFreeGood()
    {
        $modifier = $this->getModifier();
        $goodId = $modifier->getProgram()['free_good'];
        return $this->getModifier()->getGoodsModel()->get_item("`{$modifier->getGoodsModel()->get_table_name()}`.`id`='{$goodId}'");
    }
}