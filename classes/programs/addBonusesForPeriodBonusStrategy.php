<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 20.12.14
 */

/**
 * Начисление бонусов на бонусный счет за покупки за период
 */
class addBonusesForPeriodBonusStrategy extends aBonusStrategy {

    /**
     * Применяет стратегию
     * @return bool флаг применения стратегии
     */
    public function apply()
    {
        $modifier = $this->getModifier();
        $program = $modifier->getProgram();
        if ($program['bonuses_applied']) {
            return false;
        }
        
        mysql_query("START TRANSACTION");
        
        $bonusesModel = new client_bonuses();
        $clients = $this->getClientWithMoreAmountsForPeriod($program['date_begin'], $program['date_end'], $program['param_n']);
        $bonuses = $this->getModifier()->getProgramCoverage()->getAllGoodBonuses();
        
        foreach ($clients as $client) {
            $addBonuses = 0;
            $clientGoods = $this->getClientOrderedGoodsForPeriod($program['date_begin'], $program['date_end'], $client['client']);
            foreach ($clientGoods as $good) {
                if (array_key_exists($good['good'], $bonuses)) {
                    $bonus = $bonuses[$good['good']]['bonus'];
                    
                    $addBonuses += $good['price'] * $good['count'] / 100 * $bonus;
                }
            }
            
            if ($addBonuses > 0) {
                $bonusesModel->changeBalance($client['client'], $addBonuses, null, "Начисление бонусов по результатам программы лояльности #{$program['id']}");
            }
        }
        
        if ($this->getModifier()->makeUsed()) {
            mysql_query("COMMIT");
            return true;
        }
        
        return false;
    }


    /**
     * Возвращает клиентов, у оторых за заданный промежуток времени сумма успешных покупок больше чем @param $amount
     * @param string $dateBegin
     * @param string $dateEnd
     * @param int $amount
     * @return array
     */
    private function getClientWithMoreAmountsForPeriod($dateBegin, $dateEnd, $amount = 0)
    {
        return mysql_data_assoc("select sum(`order_goods`.`price` * `order_goods`.`count`) as `amount`, `orders`.`client`
                                 from `orders` join `order_goods` on `order_goods`.`order`=`orders`.`id`
                                 where " . $this->baseWhare($dateBegin, $dateEnd) . "
                                 group by `orders`.`client`
                                 having `amount` >= '" . (int) $amount . "'");
    }

    /**
     * Возвращает купленные пользователем товары за промежуток времени
     * @param string $dateBegin
     * @param string $dateEnd
     * @param int $client
     * @return array
     */
    private function getClientOrderedGoodsForPeriod($dateBegin, $dateEnd, $client)
    {
        return mysql_data_assoc("select `order_goods`.`price`, `order_goods`.`count`, `order_goods`.`good`
                                 from `orders` join `order_goods` on `order_goods`.`order`=`orders`.`id`
                                 where `orders`.`client`='" . (int) $client . "' and " . $this->baseWhare($dateBegin, $dateEnd));
    }

    /**
     * Возвращает базовую часть where
     * @param string $dateBegin
     * @param string $dateEnd
     * @return string
     */
    private function baseWhare($dateBegin, $dateEnd)
    {
        return "`orders`.`add_time` >= '" . safe($dateBegin) . "' and `orders`.`add_time` < '" . date("Y-m-d", strtotime($dateEnd) + 86400) . "' and
                `orders`.`status`='1'";
    }
}