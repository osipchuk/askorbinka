<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 20.12.14
 */

/**
 * Начисление бонусов на бонусный счет
 */
class addBonusesBonusStrategy extends aBonusStrategy {

    /**
     * Применяет стратегию
     * @return bool флаг применения стратегии
     */
    public function apply()
    {
        $modifier = $this->getModifier();
        $cart = $modifier->getCart();
        $orderInfo = $cart->getOrderInfo();
        
//        Бонусы начисляются только после оформления заказа и только авторизированным пользователям
        if (!$cart->getOrderId() or empty($orderInfo) or empty($orderInfo['client'])) {
            return false;
        }


        $programCoverage = $modifier->getProgramCoverage();
        $coverageGoods = $programCoverage->getAllGoodBonuses();
        $addBonuses = 0;
        
        foreach ($cart->get_goods() as $good) {
            $goodId = $good['good']['id'];
            if (array_key_exists($goodId, $coverageGoods)) {
                $bonusPercentages = $coverageGoods[$goodId]['bonus'];
                $bonus = $good['price'] / 100 * $bonusPercentages;
                $addBonuses += $bonus;
            }
        }
        
        if ($addBonuses <= 0) {
            return false;
        }
        
        $clientBonuses = new client_bonuses();
        try {
            $clientBonuses->changeBalance($orderInfo['client'], $addBonuses, $cart->getOrderId());
        } catch (Exception $e) {
            return false;
        }
        
        return true;
    }
}