<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 20.12.14
 */

/**
 * Абстрактная стратегия бонусов
 */
abstract class aBonusStrategy {

    /**
     * @var aProgramModifier
     */
    private $modifier;

    /**
     * Конструктор
     * @param aProgramModifier $modifier модификатор цены
     */
    public function __construct(aProgramModifier $modifier)
    {
        $this->setModifier($modifier);
    }

    /**
     * Возвращает модификатор цены
     * @return aProgramModifier
     * @throws Exception если модификатор не задан не задана
     */
    public function getModifier()
    {
        if (empty($this->modifier)) {
            throw new Exception('cat is empty');
        }
        return $this->modifier;
    }

    /**
     * Устанавливает модификатор цены
     * @param aProgramModifier $modifier
     * @return $this
     */
    public function setModifier(aProgramModifier $modifier)
    {
        $this->modifier = $modifier;

        return $this;
    }
    
    /**
     * Отменяет стратегию
     * @return bool
     */
    public function disApply()
    {
        return true;
    }
    
    /**
     * Применяет стратегию
     * @return bool флаг применения стратегии
     */
    abstract public function apply();
}