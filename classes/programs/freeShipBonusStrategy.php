<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 20.12.14
 */

/**
 * Бесплатная доставка
 */
class freeShipBonusStrategy extends aBonusStrategy {

    /**
     * Применяет стратегию
     * @return bool флаг применения стратегии
     */
    public function apply()
    {
        $this->getModifier()->getCart()->setFreeShip(true);
        return true;
    }
}