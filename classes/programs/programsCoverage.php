<?php
/**
 * @package default
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 15.12.14
 */

/**
 * Класс, отвечающий за покрытие акциями товаров
 */
class programsCoverage {
	
	const BRANDS_DATA_TABLE = 'program_cover_brands';
	
	const GOODS_DATA_TABLE = 'program_cover_goods';
	
	const CATEGORIES_DATA_TABLE = 'program_cover_categories';

	/**
	 * @var int id акции
	 */
	private $actionId;

	/**
	 * @var a_model
	 */
	private $model;

	/**
	 * @var a_model
	 */
	private $goodsModel;

	/**
	 * @var a_model
	 */
	private $categoriesModel;

	/**
	 * @var a_model
	 */
	private $brandsModel;

	/**
	 * @var programsCoverageIndex
	 */
	private $coverageIndex;

	/**
	 * @param int $actionId
	 */
	function __construct($actionId)
	{
		$this->model = new programs();
		$this->model->ignore_page_num(true);
		
		$this->goodsModel = new goods();
		$this->goodsModel->ignore_page_num(true);
		
		$this->categoriesModel = new categories();
		$this->categoriesModel->ignore_page_num(true);
		
		$this->brandsModel = new brands();
		$this->brandsModel->ignore_page_num(true);
		
		$this->setActionId($actionId);
		
		$this->coverageIndex = new programsCoverageIndex($this);
	}

	/**
	 * Возвращает массив программ, под действие которых попадает товар
	 * @param int $goodId
	 * @return array
	 */
	public static function getGoodActions($goodId)
	{
		return mysql_array("select `program` from `" . programsCoverageIndex::TABLE_NAME . "` where `good`='" . (int) $goodId . "'");
	}

	/**
	 * Устанавливает id акции
	 * @param int $actionId
	 * @return $this
	 */
	public function setActionId($actionId)
	{
		$this->actionId = (int) $actionId;

		return $this;
	}

	/**
	 * Возвращает id акции
	 * @return int
	 */
	public function getActionId()
	{
		return $this->actionId;
	}

	/**
	 * Возвращает все товары, попадающие под программу
	 * @param bool $justForActualPrograms флаг вывода товаров только если программа актуальна по времени
	 * @return array
	 * @throws Exception если программа не найдена
	 */
	public function getAllGoods($justForActualPrograms = true)
	{
		$program = $this->model->get_item("`{$this->model->get_table_name()}`.`id`='{$this->getActionId()}'");
		if (empty($program)) {
			throw new Exception('program not found');
		}
		
		if ($justForActualPrograms and !$this->model->isProgramActual($program)) {
			return [];
		}
		
		if ($program['all_goods']) {
			return $this->goodsModel->get_all();
		}
		
		$whereParts = [
			'goods' => null,
			'brands' => null,
			'categories' => null,
		];
		
		foreach ($whereParts as $type => $value) {
			$method = 'get' . ucfirst($type);
			$whereParts[$type] = array_keys($this->$method());
		}
		
		if (!empty($whereParts['categories'])) {
			$allCategories = [];
			foreach ($whereParts['categories'] as $category) {
				$subcats = $this->categoriesModel->get_all_subcategories_by_id_with_current_id($category, true);
				$allCategories = array_merge($allCategories, $subcats);
			}
			$whereParts['categories'] = $allCategories;
		}


		$table = $this->goodsModel->get_table_name();
		if (!empty($whereParts['goods'])) {
			$where = "`$table`.`id` in (" . implode(', ', $whereParts['goods']) . ")";
		} else {
			$where = '';
		}
		
		if (!empty($whereParts['categories']) or !empty($whereParts['brands'])) {
			if (!empty($where)) $where = "($where) or ";
			$where .= "(";

			if (!empty($whereParts['brands'])) {
				$where .= "`$table`.`brand` in (" . implode(', ', $whereParts['brands']) . ")";
			}
			
			if (!empty($whereParts['categories'])) {
				if (!empty($whereParts['brands'])) $where .= ' and ';
				$implodedCategories = implode(', ', $whereParts['categories']);
				$where .= "(`$table`.`external_parent` in (" . $implodedCategories . ") or
				            `$table`.`id` in (select `good` from `good_append_categories` where `category` in (" . $implodedCategories . ")))";
			}
			
			$where .= ")";
		}
		
		if (empty($where)) {
			return [];
		}
		
		return $this->goodsModel->index_by_field($this->goodsModel->get_all($where));
	}

	/**
	 * Возвращает ИЗ ИНДЕКСА все товары, попадающие под программу
	 * @param int $itemsOnPage количество товаров на странице
	 * @return array
	 * @throws Exception если программа не найдена
	 */
	public function getAllGoodsFromIndex($itemsOnPage = 999999)
	{
		return $this->coverageIndex->getGoods($itemsOnPage);
	}

	/**
	 * Возвращает бренды, которые попадают под действие акции
	 * @return array
	 */
	public function getBrands()
	{
		$table = $this->brandsModel->get_table_name();
		return $this->goodsModel->index_by_field($this->brandsModel->get_all("`$table`.`id` in (select `brand` from `" . self::BRANDS_DATA_TABLE . "` where `program`='{$this->getActionId()}')"));
	}

	/**
	 * Добавляет покрытие программы брендом
	 * @param int $id
	 * @return bool
	 */
	public function addBrandCoverage($id)
	{
		$id = mysql_insert(self::BRANDS_DATA_TABLE, "`brand`='" . (int) $id . "', `program`='{$this->getActionId()}'");
		if (empty($id)) {
			return false;
		}

		return true;
	}

	/**
	 * Удаляет покрытие программы брендом
	 * @param int $id
	 * @return bool
	 */
	public function removeBrandCoverage($id)
	{
		mysql_delete(self::BRANDS_DATA_TABLE, "`brand`='" . (int) $id . "' and `program`='{$this->getActionId()}'");
		return mysql_affected_rows() != 0;
	}

	/**
	 * Обновляет количество бонусов
	 * @param int $id
	 * @param int $bonus
	 * @return bool
	 */
	public function updateBrandCoverage($id, $bonus)
	{
		mysql_update(self::BRANDS_DATA_TABLE, "`bonus`='" . (int) $bonus . "'", "`brand`='" . (int) $id . "' and `program`='{$this->getActionId()}'");
		return mysql_error() == '';
	}
	


	/**
	 * Возвращает товары, которые попадают под действие акции
	 * @return array
	 */
	public function getGoods()
	{
		$table = $this->goodsModel->get_table_name();
		return $this->goodsModel->index_by_field($this->goodsModel->get_all("`$table`.`id` in (select `good` from `" . self::GOODS_DATA_TABLE . "` where `program`='{$this->getActionId()}')"));
	}

	/**
	 * Возвращает из индекса массив всех товаров, которые попадают под акцию + размер начисляемых бонусов (в %)
	 * @return array
	 */
	public function getAllGoodBonuses()
	{
		$indexModel = $this->getCoverageIndexModel();
		$result = a_model::index_by_field(mysql_data_assoc($query = "select * from `" . $indexModel::TABLE_NAME . "` where `program`='{$this->getActionId()}'"), 'good');
		
		return $result;
	}

	/**
	 * Добавляет покрытие программы товаром
	 * @param int $id
	 * @return bool
	 */
	public function addGoodCoverage($id)
	{
		$id = mysql_insert(self::GOODS_DATA_TABLE, "`good`='" . (int) $id . "', `program`='{$this->getActionId()}'");
		if (empty($id)) {
			return false;
		}

		return true;
	}

	/**
	 * Удаляет покрытие программы товаром
	 * @param int $id
	 * @return bool
	 */
	public function removeGoodCoverage($id)
	{
		mysql_delete(self::GOODS_DATA_TABLE, "`good`='" . (int) $id . "' and `program`='{$this->getActionId()}'");
		return mysql_affected_rows() != 0;
	}

	/**
	 * Обновляет количество бонусов
	 * @param int $id
	 * @param int $bonus
	 * @return bool
	 */
	public function updateGoodCoverage($id, $bonus)
	{
		mysql_update(self::GOODS_DATA_TABLE, "`bonus`='" . (int) $bonus . "'", "`good`='" . (int) $id . "' and `program`='{$this->getActionId()}'");
		return mysql_error() == '';
	}




	/**
	 * Возвращает категории, которые попадают под действие акции
	 * @return array
	 */
	public function getCategories()
	{
		$table = $this->categoriesModel->get_table_name();
		return $this->goodsModel->index_by_field($this->categoriesModel->get_all("`$table`.`id` in (select `category` from `" . self::CATEGORIES_DATA_TABLE . "` where `program`='{$this->getActionId()}')"));
	}

	/**
	 * Добавляет покрытие программы категорией
	 * @param int $id
	 * @return bool
	 */
	public function addCategoryCoverage($id)
	{
		$id = mysql_insert(self::CATEGORIES_DATA_TABLE, "`category`='" . (int) $id . "', `program`='{$this->getActionId()}'");
		if (empty($id)) {
			return false;
		}

		return true;
	}

	/**
	 * Удаляет покрытие программы категорией
	 * @param int $id
	 * @return bool
	 */
	public function removeCategoryCoverage($id)
	{
		mysql_delete(self::CATEGORIES_DATA_TABLE, "`category`='" . (int) $id . "' and `program`='{$this->getActionId()}'");
		return mysql_affected_rows() != 0;
	}

	/**
	 * Обновляет количество бонусов
	 * @param int $id
	 * @param int $bonus
	 * @return bool
	 */
	public function updateCategoryCoverage($id, $bonus)
	{
		mysql_update(self::CATEGORIES_DATA_TABLE, "`bonus`='" . (int) $bonus . "'", "`category`='" . (int) $id . "' and `program`='{$this->getActionId()}'");
		return mysql_error() == '';
	}

	/**
	 * Возвращает запрашиваемую модель
	 * @param string $model
	 * @return a_model
	 * @throws Exception если модель не найдена
	 */
	public function getModel($model)
	{
		if (property_exists($this, $model) and $this->$model instanceof a_model) {
			return $this->$model;
		}

		throw new Exception('model ' . $model . ' not found');
	}

	/**
	 * Обновление индекса программы
	 * @param int $actionId
	 * @return bool
	 * @throws Exception в случае ошибки
	 */
	public static function updateIndex($actionId)
	{
		$coverage = new programsCoverage($actionId);
		return $coverage->coverageIndex->updateIndex();
	}

	/**
	 * ОБновляет индекс всех програм
	 * @return bool
	 */
	public static function updateAllProgramsIndex()
	{
		$result = true;
		$data = (new programs())->get_all();
		foreach ($data as $line) {
			$result = self::updateIndex($line['id']) and $result;
		}
		
		return $result;
	}

	/**
	 * Возвращает модель для работы с индексом
	 * @return programsCoverageIndex
	 */
	public function getCoverageIndexModel()
	{
		return $this->coverageIndex;
	}

}