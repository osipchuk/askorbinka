<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 12.05.14
 * @version 1.1 добавленв документация более удобный доступ к свойствам
 */

/**
 * Класс для управления ключевыми совами из БД
 */
class translatesResolver {
 /**
  * @var string языковая версия
  */
 protected $lang;

 /**
  * @var array дамп данных
  */
 protected $words;

 /**
  * @var applicationHelper
  */
 protected  $appHelper;

 /**
  * @var translatesResolver
  */
 private static $instance;
 
 protected function __construct() {
  $this->appHelper = applicationHelper::getInstance();
  $this->lang = $this->appHelper->getLang();
  
  $data = mysql_data("select `mode`, `word_{$this->lang}` from `translation`");
  foreach ($data as $line) $this->words[$line[0]] = $line[1];
 }
 
 /**
  * Доступ к свойству
  * @param string $key свойство
  * @param null|string $default_value значение по умолчанию. Если значение в БД не задано -записывается
  * @param bool $log флаг записи запроса в лог
  * @return string
  */
 public function _($key, $default_value = null, $log = true) {
  if (!array_key_exists($key, $this->words) and !is_null($default_value))
  {
   $this->words[$key] = $default_value;
   mysql_insert('translation', "`mode`='".safe($key)."', `word_{$this->lang}`='".safe($default_value)."'");
  }
  
  return $this->get_value_by_key($key);
 }

 /**
  * Возвращает значение по ключу, созданному на основе всех параметров.
  * @return string
  */
 public function _w()
 {
  return $this->get_value_by_key(implode(func_get_args()));
 }

 /**
  * Singleton
  * @return translatesResolver
  */
 public static function getInstance() {
  if (empty(self::$instance)) {
   self::$instance = new translatesResolver();
  }
  return self::$instance;
 }

 /**
  * Доступ к значению напрямую через свойство
  * @param $name
  * @return string
  */
 public function __get($name)
 {
  return $this->_($name);
 }

 /**
  * Доступ к значению напрямю через метод
  * @param $name
  * @param $arguments
  * @return string
  */
 public function __call($name, $arguments)
 {
  return $this->_($name, array_key_exists(0, $arguments) ? $arguments[0] : NULL);
 }


 /**
  * Возвращает значение элемента по ключу
  * @param string $key
  * @return string
  */
 protected function get_value_by_key($key)
 {
  if (array_key_exists($key, $this->words)) return $this->words[$key];
  return $key;
 }
} 