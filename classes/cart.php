<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 13.06.14
 */

/**
 * Корзина товаров
 * Добавлен функционал выбора размера и цвета
 */
class cart extends a_cart {

 public function __construct()
 {
  parent::__construct();
 }
}