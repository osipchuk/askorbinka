<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 24.04.14
 */

/**
 * Модель древовидной структуры
 * 
 * Базовая часть
 */
abstract class a_tree_model extends a_model {

 /**
  * @var null|int external parent
  */
 protected $external_parent = -1;

 public function __construct()
 {
  parent::__construct();
  $this->ignore_page_num(TRUE);
 }

 /**
  * @var array массив эллементов
  */
 static protected $structures = array();

 /**
  * Устанавливает external parent
  * @param int|null $external_parent
  * @return $this
  */
 public function set_external_parent($external_parent)
 {
  $this->external_parent = $external_parent;

  $key = get_class($this);
  if (array_key_exists($key, static::$structures)) unset(static::$structures[$key]);

  return $this;
 }

 

 /**
  * Возвращает массив эллементов
  * @return array
  */
 public function get_structure()
 {
  $key = get_class($this);
  if (array_key_exists($key, self::$structures)) return self::$structures[$key];
  
  return static::$structures[$key] = $this->generate_structure_level();
 }

 /**
  * Формирует древовидную структуру для элемента с id @param $item_id
  * @param int $item_id
  * @return null|array
  */
 public function get_structure_item($item_id)
 {
  return $this->find_item_in_structure_by_id($this->get_structure(), $item_id);
 }

 /**
  * Поиск элемента по id в древовиной структуре
  * @param array $structure
  * @param int $item_id
  * @return null|array
  */
 public function find_item_in_structure_by_id($structure, $item_id)
 {
  return $this->find_item_in_structure_by_field($structure, $item_id, 'id');
 }

 /**
  * Поиск элемента по static
  * @param array $structure
  * @param string $item_static
  * @return null|array
  */
 public function find_item_in_structure_by_static($structure, $item_static)
 {
  return $this->find_item_in_structure_by_field($structure, $item_static, 'static', FALSE);
 }

 /**
  * Поиск элемента по значению заданого поля
  * @param array $structure 
  * @param mixed $value
  * @param string $field поле, в котором нужно искать
  * @param bool $include_sub_nav включая подменю
  * @return null|array
  */
 public function find_item_in_structure_by_field($structure, $value, $field, $include_sub_nav = TRUE)
 {
  if (is_array($structure))
  {
   foreach ($structure as $structure_item)
   {
    if ($structure_item[$field] == $value) return $structure_item;
    if ($include_sub_nav and array_key_exists('sub', $structure_item))
    {
     $result = $this->find_item_in_structure_by_field($structure_item['sub'], $value, $field, $include_sub_nav);
     if (!is_null($result)) return $result;
    }
   }
  }

  return NULL;
 }

 /**
  * Находит родительскую категорию первого уровня для переданного раздела 
  * @param int $item_id
  * @return null|array
  * @throws Exception если раздел не найден
  */
 public function get_parent_item($item_id)
 {
  $result = NULL;
  
  $item = array('parent'=>$item_id);
  while ($item = $this->find_item_in_structure_by_id($this->get_structure(), $item['parent'])) $result = $item;
  
  if (is_array($result)) return $result['id'];
  throw new Exception('part not found');
 }
 
 
 /**
  * Генерирует уровень древовидной структуры для предка @param $parent
  * @param null|int $parent
  * @param int $level
  * @param string $item_url
  * @return bool
  */
 protected function generate_structure_level($parent = NULL, $level = 0, $item_url = '')
 {
  $where = "`parent` ".(is_null($parent) ? "is null" : "='".(int)$parent."'");
  if ($this->external_parent >= 0) $where .= " and `external_parent` ".(is_null($this->external_parent) ? "is null" : "='".(int)$this->external_parent."'");
   
  $data = $this->get_all($where, 999999, FALSE);
  
  foreach ($data as $index => $line)
  {
   if ($this->site_structure_model) $base_url = ''; else $base_url = $this->app_helper->get_url_maker()->d_id_url($line['external_parent']);
   
   $data[$index]['level'] = $level;
   $data[$index]['url'] = $base_url.$item_url.'/'.$line['static'];
   $sub = $this->generate_structure_level($line['id'], $level + 1, $item_url.'/'.$line['static']);
   if (!empty($sub)) $data[$index]['sub'] = $sub;
  }
  
  return $data;
 }
}