<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 19.11.14
 */

/**
 * Распечатка кода
 */
class discount_code_send_print_form extends discount_code_send_email_form {

	/**
	 * Отправляет текст на заданный список почтовых ящиков.
	 * Заглушка
	 * @return bool Возвращает TRUE, если почта отправилась на все почтовые ящики, иначе - FALSE.
	 */
	protected function mail()
	{
		return true;
	}

	/**
	 * Метод для выполнения определенных задач после отправки формы на email
	 * @return bool
	 */
	protected function after_mail()
	{
        $discountCode = $this->getDiscountCodeId();
        if (empty($discountCode)) {
            return false;
        }
        
		$this->set_field('card_url', url($this->app_helper->get_url_maker()->d_module_url('discount_card') . '/' . $this->get_field('code') . '?print=1'));
		return true;
	}
}