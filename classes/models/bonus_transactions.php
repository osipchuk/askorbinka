<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 25.11.14
 */

/**
 * Модель заявок на вывод бонусов
 */
class bonus_transactions extends a_simple_model {

	/**
	 * @inheritdoc
	 */
	protected function get_order()
	{
		return "`{$this->get_table_name()}`.`id` desc";
	}

	/**
	 * @inheritdoc
	 */
	protected function joins()
	{
		$clientsModel = (new clients())->ignore_page_num(true);
		$this->join_model($clientsModel, ["concat(`name`, ' ', `surname`) as `fio`"], 'external_parent', 'id' );
		return $this;
	}
}