<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 16.05.14
 */

class delivery_emails extends a_simple_model {

 /**
  * Подтверждает email
  * @param int $id
  * @param int $submited
  * @return int количество обновленных записей
  */
 public function submit($id, $submited = 1)
 {
  mysql_update($this->get_table_name(), "`submited`='".(int)$submited."'", "`id`='".(int)$id."'");
  return mysql_affected_rows();
 }

 /**
  * Проверяет, зарегистрирован ли переданный email в БД
  * @param string $email
  * @return bool
  */
 public function check_email_exist($email)
 {
  $item = $this->get_item("`email`='".safe($email)."'");
  return !empty($item);
 }

 /**
  * Добавляет email в БД
  * @param string $email
  * @param array $categories массив категорий
  * @return FALSE|int
  */
 public function add_email($email, $categories, $fio = '', $client = null)
 {
  if (empty($email) or empty($categories) or !is_array($categories)) return FALSE;
  
  $id = mysql_insert('delivery_emails', "`email`='".safe($email)."', `fio`='".safe($fio)."'".(!empty($client) ? ", `client`='".(int)$client."'" : ''));
  if (empty($id)) return FALSE;

  foreach ($categories as $category)
  {
   mysql_insert('delivery_categories_email', "`email`='$id', `category`='".(int)$category."'");
   if (mysql_error()) return FALSE;
  }
  
  return $id;
 }

 /**
  * Параметр сортировки
  * @return string
  */
 protected function get_order()
 {
  return "`{$this->get_table_name()}`.`id` desc";
 }
}