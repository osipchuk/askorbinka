<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 21.10.14
 */

/**
 * Модель для использования с модулем фильтра
 */
abstract class a_filter_model extends a_model {
 
// TODO: Додати метод filterGet() для отримання і забрати переприсвоєнння стандартних методів отримання
 
 /**
  * @var a_model|null
  */
 private $category_model;
	
	/** @var a_model|null */
	private $brandsModel;

 /**
  * Возвращает массив элементов для фильров
  * @param string $lang
  * @param int|null $parent
  * @param string|null $where
  * @param int $items_on_page
  * @return array
  */
 abstract public function filterGet($lang, $parent = null, $where = null, $items_on_page = 999999);
 
 /**
  * Возвращает where условие для выбора всех элементов категории
  * @param int $category
  * @return null|string
  */
 public function category_where($category)
 {
  return '';
 }

 /**
  * Возвращает массив категорий
  * @return array
  */
 public function getCategories()
 {
  return [];
 }

 /**
  * Возвращает название выбранной категории
  * @param int $category
  * @return string
  */
 public function getCategoryCaption($category)
 {
  $model = $this->getCategoryModel();
  
  if (is_null($model)) return '';
  $item = $model->get_item("`{$model->get_table_name()}`.`id`='".(int)$category."'");
  if (empty($item)) return '';
  
  return $item['caption'];
 }

 /**
  * Возвращает модель категорий
  * @return a_model|null
  */
 public function getCategoryModel()
 {
  return $this->category_model;
 }

 /**
  * Устанавливает модель категорий
  * @param a_model|null $category_model
  * @return $this
  */
 public function setCategoryModel($category_model)
 {
  $this->category_model = $category_model;
  if (is_object($category_model)) $category_model->ignore_page_num(true);

  return $this;
 }

	/**
	 * Возвращает where условие для выбора всех элементов бренда
	 * @param int $brand
	 * @return string
	 */
	public function brandWhere($brand)
	{
		return '';
	}

	/**
	 * Возвращает массив брендов
	 * @return array
	 */
	public function getBrands()
	{
		return [];
	}

	/**
	 * Возвращает название выбранного бренда
	 * @param int $brand
	 * @return string
	 */
	public function getBrandCaption($brand)
	{
		$model = $this->getBrandsModel();

		if (is_null($model)) return '';
		$item = $model->get_item("`{$model->get_table_name()}`.`id`='".(int)$brand."'");
		if (empty($item)) return '';

		return $item['caption'];
	}

	/**
	 * Возвращает модель брендов
	 * @return a_model|null
	 */
	public function getBrandsModel()
	{
		return $this->brandsModel;
	}

	/**
	 * Устанавливает модель брендов
     * @param a_model|null $brandModel
	 * @return $this
	 */
	public function setBrandsModel($brandModel)
	{
		$this->brandsModel = $brandModel;
		if (is_object($brandModel)) $brandModel->ignore_page_num(true);

		return $this;
	}
 
 
}