<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 24.07.14
 */

/**
 * Модель отзывов товаров
 */
class good_reviews extends a_filter_model {

 /**
  * Возвращает количество неподтвержденных отзывов
  * @return int
  */
 public function getCountOfNew()
 {
  $result = mysql_line_assoc($query = "select count(`id`) as `count` from `{$this->get_table_name()}` where " . $this->mix_where($this->base_where(), "`submited`='0'"));
  if (empty($result)) return 0;
  
  return $result['count'];
 }

 /**
  * Возвращает массив элементов для фильров
  * @param string $lang
  * @param int|null $parent
  * @param string|null $where
  * @param int $items_on_page
  * @return array
  */
 public function filterGet($lang, $parent = null, $where = null, $items_on_page = 999999)
 {
  $where = $this->mix_where($this->mix_where($this->base_where(), $where), "`{$this->get_table_name()}`.`submited`='1'");
  return $this->get_all_of_external_parent($parent, $where, $items_on_page);
 }
 
 public function get_all($where = null, $items_on_page = 999999, $make_items_url = TRUE)
 {
  $where = $this->base_where().(!empty($where) ? " and ($where)" : '');
  return parent::get_all($where, $items_on_page, $make_items_url);
 }

 /**
  * Базовая часть where
  * @return string
  */
 protected function base_where()
 {
  return "`{$this->get_table_name()}`.`external_parent` is not null";
 }


 /**
  * Массив полей, зависящих от языковой версии
  * @return array
  */
 protected function get_mysql_assoc_select()
 {
  return array();
 }

 /**
  * Параметр присутствия у модели мета-тегов
  * @return bool
  */
 protected function meta_tags_exist()
 {
  return FALSE;
 }

 /**
  * Возвращает название модели-предка
  * @return string|null
  */
 public function external_model()
 {
  return 'goods';
 }

 /**
  * Параметр сортировки
  * @return string
  */
 protected function get_order()
 {
  return "`{$this->get_table_name()}`.`date` desc";
 }

 /**
  * Служебная функция для создания связи между моделями
  * @return $this
  */
 protected function joins()
 {
  $goods_class = $this->external_model();
  
  $this->join_model(new clients(), array("concat_ws(' ', `name`, `surname`) as `author_fio`", "`image` as `author_image`"), 'client', 'id', 'left');
  $this->join_model(new $goods_class(), array("`caption_{$this->app_helper->getLang()}` as `good_caption`"), 'external_parent', 'id', 'left');
  
  return $this;
 }
}