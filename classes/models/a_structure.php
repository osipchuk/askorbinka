<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 25.04.14
 */

/**
 * Модель структуры сайта
 * 
 * Базовая часть
 */
abstract class a_structure extends a_tree_model {

 /**
  * @var bool флаг, который означает, что модель отвечает за структуру сайта
  */
 protected $site_structure_model = TRUE;

 /**
  * @var array главное меню
  */
 protected $main_nav;

 /**
  * Возвращает стартовую страницу
  * @return array|null
  */
 public function get_start_page()
 {
  return $this->get_item("`start`='1'");
 }

 /**
  * Возвращает элемент структуры по модулю
  * @param array $structure
  * @param string $module
  * @return array|null
  */
 public function find_item_in_structure_by_module($structure, $module)
 {
  return $this->find_item_in_structure_by_field($structure, $module, 'module');
 }

 /**
  * Возвращает структуру главного меню
  * @return array
  */
 public function get_nav()
 {
  if (empty($this->main_nav))
  {
   $this->main_nav = array();
   foreach ($this->get_structure() as $line) if (!$line['hidden']) $this->main_nav[] = $this->remove_hidden_departs($line);
  }
  
  return $this->main_nav;
 }

 /**
  * Возвращает элемент меню с удаленными скрытыми элементами из подменю sub
  * @param array $item
  * @return array
  */
 protected function remove_hidden_departs($item)
 {
  $result = $item;
  
  if (array_key_exists('sub', $item) and is_array($item['sub']))
  {
   $result['sub'] = array();
   foreach ($item['sub'] as $sub_item) if (!$sub_item['hidden']) $result['sub'][] = $this->remove_hidden_departs($sub_item);
   
   if (empty($result['sub'])) unset($result['sub']);
  }
  
  return $result;
 }

 /**
  * Возвращает название модели-предка
  * @return string|null
  */
 public function external_model()
 {
  return null;
 }
}