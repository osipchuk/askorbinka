<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 01.02.14
 * @version 1.1 - с хранением шаблонов в БД
 */

abstract class a_form_model {
 /**
  * @var array массив email'ов для отправки формы
  */
 protected $emails = array();

 /**
  * @var array значения полей
  */
 protected $fields = array();

 /**
  * @var applicationHelper
  */
 protected $app_helper;
 
 /**
  * @var translatesResolver
  */
 protected $words;

 /**
  * @var array запись из БД с текстовым шаблоном
  */
 protected $main_template;

 
 public function __construct()
 {
  $this->app_helper = applicationHelper::getInstance();
  $this->words = $this->app_helper->getTranslateResolver();
  $this->main_template = $this->get_template($this->main_template_mode());
 }

 /**
  * Отправка формы
  * @return bool
  * @throws Exception в случае возникновения ошибки валидации
  */
 public function send()
 {
  if ($this->validate() and $this->before_mail() and $result = $this->mail()) return $this->after_mail(); else return FALSE;
 }

 /**
  * Устанавливает значение поля
  * @param string $field
  * @param string|array $value
  * @param bool $filter_tags
  * @return $this
  */
 public function set_field($field, $value, $filter_tags = TRUE)
 {
  if ($filter_tags)
  {
   if (is_array($value)) foreach ($value as $key => $item) $value[$key] = strip_tags($item); else $value = strip_tags($value);
  }
  $this->fields[$field] = $value;
  
  return $this;
 }

 /**
  * Установка значений полей из ассоциативного массива
  * @param array|string $values_array ассоциативный массив, или jquery serialize-строка вида var1=val1&var2=val2&...
  * @return $this
  */
 public function set_fields($values_array)
 {
  if (!is_array($values_array)) parse_str($values_array, $values_array);
  
  foreach ($values_array as $field => $value)
  {
   if (array_key_exists($field, $this->structure()))
   {
    $this->set_field($field, $value);
   }
  }
    
  return $this;
 }

 /**
  * Возвращает значение поля
  * @param $field
  * @return string|null
  */
 public function get_field($field)
 {
  if (array_key_exists($field, $this->fields))
  {
   $structure = $this->structure();
   if (array_key_exists($field, $structure) and in_array('nl2br', $structure[$field])) return nl2br($this->fields[$field]).'!!'; else return $this->fields[$field];
  }
  else return null;
 }

 /**
  * Возвращает безопасное для БД значение поля
  * @param $field
  * @return null|string
  */
 public function get_safe_field($field)
 {
  $result = $this->get_field($field);
  if (is_null($result)) return $result;
  return safe($result);
 }

 /**
  * Добавляет email для отправки
  * @param string $email
  * @return bool
  */
 public function add_send_email($email)
 {
  if (!empty($email) and filter_var($email, FILTER_VALIDATE_EMAIL) and !in_array($email, $this->emails)) $this->emails[] = $email; else return FALSE;
  return TRUE;
 }


 
 
 /**
  * Возвращает шаблон текста с подставленными значениями
  * @return string
  */
 protected function text()
 {
  return $this->replace($this->main_template['text']);
 }

 /**
  * Возвращает шаблон текста с подставленными значениями
  * @return string
  */
 protected function subject()
 {
  return $this->replace($this->main_template['subject']);
 }

 /**
  * Заменяет в шаблоне переменные значениями
  * @param $text
  * @return string
  */
 protected function replace($text)
 {
  $text = preg_replace("/(src|href)=([\"|\'])\/admin\/([{]+)/", "$1=$2$3", $text);
  
  if (preg_match_all("/{[\$]([a-zA-Z0-9_-]+)}/", $text, $variables))
  {
   foreach ($variables[1] as $var)
   {
    if (array_key_exists($var, $this->fields)) {
		$value = $this->get_field($var);
		$structure = $this->structure();
		if (isset( $structure[$var]) and in_array('bool', $structure[$var])) {
			$value = $this->words->_w('bool_captions_', (int)$value);
		}
		
		if (empty($value)) $value = '-';
		
		$text = str_replace('{$'.$var.'}', $value, $text);
	}
    elseif (array_key_exists($var, $this->base_variables())) $text = str_replace('{$'.$var.'}', $this->base_variables($var), $text);
   }
  }
  
  $base = $this->app_helper->get_global('base');
  $text = preg_replace("/(src|href)=([\"|\'])(?!http:\/\/)(?!https:\/\/)[\/]?([^\"\']+)([\"|\'])/", "$1=$2{$base}$3$4", $text);
  
  return $text;
 }

 /**
  * Метод для выполнения определенных задач до отправки формы на email
  * @return bool
  */
 protected function before_mail()
 {
  return TRUE;
 }

 /**
  * Метод для выполнения определенных задач после отправки формы на email
  * @return bool
  */
 protected function after_mail()
 {
  return TRUE;
 }

 /**
  * Возвращает массив email'ов для отправки.
  * 
  * Если не было добавлено адресов, автоматически возвращается адрес администратора сайта.
  * 
  * @return array
  */
 protected function get_emails()
 {
  if (empty($this->emails))
  {
   $app_helper = applicationHelper::getInstance();
   return array($app_helper->get_global('global_email'));
  }
  else return $this->emails;
 }

 /**
  * Проверяет форму на правильность заполнения
  * @return bool
  * @throws Exception в случае возникновения ошибки валидации
  */
 protected function validate()
 {
  $this->check_captcha();
  
  foreach ($this->structure() as $field => $rules)
  {
   if (in_array('required', $rules) and empty($this->fields[$field])) throw new Exception($this->words->_('you_must_fill_all_the_fields').' - '.$field);
  }
  
  return TRUE;
 }

 /**
  * Отправляет текст на заданный список почтовых ящиков. 
  * @return bool Возвращает TRUE, если почта отправилась на все почтовые ящики, иначе - FALSE.
  */
 protected function mail()
 {
  $result = TRUE;
  foreach ($this->get_emails() as $email)
  {
   $r = send_mail($email, $this->subject(), $this->text());
   if (!$r) $result = $r;
  }
  
  return $result;
 }

 /**
  * Получает шаблон письма из БД
  * @param string $mode ключ шаблона
  * @return array
  * @throws Exception в случае отсутствия шаблона
  */
 protected function get_template($mode)
 {
  $result = mysql_line_assoc("select `caption_{$this->app_helper->getLang()}` as `subject`, `text_{$this->app_helper->getLang()}` as `text`
                              from `mail_templates` where `mode`='".safe($mode)."'");
  if (empty($result)) throw new Exception('template "'.$mode.'" not found');
  return $result;
 }

 /**
  * Массив базовых переменных
  * @param null|string $var ключ переменной, если нужно получить значение одной переменной
  * @return array|string|null возвращает список переменных, или значение конкретной переменнй
  */
 protected function base_variables($var = NULL)
 {
  $variables = array(
   'date'=>normal_date(),
   'time'=>date("H:i:s")
  );
  
  if (is_null($var)) return $variables;
  if (array_key_exists($var, $variables)) return $variables[$var];
  return NULL;
 }

 /**
  * Проверяет код капчи
  * @return TRUE в случае правильного кода
  * @throws Exception в случае неправильного кода
  */
 protected function check_captcha()
 {
  if (is_null($this->captcha_mode())) return TRUE;
  
  $captcha = new captcha($this->captcha_mode());
  if ($captcha->get_code() == $this->get_field('captcha')) return TRUE;
  
  throw new Exception($this->words->captcha_error('Неправильный защитный код'));
 }
 
 /**
  * Структура формы (список полей)
  * @return array
  */
 abstract protected function structure();

 /**
  * Возвращает ключ записи для выборки щаблона ыз БД
  * @return string
  */
 abstract protected function main_template_mode();

 /**
  * Возвращает mode капчи
  * 
  * Если проверка капчи не требуется, функция должна возвращать NULL
  * 
  * @return string|null
  */
 abstract protected function captcha_mode();
}