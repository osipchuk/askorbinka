<?php
/**
 * @package default
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 29.10.14
 */

/**
 * Класс текстовых страниц с описанием простых типов оплаты
 */
class payment_texts extends a_simple_model {

 /**
  * @inheritdoc
  */
 protected function get_mysql_assoc_select()
 {
  return array('caption', 'text');
 }

 /**
  * @inheritdoc
  */
 protected function get_order()
 {
  return "`id`";
 }

}