<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 24.11.14
 */

class client_bonuses extends a_simple_model {

	/**
	 * @var clients
	 */
	protected $clientsModel;

	/**
	 * @inheritdoc
	 */
	public function __construct()
	{
		$this->clientsModel = (new clients())->ignore_page_num(true);
		parent::__construct();
	}

	/**
	 * Изменяет баланс пользователя
	 * @param int $clientId
	 * @param float $value
	 * @param null|int $orderId
	 * @param string $comment
	 * @return bool
	 * @throws Exception в случае ошибки
	 */
	public function changeBalance($clientId, $value, $orderId = null, $comment = '')
	{
		$tableName = $this->clientsModel->get_table_name();
		$clientId = (int) $clientId;
		$value = (float) $value;
		
		if ($value == 0) return true;
		
		$client = $this->clientsModel->get_item("`$tableName`.`id`='$clientId'");
		if (empty($client)) throw new Exception('client ID' . $clientId . ' not found');
		
		$bonuses = $client['bonuses'] + $value;
		if ($bonuses < 0) throw new Exception('Negative bonus!');
		
		mysql_update($tableName, "`bonuses`='$bonuses'", "`id`='$clientId'");
		if (mysql_error()) throw new Exception('unknown error');
		
		mysql_insert(
			$this->get_table_name(), 
			"`external_parent`='$clientId', `value`='$value', `ballance_after`='$bonuses', `comment`='".safe($comment)."'" . ($orderId !== null ? ", `order_id` = '$orderId'" : '')
		);
		
		if (mysql_error()) throw new Exception('unknown error' + mysql_error()); 
		
		return true;
	}

	/**
	 * Возвращает название модели-предка
	 * @return string|null
	 */
	public function external_model()
	{
		return 'clients';
	}
	
	
	
	

	/**
	 * @inheritdoc
	 */
	protected function get_order()
	{
		return "`{$this->get_table_name()}`.`id` desc";
	}
}