<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 12.12.14
 */

/**
 * Модель программы лояльности
 */
class programs extends a_model {

	/**
	 * @var translatesResolver
	 */
	protected $translationResolver;

	/**
	 * @var goods
	 */
	protected $goodsModel;
	
	/**
	 * @var array типы програм
	 */
	protected $types = [
		1 => 'Одноразовая покупка на n грн',
		'Сумма всех покупок больше n грн',
		'Сумма покупок за промежуток времени',
		'В корзине не менее чем X штук акционных товаров',
		'Начисление бонусов за каждый из акционных товаров',
	];

	/**
	 * @var array типы бонусов
	 */
	protected $bonusTypes = [
		1 => 'Начисление бонусов',
		2 => 'Скидка (% от всего заказа)',
		5 => 'Скидка (% на акционные товары)',
		3 => 'Подарок из каталога товаров',
		4 => 'Бесплатная доставка',
	];

	public function __construct()
	{
		$this->translationResolver = applicationHelper::getTranslateResolver();
		$this->goodsModel = new goods();
		$this->goodsModel->ignore_page_num(true);
		parent::__construct();
	}


	/**
	 * Возвращает типы программ
	 * @param null|string $value возвращаеть значение для переданного ключа
	 * @return array|string|null
	 */
	public function getTypes($value = null)
	{
		if (is_null($value)) {
			return $this->types;
		}
		
		if (array_key_exists($value, $this->types)) {
			return $this->types[$value];
		};
		
		return null;
	}

	/**
	 * Возвращает типы акций, которые могут распространяться только на все товары
	 * @return array
	 */
	public function allGoodsTypes()
	{
		return [1, 2, 3];
	}

	/**
	 * Возвращает типы бонусов
	 * @param null|string $value возвращаеть значение для переданного ключа 
	 * @return array|string|null
	 */
	public function getBonusTypes($value = null)
	{
		if (is_null($value)) {
			return $this->bonusTypes;
		}

		if (array_key_exists($value, $this->bonusTypes)) {
			return $this->bonusTypes[$value];
		};

		return null;
	}
	
	/**
	 * Возвращает актуальные акции
	 * @param int $itemsOnPage
	 * @param string $where дополнительное словия выбора
	 * @return array
	 */
	public function getActual($itemsOnPage = 999999, $where = '')
	{
		$tableName = $this->get_table_name();
		return $this->get_all($this->mix_where("`$tableName`.`date_begin` <= '" . date("Y-m-d") . "' and `date_end` >= '" . date("Y-m-d") . "'", $where), $itemsOnPage);
	}
	
	/**
	 * Проверяе, является ли программа актуальной
	 * @param array $program
	 * @return bool
	 */
	public static function isProgramActual(array $program)
	{
		return (time() >= strtotime($program['date_begin']) and time() <= strtotime($program['date_end']) + 86400);
	}

	/**
	 * @inheritdoc
	 */
	public function get_order()
	{
		return "`rate` desc, `date_begin` desc, `date_end` desc";
	}

	/**
	 * Возвращает модель товаров
	 * @return goods
	 */
	public function getGoodsModel()
	{
		return $this->goodsModel;
	}

	/**
	 * Возвращает все товары акции
	 * @param int $actionId
	 * @param int $itemsOnPage количество товаров на странице
	 * @return array
	 */
	public function getActionGoods($actionId, $itemsOnPage = 999999)
	{
		$programsCoverage = new programsCoverage($actionId);
		return $programsCoverage->getAllGoodsFromIndex($itemsOnPage);
	}

	/**
	 * Возвращает все акции товара
	 * @param int $goodId
	 * @param bool $actualOnly
	 * @param string $appendWhere
	 * @param int $itemsOnPage
	 * @return array
	 */
	public function getGoodActions($goodId, $actualOnly = true, $appendWhere = '', $itemsOnPage = 999999)
	{
		$programsIds = programsCoverage::getGoodActions($goodId);
		if (empty($programsIds)) {
			return [];
		}
		
		$where = $this->mix_where("`{$this->get_table_name()}`.`id` in (" . implode(', ', $programsIds) . ")", $appendWhere);
		
		if ($actualOnly) {
			return $this->getActual($itemsOnPage, $where);
		}
		
		return $this->get_all($where, $itemsOnPage);
	}
	
	

	/**
	 * @inheritdoc
	 */
	protected function get_mysql_assoc_select()
	{
		return array_merge(parent::get_mysql_assoc_select(), ['preview']);
	}


}