<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 22.05.14
 */

/**
 * Простая модель
 * Без языковых переменных и мета-тегов
 */
abstract class a_simple_model extends a_model {
 
 /**
  * Массив полей, зависящих от языковой версии
  * @return array
  */
 protected function get_mysql_assoc_select()
 {
  return array();
 }

 /**
  * Параметр присутствия у модели мета-тегов
  * @return bool
  */
 protected function meta_tags_exist()
 {
  return FALSE;
 }

 /**
  * Возвращает название модели-предка
  * @return string|null
  */
 public function external_model()
 {
  return null;
 }
}