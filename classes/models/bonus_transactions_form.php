<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 25.11.14
 */

/**
 * Форма заявки на вывод бонусов
 */
class bonus_transactions_form extends a_form_model {

	/**
	 * @var clients
	 */
	protected $clientsModel;

	public function __construct()
	{
		$this->clientsModel = new clients();
		parent::__construct();
	}


	/**
	 * @inheritdoc
	 */
	protected function before_mail()
	{
		$account = (new auth())->set_accounts_model($this->clientsModel)->get_account_info();
		if (empty($account)) {
			throw new Exception($this->words->you_should_auth);
		}
		if ($account['type'] != 'pr') {
			throw new Exception($this->words->for_pro_only);
		}
		
		$this->set_field('account', $account);
		$this->set_field('fio', $this->clientsModel->account_fio($account));
		
		$this->saveCard($this->get_field('card'), $account);
		$this->checkAmount($account);
		
		return $this->saveToDB();
	}


	/**
	 * Структура формы (список полей)
	 * @return array
	 */
	protected function structure()
	{
		return [
			'amount' => ['required'],
			'card' => ['required'],
			'account' => [],
			'admin_link' => [],
		];
	}

	/**
	 * Возвращает ключ записи для выборки щаблона ыз БД
	 * @return string
	 */
	protected function main_template_mode()
	{
		return 'bonus_transaction_request';
	}

	/**
	 * Возвращает mode капчи
	 *
	 * Если проверка капчи не требуется, функция должна возвращать NULL
	 *
	 * @return string|null
	 */
	protected function captcha_mode()
	{
		return null;
	}


	/**
	 * Проверяет шаблоном номер карты и записывает ее в талицу пользователя, в случае успеха
	 * @param string $card
	 * @param array $account
	 * @return bool
	 * @throws Exception в случае ошибки
	 */
	private function saveCard($card, array $account)
	{
		if (!$this->clientsModel->checkCreditCard($card)) {
			throw new Exception($this->words->bonuses_transaction_request_send_error_wrong_card('Пожалуйста, проверьте правильность ввода Вашей карты'));
		}
		
		$this->set_field('card', $card);
		
		$tableName = $this->clientsModel->get_table_name();
		mysql_update($tableName, "`$tableName`.`credit_card`='$card'", "`$tableName`.`id`='{$account['id']}'");
		if (mysql_error()) {
			throw new Exception($this->words->unknown_error);
		}
		
		return true;
	}

	/**
	 * Проверяет размер введенных бонусов
	 * @param array $account
	 * @return bool
	 * @throws Exception
	 */
	private function checkAmount(array $account)
	{
		$this->set_field('amount', (float) $this->get_field('amount'));
		
		if ($this->get_field('amount') <= 0) {
			throw new Exception($this->words->bonuses_transaction_request_send_error_amount_error('Вы ввели неверную сумму бонусов'));
		}

		if ($this->get_field('amount') > $account['bonuses']) {
			throw new Exception($this->words->bonuses_transaction_request_send_error_amount_bonuses_not_enought('У Вас недостаточно бонусов'));
		}

		return true;
	}

	/**
	 * Сохраняет заявку в БД
	 * @return bool
	 * @throws Exception
	 */
	private function saveToDB()
	{
		$model = new bonus_transactions();
		$amount = $this->get_field('amount');
		$account = $this->get_field('account');
		
		$id = mysql_insert($model->get_table_name(), "`external_parent`='{$account['id']}', `amount`='$amount', `card`='{$this->get_field('card')}'");
		if (empty($id)) {
			throw new Exception($this->words->unkown_error);
		}

		$this->set_field('admin_link', $this->app_helper->get_global('base').'admin/admin.php?mode=bonus_transactions&creat_mode=edit&id='.$id);
		
		return true;
	}
}