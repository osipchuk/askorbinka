<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 10.12.14
 */

/**
 * Модель заказов
 */
class orders extends a_simple_model {

	/**
	 * Параметр сортировки
	 * @return string
	 */
	protected function get_order()
	{
		return "`{$this->get_table_name()}`.`id` desc";
	}
}