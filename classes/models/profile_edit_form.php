<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 07.11.14
 */

/**
 * Форма редактирования профайла
 */
class profile_edit_form extends profile_form {

	/**
	 * @var bool флаг необходимости отправки письма пользователю
	 */
	protected $needToSendMail = false;

	/**
	 * Возвращает флаг отправки письма подтверждения
	 * @return boolean
	 */
	public function getMailSended()
	{
		return $this->needToSendMail;
	}
	
	

	/**
	 * Проверка email, совпадания паролей
	 * @return bool
	 * @throws Exception в случае ошибки
	 */
	protected function before_mail()
	{
		if ($this->get_field('password') != $this->get_field('password_repeat')) throw new Exception($this->words->_('profile_register_passwords_not_match', 'Пароли не совпадают'));
		
		$id = $this->updateDb();
		$this->add_send_email($this->get_field('email'));
		
		return $this->format_link($id);
	}

	/**
	 * Структура формы (список полей)
	 * @return array
	 */
	protected function structure()
	{
		$result = parent::structure();
		$result['password'] = [];
		$result['password_repeat'] = [];
		
		return $result;
	}

	/**
	 * Возвращает ключ записи для выборки щаблона ыз БД
	 * @return string
	 */
	protected function main_template_mode()
	{
		return 'profile_edit_email';
	}

	/**
	 * Возвращает аккаунт авторизированного пользователя
	 * @return array
	 * @throws Exception
	 */
	private function getAccount()
	{
		$auth = new auth();
		$auth->set_accounts_model($this->get_accounts_model());
		$account = $auth->get_account_info();
		if (!$account) throw new Exception($this->words->you_should_auth);
		return $account;
	}


	/**
	 * Обновляет запись в БД
	 * @return int id аккаунта
	 * @throws Exception
	 */
	protected function updateDb() 
	{
		$account = $this->getAccount();
		$set = "`id`=`id`";
		
		$password = $this->get_field('password');
		if (!empty($password)) $set .= ", `pass`='" . md5($password) . "'";
		$db_fields = ['name', 'surname', 'tel', 'post', 'job', 'city'];
		foreach ($db_fields as $field) $set .= ", `{$field}`='{$this->get_safe_field($field)}'";

		$email = $this->get_field('email');
		
		if (!empty($email) and $email != $account['email']) {
			$this->check_email_exist();
			$set .= ", `email`='".safe($email)."', `submited`='0'";
			$this->needToSendMail = true;
			$this->format_link($account['id']);
		}
		
		$deleteAvatar = $this->get_field('delete_image');
		$newAvatar = $this->get_field('image');
			
		if ($deleteAvatar) {
			$this->deleteAvatar($account);
			$set .= ", `image`=''";
		} elseif ($newAvatar) {
			$this->deleteAvatar($account);
			$image = copy_image('admin/cache/' . $newAvatar, 'admin/project_images');
			
			$set .= ", `image`='" . safe($image) . "'";
		}

		mysql_update($this->get_accounts_model()->get_table_name(), $set, "`id`='{$account['id']}'");
		if (mysql_error()) throw new Exception($this->words->_('unknown_error') . ' ' . mysql_error());

		return $account['id'];
	}

	/**
	 * Отправляет текст на заданный список почтовых ящиков.
	 * @return bool Возвращает TRUE, если почта отправилась на все почтовые ящики, иначе - FALSE.
	 */
	protected function mail()
	{
		if (!$this->needToSendMail) return true;
		
		return parent::mail();
	}

	/**
	 * Удаляет файл аватара
	 * @param array $account
	 * @return bool
	 */
	private function deleteAvatar(array $account)
	{
		if (!empty($account['image'])) {
			$file = 'admin/project_images/' . $account['image'];
			if (file_exists($file)) {
				unlink($file);
				return true;
			}
		}
		
		return false;
	}
}