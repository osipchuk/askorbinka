<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 17.10.14
 */

/**
 * Модель новинок и акций
 */
class news_and_actions extends a_filter_model {

 /**
  * @var null|a_model
  */
 private $goodsModel;

 /**
  * Возвращает элемент переданной языковой версии
  * @param string $lang
  * @param int|null $parent
  * @param string|null $where
  * @return array|null
  */
 public function get_lang_item($lang, $parent = null, $where = null)
 {
  return $this->get_item($where);
 }

 /**
  * Возвращает массив элементов для фильров
  * @param string $lang
  * @param int|null $parent
  * @param string|null $where
  * @param int $items_on_page
  * @return array
  */
 public function filterGet($lang, $parent = null, $where = null, $items_on_page = 999999)
 {
  $result = array(
   'actions'=>array(),
   'news'=>array()
  );
  
  $date = date("Y-m-d");
  $data = $this->get_all($this->mix_where($where, "`date_begin`<='$date' and `date_end`>='$date'"), $items_on_page);
  foreach ($data as $line)
  {
   if ($line['action']) $result['actions'][] = $line;
   if ($line['new']) $result['news'][] = $line;
  }
  
  return $result;
 }

 /**
  * Возвращает массив категорий
  * @return array
  */
 public function getCategories()
 {
  /** @var a_tree_model $categoryModel */
  $categoryModel = $this->getCategoryModel();
  $structure = $categoryModel->get_structure();
  
  $categories = $categoryModel->get_all("`{$categoryModel->get_table_name()}`.`external_parent` is not null and
                                         `{$categoryModel->get_table_name()}`.`id` in (select `external_parent` from `{$this->get_table_name()}`)");
  
  foreach ($categories as $index => $category)
  {
   $item = $category;
   $caption = $category['caption'];
   
   while ($item = $categoryModel->find_item_in_structure_by_id($structure, $item['parent']))
   {
	$caption = $item['caption'].' - '.$caption;
   }
   $categories[$index]['caption'] = $caption;
  }
	  
  return $categories;
 }
 
 /**
  * Возвращает название модели-предка
  * @return string|null
  */
 public function external_model()
 {
  return 'categories';
 }


 public function get_order()
 {
  return "`weight` desc, `date_begin` desc, `date_end` desc";
 }

 /**
  * Возвращает where условие для выбора всех элементов категории
  * @param int $category
  * @return null|string
  */
 public function category_where($category)
 {
  return "`{$this->get_table_name()}`.`external_parent`='".(int)$category."'";
 }

 /**
  * Возращает товары акции
  * @param int $action_id
  * @return array
  */
 public function getGoods($action_id)
 {
  $model = $this->getGoodsModel();
  return $this->goodsModel->get_all("`{$model->get_table_name()}`.`id` in (select `good` from `news_and_actions_goods` where `external_parent`='".(int)$action_id."')");
 }

 /**
  * Возвращает модель товаров
  * @return goods
  */
 public function getGoodsModel()
 {
  if (empty($this->goodsModel))
  {
   $this->goodsModel = new goods();
   $this->goodsModel->ignore_page_num(true);
  }
  
  return $this->goodsModel;
 }

 
 /**
  * @inheritdoc
  */
 protected function make_item_url(&$item)
 {
  if (!is_array($item)) return FALSE;
  if (empty($item['url'])) $item['url'] = $this->app_helper->get_url_maker()->d_module_url('news_and_actions').'/'.$item['static'];
  return TRUE;
 }
 
 /**
  * Массив полей, зависящих от языковой версии
  * @return array
  */
 protected function get_mysql_assoc_select()
 {
  return array_merge(parent::get_mysql_assoc_select(), array('label_text', 'preview'));
 }
}