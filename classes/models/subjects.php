<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 26.08.14
 */

/**
 * Модель новостных тем
 */
class subjects extends a_simple_model {

 /**
  * Возвращает список тем с привязанными новостями
  * @param null|int $external_parent если null - все разделы новостей
  * @return array
  */
 public function get_all_with_news($external_parent = NULL)
 {
  return $this->get_all($where = "`id` in (select `subject` from `news_subjects` join `news` on `news_subjects`.`new`=`news`.`id`".
	  (!empty($external_parent) ? " where `news`.`external_parent`='".(int)$external_parent."'" : '').")");
 }
 

 /**
  * Массив полей, зависящих от языковой версии
  * @return array
  */
 protected function get_mysql_assoc_select()
 {
  return array('caption');
 }
}