<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 20.10.14
 */

/**
 * Модель видео брендов
 */
class brand_videos extends videos {

 /**
  * Выборка всех элементов с предком @param $parent
  * @param null $parent
  * @param null $where
  * @param int $items_on_page
  * @param bool $make_items_url
  * @return array
  */
 public function get_all_of_external_parent($parent = null, $where = null, $items_on_page = 999999, $make_items_url = TRUE)
 {
  return $this->get_all($this->mix_where($where, $parent != -1 ? "`brand`='".(int)$parent."'" : "`brand` is not null"), $items_on_page, $make_items_url);
 }

 /**
  * Возвращает название модели-предка
  * @return string|null
  */
 public function external_model()
 {
  return 'brands';
 }

 /**
  * Возвращает ключ для связи с родительским элементом
  * @return string|null
  */
 public function external_parent_key()
 {
  return 'brand';
 }

	/**
	 * Возвращает массив элементов для фильров
	 * @param string $lang
	 * @param int|null $parent
	 * @param string|null $where
	 * @param int $items_on_page
	 * @return array
	 */
	public function filterGet($lang, $parent = null, $where = null, $items_on_page = 999999)
	{
		return $this->get_all_of_external_parent(-1, $where, $items_on_page);
	}
	
	/**
	 * Возвращает массив категорий
	 * @return array
	 */
	public function getCategories()
	{
		return $this->getCategoryModel()->get_all();
	}

	/**
	 * @inheritdoc
	 */
	public function getCategoryModel()
	{
		$className = $this->external_model();
		return new $className;
	}


	/**
	 * @inheritdoc
	 */
	public function category_where($category)
	{
		return "`brand`='".(int)$category."'";
	}
}