<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 03.02.14
 */

/**
 * Форма добавления отзыва
 */
class reviews_form extends a_form_model {

 /**
  * Отправка формы
  * @return bool
  * @throws Exception в случае возникновения ошибки валидации
  */
 public function send()
 {
  return $this->set_account_id() and parent::send();
 }

 protected function before_mail()
 {
  return $this->insert_db();
 }

 /**
  * Добавляет отзыв в БД
  * @return bool
  * @throws Exception
  */
 protected function insert_db()
 {
  $good_id = (int)$this->get_field('good');
  
  if (!empty($good_id))
  {
   $goods_model = new goods();
   $good = $goods_model->get_item("`{$goods_model->get_table_name()}`.`id`='$good_id'");
   if (empty($good) or $good['disable_reviews']) throw new Exception($this->words->_('reviews_admin_disable_reviews_for_this_good', 'Администратор отключил отзывы для этого товара'));
  }
  
  $set = ($good_id ? "`external_parent`='$good_id', " : '').
         "`client`='{$this->get_field('client_id')}', `text`='{$this->get_safe_field('text')}', `rating`='".(int)$this->get_field('rating')."'";
  
  $id = mysql_insert('good_reviews', $set);
  if (empty($id)) throw new Exception($this->words->_('unknown_error'));

  $this->set_field('admin_link', $this->app_helper->get_global('base').'admin/admin.php?mode='.(!empty($good_id) ? 'good_' : '').'reviews&creat_mode=edit&id='.$id);
  return TRUE;
 }

 /**
  * Структура формы (список полей)
  * @return array
  */
 protected function structure()
 {
  return array(
      'good'=>array(),
      'rating'=>array(),
      'text'=>array('required','nl2br'),
      'edit_url'=>array(),
      'account'=>array('required'),
      'client_id'=>array('required')
  );
 }

 /**
  * Возвращает ключ записи для выборки щаблона ыз БД
  * @return string
  */
 protected function main_template_mode()
 {
  return 'review_new';
 }

 /**
  * Возвращает mode капчи
  *
  * Если проверка капчи не требуется, функция должна возвращать NULL
  *
  * @return string|null
  */
 protected function captcha_mode()
 {
  return null;
 }

 /**
  * Устанавливает id клиента
  * @throws Exception в случае ошибки
  * @return bool
  */
 public function set_account_id()
 {
  $account = $this->get_field('account');
  if (!empty($account) and is_array($account))
  {
   $clients_model = new clients();
   
   $this->set_field('client_id', $account['id']);
   $this->set_field('fio', $clients_model->account_fio($account));
   return true;
  }

  throw new Exception($this->words->_('you_should_auth'));
 }
 
}