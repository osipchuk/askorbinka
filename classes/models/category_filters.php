<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 23.01.15
 */

/**
 * Модель фильтров категорий
 */
class category_filters extends a_model {

    /**
     * Возвращает элементы фильтра
     * @param int $filterId
     * @param array $inGoods только элементы, которые относятся к товарам
     * @return array
     */
    public function getFilterItems($filterId, array $inGoods = [])
    {
        $query = "select `category_filter_values`.* " . mysql_assoc_select(['caption'], $this->app_helper->getLang(), 'category_filter_values') . "
                  from `category_filter_values` left join `good_filters` on `good_filters`.`value`=`category_filter_values`.`id`
                  where `category_filter_values`.`external_parent`='" . (int) $filterId . "'".
                        (!empty($inGoods) ? " and `good_filters`.`external_parent` in (" . implode(', ', $inGoods) . ")" : '') ."
                  group by `category_filter_values`.`id`
                  order by `category_filter_values`.`rate` desc";
        
        return mysql_data_assoc($query);
    }

    /**
     * Возвращает элементы фильтра с отмеченными значениями для товара
     * @param int $filterId
     * @param int $goodId
     * @return array
     */
    public function getFilterItemsWithGoodValues($filterId, $goodId)
    {
        $checkedValues = [];
        $data = mysql_data_assoc("select * from `good_filters` where `external_parent`='" . (int) $goodId . "'");
        foreach ($data as $value) {
            $checkedValues[] = $value['value'];
        }
        
        $data = $this->getFilterItems($filterId);
        foreach ($data as $index => $line) {
            $data[$index]['value'] = in_array($line['id'], $checkedValues);
        }
        
        return $data;
    }
    
    /**
     * @inheritdoc
     */
    public function external_model()
    {
        return 'categories';
    }

    /**
     * Массив полей, зависящих от языковой версии
     * @return array
     */
    protected function get_mysql_assoc_select()
    {
        return ['caption'];
    }

    /**
     * Параметр присутствия у модели мета-тегов
     * @return bool
     */
    protected function meta_tags_exist()
    {
        return FALSE;
    }

}