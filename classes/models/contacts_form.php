<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 01.02.14
 */

class contacts_form extends a_form_model {

 /**
  * Структура формы (список полей)
  * @return array
  */
 protected function structure()
 {
  return array(
      'fio'=>array('required'),
      'text'=>array('required','nl2br'),
      'email'=>array('required'),
      'captcha'=>array('required')
  );
 }

 /**
  * Возвращает ключ записи для выборки щаблона ыз БД
  * @return string
  */
 protected function main_template_mode()
 {
  return 'contacts';
 }

 /**
  * Возвращает mode капчи
  *
  * Если проверка капчи не требуется, функция должна возвращать NULL
  *
  * @return string|null
  */
 protected function captcha_mode()
 {
  return 'contacts';
 }
}