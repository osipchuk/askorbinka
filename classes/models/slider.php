<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 19.02.14
 */

/**
 * Модель слайдера изображений
 */
class slider extends a_model {

 /**
  * Возвращает записи с изображениями
  * @param string|null $lang языковая версия
  * @return array
  */
 public function get_with_images($lang = NULL)
 {
  return $this->get_all("`image`<>'' and `image` is not null".(!is_null($lang) ? " and `langs` like '%;{$lang};%'" : ''), 999999, false);
 }


 protected function get_mysql_assoc_select()
 {
  return array('caption', 'text');
 }

 protected function meta_tags_exist()
 {
  return FALSE;
 }
}