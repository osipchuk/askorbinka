<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 21.05.14
 */

/**
 * Модель товаров
 */
class goods extends a_model {

 public function get_all($where = null, $items_on_page = 999999, $make_items_url = TRUE, $with_hidden = false)
 {
  if (!$with_hidden)
  {
   $where = $this->mix_where($where, $this->mix_where("`{$this->get_table_name()}`.`hidden`='0'", "`categories`.`hidden`='0'"));
  }

  return parent::get_all($where, $items_on_page, $make_items_url);
 }

 /**
  * Выборка всех элементов с предком @param $parent
  * @param null $parent
  * @param null $where
  * @param int $items_on_page
  * @param bool $make_items_url
  * @param bool $with_hidden делать выборку вместе скрытыми элементами
  * @return array
  */
 public function get_all_of_external_parent($parent = null, $where = null, $items_on_page = 999999, $make_items_url = TRUE, $with_hidden = false)
 {
  return $this->get_all($this->make_external_where($parent, $where), $items_on_page, $make_items_url, $with_hidden);
 }

 /**
  * Возвращает цену товара
  * @param array $good
  * @param bool $withCodeDiscount учитывать скидку по коду?
  * @return float
  */
 public function price($good, $withCodeDiscount = false)
 {
  if (is_array($good) and array_key_exists('price', $good))
  {
	  if ($withCodeDiscount and isset($good['code_discount']) and $good['code_discount'] > 0) {
		  $price = floatval($good['price']);
		  return $price - ($price / 100 * floatval($good['code_discount']));
	  } else {
		  if (array_key_exists('action_price', $good) and floatval($good['action_price']) > 0) return floatval($good['action_price']); else return floatval($good['price']);
	  }
  }

  return 0;
 }

 /**
  * Возвращает все параметры для категории товара с отмеченными значениями
  * @param array|int $good
  * @return array
  * @throws Exception
  */
 public function get_params($good)
 {
  $categories_class = $this->external_model();
  /** @var categories $categories_model */
  $categories_model = new $categories_class();
  
  if (!is_array($good)) $good = $this->get_item("`{$this->get_table_name()}`.`id`='".(int)$good."'");
  if (!isset($good['id']) or ((int)($good['id'])) <= 0) throw new Exception('good error');
  
  $param_values = [];
  $data = mysql_data_assoc("select * from `good_params` where `external_parent`='{$good['id']}'");
  foreach ($data as $line) $param_values[$line['param']] = $line;

  $result = $categories_model->get_category_params($good['external_parent']);
  foreach ($result as &$item)
  {
   if (array_key_exists($item['id'], $param_values)) $item['value'] = $param_values[$item['id']]['value_'.$item['type']];
  }
  
  return $result;
 }

	/**
	 * Возвращает массив товаров с переданной технологией
     * @param int $tech_id
	 * @param int|null $category
	 * @param int|null $brand
	 * @return array
	 */
	public function getAllWithTech($tech_id, $category = null, $brand = null)
	{
		$query = "select" . $this->select() . "
		          where `{$this->get_table_name()}`.`id` in (select `external_parent` from `good_params` where `value_image`='".(int)$tech_id."')";
		if (!empty($category)) {
			$category = (int)$category;
			$query .= " and (`{$this->get_table_name()}`.`external_parent`='$category' or `categories`.`parent`='$category')";
		}
		if (!empty($brand)) $query .= " and `{$this->get_table_name()}`.`brand`='" . (int)$brand . "'";
		
		$result = mysql_data_assoc($query);
        $this->make_items_url($result);
     
        return $result;
	}

	public function search($searchQuery)
	{
		$searchQuery = safe($searchQuery);
		$tabsModel = new good_tabs();
		
		$tableName = $this->get_table_name();
		$tabsTableName = $tabsModel->get_table_name();
		$lang = $this->app_helper->getLang();
		
		$query = "select distinct" . $this->select() . "
		          left join `$tabsTableName` on `$tabsTableName`.`external_parent`=`$tableName`.`id`
			      where `$tableName`.`hidden`='0' and `categories`.`hidden`='0' and
			            `$tableName`.`external_parent` is not null and ( 
			            `$tableName`.`caption_{$lang}` like '%{$searchQuery}%' or `$tableName`.`category_{$lang}` like '%{$searchQuery}%' or
			            `$tabsTableName`.`caption_{$lang}` like '%{$searchQuery}%' or `$tabsTableName`.`text_{$lang}` like '%{$searchQuery}%'
			           )";
		
		$data = mysql_data_assoc($query);
		$this->make_items_url($data);
		
		return $data;
	}

 /**
  * Возвращает бестселлеры
  * @return array
  */
 public function get_bestsellers()
 {
  return $this->get_all("`best_sell`='1' and `{$this->get_table_name()}`.`external_parent` is not null");
 }

 /**
  * Возвращает аналоги товаров
  * @param int $good_id
  * @return array
  */
 public function get_analogs($good_id)
 {
  return $this->get_all("`{$this->get_table_name()}`.`id` in (select `analog` from `good_analogs` where `good`='".(int)$good_id."')");
 }

 /**
  * Возвращает главное фото для товара @param $item_id
  * @param int $item_id
  * @return array
  */
 public function get_main_photo($item_id)
 {
  $data = $this->get_photos($item_id);
  if (empty($data)) return [];
  return reset($data);
 }

 public function get_videos($good_id)
 {
  $videos_model = new videos();
  $videos_model->ignore_page_num(true);
     $good_id = (int) $good_id;
     
     $item = $this->get_item("`{$this->get_table_name()}`.`id`='$good_id'");
     if (empty($item)) return [];
     
     if (!empty($item['video_review']))
     {
         $where = "`id`='{$item['video_review']}'";
     }
     else
     {
         $where = "`good`='$good_id'";
     }
  
  return mysql_data_assoc("select * ".mysql_assoc_select($videos_model->get_mysql_assoc_select())."
                           from `{$videos_model->get_table_name()}` where $where order by `good_rate` desc, `rate` desc");
 }

 /**
  * Возвращает скидку в процентах для товара
  * @param array $good_item
  * @return int
  */
 public function getDiscountInPercentages(array $good_item)
 {
  if (!isset($good_item['price']) or !isset($good_item['action_price']) or $good_item['price'] <= 0 or $good_item['action_price'] <= 0) return 0;

  $diff = $good_item['price'] - $good_item['action_price'];
  return round(100 * $diff / $good_item['price']);
 }

 /**
  * Выделяет из полного названия товара модель (откидая бренд)
  * @param array $good_item
  * @return string
  */
 public function goodModel(array $good_item)
 {
  if (!isset($good_item['caption'])) return '';
  if (isset($good_item['brand_caption']))
  {
   $good_item['caption'] = trim(preg_replace("/^" . preg_quote($good_item['brand_caption']) . "/", '', $good_item['caption']));
  }

  return $good_item['caption'];
 }


 /**
  * Возвращает название модели-предка
  * @return string
  */
 public function external_model()
 {
  return 'categories';
 }

 /**
  * Возвращает название таблицы со списком дополнительных категорий товара
  * @return string
  */
 public function append_categories_table_name()
 {
  return 'good_append_categories';
 }

 /**
  * Возвращает массив id дополнительных категорий товара
  * @param int $good_id
  * @return array
  */
 public function get_good_append_categories($good_id)
 {
  $good_id = (int) $good_id;
  if ($good_id <= 0) return [];
  
  return mysql_array("select `category` from `{$this->append_categories_table_name()}` where `good`='" . (int) $good_id . "'");
 }


 /**
  * Флаг необходимости добавлять в select выборку главного project_images изображения
  * @return bool
  */
 protected function select_main_project_image()
 {
  return TRUE;
 }

 protected function get_mysql_assoc_select()
 {
  return ['caption','category','title','description','keywords','ship','pay','back','warranty'];
 }

 /**
  * Параметр сортировки
  * @return string
  */
 protected function get_order()
 {
  return "`{$this->get_table_name()}`.`avail` desc, `{$this->get_table_name()}`.`rate` desc";
 }


 /**
  * Служебная функция для создания связи между моделями
  * @return $this
  */
 protected function joins()
 {
  $this->join_model(new brands(), ["`caption_{$this->app_helper->getLang()}` as `brand_caption`", "`image` as `brand_image`"], 'brand', 'id', 'left');
  $this->join_model(new categories(), [], 'external_parent', 'id', 'left');
  return $this;
 }

}