<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 17.10.14
 */

/**
 * Модель видео
 */
class videos extends a_filter_model {

 /**
  * Выборка всех элементов с предком @param $parent
  * @param null $parent
  * @param null $where
  * @param int $items_on_page
  * @param bool $make_items_url
  * @return array
  */
 public function get_all_of_external_parent($parent = null, $where = null, $items_on_page = 999999, $make_items_url = TRUE)
 {
  return $this->get_all($this->mix_where($where, "`good` is null and `brand` is null and `category` is null"), $items_on_page, $make_items_url);
 }

 /**
  * Возвращает ключ для связи с родительским элементом
  * @return string|null
  */
 public function external_parent_key()
 {
  return null;
 }


 /**
  * Имя таблицы модели
  * @return string
  */
 public function get_table_name()
 {
  return 'videos';
 }

 /**
  * Массив полей, зависящих от языковой версии
  * @return array
  */
 public function get_mysql_assoc_select()
 {
  return ['caption'];
 }
	
	
   /**
    * Возвращает массив элементов для фильров
    * @param string $lang
    * @param int|null $parent
    * @param string|null $where
    * @param int $items_on_page
    * @return array
    */
	public function filterGet($lang, $parent = null, $where = null, $items_on_page = 999999)
	{
		return $this->get_all_of_external_parent($parent, $where, $items_on_page);
	}
	
	
	

	/**
	 * @inheritdoc
	 */
	protected function meta_tags_exist()
	{
		return false;
	}
}