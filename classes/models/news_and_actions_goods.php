<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 17.10.14
 */

/**
 * Модель акционных товаров
 */
class news_and_actions_goods extends a_simple_model {

 /**
  * @var a_model модель товаров
  */
 private $goods_model;

 /**
  * @var a_model модель акций и новинок
  */
 private $news_and_actions_model;

 public function __construct()
 {
  $this->goods_model = new goods();

  $news_and_models_class = $this->external_model();
  $this->news_and_actions_model = new $news_and_models_class();
  
  parent::__construct();
 }

 /**
  * Возвращает список акций, в которых участвует товар
  * @param int $good_id
  * @param bool $just_actual возвращать только актуальные акции
  * @return array
  */
 public function get_good_actions($good_id, $just_actual = false)
 {
  return $this->news_and_actions_model->get_all("`id` in (select `external_parent` from `{$this->get_table_name()}` where `good`='".(int)$good_id."')" . $this->just_actual_where($just_actual));
 }

 /**
  * Возвращает список акций, в которых участвует каждый товар из массива
  * @param array $goods массив id товаров или элементов товаров
  * @param bool $just_actual возвращать только актуальные акции
  * @return array
  */
 public function get_goods_array_actions(array $goods, $just_actual = false)
 {
  if (empty($goods)) return [];
  if (is_array(reset($goods)))
  {
   $good_ids = [];
   foreach ($goods as $good)
   {
    if (isset($good['id'])) $good_ids[] = $good['id'];
   }
  }
  else $good_ids = $goods;
  
  $query = 
      "select `{$this->get_table_name()}`.`good`, " . $this->news_and_actions_model->select() . "
       join " . $this->get_table_name() . " on `{$this->news_and_actions_model->get_table_name()}`.`id`=`{$this->get_table_name()}`.`external_parent`
       where `{$this->get_table_name()}`.`good` in (" . implode(', ', $good_ids) . ")" . $this->just_actual_where($just_actual);
  
  $result = [];
  $data = mysql_data_assoc($query);
  $this->news_and_actions_model->make_items_url($data);
  foreach ($data as $line)
  {
   if (!isset($result[$line['good']])) $result[$line['good']] = [];
   $result[$line['good']][] = $line;
  }
  
  return $result;
 }

 private function just_actual_where($just_actual)
 {
  if ($just_actual)
  {
   $date = date("Y-m-d");
   return " and `date_begin`<='$date' and `date_end`>='$date'";
  }
  
  return '';
 }

 /**
  * Возвращает список акций, в которых не участвует товар
  * @param int $good_id
  * @return array
  */
 public function get_not_good_actions($good_id)
 {
  return $this->news_and_actions_model->get_all("`id` not in (select `external_parent` from `{$this->get_table_name()}` where `good`='".(int)$good_id."')");
 }

 
 
 /**
  * Служебная функция для создания связи между моделями
  * @return $this
  */
 protected function joins()
 {
  $this->join_model($this->goods_model, array("`id` as `a_id`", "`caption_{$this->app_helper->getLang()}` as `good_caption`", project_image_select('goods')), 'good', 'id');
  $this->join_model($this->news_and_actions_model, array("`caption_{$this->app_helper->getLang()}` as `action_caption`"), 'external_parent', 'id');
  return $this;
 }

 /**
  * Возвращает название модели-предка
  * @return string|null
  */
 public function external_model()
 {
  return 'news_and_actions';
 }

 
 
 /**
  * Параметр сортировки
  * @return string
  */
 protected function get_order()
 {
  return "`good_caption`";
 }

}