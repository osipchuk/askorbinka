<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 30.01.14
 */

/**
 * Модель новостей
 */
class news extends a_filter_model {

 /**
  * Возвращает массив элементов для фильров
  * @param string $lang
  * @param int|null $parent
  * @param string|null $where
  * @param int $items_on_page
  * @return array
  */
 public function filterGet($lang, $parent = null, $where = null, $items_on_page = 999999)
 {
  return $this->get_all_lang_items($lang, $parent, $where, $items_on_page);
 }

 /**
  * Возвращает массив категорий
  * @return array
  */
 public function getCategories()
 {
  $categoryModel = $this->getCategoryModel();
  $subjects_table_name = $this->subjects_table_name();
  
  return $categoryModel->get_all("`{$categoryModel->get_table_name()}`.`id` in 
            (select `subject` from `$subjects_table_name` join `{$this->get_table_name()}` on `$subjects_table_name`.`a_item`=`{$this->get_table_name()}`.`id`
             where `{$this->get_table_name()}`.`langs` like '%{$this->app_helper->getLang()}%')");
 }

 /**
  * Возвращает массив связанных обьектов
  * @param array $item
  * @param int $limit
  * @return array
  */
 public function get_related_items($item, $limit = 3)
 {
  $item_subjects = array_keys($this->index_by_field(mysql_data_assoc("select * from {$this->subjects_table_name()} where `a_item`='{$item['id']}'"), 'subject'));
  
  $where = "`{$this->get_table_name()}`.`id` <> '{$item['id']}' and 
            `{$this->get_table_name()}`.`id` in (select `a_item` from `{$this->subjects_table_name()}` where `subject` in (" . implode(', ', $item_subjects) . "))";
  
  return $this->get_all_lang_items($this->app_helper->getLang(), $item['external_parent'], $where, $limit);
 }

 /**
  * Возвращает where условие для выбора всех элементов категории
  * @param int $category
  * @return null|string
  */
 public function category_where($category)
 {
  return "`{$this->get_table_name()}`.`id` in (select `a_item` from `{$this->subjects_table_name()}` where `subject`='".(int)$category."')";
 }

 /**
  * Возвращает название таблицы с информацией о привязанных категориях
  * @return string
  */
 public function subjects_table_name()
 {
  return $this->get_table_name() . '_subjects';
 }

 public function get_order()
 {
  return "`date` desc";
 }

 protected function get_mysql_assoc_select()
 {
  return array_merge(parent::get_mysql_assoc_select(), array('preview'));
 }


}