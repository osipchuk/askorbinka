<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 17.10.14
 */

/**
 * Модель отзывов о магазине
 */
class reviews extends good_reviews {

 /**
  * Базовая часть where
  * @return string
  */
 protected function base_where()
 {
  return "`{$this->get_table_name()}`.`external_parent` is null";
 }


 /**
  * Возвращает название модели-предка
  * @return string|null
  */
 public function external_model()
 {
  return null;
 }

 /**
  * Служебная функция для создания связи между моделями
  * @return $this
  */
 protected function joins()
 {
  $this->join_model(new clients(), array("concat_ws(' ', `name`, `surname`) as `author_fio`", "`image` as `author_image`"), 'client', 'id', 'left');

  return $this;
 }

 /**
  * Имя таблицы модели
  * @return string
  */
 public function get_table_name()
 {
  return 'good_reviews';
 }
}