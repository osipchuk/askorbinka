<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 19.11.14
 */

/**
 * Отправка кода через email
 */
class discount_code_send_email_form extends a_form_model {

	/**
	 * @var discount_codes
	 */
	protected $model;

	/**
	 * @var int|null
	 */
	protected $discountCodeId;

	/**
	 * @var array|bool
	 */
	protected $account;

    /**
     * @var goods
     */
    protected $goodsModel;

	/**
	 * @inheritdoc
	 */
	public function __construct()
	{
		parent::__construct();
		$this->model = new discount_codes();

		$auth = new auth();
		$this->account = $auth->set_accounts_model(new clients())->get_account_info();
		if (empty($this->account) or $this->account['type'] != 'pr') throw new Exception($this->words->you_should_auth);
        
        $this->goodsModel = new goods();
	}

	/**
	 * Устанавливает id уже сохраненного кода
	 * @param int|null $discountCodeId
	 * @return $this
	 */
	public function setDiscountCodeId($discountCodeId)
	{
		$this->discountCodeId = $discountCodeId;

		return $this;
	}

	/**
	 * Возвращает id сохраненного кода
	 * @return int|null
	 */
	public function getDiscountCodeId()
	{
		return $this->discountCodeId;
	}
	

	

	/**
	 * Структура формы (список полей)
	 * @return array
	 */
	protected function structure()
	{
		return [
			'good' => ['required'],
			'code' => ['required'],
			'fio' => ['required'],
			'city' => ['required'],
			'email' => ['required'],
			'show_alts' => [],
			'show_description' => [],
			'tel' => [],
            'good_caption' => [],
			'card_url' => []
			
		];
	}

	/**
	 * Возвращает ключ записи для выборки щаблона ыз БД
	 * @return string
	 */
	protected function main_template_mode()
	{
		return 'code_send_email';
	}

	/**
	 * Возвращает mode капчи
	 *
	 * Если проверка капчи не требуется, функция должна возвращать NULL
	 *
	 * @return string|null
	 */
	protected function captcha_mode()
	{
		return null;
	}

	protected function before_mail()
	{
        $this->add_send_email($this->get_field('email'));
        
        $good = $this->goodsModel->get_item("`{$this->goodsModel->get_table_name()}`.`id`='{$this->get_safe_field('good')}'");
        if (empty($good)) return false;
        $this->set_field('good_caption', $good['category'] . ' ' . $good['caption']);
        $this->set_field('card_url', url($this->app_helper->get_url_maker()->d_module_url('discount_card') . '/' . $this->get_field('code'), '', true));
        
		return $this->checkCode() and $this->insert_db();
	}

	/**
	 * Добавляет отзыв в БД
	 * @return bool
	 * @throws Exception
	 */
	protected function insert_db()
	{
		$codeId = $this->discountCodeId;
		if (!empty($codeId)) return true;

		$id = mysql_insert($this->model->get_table_name(),
			$set = "`client`='{$this->account['id']}', `good`='{$this->get_safe_field('good')}', `code`='{$this->get_safe_field('code')}',
		     `fio`='{$this->get_safe_field('fio')}', `city`='{$this->get_safe_field('city')}', `email`='{$this->get_safe_field('email')}',
		     `show_alts`='{$this->get_safe_field('show_alts')}', `show_description`='{$this->get_safe_field('show_description')}'");

		if (empty($id)) throw new Exception($this->words->_('unknown_error'));
		
		$this->setDiscountCodeId($id);
		return TRUE;
	}

	/**
	 * Проверяет соответствие кода шаблону
	 * @return bool
	 * @throws Exception
	 */
	protected function checkCode()
	{
		if (!$this->model->isCodeCorrect($this->get_field('code'))) {
			throw new Exception($this->words->profile_pro_code_wrong_length('Код должен быть длиной в 14 символов, должен содержать только латиницу и цифры.'));
		}
		
		if (empty($this->discountCodeId) and !$this->model->isCodeUnique($this->get_field('code'))) {
			throw new Exception($this->words->profile_pro_code_is_not_uniqe('Вы ввели уже сущестующий код.'));
		}
		
		return true;
	}
	
	
}