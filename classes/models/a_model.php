<?php
/**
 * Модель с исправлением ошибок пагинации связаных с join, возможность выбрать project_image в get_all и get_item
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 29.01.14
 * @version 1.2
 */


/**
 * Абстрактный класс модели
 */
abstract class a_model {

 /**
  * @var bool флаг, который означает, что модель отвечает за структуру сайта
  */
 protected $site_structure_model = FALSE;
 
 /**
  * @var applicationHelper
  */
 protected $app_helper;

 /**
  * @var applicationController
  */
 protected $app_controller;

 /**
  * @var string язык приложения
  */
 protected $lang;

 /**
  * @var array массив связанных моделей
  */
 protected $join_models = array();

 /**
  * @var array дополнительные поля, которые нужно вставить в select
  */
 protected $added_select_fields = array();

 /**
  * @var bool отображать первую страницу, игнорируя номер страницы пагинации (если на странице несколько моделей с пагинацией)
  */
 protected $ignore_page_num = FALSE;

 public function __construct()
 {
  $this->app_helper = applicationHelper::getInstance();
  $this->app_controller = applicationHelper::getAppController();
  $this->lang = $this->app_helper->getLang();
  $this->joins();
 }

 public function get_all($where = null, $items_on_page = 999999, $make_items_url = TRUE)
 {
  if (!empty($where)) $where = ' where '.$where;
  
  if (!$this->ignore_page_num) $count = count(mysql_data('select '.$this->select().$where));
  
  if ($this->ignore_page_num or $count)
  {
   if (!$this->ignore_page_num)
   {
    $this->app_controller->pagination()->set_items_on_page($items_on_page);
    $this->app_controller->pagination()->set_items_count($count);
   }

   $result = mysql_data_assoc("select {$this->select()}$where order by ".$this->get_order().' '.($this->ignore_page_num ? "limit 0, ".$items_on_page : $this->app_controller->pagination()->get_limit()));
   if ($make_items_url) $this->make_items_url($result);
   return $result;
  }
  else return array();
 }

 /**
  * Получение одной записи
  * @param string|null $where
  * @param bool $make_items_url
  * @return array|null
  */
 public function get_item($where = null, $make_items_url = TRUE)
 {
  if (!is_null($where)) $where = ' where '.$where;
  $result = mysql_line_assoc("select {$this->select()}{$where} order by {$this->get_order()} limit 0, 1");
  if ($make_items_url) $this->make_item_url($result);
  return $result;
 }

 /**
  * Возвращает массив изображений полученых из project_images
  * @param int $item_id
  * @return array
  */
 public function get_photos($item_id)
 {
  return mysql_data_assoc("select `id`, `caption_{$this->app_helper->getLang()}` as `caption`, `image`
                           from `project_images`
                           where `mode`='{$this->get_images_mode()}__".(int)$item_id."' and `image`<>''
                           order by `rate` desc");
 }

 /**
  * Имя таблицы модели
  * @return string
  */
 public function get_table_name()
 {
  return get_class($this);
 }

 /**
  * Отображать первую страницу, игнорируя номер страницы пагинации (если на странице несколько моделей с пагинацией)
  * @param bool $value
  * @return $this
  */
 public function ignore_page_num($value)
 {
  $this->ignore_page_num = (bool)$value;
  return $this;
 }


 /**
  * Функционал external_parent
  */

 /**
  * Выборка одного элемента с предком @param $parent
  * @param null $parent
  * @param null $where
  * @return array|null
  */
 public function get_item_of_external_parent($parent = null, $where = null)
 {
  return $this->get_item($this->make_external_where($parent, $where));
 }

 /**
  * Выборка всех элементов с предком @param $parent
  * @param null $parent
  * @param null $where
  * @param int $items_on_page
  * @param bool $make_items_url
  * @return array
  */
 public function get_all_of_external_parent($parent = null, $where = null, $items_on_page = 999999, $make_items_url = TRUE)
 {
  return $this->get_all($this->make_external_where($parent, $where), $items_on_page, $make_items_url);
 }

 /**
  * Возвращает название модели-предка
  * @return string|null
  */
 public function external_model()
 {
  return 'structure';
 }

 /**
  * Возвращает предыдущий и следующий элементы 
  * @param array $item
  * @param NULL|int $external_parent если -1 - берется из переданного элемента
  * @return array
  */
 public function get_next_and_prev_items($item, $external_parent = -1)
 {
  $result = array('prev'=>null, 'next'=>null);
  
  if ($external_parent == -1)
  {
   if (array_key_exists('external_parent', $item)) $external_parent = $item['external_parent']; else $external_parent = NULL;
  }
  
  $ignore_page_num = $this->ignore_page_num;
  $data = $external_parent ? $this->ignore_page_num(TRUE)->get_all_of_external_parent($external_parent, null) : $this->ignore_page_num(TRUE)->get_all();
  $this->ignore_page_num($ignore_page_num);
  
  $i = 0;
  $count = count($data);
  while ($i < $count and $data[$i]['id'] != $item['id']) $i++;
  
  if ($data[$i]['id'] == $item['id'])
  {
   if ($i > 0) $result['prev'] = $data[$i-1];
   if ($i < $count - 1) $result['next'] = $data[$i+1];
  }
  
  return $result;
 }

 /**
  * Возвращает элемент переданной языковой версии
  * @param string $lang
  * @param int|null $parent
  * @param string|null $where
  * @return array|null
  */
 public function get_lang_item($lang, $parent = null, $where = null)
 {
  $where = (empty($where) ? '' : '('.$where.') and ')."`langs` like '%{$lang}%'";

  return $this->get_item_of_external_parent($parent, $where);
 }

 /**
  * Возвращает все элементы переданной языковой версии
  * @param string $lang
  * @param int|null $parent
  * @param string|null $where
  * @param int $items_on_page
  * @param bool $make_items_url
  * @return array
  */
 public function get_all_lang_items($lang, $parent = null, $where = null, $items_on_page = 999999, $make_items_url = TRUE)
 {
  $where = (empty($where) ? '' : '('.$where.') and ')."`langs` like '%{$lang}%'";

  return $this->get_all_of_external_parent($parent, $where, $items_on_page, $make_items_url);
 }

 /**
  * Возвращает массив связанных обьектов
  * @param array $item
  * @param int $limit
  * @return array
  */
 public function get_related_items($item, $limit = 3)
 {
  return array();
 }

 /**
  * Подготавливает параметр where для передачи в функцию для выборки
  * @param null|int $parent
  * @param null|string $where
  * @return string
  */
 protected function make_external_where($parent = null, $where = null)
 {
  $parent_where = "`{$this->get_table_name()}`.`external_parent` ".((is_null($parent) or $parent == 'null') ? "is null" : "='".(int)$parent."'");
  if (empty($where)) return $parent_where; else return $parent_where." and ($where)";
 }

 /**
  * <!-- Функционал external_parent
  */
 
 
 /**
  * Индексация результата по полю
  * @param array $data
  * @param string $field
  * @return array
  */
 public static function index_by_field(array $data, $field = 'id')
 {
  if (!is_array($data)) return $data;
  
  $result = [];
  foreach ($data as $item)
  {
   if (array_key_exists($field, $item))
   {
    $result[$item[$field]] = $item;
   }
   else
   {
    $result[] = $item;
   }
  }
   
  return $result;
 }
 


 /**
  * Добавляет дополнительное поле в select
  * @param string|array $fields
  * @return $this
  */
 protected function add_select_fields($fields)
 {
  if (!is_null($fields))
  {
   if (!is_array($fields)) $fields = array($fields);
   foreach ($fields as $field) $this->added_select_fields[] = $field;
  }
  return $this;
 }
 
 /**
  * Служебная функция для создания связи между моделями
  * @return $this
  */
 protected function joins()
 {
  return $this;
 }
 
 /**
  * Устанавливает связь между моделями
  * @param a_model $model модель, с которой нужно установить связь
  * @param array $select_fields массив полей, которые нужно выбрать из связанной таблицы
  * @param string $current_index индекс текущей модели
  * @param string $model_index индекс внешней модели
  * @param string|null $join_type - left, right, null
  * @return $this
  */
 protected function join_model(a_model $model, $select_fields = array(), $current_index = 'external_parent', $model_index = 'id', $join_type = null)
 {
  $this->join_models[] = array('model'=>$model, 'select_fields'=>$select_fields, 'current_index'=>$current_index, 'model_index'=>$model_index, 'join_type'=>$join_type);
  return $this;
 }
 
 /**
  * Параметр сортировки
  * @return string
  */
 protected function get_order()
 {
  return "`{$this->get_table_name()}`.`rate` desc";
 }

 /**
  * Построение запроса на выборку
  * @return string
  */
 protected function select()
 {
  $select = "`{$this->get_table_name()}`.*".mysql_assoc_select($this->get_mysql_assoc_select(), $this->app_helper->getLang(), $this->get_table_name()).
                                           ($this->meta_tags_exist() ? $this->meta_select_add() : '');

  foreach ($this->join_models as $join) foreach ($join['select_fields'] as $field)
  {
   $join_field = $field;
   if (mb_substr($join_field, 0, 1) == '`') $join_field = "`{$join['model']->get_table_name()}`.{$join_field}";
   
   $select .= ", $join_field";
  }
  
  foreach ($this->added_select_fields as $field) $select .= ", {$field}";
  if ($this->select_main_project_image() === TRUE) $select .= ", `{$this->get_table_name()}`.`id` as `a_id`, ".project_image_select($this->get_images_mode());
  
  $select .= " from `".$this->get_table_name()."`";
  foreach ($this->join_models as $join)
   $select .= (!is_null($join['join_type']) ? $join['join_type'] : '')." join `{$join['model']->get_table_name()}` on `{$this->get_table_name()}`.`{$join['current_index']}`=`{$join['model']->get_table_name()}`.`{$join['model_index']}`";
  
  return $select;
 }

 /**
  * Параметр присутствия у модели мета-тегов
  * @return bool
  */
 protected function meta_tags_exist()
 {
  return TRUE;
 }

 /**
  * Флаг необходимости добавлять в select выборку главного project_images изображения
  * @return bool
  */
 protected function select_main_project_image()
 {
  return FALSE;
 }

 protected function meta_select_add()
 {
  return meta_select_add($this->app_helper->getLang(), $this->get_table_name());
 }

 /**
  * Массив полей, зависящих от языковой версии
  * @return array
  */
 protected function get_mysql_assoc_select()
 {
  return array('caption', 'text');
 }

 /**
  * Возвращает mode для project_images
  * @return string
  */
 protected function get_images_mode()
 {
  return get_class($this);
 }

 /**
  * Добавляет ссылки на все элементы из набора
  * @param array $data
  * @return bool
  */
 protected function make_items_url(&$data)
 {
  if (is_array($data))
  {
   $result = TRUE;
   foreach ($data as &$line)
   {
    $r = $this->make_item_url($line, $this);
    $result = ($result and $r);
   }
   
   return $result;
  }
  
  return FALSE;
 }

 /**
  * Добавляет ссылку на элемент
  * @param array $item
  * @return bool
  */
 protected function make_item_url(&$item)
 {
  if (!is_array($item)) return FALSE;
  $item = $this->app_helper->get_url_maker()->item_url($item, $this);
  return TRUE;
 }

 /**
  * Метод для генерации целого условия where на основе двух отдельных условий
  * @param string $base_where
  * @param string $where
  * @param string $operation
  * @return string
  */
 public function mix_where($base_where, $where, $operation = 'and')
 {
  if (empty($where) and empty($base_where)) return '';
  if (empty($where) or empty($base_where)) return $where.$base_where;
  return '('.$where.') '.$operation.' ('.$base_where.')';
 }
}