<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 20.05.14
 */

/**
 * Модель категорий каталога
 */
class categories extends a_tree_model {
 
 /**
  * @var a_model модель параметров категории
  */
 private $params_model;

 /**
  * @var brands модель брендов
  */
 private $brands_model;

 /**
  * @var a_model модель фильтров категории
  */
 private $filters_model;

 /**
  * @var array параметры категорий
  */
 private static $categories_params = [];

 /**
  * @var array фильтры категорий
  */
 private static $categories_filters = [];

 public function __construct()
 {
  parent::__construct();
  $this->params_model = new category_params();
  $this->brands_model = new brands();
  $this->filters_model = new category_filters();
 }

 
 public function get_all($where = null, $items_on_page = 999999, $make_items_url = TRUE, $with_hidden = false)
 {
  if (!$with_hidden) $where = $this->mix_where($where, "`{$this->get_table_name()}`.`hidden`='0'");

  return parent::get_all($where, $items_on_page, $make_items_url);
 }


 /**
  * Возвращает массив эллементов
  * @param bool $with_hidden делать выборку вместе скрытыми элементами
  * @return array
  */
 public function get_structure($with_hidden = false)
 {
  $key = get_class($this);
  if (!$with_hidden and array_key_exists($key, self::$structures)) return self::$structures[$key];

  return static::$structures[$key] = $this->generate_structure_level(null, 0, '', $with_hidden);
 }

 /**
  * Генерирует уровень древовидной структуры для предка @param $parent
  * @param null|int $parent
  * @param int $level
  * @param string $item_url
  * @param bool $with_hidden делать выборку вместе скрытыми элементами
  * @return bool
  */
 protected function generate_structure_level($parent = NULL, $level = 0, $item_url = '', $with_hidden = false)
 {
  $where = "`parent` ".(is_null($parent) ? "is null" : "='".(int)$parent."'");
  if ($this->external_parent >= 0) $where .= " and `external_parent` ".(is_null($this->external_parent) ? "is null" : "='".(int)$this->external_parent."'");

  $data = $this->get_all($where, 999999, FALSE, $with_hidden);

  foreach ($data as $index => $line)
  {
   if ($this->site_structure_model) $base_url = ''; else $base_url = $this->app_helper->get_url_maker()->d_id_url($line['external_parent']);

   $data[$index]['level'] = $level;
   $data[$index]['url'] = $base_url.$item_url.'/'.$line['static'];
   $sub = $this->generate_structure_level($line['id'], $level + 1, $item_url.'/'.$line['static'], $with_hidden);
   if (!empty($sub)) $data[$index]['sub'] = $sub;
  }

  return $data;
 }

 /**
  * Возвращает ID всех подкатегорий
  * @param array $structure
  * @return array
  */
 public function get_all_subcategories_id($structure)
 {
  $result = [];
  foreach ($structure as $sub_item)
  {
   $result[] = $sub_item['id'];
   if (isset($sub_item['sub'])) $result = array_merge($result, $this->get_all_subcategories_id($sub_item['sub']));
  }
  
  return $result;
 }

 /**
  * Возвращает массив id всех подкатегорий + id поточной категории по id категории
  * @param int $category_id
  * @param bool $with_hidden делать выборку вместе скрытыми элементами
  * @return array
  */
 public function get_all_subcategories_by_id_with_current_id($category_id, $with_hidden = false)
 {
  $category_structure = $this->find_item_in_structure_by_id($this->get_structure($with_hidden), $category_id);
  if (isset($category_structure['sub'])) $subcategories_id = $this->get_all_subcategories_id($category_structure['sub']); else $subcategories_id = [];
  
  return array_merge([$category_id], $subcategories_id);
 }

 /**
  * Возвращает с кеша список параметров для категории. Если список пустой - рекурсивно для родительской категории.
  * @param int $category_id
  * @return array
  * @throws Exception если категория не найдена
  */
 public function get_category_params($category_id)
 {
  if (!array_key_exists($category_id, self::$categories_params))
  {
   self::$categories_params[$category_id] = $this->find_category_params($category_id);
  }

  return self::$categories_params[$category_id];
 }

 /**
  * Возвращает список параметров для категории. Если список пустой - рекурсивно для родительской категории.
  * @param int $category_id
  * @return array
  * @throws Exception
  */
 private function find_category_params($category_id)
 {
  $category = $this->find_item_in_structure_by_id($this->get_structure(), $category_id);
  if (empty($category)) throw new Exception('category ERROR');
  if (!empty($category['params_from_cat'])) return $this->get_category_params($category['params_from_cat']);

  $data = $this->params_model->get_all_of_external_parent($category_id);
  if (!empty($data)) return $data;

  if (!empty($category['parent'])) return $this->get_category_params($category['parent']);
  return [];
 }

 /**
  * Возвращает с кеша список фильтров для категории. Если список пустой - рекурсивно для родительской категории.
  * @param int $category_id
  * @return array
  * @throws Exception
  */
 public function get_category_filters($category_id)
 {
  if (!array_key_exists($category_id, self::$categories_filters))
  {
   self::$categories_filters[$category_id] = $this->find_category_filters($category_id);
  }

  return self::$categories_filters[$category_id];
 }

 /**
  * Возвращает список фильтров для категории. Если список пустой - рекурсивно для родительской категории.
  * @param int $category_id
  * @return array
  * @throws Exception
  */
 private function find_category_filters($category_id)
 {
  $category = $this->find_item_in_structure_by_id($this->get_structure(), $category_id);
  if (empty($category)) throw new Exception('category ERROR');
  if (!empty($category['filters_from_cat'])) return $this->get_category_filters($category['filters_from_cat']);

  $data = $this->filters_model->get_all_of_external_parent($category_id);
  if (!empty($data)) return $data;

  if (!empty($category['parent'])) return $this->get_category_filters($category['parent']);
  return [];
 }

 /**
  * Проверяет, должен ли быть элемент меню двоуровневым
  * @param array $item
  * @return bool
  */
 public function is_nav_item_large($item)
 {
  if (!isset($item['sub']) or !is_array($item['sub']) or empty($item['sub'])) return FALSE;
  
  $i = 0;
  $count = count($item['sub']);
  $result = false;
  
  while ($result == false and $i < $count) if (isset($item['sub'][$i++]['sub'])) $result = true;
  
  return $result;
 }

 /**
  * Возвращает список брендов для категории
  * @param int $cat_id
  * @return array
  */
 public function get_category_brands($cat_id)
 {
  return $this->brands_model->ignore_page_num(true)->get_category_brands($cat_id, $this);
 }

 /**
  * Проверяет, можно ли сравнивать товар с товарами из категории
  * @param array $current_category
  * @param array $good_item
  * @return bool
  */
 public function check_good_item_can_be_compared_in_category($current_category, $good_item)
 {
  if (empty($current_category)) return false;
  $category_params = $this->get_category_params($good_item['external_parent']);
  if (empty($category_params)) return false;
  if ($good_item['external_parent'] == $current_category['id']) return true;
  
  return $category_params == $this->get_category_params($current_category['id']);
 }

    /**
	 * Возвращает массив категорий с товарами переданного бренда
     * @param int $brandId
	 * @return array
	 */
	public function getCategoriesWithBrandGoods($brandId)
	{
		$goodsModel = new goods();
		return $this->get_all("`{$this->get_table_name()}`.`id` in (select `external_parent` from `{$goodsModel->get_table_name()}` where `brand`='".(int)$brandId."')");
	}
}