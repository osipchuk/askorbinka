<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 18.11.14
 */

/**
 * Модель кодов для скидок
 */
class discount_codes extends a_simple_model {

	/**
	 * Проверяет код на уникальность
	 * @param string $code
	 * @return bool
	 */
	public function isCodeUnique($code)
	{
		$result = $this->get_all("`{$this->get_table_name()}`.`code`='".safe($code)."'");
		return count($result) == 0;
	}

	/**
	 * Проверяет код на корректность
	 * @param string $code
	 * @return int
	 */
	public function isCodeCorrect($code)
	{
		return preg_match("/^[a-zA-Z0-9]{14}$/", $code);
	}

	/**
	 * Возвращает статистику по использованных кодах для профи
	 * @param int $proId
	 * @pram bool $usedOnly
	 * @return array
	 */
	public function getCodesStatistic($proId, $usedOnly = true)
	{
		$proId = (int) $proId;
		$tablename = $this->get_table_name();
		$orderTableName = 'orders';
		$goodsModel = (new goods())->ignore_page_num(true);
		$goodTable = $goodsModel->get_table_name();
		
		$result = [];
		$data = mysql_data_assoc(
			"select `$tablename`.*, `$orderTableName`.`add_time` as `order_date`, `order_goods`.`price`" .
			        mysql_assoc_select(array('caption', 'category'), $this->app_helper->getLang(), $goodTable) . "
             from `$tablename`
                  left join `$orderTableName` on `$tablename`.`id`=`$orderTableName`.`discount_code_id`
                  left join `order_goods` on `order_goods`.`order`=`$orderTableName`.`id`
                  left join `$goodTable` on `order_goods`.`good`=`$goodTable`.`id`
             where `$tablename`.`client`='$proId'" . ($usedOnly ? " and `$tablename`.`used`='1'" : '') . "
             order by `order_date` desc, `$tablename`.`id`"
		);
		
		foreach ($data as $line) {
			if (empty($result) or end($result)['id'] != $line['id']) {
				$line['goods'] = [];
				$line['order_price'] = 0;
				$result[] = $line;
			}

			$item = &$result[count($result) - 1];
			$item['goods'][] = trim($line['category'] . ' ' . $line['caption']);
			$item['order_price'] += (float) $line['price'];
		}
		
		return $result;
	}

	
	
	/**
	 * @inheritdoc
	 */
	protected function get_order()
	{
		return "`id` desc";
	}

	/**
	 * @inheritdoc
	 */
	protected function joins()
	{
		$this->join_model(
			new goods(),
			[
				"`caption_{$this->app_helper->getLang()}` as `good_caption`",
				"`category_{$this->app_helper->getLang()}` as `good_category`"
			],
			'good',
			'id',
			'left'
		);
		
		return $this;
	}
}