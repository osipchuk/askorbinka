<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 19.11.14
 */

/**
 * Отправка кода через sms
 */
class discount_code_send_sms_form extends discount_code_send_email_form {

	/**
	 * Возвращает ключ записи для выборки щаблона ыз БД
	 * @return string
	 */
	protected function main_template_mode()
	{
		return 'code_send_sms';
	}

	/**
	 * Заменяет в шаблоне переменные значениями
	 * @param $text
	 * @return string
	 */
	protected function replace($text)
	{
		return preg_replace("/[\s]+/", ' ', strip_tags(parent::replace($text)));
	}

	/**
	 * Отправляет текст на заданный список почтовых ящиков.
	 * @return bool Возвращает TRUE, если почта отправилась на все почтовые ящики, иначе - FALSE.
	 */
	protected function mail()
	{
//		TODO: заменить на отправку через смс-гейт
		return parent::mail();
	}


}