<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 17.10.14
 */

/**
 * Модель ссылок на каталог 
 */
class catalog_links extends a_simple_model {

 /**
  * Возвращает структурированный по категориям массив ссылок для элемента id модели a_model
  * @param a_model $item_model
  * @param int $id
  * @return array
  */
 public function get_structured_links_of_item(a_model $item_model, $id)
 {
  $result = array();
  $data = $this->get_all("`{$this->get_table_name()}`.`id` in (select `link` from `".get_class($item_model)."_catalog_links` where `external_parent`='".(int)$id."')");
  
  foreach ($data as $line)
  {
   if (!isset($result[$line['category_caption']])) $result[$line['category_caption']] = array();
   
   $result[$line['category_caption']][] = $line;
  }
  
  return $result;
 }

 /**
  * Возвращает массив id ссылок для элемента id модели a_model
  * @param a_model $item_model
  * @param int $id
  * @return array
  */
 public function get_ids_links_of_item(a_model $item_model, $id)
 {
  $result = array();
  $data = $this->get_structured_links_of_item($item_model, $id);
  
  foreach ($data as $cat => $array)
  {
   foreach ($array as $item) $result[] = $item['id'];
  }
  
  return $result;
 }

 /**
  * Массив полей, зависящих от языковой версии
  * @return array
  */
 protected function get_mysql_assoc_select()
 {
  return array('caption');
 }

 /**
  * Возвращает название модели-предка
  * @return string|null
  */
 public function external_model()
 {
  return 'catalog_link_categories';
 }

 /**
  * Служебная функция для создания связи между моделями
  * @return $this
  */
 protected function joins()
 {
  $external_model = $this->external_model();
  $this->join_model(new $external_model(), array("`caption_{$this->app_helper->getLang()}` as `category_caption`"), 'external_parent', 'id', 'left');
  return $this;
 }


}