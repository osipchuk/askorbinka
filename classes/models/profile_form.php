<?php

/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 22.08.14
 */
class profile_form extends a_form_model
{

	/**
	 * @var NULL|clients
	 */
	protected $accounts_model;

	/**
	 * @var bool флаг регистрации аккаунта профи
	 */
	protected $pro = false;

	/**
	 * Возвращает флаг регистрации аккаунта профи
	 * @return boolean
	 */
	public function getPro()
	{
		return (boolean)$this->pro;
	}

	/**
	 * Устанавливает флаг регистрации аккаунта профи
	 * @param boolean $pro
	 * @return $this
	 */
	public function setPro($pro)
	{
		$this->pro = (boolean)$pro;
		$this->main_template = $this->get_template($this->main_template_mode());

		return $this;
	}

	

	/**
	 * Подтвержждает email
	 * @param int $email_id
	 * @param string $hash
	 * @return bool
	 */
	public function submit($email_id, $hash)
	{
		if ($this->format_hash($email_id) == $hash) {
			$this->get_accounts_model()->submit($email_id);
			if (mysql_error()) return false;
			
			$deliveryEmails = new delivery_emails();
			$email_item = $deliveryEmails->get_item("`client`='$email_id'");
			if (empty($email_item)) return true;
			
			return (bool)$deliveryEmails->submit($email_item['id']);
		}

		return FALSE;
	}

	/**
	 * Формирует hash на основе записи о email'е
	 * @param int $id
	 * @return string
	 * @throws Exception в случае ошибки
	 */
	protected function format_hash($id)
	{
		$line = $this->get_accounts_model()->get_item("`id`='" . (int)$id . "'");
		if (empty($line)) throw new Exception($this->words->_('unknown_error'));

		return md5($line['id'] . 'ff' . $line['register_time']);
	}

	/**
	 * Возвращает модель аккаунтов пользователей
	 * @return clients
	 */
	public function get_accounts_model()
	{
		if (is_null($this->accounts_model)) $this->accounts_model = new clients();

		return $this->accounts_model;
	}

	/**
	 * Проверка email, совпадания паролей
	 * @return bool
	 * @throws Exception в случае ошибки
	 */
	protected function before_mail()
	{
		if ($this->get_field('password') != $this->get_field('password_repeat')) throw new Exception($this->words->_('profile_register_passwords_not_match', 'Пароли не совпадают'));
		$this->check_email_exist();
		$id = $this->insert_db();
		$this->add_send_email($this->get_field('email'));
		$this->set_field('account_id', $id);
		
		(new delivery_emails())->add_email($this->get_field('email'), $this->get_field('delivery_categories'), $this->get_field('surname').' '.$this->get_field('name'), $id);

		return $this->format_link($id);
	}

	/**
	 * Проверка уникальности email
	 * @return bool
	 * @throws Exception в случае неуникальности
	 */
	protected function check_email_exist()
	{
		$item = $this->get_accounts_model()->get_item("`email`='{$this->get_safe_field('email')}'");
		if (!empty($item)) throw new Exception($this->words->_('profile_register_email_exist', 'Ваш email уже зарегистрирован в системе'));

		return TRUE;
	}

	/**
	 * Вставляет запись в БД
	 * @return int id аккаунта
	 * @throws Exception в случае ошибки
	 */
	protected function insert_db()
	{
		$set = "`pass`='" . md5($this->get_field('password')) . "'";
		if ($this->getPro()) $set .= ", `type`='pr'";
		$db_fields = ['email', 'name', 'surname', 'tel', 'post', 'job', 'city'];
		foreach ($db_fields as $field) $set .= ", `{$field}`='{$this->get_safe_field($field)}'";

		$id = mysql_insert($this->get_accounts_model()->get_table_name(), $set);
		if (empty($id)) throw new Exception($this->words->_('unknown_error') . ' ' . mysql_error());

		return $id;
	}

	/**
	 * Формирует линк на страницу подтверждения и записывает ее в переменную submit_link
	 * @param int $id
	 * @return bool
	 */
	protected function format_link($id)
	{
		$link = mb_substr(url($this->app_helper->get_url_maker()->d_module_url('profile')), 1);

		$this->set_field('submit_link', $this->app_helper->get_global('base') . $link . '/submit?id=' . $id . '&amp;hash=' . $this->format_hash($id));

		return TRUE;
	}

	/**
	 * Структура формы (список полей)
	 * @return array
	 */
	protected function structure()
	{
		$result = [
			'image' => [],
			'delete_image' => [],
			'account_id' => [],
			'email' => ['required'],
			'name' => ['required'],
			'surname' => ['required'],
			'tel' => ['required'],
			'city' => ['required'],
			'password' => [],
			'password_repeat' => [],
			'submited' => ['bool'],
			'delivery_categories' => []
		];


		if ($this->getPro()) {
			$result = array_merge($result, [
					'post' => ['required'],
					'job' => ['required']
				]
			);
		}


		return $result;
	}

	/**
	 * Возвращает ключ записи для выборки щаблона ыз БД
	 * @return string
	 */
	protected function main_template_mode()
	{
		return $this->getPro() ? 'register-pro' : 'register';
	}

	/**
	 * Возвращает mode капчи
	 *
	 * Если проверка капчи не требуется, функция должна возвращать NULL
	 *
	 * @return string|null
	 */
	protected function captcha_mode()
	{
		return NULL;
	}
}