<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 16.10.14
 */

/**
 * Модель категорий ссылок на страницы каталога
 */
class catalog_link_categories extends a_simple_model {

 /**
  * Массив полей, зависящих от языковой версии
  * @return array
  */
 protected function get_mysql_assoc_select()
 {
  return array('caption');
 }
}