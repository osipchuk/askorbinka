<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 07.03.14
 */

/**
 * Обработчик заказов онлайн магазина
 */
class order_processor extends a_form_model {

 /**
  * @var cart
  */
 protected $cart;

 /**
  * @var int ID заказа после загрузки из БД
  */
 protected $order_id;

 /**
  * @var array|NULL
  */
 protected $account;

 public function __construct()
 {
  parent::__construct();
  $this->cart = new cart();
 }

 /**
  * Устанавливает аккаунт пользователя после проверки авторизации
  * @param array $account 
  * @return $this
  */
 public function set_account($account)
 {
  if (is_array($account) or is_bool($account)) $this->account = $account;
  return $this;
 }

 /**
  * Возвращает аккаунт пользователя после проверки авторизации
  * @return array
  * @throws Exception если не задан
  */
 public function get_account()
 {
  if (is_null($this->account)) throw new Exception('Auth account is not set');
  return $this->account;
 }
 
 

 /**
  * Добавляет корзину с товарами
  * @param cart $cart
  * @return $this
  */
 public function set_cart(cart $cart)
 {
  unset($this->cart);
  $this->cart = $cart;
  $this->set_field('goods_table', $this->cart->format_table(), FALSE);
  
  return $this;
 }

 /**
  * Возвращает список заказов
  * @return array
  */
 public function get_orders()
 {
  return mysql_data_assoc("select * from `orders` order by `id` desc");
 }

 /**
  * Загружает заказ из БД
  * @param int $order_id
  * @return bool
  * @throws Exception в случае отсутствия заказа ID
  */
 public function load_from_db($order_id)
 {
  $line = mysql_line_assoc("select * from `orders` where `id`='".(int)$order_id."'");
  if (empty($line)) throw new Exception('order not exist');
  
  $this->order_id = $line['id'];
  $this->set_field('order_id', $line['id']);
  
  foreach ($line as $field => $value) $this->set_field($field, $value);
  if (!empty($line['client'])) $this->fill_client_info_from_account($line['client']);
  $this->cart->load_from_db($order_id);
 
  return TRUE;
 }

 /**
  * Возвращает количество товаров и цену всех товаров
  * @return array
  */
 public function get_cart_summary()
 {
  return $this->cart->get_summary(true);
 }

 /**
  * Возвращает массив товаров для поточного заказа
  * @return array
  */
 public function get_goods()
 {
  return $this->cart->get_goods();
 }

 /**
  * Возвращает статус заказа
  * @param int|$order_id
  * @return int|null
  */
 public function get_order_status($order_id)
 {
  $line = mysql_line_assoc("select * from `orders` where `id`='".(int)$order_id."'");
  if (empty($line)) return null;
  
  return $line['status'];
 }

 /**
  * Подтверждает оплату заказа
  * @return bool
  * @throws Exception в случае ошибки
  */
 public function submit_payments()
 {
  $status = $this->getOrderInfo($this->order_id);
  if ($status['status'] == 1) return TRUE;

  mysql_update('orders', "`status`='1'", "`id`='{$this->order_id}'");
  if (mysql_error()) throw new Exception('unknown_error');

  $client_template = $this->get_template('payment_submit_client');
  $admin_template = $this->get_template('payment_submit_admin');

  if (!empty($status['client'])) $this->fill_client_info_from_account($status['client']);

  try {
   $client_email = $this->get_field('email');
   
   $r1 = send_mail($client_email, $this->replace($client_template['subject']), $this->replace($client_template['text']));
   $r2 = send_mail($this->get_emails()[0], $this->replace($admin_template['subject']), $this->replace($admin_template['text']));

   return $r1 and $r2;
  }
  catch (Exception $e)
  {
   return false;
  }
 }

	/**
	 * Отменяет заказ
	 * @return bool
	 * @throws Exception в случае ошибки
	 */
	public function cancelOrder()
	{
		$order = $this->getOrderInfo($this->order_id);
		if ($order['status'] == -1) return TRUE;
		
		$clientBonusesModel = new client_bonuses();
		
		if ($order['bonuses_applied'] > 0) {
			$clientBonusesModel->changeBalance($order['client'], $order['bonuses_applied'], $order['id'], 'Возвращение бонусов после отмены заказа');
		}
		
		if ($order['discount_code_id']) {
			$discountCodesModel = new discount_codes();
			
			$code = $discountCodesModel->get_item("`{$discountCodesModel->get_table_name()}`.`id`='{$order['discount_code_id']}'");
			if (empty($code)) {
				throw new Exception('code error');
			}
			
			$clientBonusesModel->changeBalance($order['client'], -$code['bonuses'], $order['id'], 'Списание бонусов после отмены заказа');
			mysql_update($discountCodesModel->get_table_name(), "`used`='0', `bonuses`='0'", "`id`='{$code['id']}'");
        } elseif ($bonusesAdded = $clientBonusesModel->get_item("`{$clientBonusesModel->get_table_name()}`.`order_id`='{$order['id']}'") ) {
            $clientBonusesModel->changeBalance($order['client'], -$bonusesAdded['value'], $order['id'], 'Списание бонусов после отмены заказа');
		}
		
		mysql_update('orders', "`status`='-1'", "`id`='{$this->order_id}'");
		if (mysql_error()) throw new Exception('unknown_error');
		
		
		return true;
	}

 
 

 /**
  * Структура формы (список полей)
  * @return array
  */
 protected function structure()
 {
  $result = [
   'client' => ['required'],
   'note'=>['nl2br'],
   'ship'=>['required'],
   'pay'=>['required'],
   'address'=>['required'],
   'goods_table'=>['required'],
   'other-receiver'=>[],
   'pay_with_bonuses'=>[],
   'receiver-name'=>[],
   'receiver-surname'=>[],
   'receiver-tel'=>[],
   'receiver-email'=>[], 
   'discount_code_id' => [],
   'bonuses_applied' => [],
  ];
  if (array_key_exists('ship', $this->fields)){
    $ship_type = $this->fields['ship'];
    if ($ship_type === '4'){
      $result['address'] = [];
    }
  }
  
  try
  {
   $account = $this->get_account();
   $this->set_field('client', $account['id']);
  }
  catch (Exception $e)
  {
   
  }
  
  return $result;
 }

 /**
  * Возвращает ключ записи для выборки щаблона ыз БД
  * @return string
  */
 protected function main_template_mode()
 {
  return 'process_admin';
 }

 /**
  * Вставка заказа в БД
  * @return bool
  * @throws Exception в случае ошибки
  */
 protected function before_mail()
 {
  $goods = $this->cart->get_goods();
  if (!empty($goods))
  {
   $ships = new ships();
   
   $ship_caption = $ships->get_item("`id`='".(int)$this->get_field('ship')."'");
   $pay_caption = $this->words->_w('pay_system_', $this->get_field('pay'));
   
   if (empty($ship_caption) or empty($pay_caption)) return false;
   $this->set_field('ship_caption', $ship_caption['caption']);
   $this->set_field('pay_caption', $pay_caption);

   $programsContainer = applicationHelper::getAmountMaker();
   if ($programsContainer->isActionGoodsInCart())
   {
    $this->set_field('pay_with_bonuses', 0);
   }
   
   $set = "`note`='".safe($this->get_field('note'))."', `ship`=".safe($this->get_field('ship')).", `pay`='".safe($this->get_field('pay'))."',
           `address`='".safe($this->get_field('address'))."', `free_ship`='{$this->cart->getFreeShip()}'";
	  
	  $discountCode = null;
	  $discountCodeId = $this->get_safe_field('discount_code_id');
	  if ($discountCodeId !== null) {
		  $discountCodeModel = new discount_codes();
		  $discountCodeItem = $discountCodeModel->get_item("`{$discountCodeModel->get_table_name()}`.`id`='$discountCodeId'");
		  if (!empty($discountCodeItem) and isset($discountCodeItem['code'])) {
			  $discountCode = $discountCodeItem['code'];
			  $set .= ", `discount_code_id` = {$this->get_safe_field('discount_code_id')}";
		  }
	  }
	  
	  $amountMaker = amountMakerContainer::init([
		  'discountCode' => $discountCode
	  ]);
	  $amountMaker->setCart($this->cart)->resetAllDiscountsInCart();
	  $amountBefore = $this->cart->get_summary()['price'];
	  $amountMaker->apply();
	  $goods = $this->cart->get_goods();
	  $amountAfter = $this->cart->get_summary()['price'];
	  
	  
	  $bonuses_applied = 0;
   
   $account = $this->get_account();
   if (!empty($account))
   {
	$set .= ", `client`='{$account['id']}'";
	$this->set_field('name', $account['name']);
	$this->set_field('surname', $account['surname']);
	$this->set_field('tel', $account['tel']);
	$this->set_field('email', $account['email']);
	$this->set_field('city', $account['city']);

	   $bonuses = $account['bonuses'];
	   if ($this->get_field('pay_with_bonuses') and $bonuses > 0) {
		   
		   if ($amountBefore != $amountAfter) {
			   throw new Exception($this->words->cart_you_cant_use_bonuses_with_actions('К сожалению, Вы не можете оплатить бонусами акционные товары'));
		   }
		   
		   $amount = $this->cart->get_summary()['price'];
		   if ($amount >= $bonuses) {
			   $bonuses_applied = $bonuses;
		   } else {
			   $bonuses_applied = round($bonuses - ($bonuses - $amount));
		   }
		   
		   $set .= ", `bonuses_applied`='$bonuses_applied', `pay_with_bonuses`='1'";
	   }
	
   }
   else
   {
	$set .= ", `name`='".safe($this->get_field('name'))."', `surname`='".safe($this->get_field('surname'))."', `tel`='".safe($this->get_field('tel'))."',
	           `email`='".safe($this->get_field('email'))."', `city`='".safe($this->get_field('city'))."'";
   }

   if ($this->get_field('other-receiver'))
   {
	$name = safe($this->get_field('receiver-name'));
	$surname = safe($this->get_field('receiver-surname'));
	$tel = safe($this->get_field('receiver-tel'));
	$email = safe($this->get_field('receiver-email'));
	if (empty($name) or empty($surname) or empty($tel) or empty($email)) throw new Exception($this->words->_('you_must_fill_all_the_fields'));
	
	$set .= ", `other_receiver`='".(int)$this->get_field('other-receiver')."', `receiver_name`='$name', `receiver_surname`='$surname', `receiver_tel`='$tel', `receiver_email`='$email'";
   }
   
   $order_id = mysql_insert('orders', $set);
   $full_order_id = mysql_line("select `id` from `orders` where `id`='$order_id'");
   if (is_array($full_order_id) and $full_order_id = $full_order_id[0])
   {
    $this->set_field('order_id', $full_order_id);
    
    $result = TRUE;
    foreach ($goods as $good)
    {
     $id = mysql_insert(
		 'order_goods',
		 "`order`='$order_id', `good`='{$good['good']['id']}', `count`='{$good['count']}', `price`='{$good['price']}', `price_without_discounts`='{$good['price_without_discounts']}'"
	 );
		
     if ($id) {
		 $amountMaker->makeUsed();
		 $result = true;
	 } else {
		 $result = FALSE;
	 }
    }

	   $this->cart->setOrderId($order_id);
	   $this->set_field('goods_table', $this->cart->format_table(), FALSE);
	   
	   if ($bonuses_applied > 0) {
		   (new client_bonuses())->changeBalance($account['id'], -$bonuses_applied, $order_id);
	   }

    $amountMaker = amountMakerContainer::init([]);
    $amountMaker->setCart($this->cart)->resetAllDiscountsInCart()->apply();
    
    return $result;
   }
  }

  throw new Exception($this->words->_('cart_no_goods'));
 }

 /**
  * Отправка письма заказчику
  * @return bool
  */
 protected function send_mail_client()
 {
  $template = $this->get_template('process_client');
  if (!empty($template)) return send_mail($this->get_field('email'), $this->replace($template['subject']), $this->replace($template['text']));
  
  return FALSE;
 }

 /**
  * Отправка письма заказчику, очистка корзины
  * @return bool
  */
 protected function after_mail()
 {
  return ($this->send_mail_client() and $this->cart->clear());
 }

 /**
  * Возвращает mode капчи
  *
  * Если проверка капчи не требуется, функция должна возвращать NULL
  *
  * @return string|null
  */
 protected function captcha_mode()
 {
  return NULL;
 }

 /**
  * Заполняет модель информацией пользователя из модели
  * @param int $client
  * @return bool
  */
 private function fill_client_info_from_account($client)
 {
  $client = (int)$client;
  $clients_model = new clients();
  $account = $clients_model->get_item("`id`='$client'");
  
  if (!empty($client))
  {
   $this->set_account($account);

   $this->set_field('name', $account['name']);
   $this->set_field('surname', $account['surname']);
   $this->set_field('tel', $account['tel']);
   $this->set_field('email', $account['email']);
   $this->set_field('city', $account['city']);
   
   return true;
  }
  
  return false;
 }

	/**
	 * Возвращает информацию о заказе
	 * @param $orderId
	 * @return array
	 * @throws Exception в случае ошибки
	 */
	private function getOrderInfo($orderId)
	{
		$orderId = (int) $orderId;
		$result = mysql_line_assoc("select * from `orders` where `id`='$orderId'");
		if (empty($result)) throw new Exception('order error');
		
		return $result;
	}
}