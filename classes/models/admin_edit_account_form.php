<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 14.08.14
 */

class admin_edit_account_form extends profile_form
{
	/**
	 * Возвращает ключ записи для выборки щаблона из БД
	 * @return string
	 */
	protected function main_template_mode()
	{
		return 'admin_edit_account';
	}

	/**
	 * Проверка уникальности email
	 * @return bool
	 * @throws Exception в случае неуникальности
	 */
	protected function check_email_exist()
	{
		return true;
	}

	/**
	 * Вставляет запись в БД
	 * @return int id аккаунта
	 * @throws Exception в случае ошибки
	 */
	protected function insert_db()
	{
		return true;
	}

	/**
	 * Структура формы (список полей)
	 * @return array
	 */
	protected function structure()
	{
		$result = parent::structure();
		unset($result['password']);
		unset($result['password_repeat']);
		return $result;
	}


}