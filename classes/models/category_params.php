<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 23.09.14
 */

/**
 * Модель параметров категорий 
 */
class category_params extends a_model {

 /**
  * @var array массив типов параметров
  */
 public static $types = array('char'=>'Строка', 'bool'=>'Логическое значение', 'image'=>'Изображение');
 
 /**
  * Возвращает название типа
  * @param string $type
  * @return string
  */
 public function type_caption($type)
 {
  if (array_key_exists($type, self::$types)) return self::$types[$type];
  return '';
 }

 /**
  * Возвращает изображения для переданного параметра
  * @param int $param_id
  * @return array
  */
 public function get_param_images($param_id)
 {
  return mysql_data_assoc("select * ".mysql_assoc_select(array('caption'))." from `category_param_images` where `external_parent`='".(int)$param_id."' order by `rate` desc");
 }

 public function get_all($where = null, $items_on_page = 999999, $make_items_url = TRUE)
 {
  $data = parent::get_all($where, $items_on_page, $make_items_url);
  foreach ($data as &$line) $line = $this->edit_line($line);
  return $data;
 }

 public function get_item($where = null, $make_items_url = TRUE)
 {
  return $this->edit_line(parent::get_item($where, $make_items_url));
 }


 /**
  * Массив полей, зависящих от языковой версии
  * @return array
  */
 protected function get_mysql_assoc_select()
 {
  return array('caption');
 }

 /**
  * Параметр присутствия у модели мета-тегов
  * @return bool
  */
 protected function meta_tags_exist()
 {
  return FALSE;
 }

 /**
  * Возвращает название модели-предка
  * @return string|null
  */
 public function external_model()
 {
  return 'categories';
 }

 /**
  * Вносит правки в элемент после его загрузки из БД
  * @param array $line
  * @return array
  */
 private function edit_line($line)
 {
  if ($line['type'] == 'image') $line['images'] = $this->get_param_images($line['id']); else $line['images'] = array();
  $line['value'] = NULL;
  return $line;
 }
}