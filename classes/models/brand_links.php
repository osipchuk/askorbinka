<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 12.08.14
 */

/**
 * Модель ссылок брендов 
 */
class brand_links extends a_model {

 /**
  * Возвращает название модели-предка
  * @return string|null
  */
 public function external_model()
 {
  return 'brands';
 }
 
 

 /**
  * Параметр присутствия у модели мета-тегов
  * @return bool
  */
 protected function meta_tags_exist()
 {
  return FALSE;
 }

 /**
  * Массив полей, зависящих от языковой версии
  * @return array
  */
 protected function get_mysql_assoc_select()
 {
  return array('caption', 'url');
 }
}