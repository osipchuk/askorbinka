<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 11.08.14
 */

/**
 * Модель брендов
 */
class brands extends a_model {

 /**
  * @var a_model
  */
 private $links_model;

 /**
  * Возвращает массив брендов с товарами из переданной категории и ее подкатегорий
  * @param int|null $category
  * @param categories|null $category_model
  * @return array
  */
 public function get_category_brands($category = null, $category_model = null)
 {
	 if (!is_null($category) and !is_null($category_model)) {
		 $category = (int)$category;
		 $structure = $category_model->find_item_in_structure_by_id($category_model->get_structure(), $category);

		 $subcategories = [$category];
		 if (isset($structure['sub'])) $subcategories = array_merge($subcategories, $category_model->get_all_subcategories_id($structure['sub']));
	 }
	 else {
		 $subcategories = null;
	 }
  
  
  return $this->get_all($where = "`{$this->get_table_name()}`.`id` in (select `brand` from `goods`".
	  (!is_null($subcategories) ? " where `external_parent` in (".implode(', ', $subcategories).")" : '').")");
 }

	/**
	 * Возвращает список категорий второго уровня с товарами переданного бренда
     * @param int $brand
	 * @param a_tree_model $categoryModel
	 * @param a_model $goodsModel
	 * @return array
	 */
	public function get_brand_categories_of_2_level($brand, a_tree_model $categoryModel, a_model $goodsModel)
	{
		$tableName = $categoryModel->get_table_name();
		$goodsTableName = $goodsModel->get_table_name();
		
		$subQuery = "select `external_parent` from `$goodsTableName`
                     where `brand`='".(int)$brand."' and `id` in (select `external_parent` from `good_params` where `value_image` is not null)";
		
		$query = "select ".$categoryModel->select()."
			      join `$tableName` as `parent_cat` on `$tableName`.`parent`=`parent_cat`.`id`
		          where `parent_cat`.`parent` is null and (`$tableName`.`id` in ($subQuery) or
		                ( `$tableName`.`id` in (select `parent` from `$tableName` where `id` in ($subQuery)) ))";
		
		return mysql_data_assoc($query);
	}

	/**
	 * Возвращает массив технологий для переданной категории
	 * @param int $category
	 * @param int|null $brand
	 * @param bool $withGoodsOnly
	 * @return array
	 */
	public function getAllCategoryImageTechnologies($category, $brand = null, $withGoodsOnly = false)
	{
		$tableName = 'category_param_images';
		$category = (int)$category;
		
		$categoriesModel = new categories();
		$categoryItem = $categoriesModel->get_item("`id`='$category'");
		if (empty($categoryItem)) {
			return [];
		} elseif (!empty($categoryItem['params_from_cat'])) {
			$catId = $categoryItem['params_from_cat'];
		} else {
			$catId = $category;
		}
		
		
		$query = "select `$tableName`.* ".mysql_assoc_select(array('caption'), null, $tableName).", `categories`.`id` as `cat_id`
		          from `$tableName`
		               join `category_params` on `$tableName`.`external_parent`=`category_params`.`id`
		               join `categories` on `category_params`.`external_parent`=`categories`.`id`
		               join `categories` as `parent_categories` on `categories`.`parent`=`parent_categories`.`id` 
		          where (`categories`.`id`='$catId' or `categories`.`parent`='$catId')";
		
		if ($withGoodsOnly) {
			$query .= " and `$tableName`.`id` in
			           (select `value_image`
			            from `good_params`
			            join `goods` on `good_params`.`external_parent`=`goods`.`id` join `categories` on `goods`.`external_parent`=`categories`.`id`
			            where (`goods`.`external_parent`='$category' or `categories`.`parent`='$category')".(!is_null($brand) ? " and `goods`.`brand`='$brand'" : '')."
			            )";
		}
		
		$query .= " order by `$tableName`.`rate` desc";
		return mysql_data_assoc($query);
	}

 /**
  * Возвращает бренды с файлами
  * @param a_model $files_model модель файлов
  * @param null|array $goods_categories массив ID категорий товаров. Если NULL - все товары 
  * @return array
  */
 public function get_brands_with_files(a_model $files_model = NULL, $goods_categories = NULL)
 {
  return $this->get_all("`external_parent` is not null and `{$this->get_table_name()}`.`id` in (select `brand` from `{$files_model->get_table_name()}`".
	  (is_array($goods_categories) ? " where `external_parent` in (".implode(', ', $goods_categories).")" : '')." )");
 }

 /**
  * Возвращает модель ссылок
  * @return a_model
  */
 public function get_links_model()
 {
  if (empty($this->links_model))
  {
   $this->links_model = new brand_links();
   $this->links_model->ignore_page_num(true);
  }
  
  return $this->links_model;
 }

 /**
  * Возвращает список линков бренда
  * @param int $brand_id
  * @return array
  */
 public function get_links($brand_id)
 {
  return $this->get_links_model()->get_all_of_external_parent($brand_id, NULL, 999999, FALSE);
 }

 /**
  * Служебная функция для создания связи между моделями
  * @return $this
  */
 protected function joins()
 {
  $this->join_model(new countries(), ["`caption_{$this->app_helper->getLang()}` as `country_caption`", "`image` as `country_image`"], 'country', 'id', 'left');
  return $this;
 }

 /**
  * Массив полей, зависящих от языковой версии
  * @return array
  */
 protected function get_mysql_assoc_select()
 {
  return array_merge(parent::get_mysql_assoc_select(), ['good_category']);
 }
}