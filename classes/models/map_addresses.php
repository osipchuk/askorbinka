<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 13.08.14
 */

/**
 * Модель адрессов карты
 */
class map_addresses extends a_simple_model {

 /**
  * Возвращает название модели-предка
  * @return string|null
  */
 public function external_model()
 {
  return 'map_cities';
 }

 
 /**
  * Служебная функция для создания связи между моделями
  * @return $this
  */
 protected function joins()
 {
  $model = new map_cities();
  $this->join_model($model, array("`caption_{$this->app_helper->getLang()}` as `city_caption`", "`region`", "`nearest_city`", "`align`"));
  
  return $this;
 }


 /**
  * Массив полей, зависящих от языковой версии
  * @return array
  */
 protected function get_mysql_assoc_select()
 {
  return array('address');
 }

 /**
  * Параметр сортировки
  * @return string
  */
 protected function get_order()
 {
  return "`{$this->get_table_name()}`.`type`, `{$this->get_table_name()}`.`id`";
 }
}