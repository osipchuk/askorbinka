<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 29.01.14
 */

/**
 * Модель текстовой страницы
 */
class text extends a_model {

 protected function get_order()
 {
  return "`id` desc";
 }
}