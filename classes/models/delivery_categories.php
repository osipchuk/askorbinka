<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 16.05.14
 */

/**
 * Модель категорий рассылки
 */
class delivery_categories extends a_model {

 /**
  * Параметр присутствия у модели мета-тегов
  * @return bool
  */
 protected function meta_tags_exist()
 {
  return FALSE;
 }

 /**
  * Массив полей, зависящих от языковой версии
  * @return array
  */
 protected function get_mysql_assoc_select()
 {
  return array('caption');
 }
}