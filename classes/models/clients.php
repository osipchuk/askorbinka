<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 14.08.14
 */

/**
 * Модель аккаунтов пользователей
 */
class clients extends a_simple_model {
 
 public function get_all($where = null, $items_on_page = 999999, $make_items_url = TRUE)
 {
  $data = parent::get_all($where, $items_on_page, $make_items_url);
  foreach ($data as &$line) $line['caption'] = $this->account_fio($line);
  
  return $data;
 }

 public function get_item($where = null, $make_items_url = TRUE)
 {
  $result = parent::get_item($where, $make_items_url);
  if (!empty($result)) $result['caption'] = $this->account_fio($result);
  return $result;
 }


 /**
  * Возвращает Ф.И.О.
  * @param array $account
  * @return string
  */
 public function account_fio($account)
 {
  return $account['name'].' '.$account['surname'];
 }

 /**
  * Возвращает аватар для аккаунта пользователя
  * @param array $account
  * @param int $width
  * @param int $height
  * @return bool|string
  */
 public function account_avatar($account, $width = 80, $height = 80)
 {
  if (empty($account) or !is_array($account)) return false;
  if (!empty($account['image']) or !empty($account['author_image'])) $image = 'project_images/'.($account['image'] ? : $account['author_image']); else $image = '../img/default-avatar.jpg';
  
  return '/admin/image.php?file='.$image.'&amp;width='.$width.'&amp;height='.$height;
 }

 /**
  * Возвращает ссылку на редактирования аккаунта клиента
  * @param int $client_id
  * @return string
  */
 public function get_client_admin_url($client_id)
 {
  $client_id = (int)$client_id;
  if ($client_id > 0 and $client = $this->get_item("`id`='$client_id'"))
  {
   return '<a href="admin.php?mode=clients&creat_mode=edit&id='.$client_id.'" target="_blank">'.$this->account_fio($client).'</a>';
  }

  return 'Быстрый заказ';
 }

 /**
  * Возвращает название типа аккаунта пользователя
  * @param array $account аккаунт
  * @return string|FALSE
  */
 public function account_type_caption($account)
 {
  $types = $this->get_account_types();
  if (array_key_exists($account['type'], $types)) return $types[$account['type']];
  return FALSE;
 }

 /**
  * Возвращает массив типов аккаунтов пользователей
  * @return array
  */
 public function get_account_types()
 {
  $types = $this->app_helper->get_global('client_account_types');
  return $types;
 }
 
 /**
  * Отправка информации о изменении аккаунта администратором сайта на почту пользователя
  * @param array $account_info
  * @param string $pass новый пароль
  */
 public function send_info_mail($account_info, $pass)
 {
  $form_model = new admin_edit_account_form();
  $form_model->add_send_email($account_info['email']);
  $form_model->set_fields($account_info)->set_field('password', $pass)->set_field('password_repeat', $pass);
  $form_model->send();
 }

 /**
  * Подтверждает аккаунт
  * @param int $id
  * @return bool
  * @throws Exception в случае ошибки
  */
 public function submit($id)
 {
	 $id = (int)$id;
	 $item = $this->get_item("`id`='$id'");
	 if (empty($item) or $item['type'] == 'pr')
		 throw new Exception($this->app_helper->getTranslateResolver()->_('you_cant_submit_pro_account', 'Подтверждением PRO-аккаунтов занимается администратор сайта'));
	 
     mysql_update($this->get_table_name(), "`submited`='1'", "`id`='$id'");
     if (mysql_error()) return FALSE;
	 
     return TRUE;
 }

	/**
	 * Проверяет номер кредитной карты по шаблону, в случае успеха преобразовывает в приятный вид
	 * @param string $card
	 * @return bool
	 */
	public function checkCreditCard(&$card)
	{
		$card = preg_replace("/[\s]+/", '', $card);
		if (!preg_match("/^[\d]{16}$/", $card)) return false;
		
		$card = preg_replace("/^([\d]{4})([\d]{4})([\d]{4})([\d]{4})$/", "$1 $2 $3 $4", $card);
		
		return true;
	}

 /**
  * Параметр сортировки
  * @return string
  */
 protected function get_order()
 {
  return "`{$this->get_table_name()}`.`submited` desc, `id` desc";
 }
}