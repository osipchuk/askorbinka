<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 22.05.14
 */

/**
 * Модель файлов импорта
 */
class import_files extends a_simple_model {

 /**
  * @var i_importer стратегия импорта
  */
 protected $importer;

 /**
  * @var string|null если задана - сохранять прайс-файлы в заданную директорию
  */
 protected $save_files_dir;

 /**
  * Проводит импорт товаров из загруженного файла
  * @return bool
  */
 public function import()
 {
  return $this->get_importer()->import($_FILES['file']);
 }
 
 /**
  * Устанавливает стратегию импорта
  * @param i_importer $importer
  * @return $this
  */
 public function set_importer(i_importer $importer)
 {
  $this->importer = $importer;

  return $this;
 }

 /**
  * Возвращает стратегию импорта
  * @return i_importer
  * @throws Exception если стратегия импорта не задана
  */
 public function get_importer()
 {
  if (is_null($this->importer)) throw new Exception('importer is null');
  return $this->importer;
 }

 /**
  * Устанавливают директорию для загрузки прайс-файлов
  * @param string $save_files_dir
  * @return $this
  */
 public function set_save_files_dir($save_files_dir)
 {
  $this->save_files_dir = $save_files_dir;

  return $this;
 }

 /**
  * Возвращает директорию для загрузки прайс-файлов
  * @return null|string
  */
 public function get_save_files_dir()
 {
  return $this->save_files_dir;
 }
 
 
 

 /**
  * Параметр сортировки
  * @return string
  */
 protected function get_order()
 {
  return "`{$this->get_table_name()}`.`id` desc";
 }
}