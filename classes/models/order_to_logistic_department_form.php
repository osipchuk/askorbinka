<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 23.03.15
 */

/**
 * Отправка заказа в логистический отдел
 */
class order_to_logistic_department_form extends order_processor
{

    public function load_from_db($order_id)
    {
        $result = parent::load_from_db($order_id);
        $this->set_field('goods_table', $this->cart->format_table(), FALSE);
        return $result;
    }

    public function submit_payments()
    {
        return true;
    }

    public function cancelOrder()
    {
        return true;
    }
    
    
    
    

    /**
     * @inheritdoc
     */
    protected function main_template_mode()
    {
        return 'order_to_logistic_department';
    }

    /**
     * @inheritdoc
     */
    protected function before_mail()
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    protected function after_mail()
    {
        return true;
    }
}
