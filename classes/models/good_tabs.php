<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 22.09.14
 */

/**
 * Модель вкладок товаров
 */
class good_tabs extends a_model {

 /**
  * Возвращает файлы для таба
  * @param int $tab_id
  * @return array
  */
 public function get_files($tab_id)
 {
  return mysql_data_assoc("select * ".mysql_assoc_select(array('caption'))." from `good_tab_files` where `external_parent`='".(int)$tab_id."' order by `rate` desc");
 }

 /**
  * Возвращает изображения с технологиями для таба
  * @param int $tab_id
  * @param bool $only_with_images
  * @return array
  */
 public function get_tech_images($tab_id, $only_with_images = FALSE)
 {
  return mysql_data_assoc("select * ".mysql_assoc_select(array('caption'))." from `good_tab_tech_images` where `external_parent`='".(int)$tab_id."'".($only_with_images ? " and `image`<>'' and `image_small`<>''" : '')." order by `rate` desc");
 }

 /**
  * Параметр присутствия у модели мета-тегов
  * @return bool
  */
 protected function meta_tags_exist()
 {
  return FALSE;
 }

 /**
  * Возвращает название модели-предка
  * @return string|null
  */
 public function external_model()
 {
  return 'goods';
 }
}