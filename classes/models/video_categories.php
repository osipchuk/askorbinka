<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 17.10.14
 */

/**
 * Модель видео категорий
 */
class video_categories extends a_model {

 /**
  * Возвращает название модели-предка
  * @return string|null
  */
 public function external_model()
 {
  return null;
 }
}