<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 20.10.14
 */

/**
 * Модель видеообзоров товаров
 */
class good_videos extends videos {

 /**
  * Выборка всех элементов с предком @param $parent
  * @param null $parent
  * @param null $where
  * @param int $items_on_page
  * @param bool $make_items_url
  * @return array
  */
 public function get_all_of_external_parent($parent = null, $where = null, $items_on_page = 999999, $make_items_url = TRUE)
 {
  return $this->get_all($this->mix_where($where, "`good`='".(int)$parent."'"), $items_on_page, $make_items_url);
 }

 /**
  * Возвращает название модели-предка
  * @return string|null
  */
 public function external_model()
 {
  return 'goods';
 }

 /**
  * Возвращает ключ для связи с родительским элементом
  * @return string|null
  */
 public function external_parent_key()
 {
  return 'good';
 }

	/**
	 * Возвращает массив элементов для фильров
	 * @param string $lang
	 * @param int|null $parent
	 * @param string|null $where
	 * @param int $items_on_page
	 * @return array
	 */
	public function filterGet($lang, $parent = null, $where = null, $items_on_page = 999999)
	{
		$goodsClass = $this->external_model();
		/** @var goods $goodsModel */
		$goodsModel = new $goodsClass;
		
		$goodsTable = $goodsModel->get_table_name();
		$videosTable = $this->get_table_name();
		
		
		$select = "select `$goodsTable`.*, `$videosTable`.`video_id`, `$videosTable`.`image` as `image` " . 
                           mysql_assoc_select($this->get_mysql_assoc_select(), $this->app_helper->getLang(), $this->get_table_name()) . "
		          from `$goodsTable` join `$videosTable` on `$videosTable`.`{$this->external_parent_key()}` = `$goodsTable`.`id`
		          ".(!empty($where) ? 'where '.$where : '');

		if (!$this->ignore_page_num) $count = count(mysql_data($select)); else $count = 0;

		if (!$this->ignore_page_num) {
			$this->app_controller->pagination()->set_items_on_page($items_on_page);
			$this->app_controller->pagination()->set_items_count($count);
		}

		$data = mysql_data_assoc($select." order by ".$this->get_order().' '.($this->ignore_page_num ? "limit 0, ".$items_on_page : $this->app_controller->pagination()->get_limit()));
		foreach ($data as &$line) $this->app_helper->get_url_maker()->item_url($line, $goodsModel); 
		
		return $data;
	}
	
	/**
	 * Возвращает массив категорий
	 * @return array
	 */
	public function getCategories()
	{
		/** @var a_tree_model $categoryModel */
		$categoryModel = $this->getCategoryModel();
		$structure = $categoryModel->get_structure();

		$goodsClass = $this->external_model();
		/** @var goods $goodsModel */
		$goodsModel = new $goodsClass;

		$categories = $categoryModel->get_all("`{$categoryModel->get_table_name()}`.`external_parent` is not null and
                                         `{$categoryModel->get_table_name()}`.`id` in 
                                         (select `external_parent` from `{$goodsModel->get_table_name()}` where `id` in (select `good` from `{$this->get_table_name()}`))");

		foreach ($categories as $index => $category)
		{
			$item = $category;
			$caption = $category['caption'];

			while ($item = $categoryModel->find_item_in_structure_by_id($structure, $item['parent']))
			{
				$caption = $item['caption'].' - '.$caption;
			}
			$categories[$index]['caption'] = $caption;
		}

		return $categories;
	}

	/**
	 * @inheritdoc
	 */
	public function getCategoryModel()
	{
		$className = 'categories';
		return new $className;
	}

	/**
	 * @inheritdoc
	 */
	public function category_where($category)
	{
		$goodsClass = $this->external_model();
		/** @var goods $goodsModel */
		$goodsModel = new $goodsClass;
		
		return "`{$goodsModel->get_table_name()}`.`external_parent`='".(int)$category."'";
	}

	/**
	 * @inheritdoc
	 */
	public function getBrands()
	{
		$brandsModel = $this->getBrandsModel();
		$goodsClass = $this->external_model();
		/** @var a_model $goodsModel */
		$goodsModel = new $goodsClass;
		
		return $brandsModel->get_all("`{$brandsModel->get_table_name()}`.`id` in
		  (select `{$goodsModel->get_table_name()}`.`brand`
		   from `{$goodsModel->get_table_name()}` join `{$this->get_table_name()}` on `{$goodsModel->get_table_name()}`.`id` = `{$this->get_table_name()}`.`good`)");
	}

	/**
	 * @inheritdoc
	 */
	public function brandWhere($brand)
	{
		$goodsClass = $this->external_model();
		/** @var a_model $goodsModel */
		$goodsModel = new $goodsClass;
		
		return "`{$goodsModel->get_table_name()}`.`brand`='".(int)$brand."'";
	}


}