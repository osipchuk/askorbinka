<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 12.08.14
 */

/**
 * Модель городов карты
 */
class map_cities extends a_simple_model {

 /**
  * @var google_maps|null
  */
 private $google_maps;

 /**
  * Возвращает массив всех адресов
  * @return array
  */
 public function get_all_addresses()
 {
  $result = array();
  $data = $this->get_addresses_model()->get_all();
  foreach ($data as $line)
  {
   $line['map'] = $this->get_google_maps()->coords_iframe_url($line['map_x'], $line['map_y'], $line['map_zoom'], $this->app_helper->getLang());
   $result[$line['external_parent']][$line['type'] == 's' ? 'service' : 'office'] = $line;
  }
  
  foreach ($result as $city_id => $addresses)
  {
   $city = reset($addresses);
   
   if (count($addresses) < 2)
   {
	$type = isset($addresses['service']) ? 'office' : 'service';
	$result[$city_id][$type] = array('city'=>$city['nearest_city'] ? $city['nearest_city'] : 27);
   }
   
   if ($city['align'] != 'c')
   {
	switch ($city['align'])
	{
	 case 'l': $align = 'left'; break;
	 case 'r': $align = 'right'; break;
	 default: $align = NULL; break;
	}
	
	if ($align) $result[$city_id]['side'] = $align; 
   }
  }
  
  return $result;
 }

 /**
  * Возвращает массив адрессов для города
  * @param int|array $city id города, или массив значений из БД
  * @return array
  * @throws Exception если город не найден
  */
 public function get_city_addresses($city)
 {
  if (!is_array($city)) $city = $this->get_item("`id`='".(int)$city."'");
  if (empty($city)) throw new Exception('City error =(');
  
  $result = array();
  $types = array();
  
  $data = $this->get_addresses_model()->get_all_of_external_parent($city['id']);
  foreach ($data as $line)
  {
   $line['city'] = 'current';
   $line['city_caption'] = $city['caption'];
   $result[] = $line;
   $types[$line['type']] = 1;
  }
  
  if (count($types) < 2 and ($nearest_city_id = (int)$city['nearest_city']) > 0 and $nearest_city = $this->get_item("`id`='$nearest_city_id'"))
  {
   $nearest_addresses = $this->get_addresses_model()->get_all_of_external_parent($nearest_city_id);
   foreach ($nearest_addresses as $nearest_address)
   {
	if (!array_key_exists($nearest_address['type'], $types))
	{
	 $nearest_address['city'] = 'nearest';
	 $nearest_address['city_caption'] = $nearest_city['caption'];
	 $result[] = $nearest_address;
	}
   }
  }
  
  return $result;
 }
 
 /**
  * Массив полей, зависящих от языковой версии
  * @return array
  */
 protected function get_mysql_assoc_select()
 {
  return array('caption');
 }

 /**
  * Параметр сортировки
  * @return string
  */
 protected function get_order()
 {
  return "`{$this->get_table_name()}`.`caption_{$this->app_helper->getLang()}`";
 }

 /**
  * Возвращает модель адресов
  * @return map_addresses
  */
 private function get_addresses_model()
 {
  return new map_addresses();
 }

 /**
  * Возвращает google_maps библиотеку
  * @return google_maps
  */
 private function get_google_maps()
 {
  if (empty($this->google_maps))
  {
   $this->google_maps = new google_maps();
   $this->google_maps->set_lang($this->app_helper->getLang());
  }
  
  return $this->google_maps;
 }
}