<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 23.10.14
 */

/**
 * Модель ссылок на странице товара
 */
class catalog_page_links extends a_simple_model {

 /**
  * Возвращает more-ссылку
  * @return array
  */
 public function getMoreLink()
 {
  return $this->get_item("`mode`='more'");
 }

 /**
  * Возвращает все ссылки кроме more
  * @return array
  */
 public function getNotMoreLinks()
 {
  return $this->get_all("`mode`<>'more'");
 }

 /**
  * Массив полей, зависящих от языковой версии
  * @return array
  */
 protected function get_mysql_assoc_select()
 {
  return array('caption');
 }
}