<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 30.10.13
 * Time: 18:04
 */

class applicationHelper {
 protected static $instance;
 protected $lang;

 /**
  * @var meta_tags_resolver
  */
 protected $meta_tags_resolver;

 /**
  * @var Smarty
  */
 protected $smarty;

 /**
  * @var a_structure
  */
 protected $site_structure;

 /**
  * @var url_maker
  */
 protected $url_maker;

 /**
  * @var amountMakerContainer
  */
 protected static $amountMaker;

 public static function getInstance() {
  if (empty(self::$instance)) {
   self::$instance = new applicationHelper();
  }
  return self::$instance;
 }

 public function getLang() {
  if (empty($this->lang)) {
   global $prime_lang;
   $this->lang = $prime_lang;
  }

  return $this->lang;
 }

 public static function getAppController() {
  return applicationController::getInstance();
 }

 public static function getTranslateResolver() {
  return translatesResolver::getInstance();
 }

 /**
  * @return meta_tags_resolver
  */
 public function get_meta_tags_resolver()
 {
  if (empty($this->meta_tags_resolver)) $this->meta_tags_resolver = new meta_tags_resolver();
  return $this->meta_tags_resolver;
 }

 /**
  * Возвращает структуру сайта
  * @return structure
  */
 public function get_site_structure()
 {
  if (empty($this->site_structure)) $this->site_structure = new structure();
  return $this->site_structure;
 }

 public function get_url_maker()
 {
  if (empty($this->url_maker)) $this->url_maker = new url_maker($this->get_site_structure());
  return $this->url_maker;
 }

 public function getSmarty() {
  if (empty($this->smarty)) {
   global $smartySettings;
   $this->smarty = new Smarty();

   $this->smarty->template_dir = $smartySettings['smarty_template_dir'];
   $this->smarty->compile_dir = $smartySettings['smarty_compile_dir'];
   $this->smarty->config_dir = $smartySettings['smarty_config_dir'];
   $this->smarty->cache_dir = $smartySettings['smarty_cache_dir'];

   $this->smarty->debugging = $smartySettings['smarty_debugging'];
   $this->smarty->compile_check = $smartySettings['smarty_compile_check'];
   $this->smarty->force_compile = $smartySettings['smarty_force_compile'];
   $this->smarty->caching = $smartySettings['smarty_caching'];
   $this->smarty->use_sub_dirs = $smartySettings['smarty_use_sub_dirs'];
   $this->smarty->config_load('config.conf');
  }

  return $this->smarty;
 }

 public function get_global($global)
 {
  global $$global;
  return $$global;
 }

 /**
  * Возвращает флаг production
  * @return bool
  */
 public function get_production()
 {
  return (bool)$this->get_global('production');
 }

 /**
  * Возвращает amountMakerContainer
  * @param array $config
  * @return amountMakerContainer
  */
 public static function getAmountMaker($config = [])
 {
  if (is_null(self::$amountMaker) or self::$amountMaker->getConfig() != $config)
  {
   self::$amountMaker = amountMakerContainer::init($config);
  }
  
  return self::$amountMaker;
 }





 protected function __construct() {
  get_modes($this->lang, $page);
 }
} 