<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 12.05.14
 */

/**
 * Класс для выбора контроллера на основе запроса пользователя
 * 
 * Общий функционал для всех проектов
 */
abstract class a_routing {

 /**
  * @var array
  */
 protected $modes_array;

 /**
  * @var applicationHelper
  */
 protected $app_helper;

 /**
  * @var a_structure
  */
 protected $site_structure;

 /**
  * @var string|null url выбранного элемента подраздела
  */
 protected $static;

 /**
  * @var int index в modes_array, начиная с которого управление передается следующему модулю
  */
 protected $static_index = -1;

 /**
  * @var int|null ID раздела сайта
  */
 protected $depart_id;

 public function __construct(a_structure $site_structure, applicationHelper $app_helper)
 {
  $this->site_structure = $site_structure;
  $this->app_helper = $app_helper;
 }

 /**
  * Устанавивает modes_array
  * @param array $modes_array
  * @return $this
  */
 public function set_modes_array($modes_array)
 {
  $this->modes_array = $modes_array;

  return $this;
 }

 /**
  * Возвращает modes_array
  * @return array
  */
 public function get_modes_array()
 {
  if (!is_array($this->modes_array)) return array();
  return $this->modes_array;
 }

 /**
  * Устанавливает static
  * @param null|string $static
  * @return $this
  */
 public function set_static($static)
 {
  if (!is_string($static) or empty($static)) $static = NULL;
  $this->static = $static;

  return $this;
 }

 /**
  * Возвращает static
  * @return null|string
  */
 public function get_static()
 {
  return $this->static;
 }

 /**
  * Устанавливает static_index
  * @param int $static_index
  * @return $this
  */
 public function set_static_index($static_index)
 {
  $static_index = (int)$static_index;
  if ($static_index < 0) $static_index = 0;
  
  $this->static_index = $static_index;

  return $this;
 }

 /**
  * Возвращает static_index
  * @return int
  */
 public function get_static_index()
 {
  return $this->static_index;
 }

 /**
  * Возвращает объект контроллера
  * @return a_controller
  * @throws Exception_404
  * @throws Exception
  */
 public function select_controller()
 {
  $controller_name = $this->select_controller_name();
  if (!class_exists($controller_name)) throw new Exception('Controller "'.$controller_name.'" not found =(');
  if (!is_subclass_of($controller_name, 'a_controller')) throw new Exception('Controller "'.$controller_name.'" is not extends a_controller');
  $controller = new $controller_name;
  $controller->set_depart_id($this->depart_id);
  
  return $controller;
 }

 /**
  * Выбирает имя контроллера на основе запроса
  * @return string
  * @throws Exception_404
  */
 protected function select_controller_name()
 {
  switch ($this->modes_array[0])
  {
   case 'ajax':
    if (empty($this->modes_array[1])) throw new Exception_404();
    $result = $this->modes_array[1];
    break;

   case 'captcha':
   case 'static_template':
    $result = $this->modes_array[0];
    break;
   
   default:
    $structure = $this->site_structure->get_structure();

    if ($this->modes_array[0] != 'start')
    {
     $item_modules = $this->app_helper->get_global('items_modules');
     foreach ($this->modes_array as $index => $mode)
     {
      $prev_item = isset($item) ? $item : NULL;
      $item = $this->site_structure->find_item_in_structure_by_static($structure, $mode);
      if (empty($item))
      {
       if (!is_null($prev_item) and array_key_exists($prev_item['module'], $item_modules) and ($item_modules[$prev_item['module']] == 'n' or (count($this->modes_array)-$index) <= $item_modules[$prev_item['module']]))
       {
        $item = $prev_item;
        $this->set_static_index($index);
        $this->set_static($mode);
        break;
       }
       else throw new Exception_404();
      }

      $structure = array_key_exists('sub', $item) ? $item['sub'] : NULL;
     }
    }

    if (!isset($item) or (is_array($item) and $item['start']))
    {
     $result = 'start';
     $item = $this->site_structure->get_item("`start`='1'");
    }
    else
    {
     $result = $item['controller'] ? $item['controller'] : $item['module'];
    }
    
    if (is_array($item) and array_key_exists('id', $item)) $this->depart_id = $item['id'];
    break;
  }
  
  return $result.'_controller';
 }
}