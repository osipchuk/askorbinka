<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 12.05.14
 */

/**
 * Класс для формирования url'ов
 */
class url_maker {

 /**
  * @var a_tree_model структура сайта
  */
 protected $structure;

 public function __construct(a_tree_model $structure)
 {
  $this->set_structure($structure);
 }

 /**
  * Устанавливает структуру на основе которой нужно формировать урл
  * @param a_tree_model $structure
  * @return $this
  */
 public function set_structure(a_tree_model $structure)
 {
  $this->structure = $structure;
  return $this;
 }

 /**
  * Возвращает url раздела сайта
  * @param int $id раздел сайта
  * @return string
  */
 public function d_id_url($id)
 {
  return $this->path_for_tree_model($this->structure, $id);
 }

 /**
  * Возвращает url для переданного модуля
  * @param string $module
  * @return string
  */
 public function d_module_url($module)
 {
  $item = $this->structure->find_item_in_structure_by_module($this->structure->get_structure(), $module);
  if (is_array($item) and array_key_exists('id', $item)) return $this->d_id_url($item['id']);
  return '';
 }

 /**
  * Возвращает url для переданного элемента (статьи, новости, товара, ...)
  * Добавляет к массиву $item элемент 'url'
  * @param array $item
  * @param a_model|null $model
  * @return array
  */
 public function item_url(&$item, $model = NULL)
 {
  $item['url'] = $this->make_item_url($item, $model);
  
  return $item;
 }

 /**
  * @param $url
  * @return string
  */
 public function url($url)
 {
//  TODO: Провести рефакторинг, сделать неограниченное количество параметров
  
  return url($url);
 }


 /**
  * Возвращает url для элемента id переданной модели
  * @param a_tree_model $model
  * @param int $id
  * @param array|null $ITEM - корневой элемент древовидной структуры
  * @return string
  */
 protected function path_for_tree_model(a_tree_model $model, $id, &$ITEM = NULL)
 {
  $result = '';

  while ($item = $model->get_structure_item($id) and is_array($item))
  {
   $result = $item['static'].(!empty($result) ? '/'.$result : '');
   $id = $item['parent'];
   $ITEM = $item;
  }

  return $result;
 }


 /**
  * 
  * @param array $item
  * @param a_model $model
  * @return string
  */
 public function make_item_url($item, $model = NULL)
 {
  $url = '';
  
  if (is_subclass_of($model, 'a_tree_model'))
  {
   $url = $this->path_for_tree_model($model, $item['id'], $root_tree_item);
  }
  elseif (array_key_exists('static', $item))
  {
   $url = $item['static'];
   $root_tree_item = $item;
  }
  else $root_tree_item = NULL;
  
  
  
  $external_model_name = $model->external_model();
  if (!empty($external_model_name) and is_array($root_tree_item) and array_key_exists('external_parent', $root_tree_item) and !empty($root_tree_item['external_parent']))
  {
   $external_model = new $external_model_name();
   $root_tree_item = array('id'=>$root_tree_item['external_parent']);
   
   $parent_url = $this->make_item_url($root_tree_item, $external_model);
   if (!empty($parent_url)) $url = $parent_url.'/'.$url; 
  }
   
  return $url;
 }
}