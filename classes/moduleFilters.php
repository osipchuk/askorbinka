<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 21.10.14
 */

/**
 * Класс для применения фильтров стандартных модулей
 */
class moduleFilters {

 /**
  * @var a_filter_model главная модель
  */
 private $model;

 /**
  * @var applicationHelper
  */
 private $appHelper;

 /**
  * @var applicationController
  */
 private $appController;

 /**
  * @var array параметры фильтров
  */
 private $filters = [
  'category' => null,
  'brand' => null,
  'dateBegin' => null,
  'dateEnd' => null,
  'limit'=>10
 ];

 /**
  * @var array доступные значения limits
  */
 private $limits = [10, 20, 50, 100];

 /**
  * Конструктор
  * @param a_filter_model $model главная модель
  */
 public function __construct(a_filter_model $model)
 {
  $this->appHelper = applicationHelper::getInstance();
  $this->appController = $this->appHelper->getAppController();
  $this->setModel($model);
  $this->setFiltersFromGet();
 }



 /**
  * Возвращает результат применения фильтров к модели
  * @param int $depart_id id раздела
  * @return array
  */
 public function getData($depart_id)
 {
  $model = $this->getModel();
  if ($this->getFilter('category')) $where = $this->getModel()->category_where($this->getFilter('category')); else $where = '';
  if ($this->getFilter('brand')) $where = $model->mix_where($where, $this->getModel()->brandWhere($this->getFilter('brand')));
  if ($this->getFilter('dateBegin')) $where = $model->mix_where($where, "`{$model->get_table_name()}`.`date`>='{$this->getFilter('dateBegin')}'");
  if ($this->getFilter('dateEnd')) $where = $model->mix_where($where, "`{$model->get_table_name()}`.`date`<='{$this->getFilter('dateEnd')}'");
  
  $data =  $this->getModel()->filterGet($this->appHelper->getLang(), $depart_id, $where, $this->getFilter('limit') ? : 999999 );
  return $data;
 }

 /**
  * Возвращает активные даты после применения фильтров к модели
  * Если дата в модели отсутствует - возвращает пустой массив
  * @param int $depart_id
  * @return array
  */
 public function getActiveDates($depart_id)
 {
  $dateBegin = $this->getFilter('dateBegin');
  $dateEnd = $this->getFilter('dateEnd');
  
  $this->setFilter('dateBegin', null)->setFilter('dateEnd', null);
  $data = $this->getData($depart_id);
  $this->setFilter('dateBegin', $dateBegin)->setFilter('dateEnd', $dateEnd);
  
  $result = [];
  $first_elem = reset($data);
  if (is_array($first_elem) and array_key_exists('date', $first_elem))
  {
   foreach ($data as $line)
   {
	$date = date("Y-n-j", $time = strtotime($line['date']));
	if (!array_key_exists($date, $result)) $result[] = $date;
   }
  }
  
  
  return $result;
 }

 /**
  * Возвращает список категорий
  * @return array
  */
 public function getCategories()
 {
  $allCaption = $this->appHelper->getTranslateResolver()->_('articles_header_categories_filter_all', 'Все');
  
  return array_merge(array(array('id' => '', 'caption' => $allCaption)), $this->getModel()->getCategories());
 }

 /**
  * Генерирует строку для передачи фильтров через GET
  * @param string|null $changedParam параметр, который нужно изменить
  * @param string|null $changedParamValue значение параметра
  * @return string
  */
 public function getParamsStr($changedParam = null, $changedParamValue = null)
 {
  $result = [];
  foreach ($this->filters as $filter => $value) if (!empty($value)) $result[$filter] = $value;
  if (!empty($changedParam) and array_key_exists($changedParam, $this->filters)) 
  {
   if (empty($changedParamValue))
   {
    unset($result[$changedParam]);
   }
   else
   {
    $result[$changedParam] = $changedParamValue;
   }
  }
  
  return http_build_query($result);
 }
 
 

 /**
  * Возвращает главную модуль
  * @return a_filter_model
  */
 public function getModel()
 {
  return $this->model;
 }

 /**
  * Устанавливает главную модель
  * @param a_filter_model $model
  * @return $this
  */
 public function setModel(a_filter_model $model)
 {
  $this->model = $model;

  return $this;
 }

 /**
  * Возвращает модель категорий
  * @return a_model|null
  */
 public function getCategoriesModel()
 {
  return $this->getModel()->getCategoryModel();
 }

 /**
  * Возвращает название выбранной категории
  * @return string
  */
 public function getCategoryCaption()
 {
  return $this->getModel()->getCategoryCaption($this->getFilter('category'));
 }

	/**
	 * Возвращает модель брендов
	 * @return a_model
	 */
	public function getBrandsModel()
	{
		return $this->getModel()->getBrandsModel();
	}

	/**
	 * Возвращает массив брендов
	 * @return array
	 */
	public function getBrands()
	{
		return $this->getModel()->getBrands();
	}

	/**
	 * Возвращает название выбранного бренда
	 * @return string
	 */
	public function getBrandCaption()
	{
		$brandsModel = $this->getBrandsModel();
		if (empty($brandsModel)) return '';
		$item = $brandsModel->get_item("`{$brandsModel->get_table_name()}`.`id`='".(int)$this->getFilter('brand')."'");
		if (empty($item)) return '';
		
		return $item['caption'];
	}
 
 
 

 /**
  * Устанавливает значение фильтра
  * @param string $filter
  * @param string|int|null $value
  * @return $this
  */
 public function setFilter($filter, $value)
 {
  if ($value == 'null') $value = null;
  if (array_key_exists($filter, $this->filters)) $this->filters[$filter] = $value;
  return $this;
 }

 /**
  * Возвращает значение фильтра
  * @param string $filter
  * @return string|int|null
  */
 public function getFilter($filter)
 {
  if (array_key_exists($filter, $this->filters)) return $this->filters[$filter];
  return null;
 }

 /**
  * Возвращает массив всех фильтров
  * @return array
  */
 public function getFilters()
 {
  return $this->filters;
 }

 /**
  * Возвращает
  * @return array
  */
 public function getLimits()
 {
  return $this->limits;
 }

 /**
  * Устанавливает
  * @param array $limits
  * @return $this
  */
 public function setLimits(array $limits)
 {
  $this->limits = $limits;
  
  if (!in_array($this->getFilter('limit'), $this->limits)) $this->setFilter('limit', $limits[0]);

  return $this;
 }
 
 


 /**
  * Устанавливает все фильтры на основе GET-параметров
  * @return $this
  */
 private function setFiltersFromGet()
 {
  foreach ($_GET as $filter => $value) $this->setFilter($filter, $value);
  return $this;
 }
}