<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 02.04.14
 */

/**
 * Оплата в терминале
 */
class pay_terminal extends a_simple_pay {

 /**
  * @inheritdoc
  */
 protected function page_mode()
 {
  return 'terminal';
 }
}