<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 26.11.14
 */

/**
 * Оплата бонусами
 */
class pay_bonuses extends a_simple_pay {

	/**
	 * Возвращает идентификатор страницы в таблице текстов
	 * @return string
	 */
	protected function page_mode()
	{
		return 'bonuses';
	}
}