<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 29.10.14
 */

/**
 * Наложенные платеж
 */
class pay_pod extends a_simple_pay {

 /**
  * Возвращает идентификатор страницы в таблице текстов
  * @return string
  */
 protected function page_mode()
 {
  return 'pod';
 }
}