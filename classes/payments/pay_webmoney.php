<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 28.10.14
 */

/**
 * Контроллер оплаты webmoney
 */
class pay_webmoney extends a_pay {

 /**
  * Метод для запуска процесса оплаты
  * @return string
  * @throws Exception в случае ошибки
  */
 public function process()
 {
  return '<form action="'.$this->get_requisite('server').'" method="POST" style="display: none;" id="pay-form">
           <input type="text" name="LMI_PAYMENT_AMOUNT" value="'.$this->get_amount().'">
           <input type="hidden" name="LMI_PAYEE_PURSE" value="'.$this->get_requisite('LMI_PAYEE_PURSE').'">
           <input type="hidden" name="LMI_PAYMENT_NO" value="'.$this->get_order_id().'">
           <input type="hidden" name="LMI_PAYMENT_DESC_BASE64" value="'.base64_encode($this->generate_payment_describe()).'">
           <input type="hidden" name="LMI_RESULT_URL" value="'.$this->get_result_url().'">
           <input type="hidden" name="LMI_SUCCESS_URL" value="'.$this->get_server_url().'">
          </form>
          <script> $("#pay-form").submit(); </script>';
 }

 /**
  * Метод для подтверждения оплаты
  * @return bool
  * @throws Exception в случае ошибки
  */
 public function submit()
 {
  extract($_POST);
  $payment_info = print_r($_POST, true);
  
  if (!isset($LMI_PAYMENT_NO) or ($order_id = (int)$LMI_PAYMENT_NO) <= 0) throw new Exception('order id error', -1);
  
  $this->set_order_id($order_id);
  if ($this->order_processor->get_order_status($order_id) == 1) return true;

  if (!isset($LMI_HASH) or !$this->check_hash($_POST, $LMI_HASH))
  {
   $this->order_processor->cancelOrder();
   $this->insert_payment_info($this->get_order_id(), $this->get_amount(), -1, $payment_info);
   throw new Exception('hash error', -1);
  }

  $this->insert_payment_info($this->get_order_id(), $this->get_amount(), 1, $payment_info);
  return $this->order_processor->submit_payments();
 }

 /**
  * Получение реквизитов для оплаты из БД
  * @return bool
  */
 protected function load_requisites()
 {
  $settings = $this->app_helper->get_global('webmoney_settings');
  $ok = TRUE;

  foreach ($settings as $param => $value)
  {
   $result = $this->set_requisite($param, $value);
   if (!$result) $ok = FALSE;
  }
  
  return $ok;
 }

 /**
  * Проверяет переданный hash
  * @param array $response ответ сервера
  * @param string $hash
  * @return bool
  */
 private function check_hash(array $response, $hash)
 {
  $str = '';
  $parameters = array('LMI_PAYEE_PURSE', 'LMI_PAYMENT_AMOUNT', 'LMI_PAYMENT_NO', 'LMI_MODE', 'LMI_SYS_INVS_NO', 'LMI_SYS_TRANS_NO', 'LMI_SYS_TRANS_DATE', 'LMI_SECRET_KEY', 'LMI_PAYER_PURSE', 'LMI_PAYER_WM');
  
  foreach ($parameters as $parameter)
  {
   if (array_key_exists($parameter, $response)) $str .= $response[$parameter];
   else
   {
	switch ($parameter)
	{
	 case 'LMI_SECRET_KEY':
	  $str .= $this->get_requisite('SecretKey');
	  break;
	}
   }
  }


  return $hash == strtoupper(hash('sha256', $str));
 }
}