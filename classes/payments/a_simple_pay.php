<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 29.10.14
 */

/**
 * Простой способ оплаты
 * Вывод справочной информации
 */
abstract class a_simple_pay extends a_pay {

 /**
  * @var a_model|null
  */
 protected $text_model;

 /**
  * @var checkPrinter
  */
 protected $checkPrinter;

 public function __construct()
 {
  parent::__construct();
  $this->checkPrinter = new checkPrinter();
 }

 /**
  * Возвращает идентификатор страницы в таблице текстов
  * @return string
  */
 abstract protected function page_mode();

 /**
  * Метод для запуска процесса оплаты
  * @return string
  * @throws Exception в случае ошибки
  */
 public function process()
 {
  return '<div class="pay-info-wrapper">'.put_content($this->replace_template_variables($this->get_requisite('text'))).'</div>';
 }

 /**
  * Метод для подтверждения оплаты
  * @return bool
  * @throws Exception в случае ошибки
  */
 public function submit()
 {
  throw new \Exception('You can\'t submit payment by this system');
 }

 /**
  * Возвращает модель текстовых страниц
  * @return a_model
  */
 public function get_text_model()
 {
  if (empty($this->text_model))
  {
   $this->text_model = new payment_texts();
   $this->text_model->ignore_page_num(true);
  }
  
  return $this->text_model;
 }


 /**
  * Получение реквизитов для оплаты из БД
  * @return bool
  */
 protected function load_requisites()
 {
  $line = $this->get_text_model()->get_item($where = "`mode`='".safe($this->page_mode())."'");
  if (is_array($line))
  {
   foreach ($line as $param => $value) $this->set_requisite($param, $value);
   return TRUE;
  }

  return FALSE;
 }

 /**
  * Формирует ссылку на скачивание счета-фактуры
  * @return string
  */
 private function format_check_url()
 {
  return url($this->app_helper->get_url_maker()->d_module_url('download_check').'/'.$this->get_order_id().'/'.$this->checkPrinter->generateHash($this->get_order_id(), $this->get_amount()));
 }

 /**
  * Заменяет переменные в шаблоне
  * @param string $content
  * @return string
  * @throws Exception
  */
 protected function replace_template_variables($content)
 {
  $result = preg_replace("/(src|href)=([\"|\'])\/admin\/([{]+)/", "$1=$2$3", $content);
  return str_replace(['{$amount}', '{$description}', '{$check_url}'] , [$this->get_amount(), $this->generate_payment_describe(), $this->format_check_url()], $result);
 }
}