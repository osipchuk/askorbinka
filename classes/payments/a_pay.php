<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 05.02.14
 */

/**
 * Абстрактный класс платежной системы
 */
abstract class a_pay {
 /**
  * @var array реквизиты оплаты
  */
 protected $requisites = array();

 /**
  * @var applicationHelper
  */
 protected $app_helper;

 /**
  * @var string урл, на который будет возвращаться пользователь после оплаты
  */
 protected $result_url;

 /**
  * @var string урл, на который будет отправляться ответ от сервера
  */
 protected $server_url;

 /**
  * @var int|string id заказа внутри системы пользователя
  */
 protected $order_id;

 /**
  * @var float сумма оплаты
  */
 protected $amount;

 /**
  * @var string валюта
  */
 protected $currency;

 /**
  * @var string назначение (описание) платежа
  */
 protected $description;

 /**
  * @var string плательщик
  */
 protected $payer;

 /**
  * @var bool флаг тестового режима платежной системы
  */
 protected $testing = FALSE;

 /**
  * @var order_processor контроллер обработки заказов
  */
 protected $order_processor;


 public function __construct()
 {
  $this->app_helper = applicationHelper::getInstance();
  $this->order_processor = new order_processor();
  if (!($this->load_requisites())) throw new Exception('Ошибка получения реквизитов');
 }

 /**
  * Устанавливает url, на который будет отправляться ответ от сервера
  * @param string $url
  * @return $this
  */
 public function set_result_url($url)
 {
  $this->result_url = $url;
  return $this;
 }

 /**
  * Устанавливает url, на который будет возвращаеться пользователь
  * @param string $url
  * @return $this
  */
 public function set_server_url($url)
 {
  $this->server_url = $url;
  return $this;
 }

 /**
  * Устанавливает id заказа
  * @param int|string $id
  * @return $this
  * @throws Exception в случае отстутствия заказа
  */
 public function set_order_id($id)
 {
  $this->order_id = $id;
  $this->order_processor->load_from_db($this->order_id);
  $summary = $this->order_processor->get_cart_summary();
  $this->set_amount($summary['price']);

  $this->set_currency('UAH');
  
  return $this;
 }

 /**
  * Возвращает id заказа
  * @return int|string
  */
 public function get_order_id()
 {
  return $this->order_id;
 }

 /**
  * Устанавливает сумму оплаты
  * @param float $amount
  * @return $this
  */
 public function set_amount($amount)
 {
  $this->amount = (float)$amount;
  return $this;
 }

 /**
  * Устанавливает валюту оплаты
  * @param string $currency
  * @return $this
  */
 public function set_currency($currency)
 {
  $this->currency = $currency;
  return $this;
 }
 
 /**
  * Возвращает валюту оплаты
  * @return string
  */
 public function get_currency()
 {
  return $this->currency;
 }

 /**
  * Усанавливает назначение (описание) платежа
  * @param string $description
  * @return $this
  */
 public function set_description($description)
 {
  $this->description = $description;
  return $this;
 }

 /**
  * Устанавливает плательщика
  * @param string $payer
  * @return $this
  */
 public function set_payer($payer)
 {
  $this->payer = $payer;
  return $this;
 }

 /**
  * Шаблон отображения страницы
  * @return string
  */
 public function template()
 {
  return 'default/text.tpl';
 }

 /**
  * @param bool $mode
  * @return $this
  */
 public function set_testing_mode($mode)
 {
  $this->testing = (bool)$mode;
  return $this;
 }
 
 /**
  * Метод для запуска процесса оплаты
  * @return string
  * @throws Exception в случае ошибки
  */
 abstract public function process();

 /**
  * Метод для подтверждения оплаты
  * @return bool
  * @throws Exception в случае ошибки
  */
 abstract public function submit();
 

 
 
 /**
  * Возвращает значение атрибута
  * @param string $param
  * @return mixed
  */
 protected function get_requisite($param)
 {
  if (array_key_exists($param, $this->requisites)) return $this->requisites[$param]; else return null;
 }

 /**
  * Устанавливает значение параметра
  * @param string $param
  * @param mixed $value
  * @return $this
  */
 protected function set_requisite($param, $value)
 {
  $this->requisites[$param] = $value;
  return !empty($value);
 }

 /**
  * Возвращает url для возврата пользователя
  * @return string
  */
 protected function get_result_url()
 {
  return $this->result_url;
 }

 /**
  * Возвращает url, на который будет отправляться ответ от сервера. Если не задан, возвращает result_url
  * @return string
  */
 protected function get_server_url()
 {
  return !empty($this->server_url) ? $this->server_url : $this->result_url;
 }

 /**
  * Возвращает сумму оплаты
  * @return float
  */
 protected function get_amount()
 {
  return (float)$this->amount;
 }

 /**
  * Возвращает назначение (описание) платежа
  * @return string
  */
 protected function get_description()
 {
  return $this->description;
 }

 /**
  * Возвращает плательщика
  * @return string
  */
 protected function get_payer()
 {
  return $this->payer;
 }

 /**
  * Конвертирует xml строку в простой объект
  * @param string $xml
  * @return SimpleXMLElement|null
  */
 protected function parse_xml($xml)
 {
  try {
   return new SimpleXMLElement($xml);
  }
  catch (Exception $e)
  {
   return null;
  }
 }

 /**
  * Вставка ответа в таблицу БД
  * @param int $order_id|null
  * @param float $amount сумма платежа
  * @param int $status
  * @param string|array $response
  * @return bool|int
  */
 protected function insert_payment_info($order_id, $amount, $status, $response)
 {
  $order_id = (int)$order_id;
  $amount = (float)$amount;
  if ($order_id == 0) $order_id = NULL;
  if (is_array($response)) $response = print_r($response, TRUE);
  
  return mysql_insert('payments', "`amount`='$amount', `order_id`= $order_id, `status`='".(int)$status."', `response`='".safe($response)."', `controller`='".safe(get_class($this))."'");
 }

 /**
  * Проверяет статус оплаты заказа
  * @param int $order_id
  * @return int mixed
  * @throws Exception
  */
 protected function get_order_status($order_id)
 {
  $order_id = (int)$order_id;
  $line = mysql_line("select `status` from `orders` where `id`='$order_id'");
  if (count(($line)) == 0) throw new Exception('order error');
  return $line[0];
 }

 /**
  * Генерирует строку назначения платежа на основе переменной из переводов и id заказа
  * @return string
  * @throws Exception
  */
 protected function generate_payment_describe()
 {
  $words = $this->app_helper->getTranslateResolver();
  
  $order_id = $this->get_order_id();
  if (empty($order_id)) throw new Exception('order id error');
  return str_replace('{$order_id}', $order_id, $words->_('payment_describe', 'Оплата заказа #{$order_id}'));
 }

 
 /**
  * Получение реквизитов для оплаты из БД
  * @return bool
  */
 abstract protected function load_requisites();
}