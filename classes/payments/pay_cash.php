<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 29.10.14
 */

/**
 * Оплата наличными
 */
class pay_cash extends a_simple_pay {

 /**
  * Возвращает идентификатор страницы в таблице текстов
  * @return string
  */
 protected function page_mode()
 {
  return 'cash';
 }
}