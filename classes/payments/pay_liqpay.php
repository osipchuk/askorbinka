<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 14.06.14
 */

/**
 * Класс для оплаты через систему liqpay
 */
class pay_liqpay extends a_pay {

 /**
  * @var LiqPay
  */
 protected $sdk;

 public function __construct()
 {
  parent::__construct();
  require_once __DIR__.'/liqpay-sdk-php/LiqPay.php';
  $this->sdk = new LiqPay($this->get_requisite('public_key'), $this->get_requisite('private_key'));
 }

 /**
  * Метод для запуска процесса оплаты
  * @return string
  * @throws Exception в случае ошибки
  */
 public function process()
 {
  $params = array(
   'amount'         => $this->get_amount() + ($this->get_amount() / 100 * 2.75),
   'currency'       => $this->get_currency(),
   'description'    => $this->generate_payment_describe(),
   'order_id'       => $this->get_order_id(),
   'type'           => 'buy', 
   'pay_way'		=> 'card', 
   'sandbox'		=> $this->testing, 
   'result_url'		=> $this->get_result_url(),
   'server_url'		=> $this->get_server_url()
  );
  
  return '<div class="pay-button-wrapper"><div class="hidden">'.$this->sdk->cnb_form($params).'</div> <script> $(".pay-button-wrapper form").submit(); </script></div>';
 }

 /**
  * Метод для подтверждения оплаты
  * @return bool
  * @throws Exception в случае ошибки
  */
 public function submit()
 {
  if (empty($_POST)) throw new Exception();
   
  $payment = $_POST['order_id'];
  $status = $_POST['status'];
  $signature = $_POST['signature'];


  if (!empty($payment) and !empty($signature))
  {
   $this->set_order_id($payment);

   if ($this->check_signature($signature, $_POST))
   {
    switch (strtolower($_POST['status']))
    {
     case 'success':
     case 'sandbox':
      $status = 1; $this->order_processor->submit_payments();
      break;

     case 'failure':
      $status = -1;
      $this->order_processor->cancelOrder();
      break;

     default:
      $status = 0;
      break;
    }
    
    $this->insert_payment_info($this->get_order_id(), $this->get_amount(), $status, print_r($_POST, TRUE));
    if ($status == 1) return $status;
   }
   
   if ($status != 1) $this->insert_payment_info($this->get_order_id(), $this->get_amount(), $status, print_r($_POST, TRUE));
  }

  throw new Exception('error');
 }

 /**
  * Получение реквизитов для оплаты из БД
  * @return bool
  */
 protected function load_requisites()
 {
  $settings = $this->app_helper->get_global('liqpay_settings');
  $ok = TRUE;

  foreach ($settings as $param => $value)
  {
   $result = $this->set_requisite($param, $value);
   if (!$result) $ok = FALSE;
  }

  return $ok;
 }

 /**
  * Генерирует строку назначения платежа на основе переменной из переводов и id заказа
  * @return string
  * @throws Exception
  */
 protected function generate_payment_describe()
 {
  return str_replace('-', ' ', translit(parent::generate_payment_describe()));
 }

 /**
  * Генерирует сигнатуру на основе ответа сервера и сравнивает с эталонной сигнатурой
  * @param string $signature эталонная сигнатура
  * @param array $post ответ сервера
  * @return bool
  */
 protected function check_signature($signature, $post)
 {
  $sign = base64_encode( sha1(
      $this->get_requisite('private_key') .
      $this->get_amount() .
      $this->get_currency() .
      $this->get_requisite('public_key') .
      $this->get_order_id() .
      $post['type'] .
      $this->generate_payment_describe() .
      $post['status'] .
      $post['transaction_id'] .
      $post['sender_phone']
      , 1 ));
  
  mysql_insert('temp', "`text`='".safe($sign.' - '.$signature)."'");
  
  return $sign = $signature;
 }
}