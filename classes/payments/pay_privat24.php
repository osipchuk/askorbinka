<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 21.03.14
 */

class pay_privat24 extends a_pay {

 /**
  * Метод для запуска процесса оплаты
  * @return string
  * @throws Exception в случае ошибки
  */
 public function process()
 {
  $words = $this->app_helper->getTranslateResolver();
  $amount = $this->get_amount() +($this->get_amount() / 100 * 0.5);
  return '<form action="'.$this->get_requisite('server').'" method="POST" style="display: none;" id="pay-form">
           <input type="text" name="amt" value="' . $amount . '">
           <input type="text" name="ccy" value="UAH">
           <input type="hidden" name="merchant" value="'.$this->get_requisite('merchant').'">
           <input type="hidden" name="order" value="'.$this->get_order_id().'">
           <input type="hidden" name="details" value="'.htmlspecialchars($this->generate_payment_describe()).'">
           <input type="hidden" name="ext_details" value="">
           <input type="hidden" name="pay_way" value="privat24">
           <input type="hidden" name="return_url" value="'.$this->get_result_url().'">
           <input type="hidden" name="server_url" value="'.$this->get_server_url().'">
           <input type="submit" value="'.htmlspecialchars($words->_('pay_link')).'">
          </form>
          <script> $("#pay-form").submit(); </script>';
 }

 /**
  * Метод для подтверждения оплаты
  * @return bool
  * @throws Exception в случае ошибки
  */
 public function submit()
 {
  $payment = $_POST['payment'];
  $signature = $_POST['signature'];
  
  if (!empty($payment) and !empty($signature))
  {
   parse_str($payment, $payment_info);
   $this->set_order_id($payment_info['order']);
   
   if ($this->check_signature($signature, $payment) and $this->get_amount() == (float)$payment_info['amt'])
   {
    switch (strtolower($payment_info['state']))
    {
     case 'ok':
     case 'test':
      $status = 1; $this->order_processor->submit_payments();
      break;

     case 'fail':
      $status = -1;
	  $this->order_processor->cancelOrder();
      break;

     default:
      $status = 0;
      break;
    }

    $this->insert_payment_info($this->get_order_id(), $this->get_amount(), $status, $payment_info);
    if ($status == 1) return $status;
   }
   
   if ($status != 1) $this->insert_payment_info($this->get_order_id(), $this->get_amount(), $status, $payment_info);
  }

  throw new Exception('error');
 }

 /**
  * Получение реквизитов для оплаты из БД
  * @return bool
  */
 protected function load_requisites()
 {
  $settings = $this->app_helper->get_global('privat24_settings');
  $ok = TRUE;

  foreach ($settings as $param => $value)
  {
   $result = $this->set_requisite($param, $value);
   if (!$result) $ok = FALSE;
  }

  return $ok;
 }

 /**
  * Генерирует сигнатуру на основе ответа сервера и сравнивает с эталонной сигнатурой
  * @param string $signature эталонная сигнатура
  * @param string $payment ответ сервера
  * @return bool
  */
 protected function check_signature($signature, $payment)
 {
  return $signature == sha1(md5($payment.$this->get_requisite('pass')));
 }
}