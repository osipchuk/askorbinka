<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 05.02.14
 */

require_once __DIR__.'/a_pay.php';

/**
 * Файбрика для контроллеров оплаты
 */
class pay_factory {

 /**
  * Возвращает объект контроллера оплаты
  * @param string $mode
  * @return a_pay
  * @throws Exception
  */
 static public function get_controller($mode)
 {
  $class = 'pay_'.$mode;
  if (file_exists($file = __DIR__.'/'.$class.'.php'))
  {
   require_once $file;
   return new $class();
  }
  else throw new Exception('Pay controller not found');
 }
}