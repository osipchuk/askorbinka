<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 25.12.13
 * Time: 11:03
 */

class pagination {
 protected $app_helper, $page, $items_count, $items_on_page, $base_url;
 
 protected $add_get_params = FALSE;

 /**
  * Количество отображаемых страниц кроме последней
  * @var array
  */
 private $li_counts = array(
  'xs'=>1,
  'sm'=>8,
  'md'=>12
 );
 
 public function __construct()
 {
  $this->app_helper = applicationHelper::getInstance();
 }

 public function set_page($page)
 {
  $this->page = (int)$page;
  return $this;
 }

 public function set_items_count($items_count)
 {
  $this->items_count = (int)$items_count;
  return $this;
 }

 public function set_items_on_page($items)
 {
  $items = (int)$items;
  if ($items <= 0) $items = 999999;
  $this->items_on_page = $items;
  return $this;
 }

 public function set_base_url($url)
 {
  $this->base_url = $url;
  return $this;
 }

 public function get_page()
 {
  if (is_null($this->page)) $this->page = $this->app_helper->getAppController()->get_page_num();
  return $this->page;
 }

 public function get_pages_count()
 {
  return ceil($this->items_count / $this->get_items_on_page());
 }

 public function get_base_url()
 {
  if (empty($this->base_url)) $this->base_url = implode('/', $this->app_helper->getAppController()->getModesArray());
  return $this->base_url;
 }

 public function get_limit()
 {
  return "limit ".$this->get_start().', '.$this->get_items_on_page();
 }

 public function first_page_url()
 {
  return $this->page_url(0);
 }

 public function last_page_url()
 {
  return $this->page_url($this->get_pages_count() - 1);
 }

 public function prev_page_url()
 {
  $page = $this->get_page() - 1;
  if ($page <= 0) $page = 0;

  return $this->page_url($page);
 }
 
 public function next_page_url()
 {
  $page = $this->get_page() + 1;
  if ($page > $this->get_pages_count() - 1) $page = $this->get_pages_count() - 1;
  
  return $this->page_url($page);
 }

 public function page_url($page)
 {
  $result = $this->get_base_url();
  if ($page > 0) $result .= '/page-'.$page;
  
  return url($result.$this->format_get_params());
 }

 /**
  * Устанавливает флагнеобходимости добавления GET параметров к url
  * @param bool $add
  * @return $this
  */
 public function add_get_params($add)
 {
  $this->add_get_params = (bool)$add;
  return $this;
 }
 
 /**
  * 
  * Проверяет, нужно ли отображать страницу пагинации для экрана
  * @param int $page
  * @param string $screen
  * @return bool
  */
 public function is_page_visible($page, $screen = 'xs')
 {
  if ($page == 0 or $page == $this->get_pages_count() - 1 or ($page >= $this->first_visible_li($screen) and $page <= $this->last_visible_li($screen))) return TRUE;
  
  return FALSE;
 }

 /**
  * Проверяет видимость точек
  * @param int $page
  * @param string $screen
  * @return bool
  */
 public function is_dots_visible($page, $screen = 'xs')
 {
  if ($this->get_pages_count() > $this->get_li_count($screen) and
	  ((($page == 1 and $this->get_page() > floor($this->get_li_count($screen) / 2) + 1) or
		  ($page == $this->get_pages_count() - 1) and $this->get_page() < $this->get_pages_count() - floor($this->get_li_count($screen) / 2) - 1 ))) return TRUE;
  return FALSE;
 }


 /**
  * Возвращает номер первого видимого элемента
  * @param string $screen
  * @return int
  */
 private function first_visible_li($screen)
 {
  $result = $this->get_page() - floor($this->get_li_count($screen) / 2);
  if ($result < 0) $result = 0;
  if ($result + $this->get_li_count($screen) > $this->get_pages_count()) $result = $this->get_pages_count() - $this->get_li_count($screen);
  
  return $result;
 }

 /**
  * Возвращает номер последнего видимого элемента
  * @param string $screen
  * @return int
  */
 private function last_visible_li($screen)
 {
  return $this->first_visible_li($screen) + $this->get_li_count($screen) - 1;
 }

 /**
  * Возвращает количество страниц для вывода
  * @param $screen
  * @return int
  */
 private function get_li_count($screen)
 {
  if (!array_key_exists($screen, $this->li_counts)) return 9999999;
  return $this->li_counts[$screen];
 }
 
 


 protected function get_items_on_page()
 {
  if ($this->items_on_page <= 0) $this->items_on_page = 10;
  return $this->items_on_page;
 }

 protected function get_start()
 {
  return $this->get_page()*$this->get_items_on_page();
 }

 /**
  * Формирует список $_GET параметров
  * @return string
  */
 protected function format_get_params()
 {
  if ($this->add_get_params and !empty($_SERVER['QUERY_STRING'])) return '?'.$_SERVER['QUERY_STRING']; else return '';
 }
}