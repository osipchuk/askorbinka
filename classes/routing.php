<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 12.05.14
 */

/**
 * Класс для выбора контроллера на основе запроса пользователя
 */
class routing extends a_routing {

 /**
  * Выбирает имя контроллера на основе запроса
  * @return string
  * @throws Exception_404
  */
 protected function select_controller_name()
 {
  try {
   $result = parent::select_controller_name();
  }
  catch (Exception_404 $e)
  {
   switch ($this->modes_array[0])
   {
    case 'search':
	case 'download-file': 
	case 'test-payment':
	case 'submit-payment':
	 $result = str_replace('-', '_', $this->modes_array[0].'_controller'); break;
    default: throw new Exception_404();
   }
  }
  
  return $result;
 }

}