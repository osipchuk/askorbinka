<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 12.06.14
 */

/**
 * Класс для построения массива ссылок и названий для всех подстраниц урла
 * 
 * Будет использоваться для генерирования хлебных крошек и тайтлов
 */
class departs_caption {

 /**
  * @var a_controller
  */
 protected $controller;

 /**
  * @var applicationHelper
  */
 protected $app_helper;

 /**
  * @var applicationController
  */
 protected $app_controller;

 /**
  * @var array результат
  */
 protected $result;

 /**
  * Устанавливает контроллер
  * @param a_controller $controller
  * @return $this
  */
 public function set_controller(a_controller $controller)
 {
  $this->controller = $controller;
  return $this;
 }

 /**
  * Возвращает результат выполнения
  * @return array
  */
 public function get_path_array()
 {
  if (!is_array($this->result)) $this->format_path_array(); 
  return $this->result;
 }

 
 

 /**
  * Генерирует результат
  * @return array
  */
 protected function format_path_array()
 {
  if (!is_null($this->controller))
  {
   $this->result = array();
   
   $index = 0;
   $result = array_merge($this->format_structure_array($index), $this->format_module_array($index));

   $item_path = $this->format_item_array();
   if (reset($item_path) != end($result))
   {
    $result = array_merge($result, $item_path);
   }

   foreach ($result as $index => &$item)
   {
    if ($index > 0) $item['static'] = $result[$index - 1]['static'].'/'.$item['static'];
    $this->result[$item['static']] = $item['caption'];
   }
   
   return $this->result;
  }
  
  return array();
 }

 /**
  * Генерирует результат для структуры
  * @param int $index
  * @return array
  */
 private function format_structure_array(&$index)
 {
  $structure = $this->controller->get_site_structure();
  return $this->format_array_from_structure($structure, $index);
 }

 /**
  * Генерирует результат для a_tree модуля
  * Если выбран a_tree модуль
  * @param int $index
  * @return array
  */
 private function format_module_array(&$index)
 {
  if (!is_subclass_of($this->controller, 'a_tree_controller')) return array();
  
  $structure = $this->controller->get_main_tree_model();
  return $this->format_array_from_structure($structure, $index);
 }

 /**
  * Генерирует результат для выбраного элемента
  * Если выбран элемент
  * @return array
  */
 private function format_item_array()
 {
  $item = $this->controller->get_variable('item');
  
  if (!is_array($item)) return array();
  return array(array('static'=>$item['static'], 'caption'=>$item['caption']));
 }


 /**
  * Формирует двумерный массив элементов на основе древовидной структуры
  * @param a_tree_model $structure
  * @param int $index
  * @return array
  */
 private function format_array_from_structure($structure, &$index)
 {
  $result = array();
  $item = $structure->get_structure();
  
  while (!empty($item))
  {
   $item = $structure->find_item_in_structure_by_static($item, $this->controller->getModesArray($index));
   if (is_array($item))
   {
    $result[] = array('static'=>$item['static'], 'caption'=>$item['caption']);
    if (array_key_exists('sub', $item) and is_array($item['sub'])) $item = $item['sub']; else $item = null;
   }

   $index++;
  }
  
  return $result;
 }
 
}