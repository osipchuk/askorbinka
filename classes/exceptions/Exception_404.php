<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 12.05.14
 */

class Exception_404 extends Exception {

 /**
  * Возвращает запрашиваемый URL
  * @return string
  */
 public function get_url()
 {
  return 'http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
 }
}