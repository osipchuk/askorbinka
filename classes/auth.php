<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 07.08.14
 */

/**
 * Библиотека авторизации
 */
class auth {

 /**
  * @var a_model модель, хранящая данные о пользователях
  */
 private $accounts_model;

 /**
  * Устанавливает модель, хранящую данные о пользователях
  * @param a_model $accounts_model
  * @return $this
  */
 public function set_accounts_model(a_model $accounts_model)
 {
  $this->accounts_model = $accounts_model;

  return $this;
 }

 /**
  * Возвращает модель, хранящую данные о пользователях
  * @return a_model
  * @throws Exception если не задана
  */
 public function get_accounts_model()
 {
  if (empty($this->accounts_model)) throw new Exception('Accounts model is empty');
  return $this->accounts_model;
 }


 /**
  * Проверяет переданные авторизационные данные и записывает идентификатор в сессию
  * @param string $login
  * @param string $pass
  * @param bool $remember_me
  * @return bool - true в случае успешной проверки
  */
 public function login($login, $pass, $remember_me = FALSE)
 {
  $account = $this->get_accounts_model()->get_item("`email`='".safe($login)."' and `pass`='".md5($pass)."' and `submited`='1'");
  
  if (empty($account))
  {
   $this->remove_account_session();
   return FALSE;
  }
  
  $this->save_account_session($account['id']);
  if ($remember_me) $this->save_auth_to_cookie($account);
  
  return TRUE;
 }

	/**
	 * Авторизация пользователя по id
	 * @param int $id
	 * @param bool $rememberMe
	 * @return bool - true в случае успешной проверки
	 */
	public function loginById($id, $rememberMe = false) {
		$account = $this->get_accounts_model()->get_item("`id`='".(int)$id."'");

		if (empty($account)) {
			$this->remove_account_session();
			return false;
		}

		$this->save_account_session($account['id']);
		if ($rememberMe) $this->save_auth_to_cookie($account);

		return true;
	}


 /**
  * Logout пользователя из системы
  * @return bool
  */
 public function logout()
 {
  return $this->remove_account_session();
 }

 /**
  * Проверка авторизации
  * @return int|bool идентификатор позьзователя или FALSE в слачае отсуствия идентификации
  */
 public function test_auth()
 {
  if ($this->get_account_id() and $this->get_account_info()) return TRUE;
  
  return FALSE;
 }

 /**
  * Возвращает информацио о авторизированном пользователе
  * @param int|NULL $account_id id аккаунта
  * @return bool|array
  */
 public function get_account_info($account_id = NULL)
 {
  if (empty($account_id)) $account_id = $this->get_account_id();
	  
  if (!empty($account_id)) return $this->get_accounts_model()->get_item("`id`='".(int)$account_id."'");
  
  return FALSE;
 }


 
 
 /**
  * Сохраняет в сессию полученный из БД ID аккаунта
  * @param int $user_id
  * @return bool
  */
 protected function save_account_session($user_id)
 {
  $_SESSION[$this->session_key()] = (int)$user_id;
  return TRUE;
 }

 /**
  * Удаляет из сессии идентифкатор авторизированного пользователя
  * @return bool
  */
 protected function remove_account_session()
 {
  unset($_SESSION[$this->session_key()]);
  setcookie($this->session_key(), null, -1, '/');
  
  return TRUE;
 }

 /**
  * Возвращает строку, под которой в сессии храниться идентификатор авторизированного пользователя
  * @return string
  */
 protected function session_key()
 {
  return get_class($this).'_'.get_class($this->get_accounts_model());
 }

 /**
  * Возвращает ID аккаунта авторизированного пользователя
  * @return bool|int
  */
 protected function get_account_id()
 {
  $account_id = (int)$this->get_session($this->session_key());
  if ($account_id > 0) return $account_id;
  
  $account_id = $this->get_account_id_from_cookie();
  if ($account_id > 0) return $account_id;
  
  return FALSE;
 }

 /**
  * Сохраняет куку авторизации
  * @param array $account
  * @return bool
  */
 protected function save_auth_to_cookie($account)
 {
  setcookie($this->session_key(), $account['id'].','.md5($account['id'].'|'.$account['pass'].'|'.$this->user_agent()), time() + 2592000, '/');
  return TRUE;
 }

 /**
  * Проверяет куку и если она задана корректно, возвращает id аккаунта
  * @return bool|int
  */
 private function get_account_id_from_cookie()
 {
  if ($result = $this->get_cookie($this->session_key()) and count($result = explode(',', $result)) == 2)
  {
   $id = (int)$result[0];
   $secure = $result[1];
   
   $account = $this->get_account_info($id);
   if (!empty($account) and md5($account['id'].'|'.$account['pass'].'|'.$this->user_agent()) == $secure) return $id;
  }
  
  return FALSE;
 }

 /**
  * Возвращает сессию
  * @param string $key
  * @return mixed
  */
 private function get_session($key)
 {
  if (isset($_SESSION[$key])) return $_SESSION[$key]; else return NULL;
 }

 /**
  * Возвращает куку
  * @param string $key
  * @return null|string
  */
 private function get_cookie($key)
 {
  if (isset($_COOKIE[$key])) return $_COOKIE[$key]; else return NULL;
 }

 /**
  * Возвращает user agent
  * @return string
  */
 private function user_agent()
 {
  return $_SERVER['HTTP_USER_AGENT'];
 }
}