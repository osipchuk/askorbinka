<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 18.08.14
 */

/**
 * Контролер крон-бработчика
 * Должен запускаться ночью сразу после полуночи
 */
class cron_controller extends a_controller
{
    /**
     * Выполнение контроллера
     * @return void
     * @throws Exception
     */
    public function execute()
    {
        $programsModel = new programs();
        
//        Применение прогламм лояльности с периодом
        $programs = $programsModel->get_all("`date_begin` < '" . date("Y-m-d") . "' and `date_end` < '" . date("Y-m-d") . "' and `type`='3' and `bonuses_applied`='0'");
        foreach ($programs as $program) {
            $modifier = new periodAmountMoreThanModifier($program);
            $modifier->setCart($this->get_cart())->setBonusStrategy(new addBonusesForPeriodBonusStrategy($modifier));
            $modifier->apply();
        }
        
//        Переиндесация программ лояльности, которые закончились в прошлый день
        $programsToIndex = $programsModel->get_all("`date_end` = '" . date("Y-m-d", time() - 86400) . "'");
        foreach ($programsToIndex as $program) {
            programsCoverage::updateIndex($program['id']);
        }
        
        die();
    }
}