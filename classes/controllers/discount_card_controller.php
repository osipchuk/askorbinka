<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 19.11.14
 */


class discount_card_controller extends a_controller {

	/**
	 * Выполнение контроллера
	 * @return void
	 * @throws Exception
	 */
	public function execute()
	{
		$static = $this->get_static();
		
		if (empty($static)) {
			$depart = $this->get_variable('depart_item');
			$this->set_variable('caption', $depart['caption']);
			$this->set_variable('text', $depart['text']);
			$this->set_template('text.tpl');
			return null;
		}
		
		$formatter = new discountCardFormatter($this);
		
		$this->set_variable('card', $formatter->renderByCode($static));
		$this->set_variable('print', get('gi', 'print'));
		$this->set_template('discount-card.tpl');
	}
}