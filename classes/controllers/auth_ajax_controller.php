<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 22.08.14
 */

/**
 * Обработчик формы авторизации
 */
class auth_ajax_controller extends a_ajax_controller {

 /**
  * Выполнение контроллера
  * @return void
  * @throws Exception
  */
 public function execute()
 {
  $mode = $this->getModesArray(2);
  
  switch ($mode)
  {
   case 'login':
   case 'logout':
	$this->$mode();
   break;
  }
 }

 
 
 
 /**
  * Возвращает класс, отвечающий за авторизацию
  * @return auth
  */
 protected function get_auth_library()
 {
  return $this->auth;
 }

 
 


 /**
  * Авторизация
  * @return void
  */
 private function login()
 {
  parse_str($_POST['form'], $params);
  if (isset($params['remember'])) $remember = (bool)$params['remember']; else $remember = FALSE;
  
  if ($this->get_auth_library()->login($params['email'], $params['password'], $remember))
  {
   $this->set_ajax_variable('status', 'ok');
   $this->set_ajax_variable('message', 'ok');
   $this->set_ajax_variable('location', url($this->app_helper->get_url_maker()->d_module_url($this->redirect_module())));
  }
  else
  {
   $this->set_ajax_variable('message', $this->words->_('auth_error', 'Ошибка авторизации'));
  }
 }

 /**
  * Logout
  */
 private function logout()
 {
  if ($this->get_auth_library()->logout()) $this->set_ajax_variable('status', 'ok');
 }
}