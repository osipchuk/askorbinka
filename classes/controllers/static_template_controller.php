<?php
/**
 * @package default
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 18.06.14
 */

/**
 * Контроллер для отображения статических шаблонов
 */
class static_template_controller extends a_controller {

 /**
  * Выполнение контроллера
  * @return void
  * @throws Exception
  */
 public function execute()
 {
  $template = $this->getModesArray(1);
  if (empty($template) or !file_exists('templates/'.$template.'.tpl')) throw new Exception_404();
  
  $this->set_template($template.'.tpl');
 }
}