<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 03.10.14
 */

/**
 * Контроллер оформления заказа
 */
class order_controller extends a_controller {

 /**
  * Выполнение контроллера
  * @return void
  * @throws Exception
  */
 public function execute()
 {
  $summary = $this->cart->get_summary();
  
  if ($summary['count'] > 0)
  {
   $ships_model = $this->load_model('ships');
   $this->set_variable('ships_model', $ships_model);
   $this->set_variable('ships', $ships_model->get_all());
   $this->set_variable('pay_systems', $this->app_helper->get_global('pay_systems')); 
   
   $programsContainer = applicationHelper::getAmountMaker();
   $this->set_variable('possible_to_pay_with_bonuses', !$programsContainer->isActionGoodsInCart());
  }
  else
  {
   $this->set_variable('caption', $this->depart_item['caption']);
   $this->set_variable('text', $this->words->cart_no_goods);
   $this->set_template('text.tpl');
  }
  
 }
}