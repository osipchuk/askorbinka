<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 14.05.14
 */

class html_constructions_controller extends a_controller {

 /**
  * Выполнение контроллера
  * @return void
  * @throws Exception
  */
 public function execute()
 {
  $model = $this->load_model('html_constructions');
  $id = (int)$this->getModesArray(1);

  if (!empty($id))
  {
   $item = $model->get_item("`id`='$id'");
   if (empty($item)) throw new Exception_404();
   $this->set_variable('item', $item);
   $this->set_template('default/html-construction-item.tpl');
  }
  else
  {
   $this->set_variable('items', $model->get_all());
   $this->set_template('default/html-constructions-list.tpl');
  }
 }
}