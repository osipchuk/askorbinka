<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 29.09.14
 */

/**
 * Контроллер каталога
 */
class categories_controller extends a_tree_controller {

 /**
  * Выполнение контроллера
  * @return void
  * @throws Exception
  */
 public function execute()
 {
  /** @var categories $categories_model */
  $categories_model = $this->get_variable('categories_model');
  
  /** @var goods $goods_model */
  $goods_model = $this->get_variable('goods_model');
  $this->set_main_tree_model($categories_model);

  $path_finder = new path_finder($categories_model, $this->getModesArray(), $this->get_static_index());
  $category = $path_finder->set_other_modules_statics_count(1)->find_item();
  if (empty($category))
  {
   $this->set_variable('caption', $this->depart_item['caption']);
   $this->set_variable('text', $this->depart_item['text']);
   $this->set_template('text.tpl');
  }
  else
  {
   $this->set_variable('category', $category);
   $this->set_variable('item', $category);
      
   /** @var good_comparison $comparison */
   $comparison = $this->get_variable('comparison');
   $comparison->set_category($category['id']);
   $static_index = $path_finder->get_static_index();

   /** @var news_and_actions_goods $news_and_actions_goods */
   $news_and_actions_goods = $this->get_variable('news_and_actions_goods');

   if ($static_index <= 0)
   {
	$filters = new good_filters($goods_model, $categories_model, $this->load_model('brands'), new category_filters(), $category);
	$filters->set_filters();
	$items = $filters->get_all_with_filters(15, true);

	$this->set_variable('items', $items);
    $this->set_variable('items_actions', $news_and_actions_goods->get_goods_array_actions($items, true));
	$this->set_variable('filters', $filters);
	$this->set_variable('filter_categories', $filters->get_categories_to_put());
	$this->set_variable('filter_brands', $filters->get_brands_to_put(true));
	$this->set_template('catalog-list.tpl');
   }
   else
   {
	$item = $goods_model->get_item_of_external_parent($category['id'], "`{$goods_model->get_table_name()}`.`static`='" . safe($this->getModesArray($static_index)) . "'");
	if (empty($item)) throw new Exception_404();

	$tabs_model = $this->load_model('good_tabs');
	
	$filters = new moduleFilters(new good_reviews());
	$reviews = $filters->getData($item['id']);
	
	/** @var catalog_page_links $catalog_page_links */
	$catalog_page_links = $this->load_model('catalog_page_links');
	$catalog_page_links->ignore_page_num(true);

    $tabs = $tabs_model->get_all_of_external_parent($item['id']);
    
    if (count($tabs) > 0)
    {
     $active_tab = 'first';
    }
    elseif (count($comparison->get_properties()) > 0)
    {
     $active_tab = 'comparison';
    }
    elseif (!$item['disable_reviews'])
    {
     $active_tab = 'reviews';
    }
    else
    {
     $active_tab = null;
    }
       
    if ($item['brand'])
    {
     $brands_model = $this->load_model('brands');
     $this->set_variable('brand', $brands_model->get_item("`{$brands_model->get_table_name()}`.`id`='{$item['brand']}'"));
    }
    

	$this->get_variable('watched_goods')->add_good($item);
	$this->set_variable('item', $item);
	$this->set_variable('photos', $goods_model->get_photos($item['id']));
    $this->set_variable('actions', $news_and_actions_goods->get_good_actions($item['id'], true));
    $this->set_variable('programs', (new programs())->getGoodActions($item['id'], true, "`good_image`<>''"));
	$this->set_variable('tabs_model', $tabs_model);
	$this->set_variable('tabs', $tabs);
    $this->set_variable('active_tab', $active_tab);
	$this->set_variable('reviews', $reviews);
	$this->set_variable('active_dates', $filters->getActiveDates($item['id']));
	$this->set_variable('filters', $filters);
	$this->set_variable('videos', $videos = $goods_model->get_videos($item['id']));
	$this->set_variable('video_more_link', $catalog_page_links->getMoreLink());
	$this->set_variable('video_links', $catalog_page_links->getNotMoreLinks());
	$this->set_template('catalog-item.tpl');
   }
  }
 }
}