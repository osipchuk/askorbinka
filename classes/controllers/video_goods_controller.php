<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 05.11.14
 */

/**
 * Контроллер видео-обзоров товаров
 */
class video_goods_controller extends videos_controller {

	/**
	 * Выполнение действий подмодуля
	 * @return void
	 * @throws Exception
	 */
	protected function exec()
	{
		$mainModel = $this->loadMainModel();
		$mainModel->setBrandsModel(new brands());
		
		/** @var moduleFilters $filters */
		$filters = $this->get_variable('filters');

		$this->set_variable('data', $filters->getData($this->get_depart_id()));
		$this->set_variable('video_template', 'goods.tpl');
	}

	/**
	 * Возвращает название класса главной модели данных
	 * @return null|string
	 */
	protected function mainModelClassName()
	{
		return 'good_videos';
	}
}