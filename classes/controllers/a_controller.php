<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 12.05.14
 */

/**
 * Контроллер
 * 
 * Функциональность для отдельного проекта
 */
abstract class a_controller extends a_base_controller {

 /**
  * @var cart корзина
  */
 protected $cart;

 /**
  * @var auth контроллер авторизации
  */
 protected $auth;

 public function __construct()
 {
  parent::__construct();

  $this->app_controller->pagination()->add_get_params(TRUE);

  $this->auth = new auth();
  $clients_model = $this->load_model('clients');
  $this->auth->set_accounts_model($clients_model);
  $account = $this->auth->get_account_info();
  
  $categories_model = $this->load_model('categories');
  $goods_model = $this->load_model('goods');
  $goods_model->ignore_page_num(true);
  $categories = $categories_model->get_structure();
  $comparison = new good_comparison($categories_model, $goods_model, $this->load_model('brands'));
  
  $this->set_variable('account', $account);
  $this->set_variable('clients_model', $clients_model);
  $this->set_variable('categories_model', $categories_model);
  $this->set_variable('goods_model', $goods_model);
  $this->set_variable('categories', $categories);
  $this->set_variable('watched_goods', new watched_goods($this->load_model('goods')));
  $this->set_variable('comparison', $comparison);
  $this->set_variable('manual_blocks', $this->load_model('manual_blocks')->ignore_page_num(true)->get_with_images($this->app_helper->getLang()));
  $this->set_variable('catalog_links_model', $this->load_model('catalog_links'));
  $this->set_variable('news_and_actions_goods', new news_and_actions_goods());
  $this->set_variable('discount_code_modifier', new discountCodeModifier());

  $this->cart = new cart();
  $this->set_variable('cart', $this->cart);
  $this->set_variable('cart_summary', $this->get_cart()->get_summary());
  applicationHelper::getAmountMaker()->setCart($this->cart);
 }

 /**
  * Возвращает корзину
  * @return cart
  */
 public function get_cart()
 {
  return $this->cart;
 }

	/**
	 * Возвращае название модуля, на который нужно произвести редирект после успешной авторизации
	 * @return string
	 */
	protected function redirect_module()
	{
		$summary = $this->get_cart()->get_summary();

		if ($summary['count'] > 0) return 'order';
		return 'profile';
	}
}