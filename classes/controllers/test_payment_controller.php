<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 28.10.14
 */

/**
 * Генерация тестового платежа
 */
class test_payment_controller extends a_controller {

 /**
  * Выполнение контроллера
  * @return void
  * @throws Exception
  */
 public function execute()
 {
  $factory = new pay_factory();
  $controller_name = $this->getModesArray(1);

  /** @var a_pay $controller */
  $controller = $factory->get_controller($controller_name);


  $controller->set_testing_mode(true);
  $controller->set_order_id($this->getModesArray(2));
  $controller->set_currency('UAH');
  $controller->set_result_url($this->app_helper->get_global('base').mb_substr($url = url('submit-payment/'.$controller_name), 1, mb_strlen($url)));

  echo '<pre>';
  print_r($controller->process());
  echo '</pre>';

  die();
 }
}