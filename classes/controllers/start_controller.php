<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 12.05.14
 */

class start_controller extends a_controller {

 /**
  * Выполнение контроллера
  * @return void
  * @throws Exception
  */
 public function execute()
 {
  $this->set_variable('slider', $this->load_model('slider')->get_with_images($this->app_helper->getLang()));
  
  $filters = new moduleFilters(new news_and_actions());
  $filters->setFilter('limit', 5);
  $newsAndActions = $filters->getData(null);
  if (count($newsAndActions['actions']) > 3) {
   $newsAndActions['actions'] = array_slice($newsAndActions['actions'], 0, 3);
  }

  switch (count($newsAndActions['actions']))
  {
   case 1:
    $news_count = 3;
     break;
     
   case 2:
   case 3:
    $news_count = 1;
    break;
         
   default:
    $news_count = 5;
    break;
  }
  $newsAndActions['news'] = array_slice($newsAndActions['news'], 0, $news_count);  
  
  $this->set_variable('news_and_actions', $newsAndActions);
 }
}