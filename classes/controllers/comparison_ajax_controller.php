<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 08.10.14
 */

/**
 * Ajax обработчик сравнения товаров
 */
class comparison_ajax_controller extends a_ajax_controller {

 /**
  * @var good_comparison
  */
 private $good_comparison;

 public function __construct()
 {
  parent::__construct();
  $this->good_comparison = new good_comparison($this->load_model('categories'), $this->load_model('goods'), $this->load_model('brands'));
  $this->set_variable('comparison', $this->good_comparison);
 }


 /**
  * Выполнение контроллера
  * @return void
  * @throws Exception
  */
 public function execute()
 {
  $mode = get('ps','mode');
  switch ($mode)
  {
   case 'add':
   case 'remove':
	$method = $mode.'_good';
	$good_id = get('pi','good');
	
	/** @var goods $goods_model */
	$goods_model = $this->get_variable('goods_model');
	$good = $goods_model->get_item("`{$goods_model->get_table_name()}`.`id`='$good_id'");
	if (empty($good)) throw new Exception('good error');
	
	try {
	 /** goods_comparison */
	 $this->good_comparison->set_category($good['external_parent']);
	 $this->good_comparison->$method($good_id);
	 
	 $table = $this->app_controller->render('catalog-comparison-table.tpl', $this, false, null, array('list'=>1));
	 $this->set_ajax_variable('status', 'ok')->set_ajax_variable('message', $table);
	}
	catch (Exception $e)
	{
	 $this->set_ajax_variable('message', $e->getMessage());
	}
	break;
  }
 }
}