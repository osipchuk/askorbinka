<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 12.11.14
 */

/**
 * Контроллер для результатов поиска
 */
class search_controller extends a_controller {

	/**
	 * Выполнение контроллера
	 * @return void
	 * @throws Exception
	 */
	public function execute()
	{
		$searchQuery = trim(get('gs', 'search'));
		
		if (mb_strlen($searchQuery, 'utf-8') > 2) {
			/** @var goods $goodsModel */
			$goodsModel = $this->get_variable('goods_model');
			$items = $goodsModel->search($searchQuery);
		}
		else {
			$items = array();
		}
		
		$this->set_variable('searchQuery', $searchQuery);
		$this->set_variable('items', $items);
		$this->set_variable('hideFilters', 1);
		$this->set_template('search-results.tpl');
	}
}