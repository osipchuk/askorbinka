<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 29.10.14
 */

/**
 * Генерация счета-фактуры
 */
class download_check_controller extends a_controller {

 /**
  * @var order_processor
  */
 protected $order_processor;

 /**
  * @var checkPrinter
  */
 protected $checkPrinter;

 public function __construct()
 {
  parent::__construct();
  $this->order_processor = new order_processor();
  $this->checkPrinter = new checkPrinter();
 }


 /**
  * Выполнение контроллера
  * @return void
  * @throws Exception
  */
 public function execute()
 {
  $order_id = safe($this->getModesArray($this->get_static_index()));
  $hash = $this->getModesArray($this->get_static_index() + 1);
  
  try {
   $this->order_processor->load_from_db($order_id);
   $amount = $this->order_processor->get_cart_summary();
   
   if ($hash != $this->checkPrinter->generateHash($order_id, $amount['price'])) throw new Exception('hash error');
   
   $this->set_variable('order_processor', $this->order_processor);
   $this->set_variable('seller', $this->app_helper->get_global('global_settings')['seller']);
   $this->set_variable('order_id', $order_id);
   $this->set_template('payment-check.tpl');
  }
  catch (Exception $e)
  {
   throw new Exception_404();
  }
 }
}