<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 12.05.14
 */

/**
 * Контроллер текстовых страниц
 */
class texts_controller extends a_controller {

 /**
  * Выполнение контроллера
  * @return void
  * @throws Exception
  */
 public function execute()
 {
  $depart = $this->get_variable('depart_item');
  
  $this->set_variable('caption', $depart['caption']);
  $this->set_variable('text', $depart['text']);
  $this->set_variable('contacts', $depart['contacts']);
  
  $this->set_template('text.tpl');
 }
}