<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 19.12.14
 */

/**
 * Контроллер программы лояльности
 */
class programs_controller extends a_controller{

    /**
     * Выполнение контроллера
     * @return void
     * @throws Exception
     */
    public function execute()
    {
        $model = new programs();
        $static = $this->get_static();
        
        if (!empty($static)) {
            $item = $model->get_item_of_external_parent($this->get_depart_id(), "`{$model->get_table_name()}`.`static`='$static'");
            if (empty($item)) {
                throw new Exception_404;
            }
            
            $actual = $model->isProgramActual($item);
            if ($actual and empty($item['all_goods']) and $data = $model->getActionGoods($item['id'], 40)) {
                $this->set_variable('goods', $data);
            } else {
                if (!empty($item['all_goods'])) {
                    $this->set_variable('message', $this->words->programs_program_all_goods('Акция действует на все товары'));
                } elseif ($actual) {
                    $this->set_variable('message', $this->words->programs_program_no_goods('К сожалению, акционные товары не найдены'));
                } else {
                    $this->set_variable('message', $this->words->programs_program_id_not_actual('Действие программы уже закончилось'));
                }
            }
            
            $this->set_variable('item', $item);
            $this->set_template('programs-item.tpl');
        } else {
            $this->set_variable('data', $model->getActual(10));
            $this->set_template('programs-list.tpl');
        }
    }
}