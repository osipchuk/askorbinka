<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 04.06.14
 */

/**
 * Контроллер страницы новостей
 */
class news_controller extends a_controller {

 /**
  * @var null|a_model главная модель
  */
 private $model;

 /**
  * @var null|a_model модель категорий
  */
 private $categoryModel;

 /**
  * Названия главной модели
  */
 const MODEL_NAME = 'news';

 /**
  * Название модели категорий
  */
 const CATEGORY_MODEL_NAME = 'subjects';
 
 

 /**
  * Выполнение контроллера
  * @return void
  * @throws Exception
  */
 public function execute()
 {
  $static = $this->get_static();
  
  $filters = $this->getFilters();
  $filters->getModel()->setCategoryModel($this->getCategoryModel());

  if (empty($static))
  {
   $data = $filters->getData($this->getFiltersDepartId());
   
   $this->set_template($this->getListTemplate());
   $this->set_variable('filters', $filters);
   $this->set_variable('data', $data);
   $this->set_variable('active_dates', $filters->getActiveDates($this->getFiltersDepartId()));
  }
  else
  {
   $item = $this->getModel()->get_lang_item($this->app_helper->getLang(), $this->get_depart_id(), "`static`='$static'");
   if (empty($item)) throw new Exception_404();

   $this->set_variable('item', $item);
   $this->set_variable('related_items', $this->getModel()->get_related_items($item, 3));
   $this->set_variable('model', $this->getModel());
   $this->set_template($this->getItemTemplate());
  }
 }

 /**
  * Возвращает главную модель
  * @return a_filter_model
  */
 protected function getModel()
 {
  if (is_null($this->model))
  {
   $class_name = static::MODEL_NAME;
   $this->model = new $class_name();
  }
  return $this->model;
 }

 /**
  * Возвращает модель категорий
  * @return a_model|null
  */
 protected function getCategoryModel()
 {
  if (is_null($this->categoryModel))
  {
   $class_name = static::CATEGORY_MODEL_NAME;
   if (!empty($class_name)) $this->categoryModel = new $class_name();
  }
  return $this->categoryModel;
 }

 /**
  * Возвращает название шаблона для просмотра списка элементов
  * @return string
  */
 protected function getListTemplate()
 {
  return 'news-list.tpl';
 }

 /**
  * Возвращает название шаблона для полного просмотра элемента
  * @return string
  */
 protected function getItemTemplate()
 {
  return 'news-item.tpl';
 }

 /**
  * Возвращает id родительской категории
  * @return int|null
  */
 protected function getFiltersDepartId()
 {
  return $this->get_depart_id();
 }

 /**
  * Возвращает обьект фильтров
  * @return moduleFilters
  */
 protected function getFilters()
 {
  return new moduleFilters($this->getModel());
 }
}