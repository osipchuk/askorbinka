<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 21.10.14
 */

/**
 * Контроллер новинок и акций
 */
class news_and_actions_controller extends news_controller {

 /**
  * Названия главной модели
  */
 const MODEL_NAME = 'news_and_actions';

 /**
  * Название модели категорий
  */
 const CATEGORY_MODEL_NAME = 'categories';

 /**
  * @inheritdoc
  */
 public function execute()
 {
  parent::execute();
  
  $static = $this->get_static();
  $item = $this->get_variable('item');
  
  if (!empty($static) and !empty($item))
  {
   $goods = $this->get_variable('model')->getGoods($item['id']);
   $this->set_variable('goods', $goods);
   $this->set_variable('items_actions', $this->get_variable('news_and_actions_goods')->get_goods_array_actions($goods, true));
  }
 }


 /**
  * Возвращает название шаблона для просмотра списка элементов
  * @return string
  */
 protected function getListTemplate()
 {
  return 'news-and-actions-list.tpl';
 }

 /**
  * Возвращает название шаблона для полного просмотра элемента
  * @return string
  */
 protected function getItemTemplate()
 {
  return 'news-and-actions-item.tpl';
 }

 /**
  * Возвращает обьект фильтров
  * @return moduleFilters
  */
 protected function getFilters()
 {
  $result = parent::getFilters();
  $result->setLimits(array(8, 12, 16, 32));
  
  return $result;
 }
}