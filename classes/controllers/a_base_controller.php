<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 12.05.14
 */


/**
 * Контроллер
 * 
 * Общая функциональность
 */
abstract class a_base_controller {

 /**
  * @var string шаблон
  */
 protected $template;

 /**
  * @var int|null ID раздела сайта
  */
 protected $depart_id;

 /**
  * @var array переменные контроллеру
  */
 protected $variables = array();

 /**
  * @var applicationHelper
  */
 protected $app_helper;

 /**
  * @var applicationController
  */
 protected $app_controller;

 /**
  * @var string|null static выбранного элемента
  */
 protected $static;

 /**
  * @var int index в modes_array, начиная с которого управление передается следующему модулю
  */
 protected $static_index;

 /**
  * @var translatesResolver
  */
 protected $words;
 
 public function __construct()
 {
  $this->app_helper = applicationHelper::getInstance();
  $this->app_controller = $this->app_helper->getAppController();
  $this->words = $this->app_helper->getTranslateResolver();
 }

 /**
  * Выполнение контроллера
  * @return void
  * @throws Exception
  */
 abstract public function execute();

 /**
  * Устанавливает шаблон
  * @param string $template
  * @return $this
  */
 public function set_template($template)
 {
  $this->template = $template;

  return $this;
 }

 /**
  * Возвращает шаблон
  * @return string
  */
 public function get_template()
 {
  if (empty($this->template)) return str_replace('_controller', '', get_class($this)).'.tpl';
  return $this->template;
 }

 /**
  * Устанавливает ID раздела сайта
  * @param int|null $depart_id
  * @return $this
  */
 public function set_depart_id($depart_id)
 {
  $this->depart_id = $depart_id;
  $this->set_variable('depart_item', $this->app_helper->get_site_structure()->find_item_in_structure_by_id($this->app_helper->get_site_structure()->get_structure(), $this->get_depart_id()));

  return $this;
 }

 /**
  * Возвращает ID раздела сайта
  * @return int|null
  */
 public function get_depart_id()
 {
  return $this->depart_id;
 }

 /**
  * Устанавливает значение переменной
  * @param string $var идентификатор
  * @param mixed $value значение
  * @return $this
  */
 public function set_variable($var, $value)
 {
  $this->variables[$var] = $value;
  return $this;
 }

 /**
  * Возвращает значение переменной
  * @param string $var идентификатор
  * @return mixed
  */
 public function get_variable($var)
 {
  if (array_key_exists($var, $this->variables)) return $this->variables[$var];
  return NULL;
 }

 /**
  * Возвращает объект структуры сайта
  * @return structure
  */
 public function get_site_structure()
 {
  return $this->app_helper->get_site_structure();
 }

 /**
  * Возвращает массив значений запроса пользователя.
  * 
  * Или одно значение, если задан index
  * 
  * @param string|null $index
  * @return array|string
  */
 public function getModesArray($index = null)
 {
  return $this->app_controller->getModesArray($index);
 }

 /**
  * Устанавливает static
  * @param null|string $static
  * @return $this
  */
 public function set_static($static)
 {
  if (!is_string($static) or empty($static)) $static = NULL;
  $this->static = $static;

  return $this;
 }

 /**
  * Возвращает static
  * @return null|string
  */
 public function get_static()
 {
  return is_null($this->static) ? NULL : safe($this->static);
 }

 /**
  * Устанавливает static_index
  * @param int $static_index
  * @return $this
  */
 public function set_static_index($static_index)
 {
  $static_index = (int)$static_index;
  if ($static_index < 0) $static_index = 0;

  $this->static_index = $static_index;

  return $this;
 }

 /**
  * Возвращает static_index
  * @return int
  */
 public function get_static_index()
 {
  return $this->static_index;
 }

 /**
  * Загрузка модели
  * @param $model_name
  * @return a_model|a_form_model
  * @throws Exception
  */
 public function load_model($model_name = null)
 {
  if (is_null($model_name)) $model_name = str_replace('_controller', '', get_class($this));

  if (file_exists($file = __DIR__.'/../models/'.$model_name.'.php'))
  {
   require_once $file;
   if (class_exists($model_name)) return new $model_name();
  }

  throw new Exception('Model "'.$model_name.'" not found');
 }

 /**
  * Получение переменной напрямую
  * @param string $name
  * @return mixed
  */
 public function __get($name)
 {
  return $this->get_variable($name);
 }

 /**
  * Установка переменной напрямую
  * @param string $name
  * @param mixed $value
  */
 function __set($name, $value)
 {
  $this->set_variable($name, $value);
 }


}