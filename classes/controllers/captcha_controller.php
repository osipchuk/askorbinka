<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 20.05.14
 */

/**
 * Контроллер формирования капчи
 */
class captcha_controller extends a_controller {

 /**
  * Выполнение контроллера
  * @return void
  * @throws Exception
  */
 public function execute()
 {
  $captcha = new captcha(get('gs','captcha'));
  $captcha->initialize()->set_width(get('gi','width'))->set_height(get('gi','height'))->set_color(get('gs','color'))->set_bg_color(get('gs','bg_color'))->set_noise(get('gb','noise'));
  $captcha->render();
  die();
 }
}