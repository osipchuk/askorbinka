<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 27.10.14
 */

class download_file_controller extends a_controller {

 /**
  * Выполнение контроллера
  * @return void
  * @throws Exception
  */
 public function execute()
 {
  $table_name = $this->getModesArray(2);
  if (empty($table_name)) $table_name = 'good_tab_files';
  
  $file = mysql_line_assoc("select * ".mysql_assoc_select('caption')." from `$table_name` where `id`='".(int)$this->getModesArray(1)."'");
  if (empty($file)) throw new Exception_404();
 
  $file_sender = new send_file();
  if ($result = $file_sender->set_filename($this->app_helper->get_global('files_dir').'/'.$file['file'])->set_new_filename($file['file_filename'])->send()) die();
  
  throw new Exception_404();
 }
}