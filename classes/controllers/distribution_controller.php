<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 21.08.14
 */

/**
 * Контроллер карты дистрибюторов
 */
class distribution_controller extends a_controller {

 /**
  * Выполнение контроллера
  * @return void
  * @throws Exception
  */
 public function execute()
 {
  $cities_model = $this->load_model('map_cities');
  $this->set_variable('cities', $cities_model->get_all());
  $this->set_variable('addresses', json($addresses = $cities_model->get_all_addresses()));
 }
}