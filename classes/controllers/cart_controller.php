<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 13.06.14
 */

/**
 * Контроллер корзины
 */
class cart_controller extends a_controller {

 /**
  * Выполнение контроллера
  * @return void
  * @throws Exception
  */
 public function execute()
 {
  $this->set_variable('cart', $this->get_cart());
  $this->set_variable('items', $this->get_cart()->get_goods());
//  $this->set_variable('pays', $this->load_model('pays')->get_all());
//  $this->set_variable('ships', $this->load_model('ships')->get_all());
  $this->set_template('cart.tpl');
 }
}