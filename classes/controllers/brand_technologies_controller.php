<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 11.11.14
 */

/**
 * Контроллер технологий брендов
 */
class brand_technologies_controller extends a_controller {

	/**
	 * Выполнение контроллера
	 * @return void
	 * @throws Exception
	 */
	public function execute()
	{
		/** @var brands $model */
		$model = $this->load_model('brands');
		$item = $model->get_item("`static`='".safe($this->get_static())."'");
		if (empty($item)) {
			$depart = $this->get_variable('depart_item');
			$this->set_variable('caption', $depart['caption']);
			$this->set_variable('text', $depart['text']);
			$this->set_template('text.tpl');
			return null;
		}
		
		$brands = $model->get_category_brands();
		
		/** @var categories $categories_model */
		$categories_model = $this->get_variable('categories_model');
		/** @var goods $goods_model */
		$goods_model = $this->get_variable('goods_model');
		
		$categories = $model->get_brand_categories_of_2_level($item['id'], $categories_model, $goods_model);

		$category_id = get('gi', 'category');
		if (empty($category_id) and !empty($categories)) $category_id = reset($categories)['id'];
		$category = $categories_model->get_item("`id`='$category_id'");
		if (!empty($category)) {
			$technologies = $model->getAllCategoryImageTechnologies($category['id'], $item['id'], true);

			$tech_id = get('gi', 'tech');
			if (!empty($tech_id)) {
				$tech = mysql_line_assoc("select * " . mysql_assoc_select(array('caption')) . " from `category_param_images` where `id`='$tech_id' order by `rate` desc");
				if (empty($tech)) throw new Exception_404();
				$goods = $goods_model->getAllWithTech($tech['id'], $category['id'], $item['id']);
			} else {
				$tech = null;
				$goods = null;
			}
		} else {
			$technologies = [];
			$tech = null;
			$goods = [];
		}


		$this->set_variable('base_url', $this->app_helper->get_url_maker()->d_module_url('brand_technologies'));
		$this->set_variable('item', $item);
		$this->set_variable('category', $category);
		$this->set_variable('brands', $brands);
		$this->set_variable('brand_categories', $categories);
		$this->set_variable('technologies', $technologies);
		$this->set_variable('tech', $tech);
		$this->set_variable('goods', $goods);
		
		
		$this->set_template('brands-technologies.tpl');
	}
}