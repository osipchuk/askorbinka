<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 12.05.14
 */

class message_controller extends a_controller {

 /**
  * Выполнение контроллера
  * @return void
  * @throws Exception
  */
 public function execute()
 {
  $this->set_template('text.tpl');
 }
}