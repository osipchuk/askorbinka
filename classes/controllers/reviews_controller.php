<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 21.10.14
 */

/**
 * Контроллер отзывов о магазине
 */
class reviews_controller extends news_controller {

 /**
  * Названия главной модели
  */
 const MODEL_NAME = 'reviews';

 /**
  * Название модели категорий
  */
 const CATEGORY_MODEL_NAME = '';

 /**
  * Возвращает название шаблона для просмотра списка элементов
  * @return string
  */
 protected function getListTemplate()
 {
  return 'reviews-list.tpl';
 }

 /**
  * Возвращает название шаблона для полного просмотра элемента
  * @return string
  */
 protected function getItemTemplate()
 {
  return '';
 }
 
 /**
  * Возвращает id родительской категории
  * @return int|null
  */
 protected function getFiltersDepartId()
 {
  return null;
 }
}