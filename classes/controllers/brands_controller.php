<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 23.10.14
 */

/**
 * Контроллер брендов
 */
class brands_controller extends a_controller {

 /**
  * Выполнение контроллера
  * @return void
  * @throws Exception
  */
 public function execute()
 {
  $static = $this->get_static();
  $model = $this->load_model('brands');
  $this->set_variable('model', $model);
  
  if (empty($static))
  {
   $this->set_variable('data', $model->get_all_of_external_parent($this->get_depart_id(), null, 12, false));
   $this->set_template('brands-list.tpl');
  }
  else
  {
   $item = $model->get_item("`static`='$static'", false);
   if (empty($item)) throw new Exception_404();
   
   $this->set_variable('item', $item);
   $this->set_template('brands-item.tpl');
  }
 }
}