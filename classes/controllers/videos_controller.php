<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 05.11.14
 */

/**
 * Базовый контроллер видео
 */
class videos_controller extends a_controller {

	/**
	 * @var a_model|null главная модель
	 */
	protected $mainModel;

	/**
	 * Выполнение контроллера
	 * @return void
	 * @throws Exception
	 */
	public function execute()
	{
		/** @var array $base_depart */
		$video_depart = $this->get_site_structure()->find_item_in_structure_by_module($this->get_site_structure()->get_structure(), 'videos');
		if (empty($video_depart)) throw new Exception_404;
		$this->set_variable('video_depart', $video_depart);
		$this->set_template('videos.tpl');

		/** @var a_model $model */
		$model = $this->loadMainModel();
		if (!empty($model)) {
			$filters = new moduleFilters($model);
			$filters->setLimits([12, 16, 32]);
			$this->set_variable('filters', $filters);
		}
		
		
		$this->exec();
	}

	
	/**
	 * Выполнение действий подмодуля
	 * @return void
	 * @throws Exception
	 */
	protected function exec()
	{
		$this->set_variable('video_template', 'default.tpl');
	}

	/**
	 * Возвращает главную модель данных
	 * @return null|a_model
	 */
	protected function loadMainModel()
	{
		if (is_null($this->mainModel))
		{
			$className = $this->mainModelClassName();
			if (!empty($className)) $this->mainModel = $this->load_model($className);
		}
		
		return $this->mainModel;
	}

	/**
	 * Возвращает название класса главной модели данных
	 * @return null|string
	 */
	protected function mainModelClassName()
	{
		return null;
	}
}