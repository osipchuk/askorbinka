<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 14.05.14
 */

/**
 * Обработчик формы обратной связи
 */
class contacts_post_controller extends a_ajax_controller {

 /**
  * Выполнение контроллера
  * @return void
  * @throws Exception
  */
 public function execute()
 {
  try
  {
   $this->load_model('contacts_form')->set_fields($_POST['form'])->send();
   $this->set_ajax_variable('status', 'ok')->set_ajax_variable('message', $this->words->_('send_messages_ok'));
  }
  catch (Exception $e)
  {
   $this->set_ajax_variable('message', $e->getMessage());
  }
 }
}