<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 23.10.14
 */

/**
 * Контроллер для добавления нового отзыва
 */
class reviews_post_controller extends a_ajax_controller {

 /**
  * Выполнение контроллера
  * @return void
  * @throws Exception
  */
 public function execute()
 {
  $account = $this->get_variable('account');
  
  try
  {
   if (empty($account)) throw new Exception($this->words->_('you_should_auth'));
   
   $this->load_model('reviews_form')->set_fields($_POST['form'])->set_field('account', $account)->send();
   $this->set_ajax_variable('status', 'ok')->set_ajax_variable('message', $this->words->_('reviews_add_ok', 'Ваш отзыва успешно добавлен, он появится на сайте после премодерации'));
  }
  catch (Exception $e)
  {
   $this->set_ajax_variable('message', $e->getMessage());
  }
 }
}