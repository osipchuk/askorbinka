<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 14.05.14
 */

/**
 * Контроллер для обработки ajax-запроса
 * 
 * Базовая часть
 */
abstract class a_base_ajax_controller extends a_controller {

 /**
  * @var array ajax-переменные
  */
 protected $ajax_variables = array();
 
 public function __construct()
 {
  parent::__construct();

  $this->set_template('default/ajax.tpl');
  $this->set_ajax_variable('status', 'error');
  $this->set_ajax_variable('message', $this->words->_('unknown_error'));
 }

 /**
  * Устанавливает значение ajax-переменной
  * @param string $var идентификатор
  * @param mixed $value значение
  * @return $this
  */
 public function set_ajax_variable($var, $value)
 {
  $this->ajax_variables[$var] = $value;
  return $this;
 }

 /**
  * Возвращает значение ajax-переменной
  * @param string $var идентификатор
  * @return mixed
  */
 public function get_ajax_variable($var)
 {
  if (array_key_exists($var, $this->variables)) return $this->variables[$var];
  return NULL;
 }

 /**
  * Возвращает массив ajax-переменных
  * @return array
  */
 public function get_ajax_variables()
 {
  return $this->ajax_variables;
 }

 /**
  * Получение переменной напрямую
  * @param string $name
  * @return mixed
  */
 public function __get($name)
 {
  return $this->get_ajax_variable($name);
 }

 function __set($name, $value)
 {
  return $this->set_ajax_variable($name, $value);
 }


}