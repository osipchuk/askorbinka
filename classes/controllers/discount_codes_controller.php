<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 18.11.14
 */

/**
 * Контроллер операций над кодами для скидок
 */
class discount_codes_controller extends a_ajax_controller {
	
	/**
	 * Выполнение контроллера
	 * @return void
	 * @throws Exception
	 */
	public function execute()
	{
		$account = $this->get_variable('account');
		if (empty($account)) {
			$this->set_ajax_variable('message', $this->words->you_should_auth);
			return null;
		}
		
		if ($account['type'] != 'pr') {
			throw new Exception_404;
		}
		
		$mode = get('gs', 'mode');
		switch ($mode) {
			case 'save':
				$this->{'action' . ucfirst($mode)}();
				break;
		}
	}


	protected function actionSave()
	{
		parse_str($_POST['form'], $form);
		
		try {
			/** @var discount_code_send_email_form $sendController */
			$sendController = $this->load_model('discount_code_send_' . $form['send_controller'] . '_form');
			$sendController->setDiscountCodeId($form['code_id'])->set_fields($form);

			try {
				$sendController->send();

				$this->set_ajax_variable('status', 'ok');
				$this->set_ajax_variable('message', $this->words->profile_pro_code_generated_success('Код успешно сгенерирован!'));
				$this->set_ajax_variable('code', $form['code']);
				$this->set_ajax_variable('code_id', $sendController->getDiscountCodeId());
				
				$this->set_ajax_variable('card_url', $sendController->get_field('card_url'));
			} catch (Exception $e) {
				$this->set_ajax_variable('message', $e->getMessage());
			}
			
		} catch (Exception $e) {
		    $this->set_ajax_variable('message', 'send controller error');
			return null;
		}
	}
}