<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 07.11.14
 */

/**
 * Обработчик формы редактирования профайла
 */
class edit_profile_controller extends a_ajax_controller {

	/**
	 * Выполнение контроллера
	 * @return void
	 * @throws Exception
	 */
	public function execute()
	{
		$account = $this->get_variable('account');
		if (empty($account)) {
			$this->set_ajax_variable('message', $this->words->you_should_auth);
			return null;
		}
		
		$mode = $this->getModesArray(2);
		if (!empty($mode)) {
			return $this->$mode();
		}
		
		try {
			$pro = $account['type'] == 'pr';
			/** @var profile_edit_form $model */
			$model = $this->load_model('profile_edit_form');
			$model->setPro($pro)->set_fields($_POST['form'])->send();

			if ($model->getMailSended()) {
				$message = $this->words->_('edit_profile_change_message_message_ok', 'Вы успешно изменили ваш Email, Вам на почту отправлены дальнейшие инструкции');
			} else {
				$message = $this->words->_('edit_profile_saved_success', 'Данные успешно сохранены');
			}

			$this->set_ajax_variable('status', 'ok')->set_ajax_variable('message', $message);
		} catch (Exception $e) {
			$this->set_ajax_variable('message', $e->getMessage());
		}
	}

	/**
	 * Загружает аватар
	 * @return bool
	 */
	private function upload_avatar()
	{
		require_once $this->app_helper->get_global('classes_dir').'upload_classes.php';
		
		$dir = 'admin/cache/';
		$allowedExtensions = $this->app_helper->get_global('accepted_ext');
		$sizeLimit = 1 * 1024 * 1024;


		$uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
		$result = $uploader->handleUpload($dir);

		if ($result['success'] and $file = $result['filename']) {
			$this->set_ajax_variable('status', 'ok');
			$this->set_ajax_variable('filename', $file);
			$this->set_ajax_variable('message', '');
			return true;
		}
		
		$this->set_ajax_variable('message', $result['error']);
		
		return false;
	}
}