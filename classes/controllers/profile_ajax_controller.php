<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 25.11.14
 */

/**
 * Контроллер для вторичных операций с профайлом пользователя
 */
class profile_ajax_controller extends a_ajax_controller {

	/**
	 * Выполнение контроллера
	 * @return void
	 * @throws Exception
	 */
	public function execute()
	{
		/** @var array $account */
		$account = $this->get_variable('account');
		if (empty($account)) {
			$this->set_ajax_variable('message', $this->words->you_should_auth);
			return null;
		}
		
		$action = $this->getModesArray(2);

		if (!method_exists($this, $action)) throw new Exception_404();
		$this->$action();
	}


	/**
	 * Отправка заявки на вывод бонусов
	 */
	protected function bonuses_transaction_request()
	{
		try {
			$this->load_model('bonus_transactions_form')->set_fields($_POST['form'])->send();
			$this
				->set_ajax_variable('status', 'ok')
				->set_ajax_variable('message', $this->words->_('bonuses_transaction_request_send_success', 'Ваша заявка успешно отправлена'));
		}
		catch (Exception $e) {
			$this->set_ajax_variable('message', $e->getMessage());
		}
	}
}