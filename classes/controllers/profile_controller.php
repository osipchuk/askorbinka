<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 22.08.14
 */

/**
 * Контроллер профайла пользователя
 */
class profile_controller extends a_controller {

	/**
	 * @var array массив действий, для которых необходима авторизация
	 */
	protected $need_auth_actions = ['edit', 'history', ''];

 /**
  * Выполнение контроллера
  * @return void
  * @throws Exception
  */
 public function execute()
 {
	 if (isset($_POST['token'])) {
		 $socialAuth = new socialAuth($this->auth);
		 try {
			 $socialAuth->auth($_POST['token']);
			 redirect($this->app_helper->get_url_maker()->d_module_url($this->redirect_module()));
		 } catch (Exception $e) {

		 }
	 }
	 
	 
	 $mode = $this->get_static();
	 /** @var array $account */
	 $account = $this->get_variable('account');
	 if (in_array($mode, $this->need_auth_actions) and empty($account)) {
		 $this->set_template('auth.tpl');
		 return null;
	 }
	 
	 if (!empty($account) and (empty($account['email']) or $account['submited'] == 0)) {
		 $this->set_variable('profile_message',
			 empty($account['email']) ?
				 $this->words->profile_empty_email_message('Для использования всех возможностей системы, Вам нужно заполнить свой профиль')
				 :
				 $this->words->profile_not_submited_message('К сожалению, Ваш email не подтвержден. Для использования всех возможностей системы, Вам нужно подтвердить Ваш email.'));
	 }
	 
	 $profileNav = [
		 'default' => 'Профайл',
		 'edit' => 'Редактировать',
		 'orders' => 'История заказов',
	 ];
	 $this->set_variable('profile_nav', $profileNav);
	 $this->set_variable('profile_nav_base_url', url($this->app_helper->get_url_maker()->d_module_url('profile')));
	 
	 if ($account['type'] == 'pr') {
		 $this->set_variable('pro_nav', [
			 'visa' => 'cash-order',
			 'goods' => 'bonuses-describe',
			 'video' => $this->app_helper->get_global('global_settings')['pro_instruction_video_id'],
		 ]);
	 }
	 
	 switch ($mode) {
		 case 'submit':
		 case 'edit':
		 case 'history':
		 case 'orders':
			 $this->$mode();
			 break;
		 
		 case 'register':
		 case 'register-pro':
			 $this->register();
			 break;
		 
		 case '':
			 $this->indexPage();
			 break;
		 
		 case 'cash-order':
		 case 'bonuses-describe':
			 $modules = [
				 'cash-order' => 'bonuses_to_card_texts',
				 'bonuses-describe' => 'bonuses_to_goods_texts',
			 ];
			 
			 $this->textPage($modules[$mode]);
			 break;
		 
		 default:
			 throw new Exception_404();
	 }
 }

 /**
  * Регистрация пользователя
  */
 public function register()
 {
	 $this->set_template('register.tpl');
     $this->set_variable('title', $title = $this->words->_w(str_replace('-', '_', $this->get_static()), '_caption'));
     $this->add_title($title);
	 $this->set_variable('pro', $pro = ($this->get_static() == 'register-pro'));
	 $this->set_variable('delivery_categories', $this->load_model('delivery_categories')->get_all());
     
     $fields = $this->getProfileFormFields($pro);
     unset($fields['image']);
	 $this->set_variable('fields', $fields);
 }

 /**
  * Подтверждение аккаунта
  */
 public function submit()
 {
  $id = (int) $_GET['id'];
	 
  if ($this->load_model('profile_form')->submit($id, $_GET['hash']))
  {
	  $result = 'success';
	  $this->auth->loginById($id, true);
	  $this->set_variable('account', $this->auth->get_account_info());
  }
  else $result = 'error';
  
  $this->set_variable('caption', $title = $this->words->_('register_submit_title', 'Подтверждение регистрации'));
  $this->set_variable('text', '<p>'.$this->words->_('register_submit_'.$result.'_text').'</p>');
  $this->set_template('text.tpl');
  $this->add_title($title);
 }

	/**
	 * Редактирование аккаунта
	 */
	public function edit()
	{
		/** @var array $account */
		$account = $this->get_variable('account');
		$readOnlyFields = [];
		if (!empty($account['email']) and $account['submited']) $readOnlyFields[] = 'email';
		
		$this->set_variable('readOnlyFields', $readOnlyFields);
		$this->set_variable('profileTemplate', 'edit.tpl');
		$this->set_variable('fields', $this->getProfileFormFields($account['type'] == 'pr', true));
	}

	/**
	 * Действие по умолчанию
	 */
	public function indexPage()
	{
		/** @var array $account */
		$account = $this->get_variable('account');
		
		if ($account['type'] == 'cl') {
			$this->set_variable('profileTemplate', 'profile-client.tpl');
			return null;
		}

		/** @var categories $categories_model */
		$categories_model = $this->get_variable('categories_model');
		
		$category_id = get('gi', 'category');
		$subcategory_id = get('gi', 'subcategory');
		
		$brand_id = get('gi','brand');
		if ($brand_id > 0) {
			$brandsModel = $this->load_model('brands');
			$brand = $brandsModel->ignore_page_num(true)->get_item("`{$brandsModel->get_table_name()}`.`id`='$brand_id'");
		} else {
			$brand = null;
		}
		
		
		$categories = $categories_model->get_structure();
		$subcategories = [];
		
		if ($category_id > 0) {
			$category_item = $categories_model->find_item_in_structure_by_id($categories, $category_id);
			if (empty($category_item)) throw new Exception_404;
		} else {
			$category_item = reset($categories);
			$category_id = $category_item['id'];
		}

		if (isset($category_item['sub'])) $subcategories = $category_item['sub'];
		$selectedCategoryItem = $categories_model->find_item_in_structure_by_id($categories, $subcategory_id > 0 ? $subcategory_id : $category_id);
		

		/** @var goods $goods_model */
		$goods_model = clone $this->get_variable('goods_model');
		$goods_model->ignore_page_num(false);
		
		$filters = new good_filters($goods_model, $categories_model, $this->load_model('brands'), new category_filters(), $selectedCategoryItem);
		if (!is_null($brand)) {
			$filters->set_brands([$brand['static']]);
		}
		$filters->set_with_code_discount_only(true);
		$items = $filters->get_all_with_filters(20);
		
		$good_id = get('gi', 'good');
		if (!empty($good_id)) {
			$discountCardFormatter = new discountCardFormatter($this);
			$this->set_variable('good_id', $good_id);
			
			
			$this->set_variable('cardFormatter', $discountCardFormatter);
			$this->set_variable('discountCard', $discountCardFormatter->renderCard($good_id));
		}

		
		$this->set_variable('items', $items);
		$this->set_variable('filters', $filters);
		$this->set_variable('filter_categories', $categories);
		$this->set_variable('filter_subcategories', $subcategories);
		$this->set_variable('category_id', $category_id);
		$this->set_variable('subcategory_id', $subcategory_id);
		$this->set_variable('brand_id', $brand_id);
		
		$this->set_variable('filter_brands', $filters->get_brands_to_put());
		$this->set_variable('base_href', $this->app_helper->get_url_maker()->d_module_url('profile'));

		$this->set_variable('statistic', $data = (new discount_codes())->ignore_page_num(true)->getCodesStatistic($account['id']));
		
		$this->set_variable('profileTemplate', 'profile-pro.tpl');
	}

	/**
	 * История зачислений и отчеслений на бонусный счет пользователя
	 */
	public function history()
	{
		/** @var array $account */
		$account = $this->get_variable('account');
		
		$this->set_variable('profileTemplate', 'history.tpl');
		$this->set_variable('data', (new client_bonuses())->get_all_of_external_parent($account['id'], null, 30));
		$this->set_variable('caption', $this->words->_('profile_history_caption', 'История операций по бонусному счету'));
		
		$this->set_template('profile.tpl');
	}

	public function textPage($module)
	{
		$item = $this->get_site_structure()->find_item_in_structure_by_module($this->get_site_structure()->get_structure(), $module);
		if (empty($item)) throw new Exception_404;
		
		$this->set_variable('caption', $item['caption']);
		$this->set_variable('text', $item['text']);
		$this->set_variable('show_form', $module == 'bonuses_to_card_texts');
		$this->set_variable('profileTemplate', 'text.tpl');
	}

	public function orders()
	{
		$this->set_variable('cart', new cart());
		$this->set_variable('caption', $this->words->_('profile_orders_caption', 'История заказов'));
		$this->set_variable('data', $this->load_model('orders')->get_all("`client`='{$this->account['id']}'", 30));
		$this->set_variable('statuses', $this->app_helper->get_global('payment_statuses'));
		$this->set_variable('profileTemplate', 'orders.tpl');
	}

 
 
 /**
  * Добавляет название подраздела к тайтлу
  * @param string $title
  * @return $this
  */
 private function add_title($title)
 {
  if (!empty($title))
  {
   $meta_tags = $this->app_helper->get_meta_tags_resolver();
   $meta_tags->set_controller($this);
   $this->app_controller->get_departs_caption()->set_controller($this);

   $meta_tags->set_title($title.' - '.$meta_tags->get_title());
  }
 
  return $this;
 }

	/**
	 * Возвращает массив полей для формы регистрации/редактирования
	 * @param bool $pro
	 * @param bool $edit
	 * @return array
	 */
	private function getProfileFormFields($pro = false, $edit = false)
	{
		$fields = [
			'image' => ['type' => 'avatar'],
			'name' => ['required' => 1],
			'surname' => ['required' => 1],
			'city' => ['required' => 1],
			'tel' => ['required' => 1, 'type' => 'tel'],
			'email' => ['required' => 1, 'type' => 'email'],
		];

		if ($pro) {
			$fields = array_merge($fields, [
				'job' => ['required' => 1],
				'post' => ['required' => 1],
			]);
		}

		$fields = array_merge($fields, [
			'password' => ['required' => $edit ? 0 : 1, 'type' => 'password'],
			'password_repeat' => ['required' => $edit ? 0 : 1, 'type' => 'password']
		]);
		
		return $fields;
	}
}