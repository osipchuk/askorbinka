<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 22.08.14
 */

/**
 * Обработчик форми регистрации
 */
class register_controller extends a_ajax_controller
{

	/**
	 * Выполнение контроллера
	 * @return void
	 * @throws Exception
	 */
	public function execute()
	{
		parse_str($_POST['form'], $form);
		$pro = $form['pro'];
		
		try {
			$this->load_model('profile_form')->setPro($pro)->set_fields($_POST['form'])->send();
			
			if ($pro) {
				$message = $this->words->_('register_pro_messages_ok', 'Вы успешно зарегистрировались, аккаунт профи должен подтвердить администратор.');
			}
			else {
				$message = $this->words->_('register_messages_ok', 'Вы успешно зарегистрировались, Вам на почту отправлены дальнейшие инструкции');
			}
			
			$this->set_ajax_variable('status', 'ok')->set_ajax_variable('message', $message);
		} catch (Exception $e) {
			$this->set_ajax_variable('message', $e->getMessage());
		}
	}
}