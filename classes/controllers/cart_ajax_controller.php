<?php
/**
 * @package NCSM
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 13.06.14
 */

/**
 * Контроллер для выполнения дествий с корзиной
 */
class cart_ajax_controller extends a_ajax_controller {

 /**
  * Выполнение контроллера
  * @return void
  * @throws Exception
  */
 public function execute()
 {
  $action = $this->getModesArray(2);
  
  if (!method_exists($this, $action)) throw new Exception_404();
  $this->$action();

  $this->set_ajax_variable('summary', $this->get_cart()->get_summary());
 }


 /**
  * Добавляет товар в корзину
  */
 private function add_good()
 {
  if (get('pi','clear_cart_before')) $this->get_cart()->clear();
  
  if ($this->get_cart()->add_item(get('pi', 'id')))
  {
   $this->set_ajax_variable('status', 'ok');
   $this->set_ajax_variable('message', $this->words->cart_good_added_success('Товар успешно добавлен в корзину'));
   $this->set_ajax_variable('order_url', url($this->app_helper->get_url_maker()->d_module_url('order')));
  }
 }

 /**
  * Удаляет товар из корзины
  */
 private function delete_item()
 {
  if ($this->get_cart()->remove_item(get('pi', 'item_id'))) $this->set_ajax_variable('status', 'ok');
 }

 /**
  * Изменяет количество определенного товара в корзине
  */
 private function set_cart_items_count()
 {
  if ($this->get_cart()->set_items_count(get('pi', 'item_id'), get('pi', 'count'))) $this->set_ajax_variable('status', 'ok');
 }

 /**
  * Очищает корзину
  */
 private function clear_cart()
 {
  if ($this->get_cart()->clear()) $this->set_ajax_variable('status', 'ok');
 }

 /**
  * Оформляет заказ
  */
 private function order()
 {
  $account = $this->get_variable('account');
  if (empty($account))
  {
   try
   {
    /** @var profile_form $profile_form */
    $profile_form = $this->load_model('profile_form');
    $profile_form->set_fields($_POST['form'])->send();
    $account_id = $profile_form->get_field('account_id');
    if (!empty($account_id))
    {
     $this->auth->loginById($account_id, true);
     $account = $this->auth->get_account_info();
     $this->set_variable('account', $account);
    }
   }
   catch (Exception $e)
   {
    $this->set_ajax_variable('message', $e->getMessage());
    return false;
   }
  }
  
  /** @var order_processor $model */
  try {
   $model = $this->load_model('order_processor')->set_account($account)->set_fields($_POST['form'])->set_cart($this->get_cart());
   if ($model->send())
   {
	try {
		$this->get_cart()->load_from_db($model->get_field('order_id'));
		$amount = $this->get_cart()->get_summary(true)['price'];
		
		if ($amount == 0) {
			$controller_name = 'bonuses';
		} else {
			$controller_name = $model->get_field('pay');
		}
		
		$factory = new pay_factory();
		$controller = $factory->get_controller($controller_name);
		
	 

	 $controller->set_order_id($model->get_field('order_id'));
	 $controller->set_currency('UAH');
	 $controller->set_result_url($this->app_helper->get_global('base').mb_substr($url = url('submit-payment/'.$controller_name), 1, mb_strlen($url)));

	 $pay_result = $controller->process();

	 unset($controller);
	}
	catch (Exception $e)
	{
	 $pay_result = $this->words->_('process_order_ok', 'Ваш заказ №{$order_id} успешно завершен и требует оплаты. Вам на почту отправлены дальнейшие инстуркции.');
	}
	
	$this->set_ajax_variable('status', 'ok');
	$this->set_ajax_variable('message', str_replace(['{$order_id}'], [$model->get_field('order_id')], $pay_result));
   }
  }
  catch (Exception $e)
  {
   $this->set_ajax_variable('message', $e->getMessage());
  }
 }

	/**
     * Возвращает размер скидки после применения скидочного сертификата
	 */
	private function apply_discount_code()
	{
		/** @var discount_codes $model */
		$model = $this->load_model('discount_codes');
		
		$code = get('ps', 'code');
		if (!$model->isCodeCorrect($code)) {
			return $this->set_ajax_variable('message', $this->words->discount_code_is_incorrect('Пожалуйста, введите корректный скидочный код'));
		}
		
		$amountContainer = applicationHelper::getAmountMaker();
		$amountContainer->apply();
		
		$beforeAmount = $this->cart->get_summary()['price'];
		$modifier = new discountCodeModifier();
		$modifier->setCheckCodeUsed(true)->setCart($this->cart)->setCode($code);
		
		try {
            $modifier->apply();
            $afterModifier = $this->cart->get_summary()['price'];
			$result = $beforeAmount - $afterModifier;
			$this->set_ajax_variable('status', 'ok');
			$this->set_ajax_variable('message', 'ok');
			$this->set_ajax_variable('code_id', $modifier->getCodeId());
			$this->set_ajax_variable('discount_amount', price_format($result));
			
		} catch (Exception $e) {
			$message = $this->words->_w('discount_code_', $e->getMessage());
			if (empty($message)) $message = $e->getMessage();
			
			$this->set_ajax_variable('message', $message);
		}
	}
}