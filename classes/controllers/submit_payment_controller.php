<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 28.10.14
 */

/**
 * Контроллер для подтверждения платежа
 */
class submit_payment_controller extends a_controller {

 /**
  * Выполнение контроллера
  * @return void
  * @throws Exception
  */
 public function execute()
 {
  $factory = new pay_factory();
  $controller_name = $this->getModesArray(1);
  $ok = TRUE;

  /*
  $_POST = array(
		  'LMI_MODE' => '1',
    'LMI_PAYMENT_AMOUNT' => '399.00',
    'LMI_PAYEE_PURSE' => 'U422163105784',
    'LMI_PAYMENT_NO' => '151',
    'LMI_PAYER_WM' => '173425517000',
    'LMI_PAYER_PURSE' => 'U422163105784',
    'LMI_PAYER_COUNTRYID' => 'UA',
    'LMI_PAYER_PCOUNTRYID' => 'UA',
    'LMI_PAYER_IP' => '95.135.35.76',
    'LMI_SYS_INVS_NO' => '505',
    'LMI_SYS_TRANS_NO' => '747',
    'LMI_SYS_TRANS_DATE' => '20141028 12:48:55',
    'LMI_HASH' => '0F6FF98E7DE32D430E04435D33841E61FAC49784393D7F8099484082B2CDEA03',
    'LMI_PAYMENT_DESC' => ''
  );

  echo '<pre>';
  print_r($_POST);
  echo '</pre>';
  */
  
  mysql_insert('temp', "`text`='".safe(print_r($_POST, true))."'");
  

  try {
   $controller = $factory->get_controller($controller_name);
   $controller->submit();
  }
  catch (Exception $e)
  {
   if ($e->getCode() != 1) $ok = FALSE;
  }

  if ($ok)
  {
   $this->get_cart()->clear();

   $caption= $this->words->_('payment_success_caption', 'Поздравляем!');
   $text = $this->words->_('payment_success_text', 'Вы успешно подтвердили оплату');
  }
  else
  {
   if ($e->getCode() == 0) $error_mode = 'wait'; else $error_mode = 'error';
   $caption = $this->words->_('payment_'.$error_mode.'_caption');
   $text = $this->words->_('payment_'.$error_mode.'_text');
  }

  $this->set_variable('caption', $caption);
  $this->set_variable('text', $text);
  $this->set_template('text.tpl');
 }
}