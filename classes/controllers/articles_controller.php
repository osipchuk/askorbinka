<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 09.01.15
 */

/**
 * Контроллер статтей
 */
class articles_controller extends news_controller {

    /**
     * Названия главной модели
     */
    const MODEL_NAME = 'articles';
}