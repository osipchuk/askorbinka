<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 05.11.14
 */

/**
 * Контроллер видео разбитого по категориям
 */
class video_categories_controller extends videos_controller {

	/**
	 * Выполнение действий подмодуля
	 * @return void
	 * @throws Exception
	 */
	protected function exec()
	{
		/** @var moduleFilters $filters */
		$filters = $this->get_variable('filters');
		
		$this->set_variable('data', $filters->getData($this->get_depart_id()));
		
		$this->set_variable('video_template', 'categories.tpl');
	}

	/**
	 * Возвращает название класса главной модели данных
	 * @return null|string
	 */
	protected function mainModelClassName()
	{
		return 'category_videos';
	}
}