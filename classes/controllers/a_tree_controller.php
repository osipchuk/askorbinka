<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 12.06.14
 */

/**
 * Абстрактный предок для контроллеров с деревовидными моделями 
 */
abstract class a_tree_controller extends a_controller {

 /**
  * @var a_tree_model
  */
 protected $main_tree_model;

 /**
  * Возвращает главную рдревовидную модель
  * @return a_tree_model
  * @throws Exception в случае, если модель не задана
  */
 public function get_main_tree_model()
 {
  if (is_null($this->main_tree_model)) throw new Exception('main tree model is null for controller '.get_class($this));
  return $this->main_tree_model;
 }


 /**
  * Устанавливает главную древовидную модель
  * @param a_tree_model $model
  * @return a_tree_model
  */
 protected function set_main_tree_model(a_tree_model $model)
 {
  $this->main_tree_model = $model;
  return $model;
 }
}