<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 05.11.14
 */

/**
 * Контроллер видео разбитого по брендам
 */
class video_brands_controller extends videos_controller {

	/**
	 * Выполнение действий подмодуля
	 * @return void
	 * @throws Exception
	 */
	protected function exec()
	{
		/** @var moduleFilters $filters */
		$filters = $this->get_variable('filters');
		
		/** @var a_model $catModel */
		$catModel = $this->loadMainModel()->getCategoryModel();
		$brand_info = $catModel->get_item("`{$catModel->get_table_name()}`.`id`='".(int)$filters->getFilter('category')."'", false);
		if (!empty($brand_info)) {
			$this->set_variable('cat_model', $catModel);
			$this->set_variable('brand_info', $brand_info);

			$this->set_variable('brand_categories', (new categories())->getCategoriesWithBrandGoods($brand_info['id']));
			$this->set_variable('brand_categories_caption',
				str_replace('{brand_name}', $brand_info['caption'], $this->words->_('videos_brand_categories_caption', 'Смотреть товары {brand_name} по категориям:')));
		}
		
		$this->set_variable('data', $filters->getData($this->get_depart_id()));
		$this->set_variable('video_template', 'brands.tpl');
	}

	/**
	 * Возвращает название класса главной модели данных
	 * @return null|string
	 */
	protected function mainModelClassName()
	{
		return 'brand_videos';
	}
}