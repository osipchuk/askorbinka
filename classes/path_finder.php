<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 05.06.14
 */

/**
 * Класс, который ищет элемент в древовидной структуре на основе массива static
 */
class path_finder {

 /**
  * @var a_tree_model модель
  */
 protected $model;

 /**
  * @var array
  */
 protected $modes_array = array();

 /**
  * @var int
  */
 protected $start_index = 0;

 /**
  * @var stdClass результат
  */
 protected $result;

 /**
  * @var int index в modes_array, начиная с которого управление передается следующему модулю
  */
 protected $static_index = -1;

 /**
  * @var int|string количество элементов в modes_array после последнего найденого элемента даного модуля. (n - неограниченное количество)
  */
 protected $other_modules_statics_count = 0;

 public function __construct(a_tree_model $model, $modes_array, $start_index)
 {
  $this->model = $model;
  $this->modes_array = $modes_array;
  $this->start_index = (int)$start_index;
 }

 /**
  * Устанавливает количество элементов в modes_array после последнего найденого элемента даного модуля. (n - неограниченное количество)
  * @param int|string $other_modules_statics_count
  * @return $this
  */
 public function set_other_modules_statics_count($other_modules_statics_count)
 {
  $this->other_modules_statics_count = $other_modules_statics_count;

  return $this;
 }

 /**
  * Возвращает количество элементов в modes_array после последнего найденого элемента даного модуля. (n - неограниченное количество)
  * @return int|string
  */
 public function get_other_modules_statics_count()
 {
  return $this->other_modules_statics_count;
 }

 /**
  * Возвращает элемент
  * @return array|null|stdClass
  * @throws Exception_404
  */
 public function find_item()
 {
  if (empty($this->result))
  {
   $result = null;
   $item = $this->model->get_structure();
   $count = count($this->modes_array);
   $i = $this->start_index;

   while ($i < $count and !empty($item) and $item = $this->model->find_item_in_structure_by_static($item, $this->modes_array[$i]))
   {
    if (!empty($item)) $i++;
    $result = $item;
    if (array_key_exists('sub', $item)) $item = $item['sub']; else $item = array();
   }
   
   if (empty($item))
   {
    if ($i == $count) $this->static_index = -1;
    elseif ($i == $count - 1) $this->static_index = $i;
    elseif ($i < $count - 1) throw new Exception_404(); 
   }
   
   $this->result = $result;
  }
  
  return $this->result;
 }

 /**
  * Возвращает static_index
  * @return int
  */
 public function get_static_index()
 {
  if (empty($this->result)) $this->find_item();
  return $this->static_index;
 }


}