<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 28.04.14
 */

class site_structure_ext_modules_finder {

 /**
  * @var array массив нестандартных модулей
  */
 protected $extended_modules = array();

 /**
  * Конструктор
  */
 public function __construct()
 {
  global $extended_modules;
  $this->extended_modules = $extended_modules;
 }

 /**
  * Проверяет наличие в структуре нестандартных модулей
  * @param array $structure
  * @return bool
  */
 public function has_ext_modules($structure)
 {
  if (array_key_exists($structure['module'], $this->extended_modules)) return FALSE;
  if (array_key_exists('sub', $structure))
  {
   foreach ($structure['sub'] as $item)
   {
    if (!$this->has_ext_modules($item)) return FALSE;
   }
  }
  
  return TRUE;
 }
}