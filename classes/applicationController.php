<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 25.10.13
 * Time: 9:06
 */

class applicationController {
 protected $modes_array, $independentTemplates, $template, $appHelper;

// TODO: Рефакторинг, перенести в контроллер
 protected $page_num, $pagination;

 /**
  * @var structure модель структуры сайта 
  */
 protected $site_structure;

 /**
  * @var null|a_controller выбранный роутингом контроллер
  */
 protected $controller;

 /**
  * @var string название контроллера
  */
 protected $controller_name;

 /**
  * @var departs_caption объект для генерации массива ссылок и названий для всех подстраниц урла
  */
 protected $departs_caption;

 /**
  * @var meta_tags_resolver
  */
 protected $meta_tags_resolver;

 private static $instance;

 /**
  * @var null|Smarty
  */
 private $smarty;
 
 protected function __construct() {
  $this->appHelper = applicationHelper::getInstance();
  $this->departs_caption = new departs_caption();
  $this->meta_tags_resolver = $this->appHelper->get_meta_tags_resolver();
  
  $this->independentTemplates = $this->appHelper->get_global('independentTemplates');
  
  $this->modes_array = get_modes($lang, $this->page_num);
  if (empty($this->modes_array[0])) $this->modes_array[0] = 'start';
 }
 
 public static function getInstance() {
  if (empty(self::$instance)) {
   self::$instance = new applicationController();
  }
  return self::$instance;
 }
 
 public function getTemplate() {
  if (!empty($this->template)) return $this->template;

  try {
   $routing = new routing(new structure(), $this->appHelper);
   $controller = $routing->set_modes_array($this->getModesArray())->select_controller();
   $controller->set_static($routing->get_static())->set_static_index($routing->get_static_index());
   $controller->execute();
   
   $this->controller = $controller;
   $this->set_controller_name(get_class($controller));
   $this->template = $this->controller->get_template();
   $this->meta_tags_resolver->set_controller($this->controller);
   $this->departs_caption->set_controller($controller);
  }
  catch (Exception_404 $e)
  {
   $this->template = 'default/404.tpl';
  }
  catch (Exception $e)
  {
   if ($this->appHelper->get_production())
   {
    $caption = 'В разработке';
    $text = '<p>Извините, данный раздел находится в разработке. Он будет доступен в ближайшее время.</p>';
   }
   else
   {
    $caption = 'Ошибка выбора контроллера';
    $text = $e->getMessage();
   }
   
   $controller = new message_controller();
   $controller->set_variable('caption', 'Error')->set_variable('caption', $caption)->set_variable('text', $text)->execute();
   
   $this->template = $controller->get_template();
   $this->controller = $controller;
   header('HTTP/1.1 404 Not Found');
  }
  
  return $this->template;
 }
 
 public function displayTemplate() {
  $template = $this->getTemplate();
  
  echo $this->render($template, $this->controller, !in_array($template, $this->independentTemplates), implode('#', $this->getModesArray()));
 }

 /**
  * Рендерит переданный шаблон
  * @param string $template
  * @param a_controller $controller
  * @param bool $includeContainer заключать шаблон в основной контейнер
  * @param null|string $cache_id
  * @param array $appendVariables дополнительные переменные
  * @return string
  */
 public function render($template, $controller, $includeContainer = true, $cache_id = null, $appendVariables = array())
 {
  if (is_null($this->smarty))
  {
   $this->smarty = $this->appHelper->getSmarty();
   $this->smarty->assign('appHelper', $this->appHelper);
   $this->smarty->assign('app', $this);
   $this->smarty->assign('words', $this->appHelper->getTranslateResolver());
   $this->smarty->assign('metaTags', $this->meta_tags_resolver );
   $this->smarty->assign('main_nav', $this->appHelper->get_site_structure()->get_nav());
   $this->smarty->assign('url_maker', $this->appHelper->get_url_maker());
  }
  
  $this->smarty->assign('controller', $controller);
  foreach ($appendVariables as $var => $value) $this->smarty->assign($var, $value);
  
  return $this->smarty->fetch($includeContainer ? 'container.tpl' : $template, $cache_id);
 }
 
 public function getModesArray($index = null) {
  if (!is_null($index)) {
   if ($index === 'last') return end($this->modes_array); else return (!empty($this->modes_array[$index]) ? $this->modes_array[$index] : '');
  }
  else return $this->modes_array;
 }

 public function setMode($index, $value) {
  $this->modes_array[$index] = $value;
 }
 
 public function isStart() {
  return $this->getModesArray(0) == 'start';
 }
 
 public function getLang() {
  return $this->appHelper->getLang();
 }
 
 public function get_page_num()
 {
  $result = (int)$this->page_num;
  if ($result < 0) $result = 0;
  
  return $result;
 }

 /**
  * Установка названия контроллера
  * @param string $name
  * @return $this
  */
 public function set_controller_name($name)
 {
  $this->controller_name = preg_replace("/_controller$/", '', $name);
  return $this;
 }

 /**
  * Возвращает название выбранного контроллера
  * @return string
  */
 public function get_controller_name()
 {
  return $this->controller_name;
 }

 /**
  * @return pagination
  */
 public function pagination()
 {
  if (empty($this->pagination)) $this->pagination = new pagination();
  return $this->pagination;
 }

 /**
  * Возвращает объект для генерации массива ссылок и названий для всех подстраниц урла
  * @return departs_caption
  * @throws Exception в случае ошибки
  */
 public function get_departs_caption()
 {
  if (is_null($this->departs_caption)) throw new Exception('departs_caption object is null');
  return $this->departs_caption;
 }
} 