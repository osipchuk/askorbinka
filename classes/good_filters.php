<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 06.06.14
 */

/**
 * Класс для применения фильтров каталога товаров
 */
class good_filters {

 /**
  * @var goods
  */
 protected $model;

 /**
  * @var categories
  */
 protected $categories_model;

 /**
  * @var a_model
  */
 protected $brands_model;

 /**
  * @var array массив id выбранных категорий
  */
 protected $selected_categories = [];

 /**
  * @var array массив id выбранных брендов
  */
 protected $selected_brands = [];

 /**
  * @var array массив элементов выбранных брендов
  */
 protected $selected_brands_data = [];

 /**
  * @var array массив категорий
  */
 protected $categories_to_put = [];

 /**
  * @var array|NULL выбранная категория
  */
 protected $category;
	
 /**
  * @var bool отображать только элементы со скидкой по коду
  */
 protected $with_code_discount_only = false;

	/**
	 * @var applicationHelper
	 */
	protected $appHelper;
 /**
  * @var category_filters
  */
 private $category_filters_model;

 /**
  * @var array выбранные значения дополнительных фильтров в одномерном массиве
  */
 private $checkedAppendFiltersAll = [];

 /**
  * @var array выбранные значения дополнительных фильтров в двумерном массиве, разбитый по фильтру
  */
 private $checkedAppendFiltersByFilters = [];

 
 /**
  * Конструктор
  * @param goods $model
  * @param a_tree_model $categories_model
  * @param a_model $brands_model
  * @param category_filters $category_filters
  * @param array $category выбранная категория
  */
 public function __construct(goods $model, a_tree_model $categories_model, a_model $brands_model, category_filters $category_filters, $category)
 {
  $this->appHelper = applicationHelper::getInstance();
  $this->model = clone $model;
  $this->model->ignore_page_num(false);
  $this->categories_model = $categories_model;
  $this->brands_model = $brands_model;
  $this->category_filters_model = $category_filters;
  $this->category_filters_model->ignore_page_num(true);

  if (isset($category['id'])) $this->selected_categories[] = $category['id'];
  $this->category = $this->categories_model->find_item_in_structure_by_id($this->categories_model->get_structure(), $category['id']);
  if (isset($this->category['sub'])) $this->categories_to_put = $this->category['sub'];
  else
  {
   $parent_category = $this->categories_model->find_item_in_structure_by_id($this->categories_model->get_structure(), $category['parent']);
   if (isset($parent_category['sub'])) $this->categories_to_put = $parent_category['sub'];
  }
 }


 /**
  * Возвращает товары, которые попадают под переданные ранее фильтры
  * @param int $items_on_page
  * @param bool $include_append_categories
  * @return array
  */
 public function get_all_with_filters($items_on_page = 999999, $include_append_categories = false)
 {
  return $this->model->get_all($this->format_full_where($this->model->get_table_name(), $include_append_categories), $items_on_page);
 }


 /**
  * Проверяет, выбрана ли подкатегория переданной категории
  * @param array $category
  * @return bool
  */
 public function is_active_category($category)
 {
  if (in_array($category['id'], $this->get_selected_categories())) return TRUE;
  
  if (array_key_exists('sub', $category))
  {
   foreach ($category['sub'] as $subcat)
   {
    if ($this->is_active_category($subcat)) return TRUE;
   }
  }
  
  return FALSE;
 }

 /**
  * Возвращает категории для вывода
  * @return array
  */
 public function get_categories_to_put()
 {
	 if (empty($this->selected_brands)) {
		 return $this->categories_to_put;
	 }
	 
	 $result = [];
	 $tableName = $this->model->get_table_name();
	 
	 foreach ($this->categories_to_put as $category) {
		 $allCategories = $this->get_all_subcategories($category);
		 $where = "`$tableName`.`external_parent` in (". implode(', ', $allCategories) .") " . $this->format_where_for_brands($tableName);
		 $goods = $this->model->get_all($where);
		 if (!empty($goods)) {
			 $result[] = $category;
		 }
	 }
	 
  
	 
	 return $result;
 }

 /**
  * Возвращает бренды для вывода
  * @param bool $include_append_categories
  * @return array
  */
 public function get_brands_to_put($include_append_categories = false)
 {
  $main_table_name = $this->model->get_table_name();
  $table_name = $this->brands_model->get_table_name();

  return mysql_data_assoc("select `$table_name`.* ".mysql_assoc_select(['caption'], null, $table_name)."
						   from `$table_name` join `{$main_table_name}` on `{$main_table_name}`.`brand`=`{$table_name}`.`id`
						   where ({$this->format_where_for_code_discount()} {$this->format_where_for_categories($main_table_name, $include_append_categories)})
						         " . (!empty($this->selected_brands) ? " or `$table_name`.`id` in (" . implode(', ', $this->selected_brands) . ")" : '') . "
						   group by `{$table_name}`.`id`");
 }



 /**
  * Устанавливает все доступные фильтры из GET массива
  * @return $this
  */
 public function set_filters()
 {
  foreach ($_GET as $filter => $value)
  {
   if (strpos($filter, 'filter_') === 0)
   {
    $filter_name = 'set_'.str_replace('filter_', '', $filter);
    if (method_exists($this, $filter_name)) $this->$filter_name($value);
   }
  }
  
  if (isset($_GET['filter']) and is_array($_GET['filter']))
  {
   foreach ($_GET['filter'] as $filter_id => $filter_values)
   {
    foreach ($filter_values as $value)
    {
     $this->addCheckedFilter($filter_id, $value);
    }
   }
  }

  return $this;
 }

 /**
  * Возвращает массив выбранных категорий
  * @return array
  */
 public function get_selected_categories()
 {
  return $this->selected_categories;
 }

 /**
  * Проверяет, выбрана ли переденная категория
  * @param $category
  * @return bool
  */
 public function is_category_checked($category)
 {
  return in_array($category, $this->selected_categories);
 }

 /**
  * Проверяет, выбран ли переданный бренд
  * @param int $brand
  * @return bool
  */
 public function is_brand_checked($brand)
 {
  return in_array($brand, $this->selected_brands);
 }

 
 
 
 /**
  * Устанавливает массив выбранных категорий
  * @param array $categories
  * @return $this
  */
 public function set_selected_categories($categories)
 {
  if (is_array($categories)) $this->selected_categories = $categories;

  return $this;
 }

 /**
  * Устанавливает массив выбраных брендов
  * @param array $brands
  * @return $this
  */
 public function set_brands($brands)
 {
  if (is_array($brands) and !empty($brands))
  {
   $brands = safe_array($brands);
   $data = $this->brands_model->get_all("`static` in ('".implode("', '", $brands)."')");
   $this->selected_brands_data = $data;
   $this->selected_brands = [];
   foreach ($data as $line) $this->selected_brands[] = $line['id'];
  }
  
  return $this;
 }

 /**
  * Устанавливает значения фильтра
  * @param bool $value
  * @return $this
  */
 public function set_with_code_discount_only($value)
 {
  $this->with_code_discount_only = (bool)$value;
  return $this;
 }

	/**
	 * Возвращает ссылку для генерации скидочкого листа
	 * @param array $good
	 * @return string
	 */
	public function discountMakeHref(array $good)
	{
		if (!array_key_exists('id', $good)) return '';
		
		$result = $this->appHelper->get_url_maker()->d_module_url('profile') . '?category=';
		$category = $this->category;
		
		if (!empty($category['parent'])) {
			$result .= $category['parent'] . '&amp;subcategory=' . $category['id'];
		} else {
			$result .= $category['id'];
		}
		
		return $result . (!empty($this->selected_brands) ? '&amp;brand=' . reset($this->selected_brands) : '') . '&amp;good=' . $good['id'];
	}

	/**
	 * Возвращает выбранные бренды в виде параметра для передачи в url (для категорий)
	 * @return string
	 */
	public function getSelectedBrandsForUrl()
	{
		if (empty($this->selected_brands)) {
			return '';
		}
		
		$result = '?';
		foreach ($this->selected_brands_data as $brand) {
			$result .= 'filter_brands[]=' . $brand['static'] . ($brand != end($this->selected_brands_data) ? '&' : '');
		}
		
		return $result;
	}

 /**
  * Возвращает дополнительные фильтры
  * @param bool $include_append_categories
  * @return array
  */
 public function getAppendFilters($include_append_categories = false)
 {
  $tablename = $this->model->get_table_name();
  
  $data = $this->categories_model->get_category_filters($this->category['id']);
  foreach ($data as $index => $line)
  {
   $goodsWithoutFilterIds = [];
   $goodsWithoutFilter = $this->model->get_all($this->format_full_where($tablename, $include_append_categories, $line['id']), 999999, false);
   foreach ($goodsWithoutFilter as $good) $goodsWithoutFilterIds[] = $good['id'];
   
   $items = $this->category_filters_model->getFilterItems($line['id'], $goodsWithoutFilterIds);
   if (empty($items))
   {
    unset($data[$index]);
   }
   else
   {
    $data[$index]['items'] = $items;
   }
  }
  
  return $data;
 }

 /**
  * Проверяет, выбран ли переданный элемент из дополнительных фильтров
  * @param int $item
  * @return bool
  */
 public function is_filter_item_checked($item)
 {
  return in_array($item, $this->checkedAppendFiltersAll);
 }

 /**
  * Устанавливает выбранное значение для фильтра
  * @param int $filter
  * @param int $value
  * @return $this
  * @internal param int $filter
  */
 private function addCheckedFilter($filter, $value)
 {
  $filter = (int) $filter;
  $value = (int) $value;
  
  if ($value > 0 and !in_array($value, $this->checkedAppendFiltersAll))
  {
   if (!isset($this->checkedAppendFiltersByFilters[$filter])) $this->checkedAppendFiltersByFilters[$filter] = [];
   
   $this->checkedAppendFiltersAll[] = $value;
   $this->checkedAppendFiltersByFilters[$filter][] = $value;
  }
  
  return $this;
 }
 
	

 /**
  * Формирует строку where для запроса к модели товаров
  * @param string $tablename имя главной таблицы
  * @param bool $include_append_categories
  * @param int|null $exclude_filter упускать переданный динамичный фильтр 
  * @return string
  */
 protected function format_full_where($tablename, $include_append_categories, $exclude_filter = null)
 {
  $result = 
	  $this->format_where_for_code_discount() .
      $this->format_where_for_categories($tablename, $include_append_categories).
      $this->format_where_for_brands($tablename) .
      $this->format_where_for_append_filters($tablename, $exclude_filter);
  
  return $result;
 }


 /**
  * Формирует массив всех подкатегорий для переданной категории
  * @param array $category
  * @return array
  */
 private function get_all_subcategories($category)
 {
  if (is_array($category))
  {
   $result = [$category['id']];

   if (array_key_exists('sub', $category) and is_array($category['sub']))
   {
    foreach ($category['sub'] as $cat) $result = array_merge($result, $this->get_all_subcategories($cat));
   }
  }
  else $result = [];
  
  return $result;
 }

 /**
  * Возвращает параметр where для категорий
  * @param string $tablename
  * @param bool $include_append_categories
  * @return string
  */
 private function format_where_for_categories($tablename, $include_append_categories)
 {
  $categories = $this->get_all_subcategories($this->category);
  
  if ($include_append_categories)
  {
   $app_categories_table_name = $this->model->append_categories_table_name();
   $append_categories_where = " or `$tablename`.`id` in (select `good` from `$app_categories_table_name` where `category` in (".implode(', ', $categories)."))";
  }
  else
  {
   $append_categories_where = '';
  }

  
  return " and (`$tablename`.`external_parent` in (".implode(', ', $categories).") $append_categories_where )";
 }

 /**
  * Возвращает параметр where для брендов
  * @param string $tablename
  * @return string
  */
 private function format_where_for_brands($tablename)
 {
  if (empty($this->selected_brands)) return '';
  
  return " and `$tablename`.`brand` in (".implode(', ', $this->selected_brands).")";
 }

 /**
  * Возвращает параметр where для дополнительных фильтров
  * @param string $tablename
  * @param int|null $exclude_filter
  * @return string
  */
 private function format_where_for_append_filters($tablename, $exclude_filter = null)
 {
  $result = '';
  
  foreach ($this->checkedAppendFiltersByFilters as $filter_id => $checked_values)
  {
   if ($filter_id != $exclude_filter)
   {
    $result .= " and `$tablename`.`id` in (select `external_parent` from `good_filters` where `value` in (" . implode(', ', $checked_values) . "))";
   }
  }
  
  return $result;
 }

	/**
	 * Возвращает параметр where для вывода товаров только со скидкой по коду
	 * @return string
	 */
	private function format_where_for_code_discount()
	{
		if ($this->with_code_discount_only === false) return '1=1';
		return "`{$this->model->get_table_name()}`.`code_discount` > 0";
	}

}