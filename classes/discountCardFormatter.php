<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 18.11.14
 */

/**
 * Форматирует карточку скидки для товара
 */
class discountCardFormatter {

	/**
	 * @var a_controller
	 */
	protected $controller;

	/**
	 * @var applicationHelper
	 */
	protected $appHelper;

	/**
	 * @var applicationController
	 */
	protected $app;

	/**
	 * @var string шаблон
	 */
	protected $template = 'good-discount-card.tpl';

	/**
	 * @var discount_codes
	 */
	protected $model;

	/**
	 * @var goods
	 */
	protected $goodsModel;

	/**
	 * Конструктор
	 * @param a_controller $controller
	 */
	public function __construct(a_controller $controller)
	{
		$this->controller = $controller;
		$this->appHelper = applicationHelper::getInstance();
		$this->app = $this->appHelper->getAppController();
		$this->smarty = $this->appHelper->getSmarty();
		$this->model = new discount_codes();
		$this->goodsModel = new goods();
	}

	/**
	 * Возвращает карточку для скидки товара
	 * @param $good_id
	 * @param string|null $code скидочный код
	 * @param array $codeItem элемент кода из БД
	 * @return string
	 * @throws Exception_404 если товар не найден
	 */
	public function renderCard($good_id, $code = null, array $codeItem = [])
	{
		$good_id = (int)$good_id;
		$good = $this->goodsModel->get_item("`{$this->goodsModel->get_table_name()}`.`id`='$good_id'");
		if (empty($good)) throw new Exception_404;
		$this->controller->set_variable('item', $good);

		$price = (new discountCodeModifier())->goodApply($good);

		$showDescribe = 1;
		$showAlts = 1;
		$used = 0;
		
		$technologies = [];
		$tabsModel = new good_tabs();
		$tabs = $tabsModel->get_all_of_external_parent($good_id);
		if (!empty($tabs)) {
			$describe = reset($tabs)['text'];
			foreach ($tabs as $tab) {
				$images = $tabsModel->get_tech_images($tab['id']);;
				if (!empty($images)) {
					$technologies += $images;
				}
			}
		}
		else {
			$describe = '';
		}
		
		if (!empty($codeItem)) {
			$showDescribe = $codeItem['show_description'];
			$showAlts = $codeItem['show_alts'];
			$used = $codeItem['used'];
		}

		$analogs = $this->goodsModel->get_analogs($good_id);
		
		
		return $this->app->render(
			$this->template,
			$this->controller,
			false,
			null,
			[
				'good' => $good,
				'code' => $code,
				'price' => $price,
				'price_without_discount' => $good['price'],
				'describe' => $describe,
				'technologies' => $technologies,
				'show_describe' => $showDescribe,
				'show_alts' => $showAlts,
				'used' => $used,
				'analogs' => $analogs,
			]
		);
	}

	/**
	 * Возвращает карточку для скидки по коду скидки
	 * @param string $code
	 * @return string
	 * @throws Exception_404 если код не найден
	 */
	public function renderByCode($code)
	{
		$code = $this->model->get_item("`{$this->model->get_table_name()}`.`code`='" . safe($code) . "'");
		if (empty($code)) throw new Exception_404;
		
		return $this->renderCard($code['good'], $code['code'], $code);
	}
}