<?php
/**
 * @package ncms
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 16.04.14
 */

/**
 * Автозагрузка классов
 * @package ncms
 */
class autoloader {

 /**
  * @var array массив директорий для поиска классов
  */
 protected static $dir_list = array();

 /**
  * Инициализация автозагрузки
  * @param array $dir_list массив директорий для поиска классов
  */
 public static function attach($dir_list = array())
 {
  self::$dir_list = $dir_list;
  
  spl_autoload_register(array('self', 'load_class'));
 }
 
 

 /**
  * Проверка существования класса и его подключение
  * @param string $class
  * @return bool
  */
 protected static function load_class($class)
 {
  if (!empty($class) and is_array(self::$dir_list))
  {
   $filename = $class.'.php';
   foreach (self::$dir_list as $dir)
   {
    if (file_exists($dir.$filename))
    {
     require_once $dir.$filename;
     if (class_exists($class)) return TRUE;
    }
   }
  }
  
  return FALSE;
 }
}