<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 02.07.14
 */

/**
 * Интерфейс для экспорта
 */
interface i_exporter {

 /**
  * Проводит экспорт файла
  * @param string $file_name имя файла
  * @param string $dir директория для сохранения файла
  * @param bool $std_out ключ, который позволяет выдать файл в std_out
  * @return string ссылка на файл
  * @throws Exception в случае ошибки
  */
 public function export($file_name, $dir = 'project_images/', $std_out = FALSE);
}