<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 02.07.14
 */


abstract class xls_exporter extends a_importer_and_exporter implements i_exporter {

 /**
  * Проводит инициализацию библиотеки и возвращает объект для дальнейшей работы
  * @return PHPExcel
  */
 protected function get_php_excel()
 {
  require_once __DIR__.'/../PHPExcel/PHPExcel.php';
  return new PHPExcel();
 }
 
 /**
  * Сохраняет файл для чтения
  * @param PHPExcel $object объект с данными
  * @param string $file_name имя файла
  * @param string $dir директория для сохранения файла
  * @param bool $std_out ключ, который позволяет выдать файл в std_out
  * @throws Exception в случае ошибки
  * @return bool
  */
 protected function save_file($object, $file_name, $dir = 'project_images/', $std_out = FALSE)
 {
  switch (file_extension($file_name))
  {
   case 'xlsx': $format = 'Excel2007'; break;
   default: $format = 'Excel5'; break;
  }
  
  $objWriter = PHPExcel_IOFactory::createWriter($object, $format);
  
  if ($std_out)
  {
   header_remove("Content-Type");
   header('Content-Еype: application/vnd.ms-excel');
   header('Content-Disposition: attachment; filename="'.$file_name.'"');
   $objWriter->save('php://output');
  }
  else $objWriter->save($dir.$file_name);

  return TRUE;
 }
}