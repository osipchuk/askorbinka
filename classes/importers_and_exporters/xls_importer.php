<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 04.06.14
 */

/**
 * Класс для импорта XLS файлов
 */
abstract class xls_importer extends a_importer_and_exporter implements i_importer {
 
 /**
  * Открывает файл для чтения
  * @param array $file
  * @return PHPExcel
  * @throws Exception в случае неправильного формата
  */
 protected function load_file($file)
 {
  $path = pathinfo($file['name']);
  if (!in_array(strtolower($path['extension']), array('xls', 'xlsx'))) throw new Exception('File format error');
  
  require_once __DIR__.'/../PHPExcel/PHPExcel.php';
  $inputFileType = PHPExcel_IOFactory::identify($file['tmp_name']);
  $objReader = PHPExcel_IOFactory::createReader($inputFileType);
  $result = $objReader->load($file['tmp_name']);
  
  ini_set('precision',12);
  
  return $result;
 }


} 