<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 22.05.14
 */

/**
 * Класс для импорта товаров
 */
class price_importer extends xls_importer {

 /**
  * @var goods
  */
 protected $goods_model;

 /**
  * @var array категории
  */
 protected $categories = array();

 public function __construct()
 {
  $this->goods_model = new goods();
 }

 /**
  * Проводит импорт файла
  * @param array $file
  * @return int количество обновленных позиций
  * @throws Exception в случае ошибки
  */
 public function import($file)
 {
  $php_excel = $this->load_file($file);
  $worksheet = $php_excel->getActiveSheet();
  $lines_count = $worksheet->getHighestRow();
  
  $categories = $this->get_categories();
  $brands = $this->get_brands();

  
  $result = 0;
  
  for ($i=1; $i<=$lines_count; $i++)
  {
   $good = trim($worksheet->getCellByColumnAndRow(0, $i)->getValue());
   $cat_articul = (int)$worksheet->getCellByColumnAndRow(1, $i)->getValue();
   $brand_caption = strtolower($worksheet->getCellByColumnAndRow(2, $i)->getValue());
   $code = safe(trim($worksheet->getCellByColumnAndRow(3, $i)->getValue()));
   $articul = safe(trim($worksheet->getCellByColumnAndRow(4, $i)->getValue()));
   $avail = $worksheet->getCellByColumnAndRow(7, $i)->getOldCalculatedValue();
   $price = (float) $worksheet->getCellByColumnAndRow(8, $i)->getOldCalculatedValue();
   
   if (!$avail) $avail = (bool) $worksheet->getCellByColumnAndRow(7, $i)->getValue(); else $avail = true;
   if ($price <= 0) $price = (float) $worksheet->getCellByColumnAndRow(8, $i)->getValue();
   
   
   if (array_key_exists($cat_articul, $categories)) $external_parent = $categories[$cat_articul]; else $external_parent = 'null';
   if (array_key_exists($brand_caption, $brands)) $brand = $brands[$brand_caption]; else $brand = 'null';
   $good = preg_replace("/[\s]*[\d]+%$/", '', $good);
   $good_name = $good;
   $category_name = '';
   
   if (!empty($good_name) and !empty($code) and $price > 0)
   {
	
	$set = "`articul`='$articul', `code`='$code', `price`='$price', `avail`='$avail'";
	
//	echo $i.' - '.$set.'<br>';

	if ($item = $this->goods_model->get_item("`code`='$code'"))
	{
	    $id = $item['id'];
		if (empty($item['external_parent']) and !empty($external_parent)) $set .= ", `external_parent`=$external_parent";
		if (empty($item['brand']) and !empty($brand)) $set .= ", `brand`= $brand";
		
	    mysql_update($this->goods_model->get_table_name(), $set, "`id`='$id'");
	    if (mysql_error()) $id = null;
	}
	else
	{
		$static = $this->make_static([$category_name, $good_name]);
		$set .= ", `caption_{$this->get_lang()}`='".safe($good_name)."', `static`='$static', `external_parent`=$external_parent, `brand`= $brand";
	    $id = mysql_insert($this->goods_model->get_table_name(), $set);
	}

	if (!empty($id))
	{
	 $result++;
	}
	else echo $i.' - '.mysql_error() . '<br><br>';
   }
  }
  
  programsCoverage::updateAllProgramsIndex();
  
  return $result;
 }
 
 


 /**
  * Возвращает ассоциативный массив категорий
  * 
  * Ключ - артикул категории, значение - id категории
  * 
  * @return array
  */
 protected function get_categories()
 {
  $result = array();
  $categories = new categories();
  foreach ($categories->get_all() as $cat) if (!empty($cat['1c_id'])) $result[$cat['1c_id']] = $cat['id'];
  
  return $result;
 }

 /**
  * Возвращает ассоциативный массив категорий
  *
  * Ключ - артикул категории, значение - id категории
  *
  * @return array
  */
 protected function get_brands()
 {
  $result = array();
  $data = new brands();
  
  foreach ($data->get_all() as $item)
  {
   $key = 'caption_'.$this->get_lang();
   if (!empty($item[$key])) $result[strtolower($item[$key])] = $item['id'];
  }

  return $result;
 }

 /**
  * Генерирует статик
  * @param string|array $args
  * @return string
  * @throws Exception в случае ошибки
  */
 protected function make_static($args)
 {
  if (is_array($args)) $args = implode(' ', $args);
  if (!is_string($args)) throw new Exception('static args error');
  
  return preg_replace(["/[\s]+/", "/[-]+/", "/^[-]+/", "/[-]+$/"], ['-', '-', '', ''], translit($args));
 }
}