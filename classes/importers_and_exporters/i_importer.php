<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 02.07.14
 */

/**
 * Интерфейс для импорта
 */
interface i_importer {

 /**
  * Проводит импорт файла
  * @param array $file
  * @return int количество обновленных позиций
  * @throws Exception в случае ошибки
  */
 public function import($file);
}