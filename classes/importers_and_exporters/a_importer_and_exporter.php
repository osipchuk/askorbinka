<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 22.05.14
 */

/**
 * Абстрактный класс для импорта и экспорта файлов 
 */
abstract class a_importer_and_exporter {

 /**
  * @var string языковая версия
  */
 protected $lang;

 /**
  * @var a_model|null модель элементов для импорта/экспорта
  */
 protected $model;

 /**
  * Устанавливает языковую версию
  * @param string $lang
  * @return $this
  */
 public function set_lang($lang)
 {
  $this->lang = $lang;

  return $this;
 }

 /**
  * Возвращает языковую версию
  * @return string
  * @throws Exception если не задано
  */
 public function get_lang()
 {
  if (is_null($this->lang)) throw new Exception('lang not found');
  
  return $this->lang;
 }

 /**
  * Устанавливает модель
  * @param a_model $model
  * @return $this
  */
 public function set_model(a_model $model)
 {
  $this->model = $model;

  return $this;
 }

 /**
  * Возвращает модель
  * @return a_model
  * @throws Exception если модель не задана
  */
 public function get_model()
 {
  if (empty($this->model)) throw new Exception('model is empty');
  return $this->model;
 }
 
 
}