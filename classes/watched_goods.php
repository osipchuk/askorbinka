<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 02.10.14
 */

/**
 * Класс для записи просмотренных товаров
 */
class watched_goods {

 /**
  * @var a_model модель товаров
  */
 private $goods_model;

 /**
  * @var array массив просмотренных товаров
  */
 private $watched_goods = array();

 /**
  * Конструктор
  * @param a_model $goods_model модель товаров
  */
 public function __construct(a_model $goods_model)
 {
  $this->goods_model = $goods_model;
  $this->load_watched_goods_from_session();
 }

 /**
  * Деструктор
  * Сохранение просмотренных товаров в сессию
  */
 public function __destruct()
 {
  $_SESSION['watched_goods'] = $this->watched_goods;
 }

 /**
  * Добавляет товар к просмотренным
  * @param int|array $good
  * @return $this
  */
 public function add_good($good)
 {
  if (is_int($good)) $good = $this->goods_model->get_item("`id`='$good'");
  
  if (is_array($good) and !empty($good))
  {
   if (array_key_exists($good['id'], $this->watched_goods)) unset($this->watched_goods[$good['id']]);
   $this->watched_goods[$good['id']] = $good;
  }
  
  return $this;
 }

 /**
  * Возвращает массив просмотренных товаров, удаляя дубли
  * @return array
  */
 public function get_goods()
 {
  return array_reverse($this->watched_goods);
 }


 /**
  * Получает просмотренные товары из сессии и записывает их в свойство
  * @return array
  */
 private function load_watched_goods_from_session()
 {
  if (!isset($_SESSION['watched_goods'])) $_SESSION['watched_goods'] = array();
  $this->watched_goods = $_SESSION['watched_goods'];
  return $this->watched_goods;
 }

}