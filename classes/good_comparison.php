<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 08.10.14
 */

/**
 * Класс сравнения товаров
 */
class good_comparison {

 /**
  * @var categories
  */
 protected $categories_model;

 /**
  * @var goods
  */
 protected $goods_model;

 /**
  * @var brands
  */
 protected $brands_model;

 /**
  * @var good_filters
  */
 protected $good_filters;

 /**
  * @var null|array массив характеристик для категории
  */
 protected $properties;

 /**
  * @var array выбранные для сравнения товарв
  */
 protected $selected_goods = array();

 /**
  * @var array выбранные бренды
  */
 protected $brands = array();

 /**
  * @var int|null выбранная категория
  */
 protected $category;

 /**
  * Конструктор
  * @param categories $categories_model
  * @param goods $goods_model
  * @param brands $brands_model
  */
 public function __construct(categories $categories_model, goods $goods_model, brands $brands_model)
 {
  $this->categories_model = $categories_model;
  $this->goods_model = $goods_model;
  $this->brands_model = $brands_model;
  $this->selected_goods = isset($_SESSION['comparison_selected_goods']) ? $_SESSION['comparison_selected_goods'] : array();
 }

 function __destruct()
 {
  $_SESSION['comparison_selected_goods'] = $this->selected_goods;
 }


 /**
  * Возвращает значения характеристик выбранных товаров
  * @param bool $put_selected_goods true - выводить выбранные товары. false - выводить все товары категории (и бренда)
  * @return array
  */
 public function get_data($put_selected_goods = false)
 {
  $goods = $this->get_goods($put_selected_goods);
  foreach ($goods as &$good)
  {
   $params = $this->goods_model->get_params($good['id']);
   $good['params'] = $params;
  }
  
  return $goods;
 }

 /**
  * Возвращает массив атрибутов для выбранной категории
  * @return array
  */
 public function get_properties()
 {
  if (is_null($this->properties)) $this->properties = $this->categories_model->get_category_params($this->get_category());
  
  return $this->properties;
 }

 /**
  * Возвращает бренды для вывода
  * @return array
  */
 public function get_brands()
 {
  return $this->get_good_filters()->get_brands_to_put();
 }

 /**
  * Проверяет, выбран ли переданный бренд
  * @param int $brand
  * @return bool
  */
 public function is_brand_checked($brand)
 {
  return $this->good_filters->is_brand_checked($brand);
 }

 /**
  * Возвращает категорию товаров
  * @return int
  * @throws Exception если не задана
  */
 public function get_category()
 {
  if (empty($this->category)) throw new Exception('category is empty');
  return $this->category;
 }

 /**
  * Устанавливает категорию товаров
  * @param int $category
  * @return $this
  */
 public function set_category($category)
 {
  $this->category = $category;

  return $this;
 }

 /**
  * Добавляет товар к списку сравнения
  * @param int $good_id
  * @return $this
  * @throws Exception в случае ошибки товара
  */
 public function add_good($good_id)
 {
  $good_id = (int)$good_id;
  $good = $this->goods_model->get_item("`{$this->goods_model->get_table_name()}`.`id`='$good_id'");
  if (empty($good)) throw new Exception('good error');
  
  $this->selected_goods[$good_id] = $good;
  return $this;
 }

 /**
  * Удаляет товар из списка сравнения
  * @param int $good_id
  * @return $this
  */
 public function remove_good($good_id)
 {
  if (isset($this->selected_goods[$good_id])) unset($this->selected_goods[$good_id]);
  return $this;
 }

 /**
  * Проверяет, выбран ли для сравнения переданный товар
  * @param int $good_id
  * @return bool
  */
 public function is_good_selected($good_id)
 {
  return array_key_exists($good_id, $this->selected_goods);
 }


 /**
  * Возвращает класс фильтров товаров
  * @return good_filters
  * @throws Exception
  */
 private function get_good_filters()
 {
  if (is_null($this->good_filters))
  {
   $this->good_filters = new good_filters($this->goods_model, $this->categories_model, $this->brands_model, new category_filters(), $this->categories_model->get_item("`id`='{$this->get_category()}'"));
   $this->good_filters->set_filters();
  }
  
  return $this->good_filters;
 }


 /**
  * Возвращает массив товаров для вывода
  * @param bool $selected_goods
  * @return array
  */
 private function get_goods($selected_goods)
 {
  $result = array();
  $category = $this->categories_model->get_item("`id`='{$this->get_category()}'");
  
  if ($selected_goods)
  {
   $all_goods = $this->selected_goods;
  }
  else
  {
   $all_goods = $this->get_good_filters()->get_all_with_filters(999999, true);
  }

  foreach ($all_goods as $good) if ($this->categories_model->check_good_item_can_be_compared_in_category($category, $good)) $result[] = $good;
  
  return $result;
 }
}