<?php
/**
 * @package default
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 07.07.14
 */

/**
 * Класс для работы и вывода google maps
 */
class google_maps {
 
 protected $geolocation_server = 'http://maps.googleapis.com/maps/api/geocode/';

 /**
  * @var string ширина карты
  */
 protected $width = '100%';

 /**
  * @var string высота карты
  */
 protected $height = '368px';

 /**
  * @var array коррдинаты маркеров
  */
 protected $markers = array();

 /**
  * @var string|null языковая версия карты
  */
 protected $lang;

 /**
  * Выводит карту из центрои в координатах
  * @param float $x
  * @param float $y
  * @param int $zoom
  * @param string $id ID контейнера
  * @return string
  */
 public function show_coords_map($x, $y, $zoom = 9, $id = 'map_canvas')
 {
  $markers = '';
  
  foreach ($this->markers as $marker)
  {
   if (is_object($marker->icon))
   {
    $marker_icon = 'var marker_icon = {url: "'.$marker->icon->get_url().'", anchor: new google.maps.Point('.implode(', ', $marker->icon->get_anchor()).')}; ';
   }
   else $marker_icon = ' var marker_icon = null; ';
   
   $markers .= $marker_icon.'
                var marker = new google.maps.Marker({
                  position: new google.maps.LatLng('.$marker->x.', '.$marker->y.'),
                  map: map,
                  icon: marker_icon,
                  '.($marker->movable ? 'draggable: true, ' : '').'
                  title: "'.htmlspecialchars($marker->title).'"
                }); ';
   
   if ($marker->movable) $markers .= 'google.maps.event.addListener(marker, "dragend", function(event) { set_marker_coords(event); })';
  }
  
  return '<div id="'.$id.'" style="width: '.$this->width.'; height: '.$this->height.';"></div>
          <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false'.(!empty($this->lang) ? '&language='.$this->lang : '').'"></script>
          <script src="/js/google-maps.js"></script>
          <script>
           function initialize() {
            var mapOptions = {
             center: new google.maps.LatLng('.$x.', '.$y.'),
               zoom: '.$zoom.',
               mapTypeId: google.maps.MapTypeId.ROADMAP
             };
             var map = new google.maps.Map(document.getElementById("'.$id.'"), mapOptions);
            '.$markers.'
           }
           
           $(function() { initialize(); })
          </script>';
 }

 /**
  * Выводит карту из центром из адресом
  * @param string $address
  * @param int $zoom
  * @param string $id ID контейнера
  * @return string
  */
 public function show_address_map($address, $zoom = 9, $id = 'map_canvas')
 {
  try {
   $coords = $this->get_address_coords($address);
  }
  catch (Exception $e)
  {
   $coords = new stdClass();
   $coords->x = 0;
   $coords->y = 0;
  }
  
  return $this->show_coords_map($coords->x, $coords->y, $zoom, $id);
  
 }

 /**
  * Генерирует ссылку для отображения точки в iframe
  * @param int $x
  * @param int $y
  * @param int $zoom
  * @return string
  */
 public function coords_iframe_url($x, $y, $zoom = 14)
 {
  return "http://maps.google.com/?output=embed&q={$x},{$y}&z={$zoom}".(!empty($this->lang) ? '&language='.$this->lang : '');
 }

 /**
  * Установка языковой версии карты
  * @param string $lang
  * @return $this
  */
 public function set_lang($lang)
 {
  $this->lang = $lang;

  return $this;
 }

 /**
  * Добавляет на карту маркер
  * @param int $x
  * @param int $y
  * @param string $title
  * @param google_maps_icon|null $icon изображение маркера
  * @param bool $movable флаг возможности перемещения
  * @return $this
  */
 public function add_marker($x, $y, $title = '', $icon = null, $movable = FALSE)
 {
  $marker = (object)compact('x', 'y', 'title', 'icon', 'movable');
  
  $this->markers[] = $marker;
  return $this;
 }

 /**
  * Возвращает координаты перенаддого адреса
  * @param string $address
  * @return stdClass
  * @throws Exception в случае ошибки
  */
 public function get_address_coords($address)
 {
  $result = $this->get_content($this->geolocation_server.'json?address='.urldecode($address).'&sensor=false');
  if (empty($result)) throw new Exception('unknown error');
  
  $result = json_decode($result);
  if (is_object($result) and $coords = @$result->results[0]->geometry->location and @is_object($coords))
  {
   $result = new stdClass();
   $result->x = $coords->lat;
   $result->y = $coords->lng;
   
   return $result;
  }

  throw new Exception('unknown error');
 }
 
 
 
 /**
  * Возвращает HTML страницы
  * @param string $url
  * @return string
  */
 protected function get_content($url)
 {
  return get_site_content($url);
 }
}