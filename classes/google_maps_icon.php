<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 07.07.14
 */

/**
 * Маркер google maps
 * 
 * Упрощенный варииант
 */
class google_maps_icon {

 /**
  * @var string url изображения
  */
 protected $url;

 /**
  * @var array
  */
 protected $anchor = array();

 /**
  * Конструктор
  * @param string $url
  * @param array $anchor
  */
 public function __construct($url, $anchor = array(0, 0))
 {
  $this->url = (string)$url;
  $this->anchor = $anchor;
 }

 /**
  * Возвращает url иконки
  * @return string
  */
 public function get_url()
 {
  return $this->url;
 }

 /**
  * Возвращает якорь иконки
  * @return array
  */
 public function get_anchor()
 {
  return $this->anchor;
 }
}