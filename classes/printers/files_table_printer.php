<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 22.09.14
 */

/**
 * Класс для вывода списка загруженных файлов
 */
class files_table_printer extends a_printer {

 /**
  * @var string имя блока
  */
 private $name;

 /**
  * @var bool наличия caption
  */
 private $has_caption = FALSE;

 /**
  * @var array массив файлов
  */
 private $files = array();

 /**
  * Генерирует HTML
  * @return string
  */
 public function put()
 {
  $result = "<script>
			  var add_{$this->get_name()}_html = '".safe($this->add_file_str())."';
			  $(function() {
			  " .
      (empty($this->files) ?
      "$(document).on('keyup','#{$this->get_name()}-table input[type=\"text\"]', function() { text_selected( $(\"#{$this->get_name()}-table\"), add_{$this->get_name()}_html); });" :
      "$(document).on('change','#{$this->get_name()}-table input[type=\"file\"]', function() { file_selected( $(\"#{$this->get_name()}-table\"), add_{$this->get_name()}_html); });") . 
            "});
			 </script>
			 <table class='table_2' cellpadding='0' cellspacing='0' border='0' style='width:100%;' id='{$this->get_name()}-table'" . (empty($this->files) ? ' data-without-files="1"' : '') .">
			 <thead>
			  <tr>
			   {$this->table_header()}
			  </tr>
			  </thead>
			  <tbody>".$this->add_file_str();

  foreach ($this->structure as $file) $result .= '<tr>'.$this->table_tr($file).'</tr>';
  
  $result .= ' </tbody>
              </table>';
  
  return $result;
 }

 /**
  * Обновляет данные в БД
  * @param int $parent_id родительский элемент
  * @param string $table_name имя таблицы в БД
  * @return int
  */
 public function update($parent_id, $table_name)
 {
  return $this->update_update_files($table_name) + $this->update_delete_files($table_name) + $this->update_new_files($parent_id, $table_name);
 }

 /**
  * Устанавливает имя блока файлов
  * @param string $name
  * @return $this
  */
 public function set_name($name)
 {
  $this->name = $name;

  return $this;
 }

 /**
  * Возвращает имя блока файлов
  * @return string
  */
 public function get_name()
 {
  if (empty($this->name)) return 'files';
  return $this->name;
 }

 /**
  * Устанавливает наличие caption
  * @param boolean $has_caption
  * @return $this
  */
 public function set_has_caption($has_caption)
 {
  $this->has_caption = (bool)$has_caption;

  return $this;
 }

 /**
  * Возвращает наличие caption
  * @return boolean
  */
 public function get_has_caption()
 {
  return $this->has_caption;
 }

 /**
  * Добавляет файл в структуру
  * @param string $file
  * @param string $file_caption
  * @param bool $image
  * @return $this
  */
 public function add_file($file, $file_caption = '', $image = FALSE)
 {
  if (empty($file_caption)) $file_caption = 'Файл';
  $this->files[$file] = array('caption'=>$file_caption, 'image'=>(bool)$image);
  
  return $this;
 }

 /**
  * Возвращает каталог расположения файла
  * @param array $file_properties
  * @return string
  */
 public function files_dir($file_properties)
 {
  return $file_properties['image'] ? __DIR__.'/../../admin/project_images/' : $this->appHelper->get_global('files_dir');
 }
 
 
 
 
 

 /**
  * Проверка типа структуры
  * @param mixed $structure
  * @return bool
  */
 protected function check_structure_type($structure)
 {
  if (is_array($structure)) return TRUE;
  return FALSE;
 }

 /**
  * Возвращает строку таблицы для добавления нового файла
  * @return string
  */
 protected function add_file_str()
 {
  $result = '<tr>';
  
  foreach ($this->files as $file_name => $file_properties)
  {
   $result .= "<td><input type='file' name='new_{$this->get_name()}_{$file_name}[]'></td>";
  }
  
  if ($this->get_has_caption())
  {
   $result .= "<td>";
   foreach ($this->get_langs() as $lang)
	$result .= "<div><div>{$lang}:</div><input type='text' name='new_{$this->get_name()}_caption_{$lang}[]' style='width: 95%;'></div>";
   $result .= "</td>";
  }
  
  $result .= "<td style='text-align:center;'><input type='text' name='new_{$this->get_name()}_rate[]'></td>
		     <td style='text-align:center;'>-</td>
		    </tr>";
  
  return $result;
 }

 /**
  * Возвразает заголовки таблицы
  * @return string
  */
 protected function table_header()
 {
  $result = '';
  
  foreach ($this->files as $file)
  {
   $result .= "<th".($this->get_has_caption() ? " style='width: 200px;'" : '').">{$file['caption']}</th>";
  }
  
  $result .= ($this->get_has_caption() ? "<th>Названия</th>" : '')."
	        <th style='width:100px;'>Приоритет</th>
		    <th style='width:100px;'>Удалить</th>";
  
  return $result;
 }

 /**
  * Возвращает строку таблицы
  * @param array $file
  * @return string
  */
 protected function table_tr($file)
 {
  $result = '';
  foreach ($this->files as $file_name => $file_properties)
  {
   $result .= '<td>';
   if ($file_properties['image'])
	$result .= imagedelexist($file[$file_name],'', 150, 200, '', 'Отсутствует', $this->files_dir($file_properties), $file_name.'_'.$file['id']);
   else 
	$result .= $file[$file_name.'_filename'];
  }
  
  if ($this->get_has_caption())
  {
   $result .= "<td>";
   foreach ($this->get_langs() as $lang)
	$result .= "<div><div>{$lang}:</div><input type='text' name='{$this->get_name()}_caption_{$lang}[]' style='width: 95%;' value='".htmlspecialchars($file['caption_'.$lang], ENT_QUOTES)."'></div>";
   $result .= "</td>";
  }
  
  $result .= "<td style='text-align:center;'><input type='text' name='{$this->get_name()}_rate[]' value='{$file['rate']}'></td>
		      <td style='text-align:center;'>
		       <input type='checkbox' name='{$this->get_name()}_delete[]' value='{$file['id']}' title='Удалить'>
		       <input type='hidden' name='{$this->get_name()}_id[]' value='{$file['id']}'></td>
		      </td>";
  
  return $result;
 }

 /**
  * Загружает новые файлы
  * @param int $parent_id id родительского элемента
  * @param string $table_name
  * @return int
  */
 protected function update_new_files($parent_id, $table_name)
 {
  $result = 0;
  
  foreach ($_POST['new_'.$this->get_name().'_rate'] as $index => $rate)
  {
   $set = "`external_parent`='$parent_id', `rate`='".(int)$rate."'";
   if ($this->get_has_caption())
   {
	foreach ($this->get_langs() as $lang) $set .= ", `caption_{$lang}`='".safe($_POST['new_'.$this->get_name().'_caption_'.$lang][$index])."'";
   }

   $insert = FALSE;
   if (empty($this->files) and $this->get_has_caption())
   {
    $insert = false;
    $lang = $this->get_langs()[0];
    
    if (!empty($_POST['new_'.$this->get_name().'_caption_'.$lang][$index])) $insert = true;
   }
   else
   {
    foreach ($this->files as $file_name => $file_properties)
    {
     $query_add = $this->upload_file($file_name, $index);
     if (!empty($query_add))
     {
      $set .= $query_add;
      $insert = TRUE;
     }
    }
   }

   if ($insert)
   {
	mysql_insert($table_name, $set);
	$result++;
   }
  }
  
  return $result;
 }

 /**
  * Обновляет существующие файлы
  * @param string $table_name
  * @return int
  */
 protected function update_update_files($table_name)
 {
  $result = 0;
  
  foreach ($_POST[$this->get_name().'_id'] as $index => $file_id)
  {
   $set = "`rate`='".(int)$_POST[$this->get_name().'_rate'][$index]."'";
   if ($this->get_has_caption())
   {
	foreach ($this->get_langs() as $lang) $set .= ", `caption_{$lang}`='".safe($_POST[$this->get_name().'_caption_'.$lang][$index])."'";
   }
	 
	  foreach ($this->files as $inputName => $file) {
		  $set .= query_add($inputName . '_' . $file_id, $inputName, $this->files_dir($file));
	  }
   
   
   mysql_update($table_name, $set, "`id`='".(int)$file_id."'");
   $result++;
  }
  
  return $result;
 }

 /**
  * Удаляет отмеченные файлы
  * @param string $table_name
  * @return int
  */
 protected function update_delete_files($table_name)
 {
  $result = 0;
  $key = $this->get_name().'_delete';
  
  if (is_array($_POST[$key]))
  {
   $files = array();
   foreach ($this->files as $file_name => $file_properties) $files[$file_name] = $this->files_dir($file_properties);
   
   foreach ($_POST[$key] as $file)
   {
	mysql_delete($table_name, "`id`='".(int)$file."'", $files);
	$result++;
   }
  }

	 foreach ($_POST[$this->get_name().'_id'] as $index => $file_id) {
		 foreach ($this->files as $inputName => $file) {
			 delete_query_image($table_name, "`id`='$file_id'", $inputName . '_' . $file_id, $inputName);
		 }
		 
	 }

  return $result;
 }

 /**
  * Загружает файл и возвращает фрагмент mysql запроса
  * @param string $file_name
  * @param int $index
  * @return string
  */
 protected function upload_file($file_name, $index)
 {
  $result = '';
  $key = 'new_'.$this->get_name().'_'.$file_name;
  $accepted_ext = $this->appHelper->get_global('accepted_ext');
  $files_dir = $this->files_dir($this->files[$file_name]);

  if (!empty($_FILES[$key]['name'][$index]))
  {
   if ($image = select_file_name(null, $files_dir, $ext, $_FILES[$key]['name'][$index]) and !empty($ext) and in_array(strtolower($ext), $accepted_ext))
   {
	$path = $files_dir.'/'.$image.'.'.$ext;

	if (move_uploaded_file($_FILES[$key]['tmp_name'][$index], $path))
	{
	 chmod($path,0666);
	 $result = ", `{$file_name}`='{$image}.{$ext}', `{$file_name}_filename`='".safe($_FILES[$key]['name'][$index])."'";
	}
   }
  }
  
  return $result;
 }
}