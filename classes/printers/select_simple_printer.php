<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 28.04.14
 */

/**
 * Генератор HTML обычного селекта
 */
class select_simple_printer extends a_select_printer {

 /**
  * Проверка типа структуры
  * @param mixed $structure
  * @return bool
  */
 protected function check_structure_type($structure)
 {
  if (is_array($structure)) return TRUE;
  return FALSE;
 }

 /**
  * Генерация options
  * @return string
  */
 protected function put_options()
 {
  $result = '';
  foreach ($this->get_assoc_structure() as $value => $caption)
  {
   $result .= '<option value="'.htmlspecialchars($value).'"'.($value == $this->get_value() ? ' selected' : '').'>'.$caption.'</option>';
  }
  return $result;
 }

 /**
  * Возвращает значение из списка
  * @return string|int|null
  */
 protected function get_default_value()
 {
  if (empty($this->structure) or !is_array($this->structure)) return NULL;
  $value = reset($this->structure);
  if (empty($value)) return NULL;
  if (is_array($value)) return $value['id'];
  return reset(array_keys($this->structure));
 }
}