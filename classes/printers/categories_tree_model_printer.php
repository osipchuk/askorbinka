<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 29.12.14
 */

/**
 * Принтер категорий главного меню
 */
class categories_tree_model_printer extends main_tree_model_printer {
    
    /**
     * Генерирует HTML древовидной структуры
     * @return string
     */
    public function put()
    {
        return '<div class="menu_tree"><ul>'.$this->put_level($this->structure->get_structure(true)).'</ul></div>';
    }

    /**
     * @inheritdoc
     */
    protected function put_li_ext_data($item)
    {
        return ($item['hidden'] ? ' [Раздел скрыт]' : '') . parent::put_li_ext_data($item);
    }
}