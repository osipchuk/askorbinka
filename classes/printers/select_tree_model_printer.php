<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 25.04.14
 */

/**
 * Генератор древовидной структуры в виде списка select
 */
class select_tree_model_printer extends a_select_printer {

 /**
  * @var array массив элементы, которые не нужно выводить
  */
 protected $ignored_items = array();

 /**
  * @var array Выводить только заданные модули
  */
 protected $filter_modules = array();

 /**
  * @var array|null список ID элементов, которые нужно выводить, если задан фильтр модулей
  */
 protected $items_to_output;

 /**
  * Добавлят эллемент, который не должен выводиться в структуре
  * @param $item_value
  * @return $this
  */
 public function add_ignored_item($item_value)
 {
  if (!empty($item_value) and !in_array($item_value, $this->ignored_items)) $this->ignored_items[] = $item_value;
  return $this;
 }

 /**
  * Добавляет модуль для вывода
  * 
  * Еслт не добавлено ни одного модуля, выводятся все
  * 
  * @param string $module
  * @return $this
  */
 public function add_filter_module($module)
 {
  if (!in_array($module, $this->filter_modules)) $this->filter_modules[] = $module;
  return $this;
 }
 
 

 /**
  * Генерирует HTML переданного уровня древовидной структуры, вызывает рекурсивно себя же для уровней-потомков
  * @param array $level_structure
  * @return string
  */
 protected function put_level($level_structure)
 {
  $result = '';
  foreach ($level_structure as $item)
  {
   if ($this->check_filter_module($item))
   {
    $divider = '';
    for ($i=0; $i<=$item['level']*3; $i++) $divider .= '&nbsp;';

    $result .= '<option value="'.$item['id'].'"'.($item['id'] == $this->get_value() ? ' selected' : '').'>'.$divider.$item['caption'].'</option>';
    if (!empty($item['sub'])) $result .= $this->put_level($item['sub']);
   }
  }
  
  return $result;
 }

 /**
  * Проверяет, нужно ли выводить элемент @param $item
  * @param string $item
  * @return bool
  */
 protected function check_filter_module($item)
 {
  if (in_array($item['id'], $this->ignored_items)) return FALSE;
  if (empty($this->filter_modules)) return TRUE;
  
  if (is_null($this->items_to_output)) $this->format_items_to_output();
  if (!in_array($item['id'], $this->items_to_output)) return FALSE;
  
  return TRUE;
 }

 /**
  * Формирует и возвращает массив  ID элементов, которые нужно выводить
  * @return array
  */
 protected function format_items_to_output()
 {
  $this->items_to_output = array();

  if (!empty($this->filter_modules))
  {
   $data = $this->structure->get_all("`module` in ('".implode("', '", $this->filter_modules)."')");
   foreach ($data as $line)
   {
    $this->items_to_output[] = $line['id'];
    $parent = $line;
    
    while (!empty($parent['parent']))
    {
     if (!in_array($parent['parent'], $this->items_to_output)) $this->items_to_output[] = $parent['parent'];
     $parent = $this->structure->find_item_in_structure_by_id($this->structure->get_structure(), $parent['parent']);
    }
   }
  }
  
  
  return $this->items_to_output;
 }

 /**
  * Проверка типа структуры
  * @param mixed $structure
  * @return bool
  */
 protected function check_structure_type($structure)
 {
  if (is_subclass_of($structure, 'a_tree_model')) return TRUE;
  return FALSE;
 }

 /**
  * Генерация options
  * @return string
  */
 protected function put_options()
 {
  return $this->put_level($this->structure->get_structure());
 }

 /**
  * Возвращает значение из списка
  * @return string|int|null
  */
 protected function get_default_value()
 {
  $array = $this->structure->get_structure();
  if (is_array($array) and !empty($array))
  {
   return $this->get_default_value_of_level($array);
  }
  return NULL;
 }

 /**
  * Ищет значение по умолчанию в уровне структуры и если не найдено, вызывает себя рекурсивно со следующим уровнем 
  * @param array $items_array
  * @return null|int
  */
 protected function get_default_value_of_level($items_array)
 {
  $i = 0;
  $count = count($items_array);
  $result = NULL;
  
  while ($i < $count)
  {
   $item = $items_array[$i];
   if ($this->check_filter_module($item) and (!array_key_exists('module', $item) or in_array($item['module'], $this->filter_modules)))
   {
	return $item['id'];
   }
   
   if (array_key_exists('sub', $item))
   {
	$result = $this->get_default_value_of_level($item['sub']);
	if (!is_null($result)) return $result;
   }
   
   $i++;
  }
  
  return $result;
 }
}