<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 15.12.14
 */

/**
 * Генератор поля для ввода с выпадающим списком
 */
class type_head_printer extends a_input_printer {

	/**
	 * @var string поисковый запрос
	 */
	protected $query = '';

	/**
	 * Генерирует HTML
	 * @return string
	 */
	public function put()
	{
		return
			'<div class="type-head" data-type-head="block" data-type-head-model="' . get_class($this->structure) . '">
              <input type="hidden" name="' . htmlspecialchars($this->name) . '" data-type-head="value" value="' . htmlspecialchars($this->get_value()) . '"' . ($this->not_null ? ' required' : '') . '>
              <div><input type="text" name="' . htmlspecialchars($this->name) . '-caption"' . ($this->get_disabled() ? ' disabled' : '') . ' value="' . htmlspecialchars($this->get_value_caption()) . '" data-type-head="caption" autocomplete="off"' . ($this->not_null ? ' required' : '') . '></div>
			  <div class="type-header-dropdown" data-type-head="dropdown">' .
			  $this->putItems() .
			' </div>
			 </div>';
	}

	/**
	 * Выводит вслывающие подсказки
	 * @return string
	 */
	public function putItems()
	{
		$result = '';
		foreach ($this->get_assoc_structure() as $value => $caption) {
			$result .= '<div><a href="#" data-type-head="item" data-type-head-value="' . $value . '">' . $caption . '</a></div>';
		}

		return $result;
	}

	/**
	 * Возвращает поисковый запрос
	 * @return string
	 */
	public function getQuery()
	{
		return trim($this->query);
	}

	/**
	 * Устанавливает поисковый запрос
	 * @param string $query
	 * @return $this
	 */
	public function setQuery($query)
	{
		$this->query = $query;

		return $this;
	}

	/**
	 * @inheritdoc
	 */
	public function get_assoc_structure()
	{
		if ($this->getQuery() !== '') {
			$where = "{$this->structure->get_table_name()}.`caption_{$this->appHelper->getLang()}` like '%" . safe($this->getQuery()) . "%'";
		} else {
			$where = null;
		}
		
		$data = $this->structure->get_all($where);
		if (empty($data)) return [];
		
		return $this->format_assoc_structure(
			$this->replaceTreeStructureCaptions($this->structure, $data)
		);
	}
	
	

	

	/**
	 * Проверка типа структуры
	 * @param mixed $structure
	 * @return bool
	 */
	protected function check_structure_type($structure)
	{
		return $structure instanceof a_model;
	}

	/**
	 * Возвращает значение из списка
	 * @return string|int|null
	 */
	protected function get_default_value()
	{
		if (empty($this->structure) or !is_array($this->structure)) return NULL;
		$value = reset($this->structure);
		if (empty($value)) return NULL;
		if (is_array($value)) return $value['id'];
		return reset(array_keys($this->structure));
	}
}