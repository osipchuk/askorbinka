<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 25.04.14
 */

/**
 * Генератор главной древоидной структуры в виде ul>li списка
 */
class main_tree_model_printer extends a_tree_model_printer {

 /**
  * @var bool флаг отображения кнопки редактирования
  */
 protected $show_edit_button = TRUE;

 /**
  * @var bool флаг отображения кнопки удаления
  */
 protected $show_delete_button = TRUE;

 /**
  * @var bool флаг отображения кнопки добавления
  */
 protected $show_add_button = TRUE;

 /**
  * @var string базовая часть ссылок
  */
 protected $base_href = '';

 /**
  * @var string базовая часть ссылки удаления
  */
 protected $base_delete_href = '';

 /**
  * Генерирует HTML древовидной структуры
  * @return string
  */
 public function put()
 {
  return '<div class="menu_tree"><ul>'.$this->put_level($this->structure->get_structure()).'</ul></div>';
 }

 /**
  * Установка флага отображения кнопки удаления
  * @param boolean $show_delete_button
  * @return $this
  */
 public function set_show_delete_button($show_delete_button)
 {
  $this->show_delete_button = (boolean)$show_delete_button;

  return $this;
 }

 /**
  * Установка флага отображения кнопки редактирования
  * @param boolean $show_edit_button
  * @return $this
  */
 public function set_show_edit_button($show_edit_button)
 {
  $this->show_edit_button = (boolean)$show_edit_button;

  return $this;
 }

 /**
  * Установка флага отображения кнопки добавления товара
  * @param boolean $show_add_button
  * @return $this
  */
 public function set_show_add_button($show_add_button)
 {
  $this->show_add_button = (boolean)$show_add_button;

  return $this;
 }

 /**
  * Установака базовой части ссылки
  * @param string $base_href
  * @return $this
  */
 public function set_base_href($base_href)
 {
  $this->base_href = $base_href;

  return $this;
 }

 /**
  * Получение базовой части ссылки
  * @return string
  */
 protected function get_base_href()
 {
  if (!empty($this->base_href)) return $this->base_href;
  return "admin.php?mode=".$_GET['mode'];
 }

 /**
  * Установка базовой части ссылки удаления
  * @param string $base_delete_href
  * @return $this
  */
 public function set_base_delete_href($base_delete_href)
 {
  $this->base_delete_href = $base_delete_href;

  return $this;
 }

 /**
  * Получение базовой части ссылки удаления
  * @return string
  */
 public function get_base_delete_href()
 {
  if (!empty($this->base_delete_href)) return $this->base_delete_href;
  return $_GET['mode'].'_edit.php?creat_mode=delete';
 }
 
 

 /**
  * Генерирует HTML переданного уровня древовидной структуры, вызывает рекурсивно себя же для уровней-потомков
  * @param array $level_structure
  * @return string
  */
 protected function put_level($level_structure)
 {
  $result = '';
  foreach ($level_structure as $item)
  {
   $result .= '<li'.($item['level'] == 0 ? ' class="base-level"' : '').'><span>'.$item['caption'].'</span>'.$this->put_append_fields($item).$this->put_li_ext_data($item);
   if (!empty($item['sub'])) $result .= '<ul>'.$this->put_level($item['sub']).'</ul>';
   $result .= '</li>';
  }
  
  return $result;
 }

 /**
  * Проверяет возможность удалить элемент
  * @param array $item элемент для вывода
  * @return bool
  */
 protected function check_delete_button($item)
 {
  return $this->show_add_button;
 }

 /**
  * Добавляет к названию элемента дополнительную информацию
  * 
  * Например кнопки для редактирования
  * 
  * @param array $item элемент
  * @return string
  */
 protected function put_li_ext_data($item)
 {
  $result = '';

  if ($this->show_edit_button) $result .= '<a href="'.$this->get_base_href().'&creat_mode=edit&id='.$item['id'].'&parent='.get('gi','parent').'"><img src="images/edit.gif" title="Редактировать"></a>';
  if ($this->check_delete_button($item)) $result .= '<a href="#" onclick="if (confirm(\'Вы действительно хотите удалить пункт?\')) location.href=\''.$this->get_base_delete_href().'&id='.$item['id'].'&parent='.get('gi','parent').'\';"><img src="images/delete.gif" title="Удалить"></a>';

  return $result;
 }
}