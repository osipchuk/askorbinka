<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 24.03.15
 */

class type_head_multi_printer_with_counts extends type_head_multi_printer_with_bonuses
{
    /**
     * @inheritdoc
     */
    protected function putSelectedItem($item, array $itemCaptions)
    {
        return "<li>
                 <input type='text' name='{$this->name}_count[{$item}]' value='{$this->getBonusItem($item)}' title='Количество элементов'>
                 <label><input type='checkbox' name='{$this->name}_delete[]' value='{$item}' title='Удалить'>{$itemCaptions[$item]}</label>
                </li>";
    }

    /**
     * @inheritdoc
     */
    protected function getBonusItem($item)
    {
        $result = parent::getBonusItem($item);
        if ($result <= 0) {
            $result = 1;
        }
        
        return $result;
    }

    /**
     * @inheritdoc
     */
    protected function getHeadType()
    {
        return 'multi-with-counts';
    }
}
