<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 16.05.14
 */

/**
 * Вывод селекта
 */
abstract class a_select_printer extends a_input_printer {

 /**
  * @var string название null элемента
  */
 protected $null_caption;

 /**
  * Устанавливает название null элемента
  * @param string $null_caption
  * @return $this
  */
 public function set_null_caption($null_caption)
 {
  $this->null_caption = $null_caption;

  return $this;
 }

 /**
  * Возвращает название null элемента
  *
  * Если не задано, возвращает значение по умолчанию
  *
  * @return string
  */
 public function get_null_caption()
 {
  if (empty($this->null_caption)) return 'Корень';
  return $this->null_caption;
 }

 /**
  * Генерирует HTML
  * @return string
  */
 public function put()
 {
  return '<select name="'.htmlspecialchars($this->name).'"'.($this->get_disabled() ? ' disabled' : '').'>'.
           ($this->not_null ? '' : '<option value="null">['.$this->get_null_caption().']</option>').
           $this->put_options().
          '</select>';
 }

 /**
  * Генерация options
  * @return string
  */
 abstract protected function put_options();
}