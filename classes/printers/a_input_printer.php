<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 15.12.14
 */

/**
 * Абстрактный класс для вывода инпута
 */
abstract class a_input_printer extends a_printer {

	/**
	 * @var string имя инпута
	 */
	protected $name = '';

	/**
	 * @var mixed выбранное значение
	 */
	protected $value;

	/**
	 * @var bool флаг отключения
	 */
	protected $disabled = FALSE;

	/**
	 * @var bool флаг вывода в корне NULL-значения
	 */
	protected $not_null = TRUE;

	/**
	 * Устанавливает имя инпута
	 * @param string $name
	 * @return $this
	 */
	public function set_name($name)
	{
		$this->name = $name;

		return $this;
	}

	/**
	 * Возвращает имя селекста
	 * @return string
	 */
	public function get_name()
	{
		return $this->name;
	}

	/**
	 * Устанавливает выбранное значение
	 * @param mixed $value
	 * @return $this
	 */
	public function set_value($value)
	{
		if ($value == 'null') $value = NULL;
		$this->value = $value;

		return $this;
	}

	/**
	 * Возвращает выбранное значение
	 * @return mixed
	 */
	public function get_value()
	{
		if (!is_null($this->value) or $this->not_null == FALSE) return $this->value;
		return $this->get_default_value();
	}

	/**
	 * Возвращает caption выбранного значения
	 * @return null|string
	 */
	public function get_value_caption()
	{
		$value = $this->get_value();
		$assoc_structure = $this->get_assoc_structure();
		
		if (empty($value)) return null;
		if (!array_key_exists($value, $assoc_structure)) return $value;
		
		return $assoc_structure[$value];
	}

	/**
	 * Устаналивает флаг disabled
	 * @param boolean $disabled
	 * @return $this
	 */
	public function set_disabled($disabled)
	{
		$this->disabled = (bool)$disabled;

		return $this;
	}

	/**
	 * Возвращает флаг disabled
	 * @return boolean
	 */
	public function get_disabled()
	{
		return $this->disabled;
	}

	/**
	 * Устанавливает флаг вывода в корне NULL-значения
	 * @param boolean $not_null
	 * @return $this
	 */
	public function set_not_null($not_null)
	{
		$this->not_null = (boolean)$not_null;

		return $this;
	}

	/**
	 * Возвращает структуру в виде простого ассоциативного массива
	 * @return array
	 */
	public function get_assoc_structure()
	{
		return $this->format_assoc_structure($this->structure);
	}

	/**
	 * Возвращает значение из списка
	 * @return string|int|null
	 */
	abstract protected function get_default_value();

	/**
	 * Формирует структуру в виде простого ассоциативного массива на основе переданного массива
	 * @param array $structure
	 * @return array
	 */
	protected function format_assoc_structure(array $structure)
	{
		$result = [];

		foreach ($structure as $value => $caption) {
			if (is_array($caption) and array_key_exists('id', $caption) and array_key_exists('caption', $caption)) {
				$result[$caption['id']] = $caption['caption'];
			} else {
				$result[$value] = $caption;
			}
		}
		
		return $result;
	}

	/**
	 * Заменяет captions из массива на полные строки с расшифровокй названий предкой если structure - a_tree_model
	 * @param mixed $structure
	 * @param array $data
	 * @return array
	 */
	protected function replaceTreeStructureCaptions($structure, array $data)
	{
		if (!($structure instanceof a_tree_model)) {
			return $data;
		}
		
		$structureData = $structure->get_structure();
		
		foreach ($data as $index => $line) {
			$item = $line;
			$caption = $line['caption'];
			while (!empty($item['parent'])) {
				$item = $structure->find_item_in_structure_by_id($structureData, $item['parent']);
				$caption = $item['caption'] . ' - ' . $caption;
			}

			$data[$index]['caption'] = $caption;
		}
		
		return $data;
	}
}