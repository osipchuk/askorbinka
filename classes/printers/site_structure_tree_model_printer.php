<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 25.04.14
 */

/**
 * Генератор главной древоидной структуры в виде ul>li списка для структуры сайта
 */
class site_structure_tree_model_printer extends main_tree_model_printer {

 /**
  * @var array массив модулей доступных для редактирования
  */
 protected $standard_modules = array();

 /**
  * @var array массив модулей недоступных для редактирования
  */
 protected $extended_modules = array();

 /**
  * @var array полный список модулей
  */
 protected $full_modules = array();

 /**
  * Объект, который будет заниматься поиском нестандартных модулей в структуре
  * @var site_structure_ext_modules_finder
  */
 protected $finder;

 /**
  * Конструктор
  * @param a_tree_model $structure
  */
 public function __construct(a_tree_model $structure)
 {
  parent::__construct($structure);
  
  $this->standard_modules = $this->appHelper->get_global('standard_modules');
  $this->extended_modules = $this->appHelper->get_global('extended_modules');
  $this->full_modules = array_merge($this->standard_modules, $this->extended_modules);
 }

 /**
  * Устанавливает объект, который будет заниматься поиском нестандартных модулей в структуре
  * @param site_structure_ext_modules_finder $finder
  * @return $this
  */
 public function set_ext_modules_finder(site_structure_ext_modules_finder $finder)
 {
  $this->finder = $finder;

  return $this;
 }
 
 

 /**
  * Добавляет к названию элемента название модуля
  * @param array $item
  * @return string
  */
 protected function put_li_ext_data($item)
 {
  return ' <span title="Модуль"'.(array_key_exists($item['module'], $this->extended_modules) ? ' class="ext-module"' : '').'>['.$this->full_modules[$item['module']].']</span> '.
           ($item['hidden'] ? ' <span class="hidden">[раздел скрыт в меню]</span>' : '').
           parent::put_li_ext_data($item);
 }

 /**
  * Проверяет возможность удалить элемент
  * @param array $item элемент для вывода
  * @throws Exception
  * @return bool
  */
 protected function check_delete_button($item)
 {
  if (is_null($this->finder)) throw new Exception('Finder is empty =(');
  
  if (parent::check_delete_button($item)) return $this->finder->has_ext_modules($item);
 }

}