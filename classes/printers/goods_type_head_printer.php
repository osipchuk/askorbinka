<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 24.03.15
 */

class goods_type_head_printer extends type_head_multi_printer_with_counts
{

    /**
     * @inheritdoc
     */
    public function get_assoc_structure()
    {
        if ($this->getQuery() !== '') {
            $where =
                "concat(
                `{$this->structure->get_table_name()}`.`category_{$this->appHelper->getLang()}`, ' ',
                `{$this->structure->get_table_name()}`.`caption_{$this->appHelper->getLang()}`) like '%" . safe($this->getQuery()) . "%'";
        } else {
            $where = null;
        }

        $data = $this->structure->get_all($where);
        if (empty($data)) return [];

        return $this->format_assoc_structure(
            $this->replaceTreeStructureCaptions($this->structure, $data)
        );
    }
    
    

    /**
     * Формирует структуру в виде простого ассоциативного массива на основе переданного массива
     * @param array $structure
     * @return array
     */
    protected function format_assoc_structure(array $structure)
    {
        $result = [];

        foreach ($structure as $value => $caption) {
            if (is_array($caption) and array_key_exists('id', $caption) and array_key_exists('caption', $caption)) {
                $result[$caption['id']] = strip_tags($caption['category'] . ' ' . $caption['caption']);
            } else {
                $result[$value] = $caption;
            }
        }

        return $result;
    }
}
