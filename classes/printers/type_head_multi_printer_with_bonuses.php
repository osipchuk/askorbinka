<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 16.12.14
 */

class type_head_multi_printer_with_bonuses extends type_head_multi_printer {

	/**
	 * @var array
	 */
	protected $bonuses = [];

	/**
	 * Устанавливает размеры бонусов из БД
	 * Принимает массив из БД
	 * @param array $bonuses
	 * @param string $indexBy ключевое поле
	 * @return $this
	 */
	public function setBonuses(array $bonuses, $indexBy)
	{
		$this->bonuses = [];
		foreach ($bonuses as $item) {
			$this->bonuses[$item[$indexBy]] = $item['bonus'];
		}

		return $this;
	}
	
	

	/**
	 * Возвращает html выбранного элемента
	 * @param int|string $item
	 * @param array $itemCaptions
	 * @return string
	 */
	protected function putSelectedItem($item, array $itemCaptions)
	{
		return "<li>
                 <input type='text' name='{$this->name}_bonus[{$item}]' value='{$this->getBonusItem($item)}' title='Количество бонусов'>
                 <label><input type='checkbox' name='{$this->name}_delete[]' value='{$item}' title='Удалить'>{$itemCaptions[$item]}</label>
                </li>";
	}
	
	/**
	 * Возвращает тип JS-блока для
	 * @return string
	 */
	protected function getHeadType()
	{
		return 'multi-with-bonuses';
	}

	/**
	 * Возвращает размер бонусов для элемента
	 * @param int|string $item
	 * @return int
	 */
	protected function getBonusItem($item)
	{
		if (isset($this->bonuses[$item])) {
			return $this->bonuses[$item];
		}
		
		return 0;
	}
}