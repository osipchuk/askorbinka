<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 16.05.14
 */

/**
 * Класс для вывода формы с селектом вверху страницы
 */
class top_form_printer {

 /**
  * @var string имя формы
  */
 protected $form_name;

 /**
  * @var string базовая часть параметра action
  */
 protected $action;

 /**
  * @var array ассоциативный массив для передачи в $_GET запросе при отправке формы
  */
 protected $parameters = array();

 /**
  * @var string label поля select
  */
 protected $select_label;

 /**
  * @var a_select_printer
  */
 protected $select_printer;

 public function __construct(a_select_printer $select_printer)
 {
  $this->select_printer = $select_printer;
 }

 /**
  * Устанавливает имя формы
  * @param string $form_name
  * @return $this
  */
 public function set_form_name($form_name)
 {
  $this->form_name = $form_name;

  return $this;
 }

 /**
  * Возвращает имя формы
  * @return string
  */
 public function get_form_name()
 {
  if (!empty($this->form_name)) return $this->form_name;
  return 'top';
 }

 /**
  * Устанавливает базовое значение action
  * @param string $base_href
  * @return $this
  */
 public function set_action($base_href)
 {
  $this->action = $base_href;

  return $this;
 }

 /**
  * Возвращает базовое значение action
  * @return string
  */
 public function get_action()
 {
  if (empty($this->action)) return 'admin.php';
  return $this->action;
 }

 /**
  * Добавляет параметр для отправки формы
  * @param $key
  * @param $value
  * @return $this
  */
 public function add_parameter($key, $value)
 {
  if (!empty($key) and !empty($value)) $this->parameters[$key] = $value;
  return $this;
 }

 /**
  * Устанавливает подпись для селекта
  * @param string $select_label
  * @return $this
  */
 public function set_select_label($select_label)
 {
  $this->select_label = $select_label;

  return $this;
 }

 /**
  * Возвращает подпись для селекта
  * @return string
  */
 public function get_select_label()
 {
  if (!empty($this->select_label)) return $this->select_label;
  return 'Родительский элемент';
 }

 /**
  * Генерирует HTML древовидной структуры
  * @return string
  */
 public function put()
 {
  $result = '<form name="'.htmlspecialchars($this->get_form_name()).'" action="'.$this->get_action().'" method="get" class="top-form">';
  foreach ($this->parameters as $parameter => $value) $result .= '<input type="hidden" name="'.htmlspecialchars($parameter).'" value="'.htmlspecialchars($value).'">';

  $result .=  '<label>'.$this->get_select_label().'</label>'.$this->select_printer->put() .'
             </form>
             <script>
              $(\'form[name="'.htmlspecialchars($this->get_form_name()).'"] select[name=\"'.htmlspecialchars($this->select_printer->get_name()).'\"]\').change(function() { $(\'form[name="'.htmlspecialchars($this->get_form_name()).'"]\').submit(); });
             </script>';

  return $result;
 }

// /**
//  * Генерирует HTML select'а
//  * @return string
//  */
// abstract protected function put_select();
}