<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 28.04.14
 */


/**
 * Абстрактный класс для объектов вывода 
 */
abstract class a_printer {

 /**
  * @var mixed структура для вывода
  */
 protected $structure;

 /**
  * @var applicationHelper
  */
 protected $appHelper;

 /**
  * Конструктор
  * @param $structure
  */
 public function __construct($structure)
 {
  $this->set_structure($structure);
  $this->appHelper = applicationHelper::getInstance();
 }

 /**
  * Устанавливает структуру для вывода
  * @param $structure
  * @throws Exception если перед неверный аргумент
  * @return $this
  */
 public function set_structure($structure)
 {
  if ($this->check_structure_type($structure))
  {
   $this->structure = $structure;
   return $this;
  }

  throw new Exception('structure error');
 }


 /**
  * Возвращает массив языковых версий
  * @return array
  */
 protected function get_langs()
 {
  return $this->appHelper->get_global('lang_array');
 }

 /**
  * Генерирует HTML
  * @return string
  */
 abstract public function put();


 /**
  * Проверка типа структуры
  * @param mixed $structure
  * @return bool
  */
 abstract protected function check_structure_type($structure);
}