<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 25.04.14
 */

/**
 * Класс для вывода древовидной структуры в определенной форме
 */
abstract class a_tree_model_printer extends a_printer {

 /**
  * @var array дополнительные поля для вывода
  */
 protected $append_fields = array();

 /**
  * Добавляет дополнительно поле для вывода
  * @param string $field
  * @return $this
  */
 public function add_append_field($field)
 {
  $this->append_fields[] = $field;
  return $this;
 }

 /**
  * Проверка типа структуры
  * @param mixed $structure
  * @return bool
  */
 protected function check_structure_type($structure)
 {
  if (is_subclass_of($structure, 'a_tree_model')) return TRUE;
  
  return FALSE;
 }

 /**
  * Формирует список дополнительных полей для элемента $item
  * @param array $item
  * @return string
  */
 protected function put_append_fields($item)
 {
  $result = '';
  foreach ($this->append_fields as $field) if (array_key_exists($field, $item)) $result .= ' | <span>'.$item[$field].'</span>';

  return $result;
 }
 
 /**
  * Генерирует HTML переданного уровня древовидной структуры, вызывает рекурсивно себя же для уровней-потомков
  * @param array $level_structure
  * @return string
  */
 abstract protected function put_level($level_structure);
}