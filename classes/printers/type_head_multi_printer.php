<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 15.12.14
 */

/**
 * Генератор поля для ввода с выпадающим списком
 * С возможностью выбрать несколько значений
 */
class type_head_multi_printer extends type_head_printer {

	/**
	 * Генерирует HTML
	 * @return string
	 */
	public function put()
	{
		$result =
			'<div class="type-head" data-type-head="block" data-type-head-type="' . $this->getHeadType() . '" data-type-head-model="' . get_class($this->structure) . '" data-type-head-name="' . htmlspecialchars($this->get_name()) . '">
			  <div data-type-head="selected-values">
			   <ul class="ul-unstyled">';
		
		$itemCaptions = $this->format_assoc_structure(
			$this->replaceTreeStructureCaptions($this->structure, $this->structure->get_all())
		);
		
		if (is_array($this->get_value())) {
			foreach ($this->get_value() as $item) {
				$result .= $this->putSelectedItem($item, $itemCaptions);
			}
		}
	
	
	    $result .= ' </ul>
			        </div>
              
		  <div><input type="text" name="' . htmlspecialchars($this->name) . '-caption"' . ($this->get_disabled() ? ' disabled' : '') . ' data-type-head="caption" autocomplete="off"' . ($this->not_null ? ' required' : '') . '>
		    <input type="hidden" name="goods_add">    
		  </div>
		  <div class="type-header-dropdown" data-type-head="dropdown">' .
		$this->putItems() .
		' </div>
		 </div>';
		
		return $result;
	}

	/**
	 * Возвращает html выбранного элемента
	 * @param int|string $item
	 * @param array $itemCaptions
	 * @return string
	 */
	protected function putSelectedItem($item, array $itemCaptions)
	{
		return "<li><label><input type='checkbox' name='{$this->name}_delete[]' value='{$item}' title='Удалить'>{$itemCaptions[$item]}</label></li>";
	}
	
	/**
	 * Возвращает тип JS-блока для
	 * @return string
	 */
	protected function getHeadType()
	{
		return 'multi';
	}
}