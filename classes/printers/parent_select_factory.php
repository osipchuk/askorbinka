<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 14.07.14
 */

/**
 * Фабрика для создания экземпляров select для выбора parent
 */
class parent_select_factory {

 /**
  * @var a_model главная модель
  */
 protected $model;

 /**
  * @var a_model parent модель
  */
 protected $external_model;

 /**
  * Возвращает объект select для выбора parent
  * @param a_model $model
  * @param bool $add_filter_modules флаг, которые обозначает, нужно ли добавлять фильтр модулей
  * @return a_select_printer
  * @throws Exception в случае ошибки
  */
 public function get_select(a_model $model, $add_filter_modules = TRUE)
 {
  $this->model = $model;
  $select = $this->get_select_object();
  
  $select
      ->set_not_null($this->need_not_null())
      ->set_name('parent')
      ->set_null_caption('Без привязки');

  
  $value = get('gi','parent');
  if ($value > 0) $select->set_value($value);
  
  
  if ($add_filter_modules and method_exists($select, 'add_filter_module')) $select->add_filter_module(get_class($this->model));
  
  
  return $select;
 }

 
 

 /**
  * @return a_select_printer
  * @throws Exception в случае ошибки
  */
 protected function get_select_object()
 {
  $external_model = $this->model->external_model();
  if (is_null($external_model)) throw new Exception('Model has not external model');
  
  $external_model = new $external_model();
  $this->external_model = $external_model;
  
  if (is_subclass_of($external_model, 'a_tree_model'))
  {
   $structure = $external_model;
   $select_class = 'select_tree_model_printer';
  }
  else
  {
   $structure = $external_model->ignore_page_num(TRUE)->get_all();
   $select_class = 'select_simple_printer';
  }
  

  if (!class_exists($select_class)) throw new Exception('Class "'.$select_class.'" is not exists'); 
  return new $select_class($structure);
 }


 /**
  * Проверяет, нужно ли выводить в селекте нулевое значение
  * @return bool
  */
 protected function need_not_null()
 {
  return count($this->model->get_all_of_external_parent(null)) == 0;
 }


 /**
  * @return int|null
  */
 protected function get_value()
 {
 }
}