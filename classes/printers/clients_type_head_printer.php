<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 24.03.15
 */

class clients_type_head_printer extends type_head_printer
{

    /**
     * @inheritdoc
     */
    public function get_assoc_structure()
    {
        if ($this->getQuery() !== '') {
            $where = "concat({$this->structure->get_table_name()}.`name`, ' ', {$this->structure->get_table_name()}.`surname`) like '%" . safe($this->getQuery()) . "%'";
        } else {
            $where = null;
        }

        $data = $this->structure->get_all($where);
        if (empty($data)) return [];

        return $this->format_assoc_structure(
            $this->replaceTreeStructureCaptions($this->structure, $data)
        );
    }
}
