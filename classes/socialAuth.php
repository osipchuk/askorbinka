<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 07.11.14
 */

/**
 * Класс для авторизации через социальные сети
 */
class socialAuth {

	/**
	 * @var applicationHelper
	 */
	protected $appHelper;

	/**
	 * @var auth
	 */
	protected $authController;

	/**
	 * @param auth $authController
	 */
	public function __construct(auth $authController) {
		$this->appHelper = applicationHelper::getInstance();
		$this->authController = $authController;
	}

	/**
	 * Получает данные по токену от ulogin и проводит авторизацию пользователя.
	 * В случае необходимости добавляет пользователя в БД
	 * @param $token
	 * @return bool
	 * @throws Exception
	 */
	public function auth($token) {
		if (empty($token)) throw new Exception('token error');
		$s = file_get_contents($url = 'http://ulogin.ru/token.php?token=' . $token . '&host=' . $_SERVER['HTTP_HOST']);
		$user = json_decode($s, true);
		
		if (empty($user)) throw new Exception($this->appHelper->getTranslateResolver()->_('unknown_error'));
		if (isset($user['error'])) throw new Exception($user['error']);
		
		//$user['network'] - соц. сеть, через которую авторизовался пользователь
		//$user['identity'] - уникальная строка определяющая конкретного пользователя соц. сети
		//$user['first_name'] - имя пользователя
		//$user['last_name'] - фамилия пользователя
		
		
		$this->authController->loginById($this->userID($user), true);
		return true;
	}


	/**
	 * Возвращает id пользователя в БД
	 * @param array $user
	 * @return int - id аккаунта
	 * @throws Exception в случае ошибки
	 */
	protected function userID(array $user) {
		if (empty($user)) throw new Exception('account is empty');
		$accountsModel = $this->authController->get_accounts_model();
		
		if (isset($user['email']) and !empty($user['email'])) {
			$account = $accountsModel->get_item("`email`='".safe($user['email'])."'");
		}
		else {
			$account = $accountsModel->get_item("`social_sig`='".safe($this->socialSign($user))."'");
		}
		
		if (!empty($account)) return $account['id'];
		return $this->insertUser($user);
	}

	/**
	 * Вставляет в БД нового пользователя
	 * @param array $user
	 * @return int
	 * @throws Exception
	 */
	protected function insertUser(array $user) {
		if (!is_array($user) or empty($user)) throw new Exception('account is empty');
		
		$user = safe_array($user);
		$accountsModel = $this->authController->get_accounts_model();

		$fields = [
			'email' => 'email',
			'name' => 'first_name',
			'surname' => 'last_name',
			'city' => 'city',
			'tel' => 'phone',
		];
		
		$set = "`social_sig`='".safe($this->socialSign($user))."', `submited`='1'";
		foreach ($fields as $mysqlField => $arrayKey) if (isset($user[$arrayKey])) $set .= ", `$mysqlField`='{$user[$arrayKey]}'";
		
		$id = mysql_insert($accountsModel->get_table_name(), $set);
		if (!$id) throw new Exception('account insert error');

		if (isset($user['photo_big']) and $image = upload_image($user['photo_big']))
			mysql_update($accountsModel->get_table_name(), "`image`='$image'", "`id`='$id'");
		
		return $id;
	}


	/**
	 * Возвращает идентификатор социальной сети пользователя в БД
	 * @param array $user
	 * @return string
	 */
	protected function socialSign(array $user) {
		return $user['network'].'|'.$user['uid'];
	}
}