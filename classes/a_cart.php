<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 04.03.14
 */

/**
 * Корзина для интернет магазина
 * Базовая часть функционала
 */
abstract class a_cart {
 
 /**
  * @var array массив товаров в корзине
  */
 protected $cart;

 /**
  * @var applicationController
  */
 protected $app;

 /**
  * @var translatesResolver
  */
 protected $words;

 /**
  * @var int|null ID заказа, если корзина была загруженна из БД
  */
 protected $order_id;

 /**
  * @var array|null информация о заказе, если корзина была загружена из БД
  */
 protected $orderInfo;

 /**
  * @var string уникальный идентификатор корзины кользователя 
  */
 protected $user_cart_id;

 /**
  * @var goods
  */
 protected $goods_model;

 /**
  * @var bool флаг беслпатной доставки
  */
 protected $free_ship = false;

 public function __construct()
 {
  $this->goods_model = new goods();
  $this->cart = $this->load_cart();
  $this->app = applicationController::getInstance();
  $this->words = applicationHelper::getTranslateResolver();
  $this->applyDiscounts();
 }

 public function __destruct()
 {
  if (is_null($this->order_id))
  {
   $_SESSION['cart'] = $this->cart;
  }
 }

 /**
  * Добавляет товар в корзину
  * Если $price и $priceWithoutAction = null - цена извлекается из информации и товаре + к товару применяются скидки
  * @param int $item_id
  * @param int $count
  * @param float|int $price цена товара
  * @param float|int $priceWithoutAction цена без скидки
  * @return bool
  */
 public function add_item($item_id, $count = 1, $price = 0, $priceWithoutAction = 0)
 {
  $item_id = (int)$item_id;
  $count = (int)$count;
  
  $good = $this->goods_model->get_item("`{$this->goods_model->get_table_name()}`.`id`='$item_id'");

  if (!empty($good))
  {
   if ($count < 1) $count = 1;
   $this->cart[$item_id] = [
	   'good' => $good,
	   'count' => $count,
	   'price' => $price <= 0 ? $this->good_price($good) : $price,
	   'price_without_discounts' => $priceWithoutAction <= 0 ? $good['price'] : $priceWithoutAction,
   ];
	  
   if (is_null($price) and is_null($priceWithoutAction)) $this->applyDiscounts();
	  
   return TRUE;
  }
  return FALSE;
 }

 /**
  * Удаляет товар из корзины
  * @param int $item_id
  * @return bool
  */
 public function remove_item($item_id)
 {
  if ($this->is_item_in_cart($item_id))
  {
   unset($this->cart[$item_id]);
   return TRUE;
  }
  else return FALSE;
 }

 /**
  * Устанавливает количество товаров в корзине для переданного товара
  * @param int $item_id
  * @param int $items_count
  * @return bool
  */
 public function set_items_count($item_id, $items_count)
 {
  if ($this->is_item_in_cart($item_id))
  {
   $items_count = (int)$items_count;
   if ($items_count < 1) $items_count = 1;
   
   $this->cart[$item_id]['count'] = $items_count;
   return TRUE;
  }
  else return FALSE;
 }

	/**
	 * Устанавливает цену для товара
	 * @param int $itemId
	 * @param float $price
	 * @return bool
	 */
	public function setItemPriceWithDiscount($itemId, $price)
	{
		$price = (float) $price;
		
		if ($this->is_item_in_cart($itemId) and $price > 0) {
			$this->cart[$itemId]['price'] = $price;
			return true;
		}
		
		return false;
	}

 /**
  * Устанавливает цену для товара без учета скидки
  * @param int $itemId
  * @param float $price
  * @return bool
  */
 public function setItemPriceWithoutDiscount($itemId, $price)
 {
  $price = (float) $price;

  if ($this->is_item_in_cart($itemId) and $price > 0)
  {
   $this->cart[$itemId]['price_without_discounts'] = $price;
   return true;
  }

  return false;
 }

 /**
  * Проверяет, есть ли товар с переданным id в корзине
  * @param int $item_id
  * @return bool
  */
 public function is_item_in_cart($item_id)
 {
  return array_key_exists($item_id, $this->get_goods());
 }
 
 /**
  * Возвращает массив товаров в корзине
  * @return array
  */
 public function get_goods()
 {
//  var_dump($this->cart);
  return is_array($this->cart) ? $this->cart : [];
 }

 /**
  * Очищает корзину
  * @return bool
  */
 public function clear()
 {
  $this->cart = NULL;
  $this->setFreeShip(false);
  $_SESSION['cart'] = $this->cart;
  return TRUE;
 }

 /**
  * Возвращает количество товаров и цену всех товаров
  * @param bool $considerBonuses отнимать бонусы от полной стоимости, или нет
  * @return array
  */
 public function get_summary($considerBonuses = false)
 {
  $count = 0;
  $price = 0;
  $discount = 0;
  
  foreach ($this->get_goods() as $good)
  {
   $count += $good['count'];
   $price += $good['price'] * $good['count'];
   $discount += ($good['price_without_discounts'] - $good['price']) * $good['count'];
  }
	 
	 if ($considerBonuses) {
		 $price -= $this->bonusesApplied();
	 }
  
  return [
	  'count' => $count,
	  'price' => price_format($price),
	  'discount' => $discount,
  ];
 }

 /**
  * Формирует html таблицу товаров из корзины
  * @param string|null $add_class html-класс, который можно добавить для формы 
  * @return string
  */
 public function format_table($add_class = NULL)
 {
  $summary = $this->get_summary(true);
  
  $result = '<table class="cart-goods-table'.(!empty($add_class) ? ' '.$add_class : '').'">
             <thead>
             <tr>
              <th>'.$this->words->_('cart_th_code').'</th>
              <th>'.$this->words->_('cart_th_articul').'</th>
              <th>'.$this->words->_('cart_th_category').'</th>
              <th>'.$this->words->_('cart_th_caption').'</th>
              <th style="width: 150px;">'.$this->words->_('cart_th_count').'</th>
              <th style="width: 150px;">'.$this->words->_('cart_th_price_without_discounts').'</th>
              <th style="width: 150px;">'.$this->words->_('cart_th_price').'</th>
             </tr>
             </thead>
             <tbody>';
  
  foreach ($this->get_goods() as $good)
  {
   $result .= "<tr>
                <td>{$good['good']['code']}</td>
                <td>{$good['good']['articul']}</td>
                <td>{$good['good']['category']}</td>
                <td>{$good['good']['caption']}</td>
                <td>{$good['count']}</td>
                <td>".price_format($good['price_without_discounts'])." {$this->words->_('currency')}</td>
                <td>".price_format($good['price'])." {$this->words->_('currency')}</td>
               </tr>";
  }
	 
	 $bonusesApplied = $this->bonusesApplied();
	 if ($bonusesApplied > 0) {
		 $result .= '<tr>
 <td colspan="4">' . $this->words->_('cart_bonuses_applied', 'Потрачено бонусов') . '</td>
 <td>' . $bonusesApplied . '</td>
 <td>' . -$bonusesApplied . ' ' . $this->words->_('currency') . '</td>
 <td>' . -$bonusesApplied . ' ' . $this->words->_('currency') . '</td>
</tr>';
	 }
  
  if ($this->getFreeShip())
  {
   $result .= '<tr>
                <td colspan="4">' . $this->words->_('cart_free_ship') . '</td>
                <td>1</td>
                <td>-</td>
                <td>-</td>
               </tr>';
  }
  
  $result .= '<tr>
               <td colspan="4">'.$this->words->_('cart_output').'</td>
               <td>'.$summary['count'].'</td>
               <td></td>
               <td>'.price_format($summary['price']).' '.$this->words->_('currency').'</td>
              </tr>
              </tbody>
              </table>';
  
  return $result;
 }

 /**
  * Загружает в корзину товары заказа по ID
  * @param int $order_id
  * @return int Количество товаров в корзине
  * @throws Exception если заказ не найден
  */
 public function load_from_db($order_id)
 {
	 $order_id = (int) $order_id;
	 $orderInfo = mysql_line_assoc("select * from `orders` where `id`='$order_id'");
	 if (empty($orderInfo)) throw new Exception('order not found');
	 $this->orderInfo = $orderInfo;
	 $this->order_id = $order_id;
     $this->setFreeShip($orderInfo['free_ship']);
	 
	 $this->cart = [];
     $data = mysql_data_assoc("select * from `order_goods` where `order`='$order_id' order by `id`");
     foreach ($data as $line) $this->add_item($line['good'], $line['count'], $line['price'], $line['price_without_discounts']);
     return count($data);
 }

 /**
  * Формирует цену на товар
  * @param array $good
  * @return float|int
  */
 public function good_price($good)
 {
  return $this->goods_model->price($good);
 }

	/**
	 * Возвращает id заказа
	 * @return int|null
	 */
	public function getOrderId()
	{
		return $this->order_id;
	}

 /**
  * Возвращает информацию о заказе
  * @return array|null
  */
 public function getOrderInfo()
 {
  return $this->orderInfo;
 }

	/**
	 * Устанавливает id заказа
	 * @param int|null $order_id
	 * @return $this
	 */
	public function setOrderId($order_id)
	{
		$this->order_id = $order_id;
		$this->load_from_db($order_id);

		return $this;
	}

	/**
	 * Возвращает количество примененных для заказа бонусов
	 * @return int
	 */
	public function bonusesApplied()
	{
		if ($this->getOrderId() == null or
			!isset($this->orderInfo['pay_with_bonuses']) or
			$this->orderInfo['pay_with_bonuses'] == 0 or
			$this->orderInfo['bonuses_applied'] == 0) {
			return 0;
		}
		
		return $this->orderInfo['bonuses_applied'];
	}

 /**
  * Возвращает флаг бесплатной доставки
  * @return boolean
  */
 public function getFreeShip()
 {
  return $this->free_ship;
 }

 /**
  * Устанавливает флаг бесплатной доставки
  * @param boolean $free_ship
  * @return $this
  */
 public function setFreeShip($free_ship)
 {
  $this->free_ship = (bool) $free_ship;

  return $this;
 }
 
 
	
	
	
	
 
 
 

 /**
  * Возвращает корзину из сессии, или из БД
  * @return array
  */
 protected function load_cart()
 {
  if (isset($_SESSION['cart']) and is_array($_SESSION['cart'])) return $_SESSION['cart'];
  
  return [];
 }

	/**
	 * Применяет скидки к всем товарам корзины
	 * @return $this
	 */
	protected function applyDiscounts()
	{
		$amountMaker = applicationHelper::getAmountMaker();
		$amountMaker->setCart($this)->apply();
		return $this;
	}
}
