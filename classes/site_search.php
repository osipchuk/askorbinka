<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 12.05.14
 * @version 2.0
 */

/**
 * Поиск по сайу
 * С добавлением модулей, а не таблиц
 */
class site_search {
 
 protected $url_maker;

 /**
  * @var string языковая версия
  */
 protected $lang;

 /**
  * @var array массив модулей для поиска
  */
 protected $modules = array();

 /**
  * @var array массив объектов моделей модулей
  */
 protected $models = array();

 /**
  * @var array дополнительные поля для поиска
  */
 protected $additional_fields = array();

 /**
  * @var string поисковый запрос
  */
 protected $search_query = '';

 /**
  * @var int количество результатов поиска на странице
  */
 protected $results_on_page = 0;

 /**
  * @var array результат
  */
 protected $result;

 /**
  * @var pagination объект для формирования пагинации
  */
 protected $pagination;


 /**
  * Конструктор
  * @param string $lang языковая версия
  * @param url_maker $url_maker объект для формирования урлов
  * @param pagination $pagination объект для формирования пагинации
  * @param string $search_query поисковый запрос
  * @param int $results_on_page количество результатов на странице
  */
 public function __construct($lang, $url_maker, $pagination, $search_query, $results_on_page = 10)
 {
  $this->lang = $lang;
  $this->url_maker = $url_maker;
  $this->pagination = $pagination;
  $this->pagination->add_get_params(TRUE);
  $this->results_on_page = $results_on_page;
  $this->set_search_query($search_query);
 }

 /**
  * Добавляет модуль в запрос на выборку
  * @param string $module название модуля
  * @param string $describe поле с описанием (автоматичски добавляется _$lang и кавычки)
  * @param string $caption поле с заголовком (автоматичски добавляется _$lang и кавычки)
  * @return $this
  */
 public function add_module($module, $describe = 'text', $caption = 'caption')
 {
  $this->modules[$module] = array();
  if (!empty($describe)) $describe = "`{$describe}_{$this->lang}`";
  if (!empty($caption)) $caption = "`{$caption}_{$this->lang}`";
  
  $this->set_module_fields($module, $describe, $caption);
  return $this;
 }

 /**
  * Устанавливает поисковый запрос
  * @param string $search_query
  * @return $this
  */
 public function set_search_query($search_query)
 {
  $this->search_query = $search_query;
  return $this;
 }

 /**
  * Возвращает поисковый запрос
  * @param bool $safe флаг, который позволяет экранировать символы
  * @return string
  */
 public function get_search_query($safe = true)
 {
  $result = $this->search_query;
  if ($safe == true) $result = safe($result);
  return $result;
 }

 /**
  * Генерирует результат
  * @return array
  */
 public function search()
 {
  if (!empty($this->search_query))
  {
   $query = $this->make_query();
   $count = count($data = mysql_data_assoc($query));
   
   if ($count > $this->results_on_page)
   {
    $this->pagination->set_items_on_page($this->results_on_page);
    $this->pagination->set_items_count($count);
    $data = mysql_data_assoc($query.' '.$this->pagination->get_limit());
   }
   
   $this->result = $data;
   $this->format_urls();
  }
  
  return $this->get_result();
 }

 /**
  * Устанавливает mysql поля для модуля
  * @param string $module
  * @param string $describe
  * @param string $caption
  * @return $this
  */
 public function set_module_fields($module, $describe, $caption = '`caption`')
 {
  if (array_key_exists($module, $this->modules)) $this->modules[$module] = array('caption'=>$caption, 'describe'=>$describe);
  return $this;
 }

 /**
  * Добавляет дополнительное mysql поле в поисковый запрос 
  * @param string $module модуль, к которому нужно добавить поле
  * @param string $field поле
  * @return $this
  */
 public function add_module_search_field($module, $field)
 {
  if (!array_key_exists($module, $this->additional_fields)) $this->additional_fields[$module] = array();
  $this->additional_fields[$module][] = $field;
  return $this;
 }


 /**
  * Возвращает доволнительные поля для модуля
  * @param string $module
  * @return array
  */
 protected function get_module_additional_search_fields($module)
 {
  if (array_key_exists($module, $this->additional_fields)) return $this->additional_fields[$module]; else return array();
 }

 /**
  * Формирует MySQL запрос
  * @return string
  */
 protected function make_query()
 {
  $full_query = '';

  foreach ($this->modules as $module => $tableArray)
  {
   try {
    $model = $this->load_model($module);
    $tableName = $model->get_table_name();
   }
   catch (Exception $e)
   {
    $tableName = '';
   }
   
   if (!empty($tableName))
   {
    $query = "select `id`, '{$module}' as `module`, `static`, {$tableArray['caption']} as `caption`";
    if (!empty($tableArray['describe'])) $query .= ", {$tableArray['describe']} as `describe`"; else $query .= ", '' as `describe`";

    if (is_subclass_of($model, 'a_model') and $external_model = $model->external_model())
	 $query .= ", `external_parent` as `external_parent`";
	else
	 $query .= ", null as `external_parent`";

    $query.=" from `{$tableName}`";
    if (!empty($tableArray['caption'])) $query.=" where {$tableArray['caption']} like '%{$this->get_search_query()}%'";
    if (!empty($tableArray['describe'])) $query.=" or {$tableArray['describe']} like '%{$this->get_search_query()}%'";
    foreach ($this->get_module_additional_search_fields($tableName) as $field) $query.=" or `$field` like '%{$this->get_search_query()}%'";

    $full_query .= "( $query )";
    if ($module != end(array_keys($this->modules))) $full_query .= ' union '; else $full_query.=' order by `caption`';
   }
  }
  
  return $full_query;
 }

 /**
  * Формирует максимальное количество дополнительных параметров для модулей
  * @return int
  */
 protected function get_max_params_count()
 {
  $max_params_count = 0;
  foreach ($this->tables as $tableArray)
  {
   $params_count = preg_match_all("/`[a-zA-Z_]+`/",$tableArray['href'],$hrefSelect);
   if ($params_count > $max_params_count) $max_params_count = $params_count;
  }
  
  return $max_params_count;
 }

 /**
  * Формирует url'ы
  */
 protected function format_urls()
 {
  foreach ($this->get_result() as $index => $line)
  {
   if ($line['module'] == 'goods')
   {
	$external_model = new categories();

	$cat_id = $external_model->get_parent_item($line['external_parent']);
	$category = $external_model->get_item("`id`='{$cat_id}'");
	$category = $this->url_maker->item_url($category, $external_model);
	
	$this->result[$index]['url'] = $category['url'].'?filter_categories[]='.$line['external_parent'];
   }
   else
   {
	$this->result[$index] = $this->url_maker->item_url($line, $this->load_model($line['module']));
   }
  }
 }

 /**
  * Возвращает результат
  * @return array
  */
 protected function get_result()
 {
  if (is_array($this->result)) return $this->result; else return array();
 }

 /**
  * Загружакт модель
  * @param string $model
  * @return a_model
  * @throws Exception
  */
 protected function load_model($model)
 {
  if (array_key_exists($model, $this->models)) return $this->models[$model];
  if (class_exists($model))
  {
   $object = new $model();
   $this->models[$model] = $object;
   return $object;
  }
  
  throw new Exception("model '$model' not found");
 }
}