<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 08.03.14
 */

/**
 * "Выдача" файлов браузеру как бинарных файлов  
 */
class send_file {

 /**
  * @var string файл, который нужно отдать
  */
 protected $filename;

 /**
  * @var string|null новое имя файла. Если null - отдавать под старым именем.
  */
 protected $new_filename;

 /**
  * @var string mimetype
  */
 protected $mimetype = 'application/octet-stream';


 /**
  * Отправка файла
  */
 public function send()
 {
  if (file_exists($this->filename) and $f=@fopen($this->filename,'rb'))
  {
   $filesize=filesize($this->filename);
   ignore_user_abort(true);
   $from=$to=0; $cr=NULL;
   if (array_key_exists('HTTP_RANGE',$_SERVER))
   {
    $range=substr($_SERVER['HTTP_RANGE'],strpos($_SERVER['HTTP_RANGE'],'=')+1);
    $from=strtok($range,'-');
    $to=strtok('/');
    if ($to>0) $to++;
    if ($to) $to-=$from;
    header('HTTP/1.1 206 Partial Content');
    $cr='Content-Range: bytes '.$from.'-'.($to ? $to.'/'.$to+1 : $filesize);
   } else header('HTTP/1.1 200 Ok');
   header('Accept-Ranges: bytes');
   header('Content-Length: '.($filesize-$to+$from));
   if ($cr) header($cr);
   header('Content-Type: '.$this->mimetype);
   header('Content-Disposition: attachment; filename="'.$this->get_filename_to_output().'";');
   header('Content-Transfer-Encoding: binary');
   header('Last-Modified: '.gmdate('r',filemtime($this->filename)));
   if ($from) fseek($f,$from,SEEK_SET);
   if (!isset($to) or empty($to)) $size=$filesize-$from;
   else $size=$to;
   while(!feof($f) and !connection_status()) echo fread($f,16384);
   fclose($f);
   ignore_user_abort(false);
   return TRUE;
  }
  
  return FALSE;
 }

 /**
  * Устанавливает файл для выдачи
  * @param string $filename
  * @return $this
  */
 public function set_filename($filename)
 {
  if (!empty($filename)) $this->filename = $filename;
  return $this;
 }

 /**
  * Устанавливает новое имя файла
  * @param string $filename
  * @return $this
  */
 public function set_new_filename($filename)
 {
  if (!empty($filename)) $this->new_filename = $filename;
  return $this;
 }

 /**
  * Устанавливает mimetype
  * @param string $mimetype
  * @return $this
  */
 public function set_mimetype($mimetype)
 {
  if (!empty($mimetype)) $this->mimetype = $mimetype;
  return $this;
 }

 /**
  * Формирует имя файла для отдачи браузеру
  * @return null|string
  */
 public function get_filename_to_output()
 {
  if (!empty($this->new_filename)) return $this->new_filename; else return basename($this->filename);
 }
}