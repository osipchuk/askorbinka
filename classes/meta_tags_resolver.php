<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 14.05.14
 */

/**
 * Генератор мета-тегов
 */
class meta_tags_resolver {

 /**
  * @var string
  */
 protected $title;

 /**
  * @var string
  */
 protected $keywords;

 /**
  * @var string
  */
 protected $description;

 /**
  * @var a_controller
  */
 protected $controller;

 /**
  * @var applicationHelper
  */
 protected $app_helper;

 public function __construct()
 {
  $this->app_helper = applicationHelper::getInstance();
 }

 /**
  * Устанавливает контроллер
  * @param a_controller $controller
  * @return $this
  */
 public function set_controller(a_controller $controller)
 {
  $this->controller = $controller;

  return $this;
 }

 /**
  * Устанавливает description
  * @param string $description
  * @return $this
  */
 public function set_description($description)
 {
  $this->description = $description;

  return $this;
 }

 /**
  * Устанавливает keywords
  * @param string $keywords
  * @return $this
  */
 public function set_keywords($keywords)
 {
  $this->keywords = $keywords;

  return $this;
 }

 /**
  * Устанавливает title
  * @param string $title
  * @return $this
  */
 public function set_title($title)
 {
  $this->title = $title;

  return $this;
 }

 /**
  * @return string
  */
 public function get_description()
 {
  if (is_null($this->description)) $this->format_meta_tags();
  return $this->description;
 }

 /**
  * @return string
  */
 public function get_keywords()
 {
  if (is_null($this->keywords)) $this->format_meta_tags();
  return $this->keywords;
 }

 /**
  * @return string
  */
 public function get_title()
 {
  if (is_null($this->title)) $this->format_meta_tags();
  return $this->title;
 }

 /**
  * Возвращает title по-умолчанию
  * @return string
  */
 public function get_default_title()
 {
  $settings = $this->app_helper->get_global('global_settings');
  return $settings['title'];
 }


 /**
  * Формирует мета-теги
  * return void
  */
 protected function format_meta_tags()
 {
  $settings = $this->app_helper->get_global('global_settings');
  
  if (!is_null($this->controller) and $depart = $this->controller->get_variable('depart_item') and is_array($depart))
  {
   $item = $this->controller->get_variable('item');
   if (empty($item)) $item = $depart;
   
   if (!empty($item['title'])) $this->title = $item['title'];
   if (!empty($item['description'])) $this->description = $item['description'];
   if (!empty($item['keywords'])) $this->keywords = $item['keywords'];
   
   if (empty($this->title))
   {
    $this->title = implode(' - ', array_reverse($this->app_helper->getAppController()->get_departs_caption()->get_path_array()));
    if (!empty($this->title)) $this->title .= ' - ';
    $this->title .= $this->get_default_title();
   }
  }
  
  if (empty($this->title) or empty($this->description) or empty($this->keywords))
  {
   if (empty($this->title)) $this->title = $this->get_default_title();
   if (empty($this->description)) $this->description = $settings['description'];
   if (empty($this->keywords)) $this->keywords = $settings['keywords'];
  }
 }
}