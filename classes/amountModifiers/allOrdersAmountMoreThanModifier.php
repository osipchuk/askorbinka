<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 20.12.14
 */

/**
 * Модификатор, если сумма всех прошлых заказов больше N
 */
class allOrdersAmountMoreThanModifier extends aProgramModifier {

    /**
     * Применяет модификатор
     * @return bool
     */
    public function apply()
    {
        if (!empty($this->account) and $this->getAllClientAmount($this->account['id']) >= $this->getProgram()['param_n']) {
            return $this->getBonusStrategy()->apply();
        } else {
            $this->getBonusStrategy()->disApply();
        }

        return false;
    }

    /**
     * Возаращает сумму всех покупок клиента
     * @return float
     */
    private function getAllClientAmount($clientId)
    {
        $clientId = (int) $clientId;
        $result = mysql_line_assoc("select sum(`order_goods`.`price` * `order_goods`.`count`) as `result`
                                    from `orders` join `order_goods` on `orders`.`id`=`order_goods`.`order`
                                    where `orders`.`client`='$clientId' and `orders`.`status`='1'");
        
        
        return (float) $result['result'];
    }
}