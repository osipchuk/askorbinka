<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 20.11.14
 */

/**
 * Модификатор цены
 */
abstract class aAmountModifier {

	/**
	 * @var bool флаг разрешения вызова следующего модификатора из массива.
	 * Если true - нужно выполнять дальше цепочку модификаторов. Если false - остановить.
	 */
	protected $callNextModifier = true;

	/**
	 * @var a_cart|null
	 */
	protected $cart;

	/**
	 * Устанавливает корзину пользователя
	 * @param a_cart $cart
	 * @return $this
	 */
	public function setCart(a_cart $cart)
	{
		$this->cart = $cart;
		return $this;
	}
	
	/**
	 * Возвращает корзину
	 * @return a_cart
	 * @throws Exception еслт корзина не задана
	 */
	public function getCart()
	{
		if (empty($this->cart)) {
			throw new Exception('Cart is not set');
		}

		return $this->cart;
	}
	
	/**
	 * Возвращает флаг разрешения вызова следующего модификатора из массива.
	 * @return boolean
	 */
	public function getCallNextModifier()
	{
		return (bool) $this->callNextModifier;
	}

	/**
	 * Возвращает цену подарочнго товара товара
	 * @return float
	 */
	public function getFreeGoodPrice()
	{
		return .1;
	}

	/**
	 * Отменяет действие модификатора
	 * @return bool
	 */
	public function disApply()
	{
		return true;
	}
	
	
	

	

		
	/**
	 * Применяет модификатор
	 * @return bool true - если был применен модификатор, false - если нет.
	 */
	abstract public function apply();

	/**
	 * Применяет модификатор к одному товару
	 * @param array $good
	 * @return float
	 */
	abstract public function goodApply(array $good);

	/**
	 * Помечает модификатор как использованный
	 * @return bool
	 */
	abstract public function makeUsed();
}