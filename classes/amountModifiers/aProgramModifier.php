<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 19.12.14
 */

/**
 * Абстрактный класс модификатора программы лояльности
 */
abstract class aProgramModifier extends aAmountModifier {

    /**
     * @var programs
     */
    protected $programsModel;

    /**
     * @var goods модель товаров
     */
    protected $goodsModel;

    /**
     * @var array
     */
    protected $program;

    /**
     * @var aBonusStrategy|null
     */
    protected $bonusStrategy;

    /**
     * @var programsCoverage
     */
    protected $programCoverage;

    /**
     * @var bool|array аккаунт клиента
     */
    protected $clientAccount = false;

    /**
     * Конструктор
     * @param array $program программа лояльности
     * @throws Exception
     */
    function __construct(array $program)
    {
        $this->programsModel = new programs();
        $this->programsModel->ignore_page_num(true);
        
        $this->goodsModel = new goods();
        $this->goodsModel->ignore_page_num(true);
        
        $this->programCoverage = new programsCoverage($program['id']);
        
        $this->setProgram($program);

        $auth = new auth();
        $auth->set_accounts_model(new clients());
        $this->account = $auth->get_account_info();
    }

    /**
     * Возвращает программу
     * @return array
     * @throws Exception в случае ошибки
     */
    public function getProgram()
    {
        return $this->program;
    }

    /**
     * Устанавливает программу
     * @param array $program
     * @return $this
     * @throws Exception в случае ошибки
     */
    public function setProgram(array $program)
    {
        if (!isset($program['id'])) {
            throw new Exception('program error');
        }
        $this->program = $program;

        return $this;
    }

    /**
     * Возвращает стратегию начисления бонусов
     * @return aBonusStrategy
     * @throws Exception если стратегия не задана
     */
    public function getBonusStrategy()
    {
        if (empty($this->bonusStrategy)) {
            throw new Exception('bonus strategy is not set');
        }
        return $this->bonusStrategy;
    }

    /**
     * Устанавливает стратегию начисления бонусов
     * @param aBonusStrategy $bonusStrategy
     * @return $this
     */
    public function setBonusStrategy(aBonusStrategy $bonusStrategy)
    {
        $this->bonusStrategy = $bonusStrategy;

        return $this;
    }

    /**
     * Возвращает модель твоаров
     * @return goods
     */
    public function getGoodsModel()
    {
        return $this->goodsModel;
    }

    /**
     * Возвращает объект programsCoverage
     * @return programsCoverage
     */
    public function getProgramCoverage()
    {
        return $this->programCoverage;
    }


    /**
     * Применяет модификатор к одному товару
     * @param array $good
     * @return float
     * @throws Exception в случае ошибки
     */
    public function goodApply(array $good)
    {
        throw new Exception('Невозможно применить модификатор к одному товару');
    }

    /**
     * Помечает модификатор как использованный
     * @return bool
     */
    public function makeUsed()
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    public function disApply()
    {
        return $this->getBonusStrategy()->disApply();
    }
}