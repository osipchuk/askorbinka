<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 20.12.14
 */

/**
 * Модификатор, если сумма покупок пользователя за промежуток времени больше чем N
 */
class periodAmountMoreThanModifier extends aProgramModifier {

    /**
     * Применяет модификатор
     * @return bool
     */
    public function apply()
    {
        $program = $this->getProgram();
        if (strtotime($program['date_end']) < time() and empty($program['bonuses_applied'])) {
            return $this->getBonusStrategy()->apply();
        }
        
        return false;
    }

    /**
     * @inheritdoc
     */
    public function makeUsed()
    {
        mysql_update($this->programsModel->get_table_name(), "`bonuses_applied`='1'", "`id`='{$this->getProgram()['id']}'");
        if (!mysql_error() and mysql_affected_rows() == 1) {
            return true;
        }
        
        return false;
    }
}