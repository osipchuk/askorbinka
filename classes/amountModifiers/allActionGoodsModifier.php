<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 20.12.14
 */

/**
 * Модификатор всех акционных товаров без дополнительных условий
 */
class allActionGoodsModifier extends aProgramModifier {

    /**
     * Применяет модификатор
     * @return bool
     */
    public function apply()
    {
        return $this->getBonusStrategy()->apply();
    }
}