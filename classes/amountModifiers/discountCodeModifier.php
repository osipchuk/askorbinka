<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 20.11.14
 */

/**
 * Модификатор цены по коду
 */
class discountCodeModifier extends aAmountModifier {

	/**
	 * @var string скидочный код
	 */
	protected $code = '';

	/**
	 * @var null|int id скидочного кода
	 */
	protected $codeId;

	/**
	 * @var discount_codes
	 */
	protected $codesModel;

	/**
	 * @var goods
	 */
	protected $goodsModel;

	/**
	 * @var bool флаг проверки, является ли код уже использованым
	 */
	protected $checkCodeUsed = false;

	/**
	 * Конструктор
	 */
	public function __construct()
	{
		$this->codesModel = new discount_codes();
		$this->goodsModel = new goods();
	}

	/**
	 * Применяет модификатор
	 * @return bool
	 * @throws Exception в случае ошибки
	 */
	public function apply()
	{
		$this->callNextModifier = false;
		$code = $this->getCode();
		
		if (!is_null($code)) {
			$codeItem = $this->findCodeInDb($code);
			if (empty($codeItem)) throw new Exception('code_not_find');
			if ($this->checkCodeUsed and $codeItem['used'] == 1) throw new Exception('code_is_used');

			$this->codeId = $codeItem['id'];

			$items = $this->getCart()->get_goods();
			foreach ($items as $item) {
				$item['price'] = $item['good']['price'];
				$discountPercentages = (int) $item['good']['code_discount'];
				
				if ($discountPercentages != 0) {
					$priceWithoutDiscount = (float) $item['price'];
					$priceWithDiscount = $priceWithoutDiscount - ($priceWithoutDiscount / 100 * $discountPercentages);
				} else {
					$priceWithDiscount = $item['price'];
				}

				$this->getCart()->setItemPriceWithoutDiscount($item['good']['id'], $item['price']);
				$this->getCart()->setItemPriceWithDiscount($item['good']['id'], $priceWithDiscount);
			}
			
			return true;
		}
		
		return false;
	}

	/**
	 * Применяет модификатор к одному товару
	 * @param array $good
	 * @return float
	 */
	public function goodApply(array $good)
	{
		$priceWithoutDiscount = $good['price'];
		$discountPercentages = (int) $good['code_discount'];
		if ($discountPercentages != 0) {
			$priceWithDiscount = $priceWithoutDiscount - ($priceWithoutDiscount / 100 * $discountPercentages);
			return $priceWithDiscount;
		}
		
		return $priceWithoutDiscount;
	}

	/**
	 * Устанавливает скидочный код
	 * @param string $code
	 * @return $this
	 */
	public function setCode($code)
	{
		$this->code = $code;
		return $this;
	}

	/**
	 * Возвращает скидочный код
	 * @return string|null
	 * @throws Exception если код не корректный
	 */
	public function getCode()
	{
		if (empty($this->code)) return null;
		if (!$this->codesModel->isCodeCorrect($this->code)) throw new Exception('Code error');
		
		return $this->code;
	}

	/**
	 * Возвращает id скидочного кода
	 * @return int|null
	 */
	public function getCodeId()
	{
		return $this->codeId;
	}

	/**
	 * Устанавливает флаг проверки, является ли код уже использованым
	 * @param boolean $checkCodeUsed
	 * @return $this
	 */
	public function setCheckCodeUsed($checkCodeUsed)
	{
		$this->checkCodeUsed = (bool) $checkCodeUsed;

		return $this;
	}

	/**
	 * Помечает модификатор как использованный
	 * @return bool
	 */
	public function makeUsed()
	{
		$codeItem = $this->findCodeInDb($this->getCode());
		if (empty($codeItem)) return false;
		if ($codeItem['used']) return true;

		$bonusesCount = 0;
		foreach ($this->getCart()->get_goods() as $item) {
			$bonusesCount += (int) ($item['count'] * $this->codeBonusesCount($item['good']));
		}

		mysql_update($this->codesModel->get_table_name(), "`{$this->codesModel->get_table_name()}`.`used`='1', `bonuses`='$bonusesCount'", "`id`='{$codeItem['id']}'");
		if (mysql_error()) return false;

		$clientBonuses = new client_bonuses();
		try {
			$clientBonuses->changeBalance($codeItem['client'], $bonusesCount, $this->getCart()->getOrderId());
		} catch (Exception $e) {
		    return false;
		}
		
		return true;
	}

    /**
     * Возврашает количество бонусов 
     * @param array $goodItem
     * @return int
     */
    public function codeBonusesCount(array $goodItem)
    {
        
        if (!isset($goodItem['code_bonuses']) or !isset($goodItem['price'])) {
            return 0;
        }
        
        $result = round($goodItem['price'] / 100 * $goodItem['code_bonuses']);
        return $result;
    }

	


	/**
	 * Вощвращает запись кода из БД по коду
	 * @param string $code
	 * @return array|null
	 */
	private function findCodeInDb($code)
	{
		return $this->codesModel->get_item("`{$this->codesModel->get_table_name()}`.`code`='" . safe($code) . "'");
	}
}