<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 19.12.14
 */

/**
 * Модификатор, если сумма заказа больше чем N
 */
class orderAmountMoreThanModifier extends aProgramModifier {

    /**
     * Применяет модификатор
     * @return bool
     */
    public function apply()
    {
        $program = $this->getProgram();
        $cartAmount = $this->getCart()->get_summary()['price'];
        if ($cartAmount >= $program['param_n']) {
            return $this->getBonusStrategy()->apply();
        } else {
            $this->getBonusStrategy()->disApply();
        }
        
        return false;
    }
}