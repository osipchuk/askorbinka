<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 20.12.14
 */

/**
 * Модификатор, если в корзине больше чем N акционных товаров
 */
class inCartMoreThanNActionGoodsModifier extends aProgramModifier {

    /**
     * Применяет модификатор, если в корзине больше чем N акционных товаров
     * @return bool
     */
    public function apply()
    {
        $count = 0;
        $actionGoods = $this->programCoverage->getAllGoodBonuses();
        foreach ($this->getCart()->get_goods() as $goodId => $good) {
            if (array_key_exists($goodId, $actionGoods)) {
                $count += $good['count'];
            }
        }
        
        if ($count >= $this->getProgram()['param_n']) {
            return $this->getBonusStrategy()->apply();
        } else {
            $this->getBonusStrategy()->disApply();
        }
        
        
        return false;
    }
}