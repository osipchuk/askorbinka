<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 20.11.14
 */

/**
 * Модификатор цены заказа. Применяет цепочку модификаторов
 */
class amountMakerContainer extends aAmountModifier {

	/**
	 * @var aAmountModifier[]
	 */
	protected $modifiers = [];

	/**
	 * @var aAmountModifier[] примененные модификаторы
	 */
	protected $appliedModifiers = [];

	/**
	 * @var array
	 */
	protected $config = [];

	/**
	 * Автоматическая инициализация объекта на основе конфига БД, и бизнес правил проекта
	 * @param array $config
	 * @return amountMakerContainer
	 */
	public static function init(array $config = [])
	{
		$container = new self;
		$container->setConfig($config); 
		
		if (isset($config['discountCode'])) {
			$modifier = new discountCodeModifier();
			$modifier->setCode($config['discountCode']);
			$container->addModifier($modifier);
		}
		
		$programs = (new programs())->getActual();
		
		foreach ($programs as $program) {
			switch ($program['type']) {
				case 1:
					$modifier = new orderAmountMoreThanModifier($program);
					break;
				
				case 2:
					$modifier = new allOrdersAmountMoreThanModifier($program);
					break;
				
				case 4:
					$modifier = new inCartMoreThanNActionGoodsModifier($program);
					break;
				
				case 5:
					$modifier = new allActionGoodsModifier($program);
					break;
				
				default:
					$modifier = null;
			}
			
			if (!is_null($modifier)) {
				switch ($program['bonus_type']) {
					case 1:
						$bonusStrategy = new addBonusesBonusStrategy($modifier);
						break;
					
					case 2:
						$bonusStrategy = new allOrderDiscountBonusStrategy($modifier);
						break;
					
					case 3:
						$bonusStrategy = new freeGoodBonusStrategy($modifier);
						break;
					
					case 4:
						$bonusStrategy = new freeShipBonusStrategy($modifier);
						break;
					
					case 5:
						$bonusStrategy = new goodsActionPriceBonusStrategy($modifier);
						break;
					
					default:
						$bonusStrategy = null;
				}
				
				if (!is_null($bonusStrategy)) {
					$modifier->setBonusStrategy($bonusStrategy);
					$container->addModifier($modifier);
				}
			}
		}
		
		return $container;
	}

	/**
	 * Добавляет новый модификатор в цепочку
	 * @param aAmountModifier $modifier
	 * @return $this
	 */
	public function addModifier(aAmountModifier $modifier)
	{
		$this->modifiers[] = $modifier;
		return $this;
	}

	/**
	 * Применяет модификатор
	 * @return bool
	 */
	public function apply()
	{
		$this->appliedModifiers = [];
		$breakModifier = null;
		$this->resetAllDiscountsInCart();
		
		foreach ($this->modifiers as $modifier) {
			$applied = $modifier->setCart($this->getCart())->apply();
			if ($applied) {
				$this->appliedModifiers[] = $modifier;
			}
			
			if (!$modifier->callNextModifier) {
				$breakModifier = $modifier;
				break;
			}
		}
		
		if (!is_null($breakModifier)) {
			$breakModifierBehind = false;
			foreach ($this->modifiers as $modifier) {
				$modifier->setCart($this->getCart());
				
				if ($breakModifierBehind) {
					$modifier->disApply();
				}
				
				if ($modifier == $breakModifier) {
					$breakModifierBehind = true;
				}
			}
		}
		
		return !empty($this->appliedModifiers);
	}


	/**
	 * @inheritdoc
	 */
	public function goodApply(array $good)
	{
		throw new Exception('You can\'t apply all discounts to simple good');
	}

	/**
	 * Помечает модификатор как использованный
	 * @return bool
	 */
	public function makeUsed()
	{
		$result = true;
		
		foreach ($this->modifiers as $modifier) {
			$modifier->setCart($this->getCart());
			$result = $modifier->makeUsed() and $result;
			if (!$modifier->getCallNextModifier()) break;
		}
		
		return $result;
	}

	/**
	 * Сбрасывает все скидки для корзины, что бы применить их снова
	 * @return $this
	 * @throws Exception
	 */
	public function resetAllDiscountsInCart()
	{
		$items = $this->getCart()->get_goods();
		foreach ($items as $item) {
			$this->getCart()->setItemPriceWithDiscount($item['good']['id'], $this->getCart()->good_price($item['good']));
		}
		return $this;
	}

	/**
	 * Проверяет, есть ли в корзине акционные товары
	 * @param bool $skipLongTermActions
	 * @return bool
	 */
	public function isActionGoodsInCart($skipLongTermActions = true)
	{
		foreach ($this->appliedModifiers as $modifier) {
//			TODO: вынести в параметр класса
			if (!is_subclass_of($modifier, 'periodAmountMoreThanModifier')) {
				return true;
			}
		}
		
		foreach ($this->getCart()->get_goods() as $good) {
			if ($good['good']['action_price'] != 0) {
				return true;
			}
		}
		
		return false;
	}

	/**
	 * Устанавливает конфиг
	 * @param array $config
	 * @return $this
	 */
	public function setConfig(array $config = [])
	{
		$this->config = $config;
		return $this;
	}

	/**
	 * Возвращает конфиг
	 * @return array
	 */
	public function getConfig()
	{
		return $this->config;
	}
}