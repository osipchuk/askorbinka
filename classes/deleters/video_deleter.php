<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 16.02.15
 */

/**
 * Класс для удаления видео
 */
class video_deleter extends a_item_deleter
{
    /**
     * Конструктор
     * @param a_model $model
     */
    public function __construct(a_model $model)
    {
        parent::__construct($model);
        $this->add_file('image');
    }
}