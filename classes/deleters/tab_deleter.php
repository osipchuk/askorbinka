<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 22.09.14
 */

/**
 * Класс для удаления вкладок товаров
 */
class tab_deleter extends a_item_deleter {

 /**
  * Удаляет элемент
  * @param int $item_id id элемента
  * @return bool
  */
 protected function _delete($item_id)
 {
  $item_id = (int)$item_id;
  $tab_printer = new files_table_printer(array());
  
  mysql_delete('good_tab_files', "`external_parent`='$item_id'", array('file'=>$tab_printer->files_dir(array('image'=>FALSE))));
  mysql_delete('good_tab_tech_images', "`external_parent`='$item_id'", array('image'=>$tab_printer->files_dir(array('image'=>TRUE)),
	                                                                         'image_small'=>$tab_printer->files_dir(array('image'=>TRUE)) ));
  
  return parent::_delete($item_id);
 }
}