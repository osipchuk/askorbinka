<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 28.04.14
 */

/**
 * Класс для удаления страниц сайта
 * 
 * Базовая часть, стандартная для всех сайтов
 */
abstract class a_site_structure_deleter extends a_tree_structure_deleter {

 /**
  * @var site_structure_ext_modules_finder
  */
 protected $ext_modules_finder;

 /**
  * Удаляет раздел сайта, проверяя перед этим возможость удаления всех модулей
  * @param array $item
  * @return bool
  * @throws Exception
  */
 public function delete($item)
 {
  if ($this->get_ext_modules_finder()->has_ext_modules($item))
  {
   $result = TRUE;
   if (array_key_exists('sub', $item) and is_array($item['sub'])) foreach ($item['sub'] as $subitem) $result = ($this->delete($subitem) and $result);

   return $this->_delete($item['id']);
  }
  throw new Exception('Can\'t delete this category');
 }

 /**
  * Устанавливает объект, отвечающий за поиск нестандартных модулей в элементе для удаления
  * @param site_structure_ext_modules_finder $ext_modules_finder
  * @return $this
  */
 public function set_ext_modules_finder(site_structure_ext_modules_finder $ext_modules_finder)
 {
  $this->ext_modules_finder = $ext_modules_finder;

  return $this;
 }

 /**
  * Возвращает объект, отвечающий за поиск нестандартных модулей в элементе для удаления
  * @return site_structure_ext_modules_finder
  * @throws Exception
  */
 public function get_ext_modules_finder()
 {
  if (empty($this->ext_modules_finder)) throw new Exception('ext module finder error');
  return $this->ext_modules_finder;
 }
}