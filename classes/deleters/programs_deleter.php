<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 12.12.14
 */

/**
 * Класс для удаления программ лояльности
 */
class programs_deleter extends a_item_deleter {

	/**
	 * Конструктор
	 * @param a_model $model модель
	 */
	public function __construct(a_model $model)
	{
		parent::__construct($model);
		$this->add_file('image');
		$this->add_file('good_image');
	}
}