<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 16.02.15
 */

/**
 * Класс для удаления брендов
 */
class brand_deleter extends a_item_deleter
{
    /**
     * @inheritdoc
     */
    public function __construct(a_model $model)
    {
        parent::__construct($model);
        $this->add_file('image')->add_file('image_large');
    }

    /**
     * @inheritdoc
     */
    protected function _delete($item_id)
    {
        $videoModel = new videos();
        $videoDeleter = new video_deleter($videoModel);

        $videos = $videoModel->get_all("`brand`='{$item_id}'");
        foreach ($videos as $video) {
            $videoDeleter->delete($video['id']);
        }

        return parent::_delete($item_id);
    }
}