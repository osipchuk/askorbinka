<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 17.10.14
 */

/**
 * Класс для удаления новинок и акций
 */
class news_and_actions_deleter extends a_item_deleter {

 /**
  * Конструктор
  * @param a_model $model модель
  */
 public function __construct(a_model $model)
 {
  parent::__construct($model);
  $this->add_file('image')->add_file('image_label')->add_file('image_label_small');
 }


}