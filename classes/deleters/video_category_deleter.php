<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 16.02.15
 */

class video_category_deleter extends a_item_deleter
{
    /**
     * @inheritdoc
     */
    protected function _delete($item_id)
    {
        $videoModel = new videos();
        $videoDeleter = new video_deleter($videoModel);
        
        $videos = $videoModel->get_all("`category`='{$item_id}'");
        foreach ($videos as $video) {
            $videoDeleter->delete($video['id']);
        }
        
        return parent::_delete($item_id);
    }
}