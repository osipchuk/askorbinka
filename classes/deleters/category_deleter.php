<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 23.09.14
 */

/**
 * Класс для удаления категорий товаров
 */
class category_deleter extends tree_item_deleter {

 /**
  * @var a_model
  */
 protected $category_param_model;

 /**
  * @var a_item_deleter
  */
 protected $category_param_deleter;

 /**
  * @var a_item_deleter
  */
 protected $news_and_actions_deleter;
 
 protected $category_filters_model;
 
 protected $category_filters_deleter;

 /**
  * Конструктор
  * @param a_model $model модель
  */
 public function __construct(a_model $model)
 {
  parent::__construct($model);
  $this->category_param_model = new category_params();
  $this->category_param_deleter = new category_param_deleter($this->category_param_model);
  
  $this->category_filters_model = new category_filters();
  $this->category_filters_deleter = new category_filters_deleter($this->category_filters_model);
  
  $this->add_file('image')->add_file('image_filter')->add_file('image_filter_active')->add_file('image_subnav');
 }


 /**
  * Удаляет элемент
  * @param int $item_id id элемента
  * @return bool
  */
 protected function _delete($item_id)
 {
  $params = $this->category_param_model->get_all_of_external_parent($item_id);
  foreach ($params as $param) $this->category_param_deleter->delete($param['id']);
  
  $filters = $this->category_filters_model->get_all_of_external_parent($item_id);
  foreach ($filters as $filter) $this->category_filters_deleter->delete($filter['id']);

  return parent::_delete($item_id);
 }
}