<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 28.04.14
 */

/**
 * Класс для удаления элементов древовидной структуры
 */
abstract class a_tree_structure_deleter extends a_item_deleter {

 /**
  * @var a_item_deleter класс для удаления объектов
  */
 protected $item_deleter;

 /**
  * Удаляет элемент
  * @param array $item
  * @return bool
  */
 public function delete($item)
 {
  $result = TRUE;
  if (array_key_exists('sub', $item) and is_array($item['sub'])) foreach ($item['sub'] as $subitem) $result = ($this->delete($subitem) and $result);
  
  return $this->delete_item($item['id']) and $this->_delete($item['id']);
 }

 /**
  * Устанавливает класс для удаления элементов
  * @param a_item_deleter $item_deleter
  * @return $this
  */
 public function set_item_deleter(a_item_deleter $item_deleter)
 {
  $this->item_deleter = $item_deleter;
  return $this;
 }

 /**
  * Удаляет элемент
  * @param int $item_id
  * @return bool
  * @throws Exception
  */
 protected function delete_item($item_id)
 {
  if (is_null($this->item_deleter)) return TRUE;
  $data = $this->item_deleter->get_model()->get_all_of_parent($item_id);
  
  $result = TRUE;
  foreach ($data as $line) $result = $this->item_deleter->delete($line['id']) and $result;
  return $result;
 }

}