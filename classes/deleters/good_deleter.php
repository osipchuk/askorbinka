<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 22.09.14
 */

/**
 * Класс для удаления товаров
 */
class good_deleter extends a_item_deleter {

 /**
  * @var a_item_deleter класс для удаления вкладок
  */
 protected $tab_deleter;

 /**
  * @var a_model модель вкладок
  */
 protected $tabs_model;

 public function __construct(a_model $model)
 {
  parent::__construct($model);
  $this->tabs_model = new good_tabs();
  $this->tab_deleter = new tab_deleter($this->tabs_model);
 }


 /**
  * Удаляет элемент
  * @param int $item_id id элемента
  * @return bool
  */
 protected function _delete($item_id)
 {
  $videoModel = new videos();
  $videoDeleter = new video_deleter($videoModel);

  $videos = $videoModel->get_all("`good`='{$item_id}'");
  foreach ($videos as $video) $videoDeleter->delete($video['id']);
     
  return $this->delete_tabs($item_id) and parent::_delete($item_id);
 }

 /**
  * Удаляет вкладки твоара
  * @param int $item_id
  * @return bool
  */
 protected function delete_tabs($item_id)
 {
  $result = TRUE;
  $tabs = $this->tabs_model->get_all_of_external_parent($item_id);
  foreach ($tabs as $tab) $result = ($result and $this->tab_deleter->delete($tab['id']));
  
  return $result;
 }
}