<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 23.01.15
 */

/**
 * Класс для удаления фильтров категории
 */
class category_filters_deleter extends a_item_deleter {

    /**
     * Удаляет элемент
     * @param int $item_id id элемента
     * @return bool
     */
    protected function _delete($item_id)
    {
        $item_id = (int)$item_id;
        mysql_delete('category_filter_values', "`external_parent`='$item_id'", ['image']);

        return parent::_delete($item_id);
    }
}
