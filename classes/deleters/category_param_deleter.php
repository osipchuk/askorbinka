<?php
/**
 * @package default
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 23.09.14
 */

/**
 * Класс для удаления параметров категории
 */
class category_param_deleter extends a_item_deleter {

 /**
  * Удаляет элемент
  * @param int $item_id id элемента
  * @return bool
  */
 protected function _delete($item_id)
 {
  $item_id = (int)$item_id;
  $tab_printer = new files_table_printer(array());

  mysql_delete('category_param_images', "`external_parent`='$item_id'", array('image'=>$tab_printer->files_dir(array('image'=>TRUE)),
	  'image_small'=>$tab_printer->files_dir(array('image'=>TRUE)) ));

  return parent::_delete($item_id);
 }
}