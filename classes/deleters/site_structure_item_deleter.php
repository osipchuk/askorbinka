<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 28.04.14
 */

/**
 * Класс для удаления элементов структуры сайта
 * 
 * Оснащен средствами для удаления подмодулей
 * 
 */
class site_structure_item_deleter extends item_deleter {

// TODO: добавить средствам для удаления подмодулей

 /**
  * Удаляет элемент
  * @param int $item_id id элемента
  * @return bool
  */
 public function delete($item_id)
 {
  $item_id = (int)$item_id;
  
  $item = $this->get_model()->get_item("`id`='$item_id'");
  if (empty($item)) return FALSE;
  
  return parent::delete($item_id);
 }


}