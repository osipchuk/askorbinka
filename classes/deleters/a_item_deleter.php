<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 28.04.14
 */

/**
 * Класс для удаления элементов 
 */
abstract class a_item_deleter {

 /**
  * @var a_model
  */
 protected $model;

 /**
  * @var array массив файлов для удаления
  */
 protected $files = array();

 /**
  * @var string mode для удаления project_images и content_images
  */
 protected $mode;

 /**
  * Конструктор
  * @param a_model $model модель
  */
 public function __construct(a_model $model)
 {
  $this->set_model($model);
 }

 /**
  * Устанавливает модель
  * @param a_model $model
  * @return $this
  */
 public function set_model(a_model $model)
 {
  $this->model = $model;

  return $this;
 }

 /**
  * Возвращает модель
  * @return \a_model
  */
 public function get_model()
 {
  return $this->model;
 }

 /**
  * Добавляет файл для удаления
  * @param string $db_field колонка в таблице
  * @param null|string $dir директория размещения файлов
  * @return $this
  */
 public function add_file($db_field, $dir = NULL)
 {
  global $classes_dir;

  if (is_null($dir)) $dir = $classes_dir.'../admin/project_images/';
  $this->files[$db_field] = $dir;
  
  return $this;
 }

 /**
  * Устанавливает mode
  * @param string $mode
  * @return $this
  */
 public function set_mode($mode)
 {
  $this->mode = $mode;

  return $this;
 }

 /**
  * Возвращает mode
  * @return string
  */
 public function get_mode()
 {
  if (empty($this->mode)) return $this->get_model()->get_table_name();
  return $this->mode;
 }

 /**
  * Удаляет элемент
  * @param int $item_id id элемента
  * @return bool
  */
 public function delete($item_id)
 {
  return $this->_delete($item_id);
 }

 /**
  * Удаляет элемент
  * @param int $item_id id элемента
  * @return bool
  */
 protected function _delete($item_id)
 {
  $item_id = (int)$item_id;

  delete_content_images($this->get_mode().'__'.$item_id);
  delete_project_images($this->get_mode(), $item_id);
  mysql_delete($this->get_model()->get_table_name(), "`id`='$item_id'", $this->files);

  if (!mysql_error()) return TRUE; else return FALSE;
 }
}