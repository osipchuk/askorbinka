<?php
require_once 'header.php';

if (!$mode) die('mode error');
$parent = get('gq', 'parent');

$model = new programs();
$tablename = $model->get_table_name();

if ($creat_mode == 'add' or ($creat_mode == 'edit' and $id)) {
	if (get('pi', 'type') == 3) {
		$_POST['bonus_type'] = 1;
	}
	
	if (in_array(get('ps', 'type'), $model->allGoodsTypes())) {
		$_POST['all_goods'] = 1;
	}

	$set = mysql_langs_set(array('caption', 'title', 'text', 'description', 'keywords', 'preview')) .
		fill_query(array(
			'd' => array('date_begin', 'date_end'),
			'q' => array('static'),
			'i' => array('external_parent', 'all_goods', 'type', 'bonus_type', 'rate')
		)) . query_add().query_add('good_image', 'good_image');

	if ($id = standart_edit($creat_mode, $tablename, $set, '', $id)) {
		delete_query_image($tablename, "`id`='$id'", 'good_image', 'good_image');
	}
	
	programsCoverage::updateIndex($id);
}

if ($creat_mode == 'delete_image' and $id) delete_image($tablename, "`id`='$id'");

if ($creat_mode == 'delete' and $id) {
	$deleteCoverage = get('gs', 'delete_coverage');
	
	if (!empty($deleteCoverage)) {
		switch ($deleteCoverage) {
			case 'brands':
				$itemColumn = 'brand';
				break;

			case 'categories':
				$itemColumn = 'category';
				break;

			case 'goods':
				$itemColumn = 'good';
				break;

			default:
				die('error');
		}

		$deleteItem = get('gi', 'delete_item');
		$dataTable = constant('programsCoverage::' . strtoupper($deleteCoverage . '_DATA_TABLE'));
		
		mysql_delete($dataTable, "`program`='$deleteItem' and `$itemColumn`='$id'");
		programsCoverage::updateIndex($deleteItem);
		
		redirect($_SERVER['HTTP_REFERER']);
		
	} else {
		$deleter = new programs_deleter($model);
		$deleter->delete($id);
		redirect("admin.php?mode=$mode&parent=$parent");
	}
}

if ($creat_mode == 'update_settings' and $id) {
	$line = $model->get_item("`$tablename`.`id`='$id'");
	if (empty($line)) die('Program error');

	$freeGoods = get('pi', 'free_good');
	$set = "`id`=`id`" . fill_query(array(
			'i' => array('param_n', 'discount', 'base_bonus', 'bonuses_applied')
		)) . ", `free_good` = " . ($freeGoods ?: 'null');

	mysql_update($tablename, $set, "`id`='$id'");
	
	
	if ($line['bonus_type'] == 1 or !$line['all_goods']) {
		$coverage = new programsCoverage($id);
		
		$coverageTypes = ['brand', 'category', 'good'];
		foreach ($coverageTypes as $coverageType) {
			$deleteMethodName = 'remove' . ucfirst($coverageType) . 'Coverage';
			$addMethodName = 'add' . ucfirst($coverageType) . 'Coverage';
			$updateMethodName = 'update' . ucfirst($coverageType) . 'Coverage';

			foreach ($_POST['cover_' . $coverageType . '_add'] as $addId) {
				$coverage->$addMethodName($addId);
			}
			
			foreach ($_POST['cover_' . $coverageType . '_delete'] as $deleteId) {
				$coverage->$deleteMethodName($deleteId);
			}
			
			foreach ($_POST['cover_' . $coverageType . '_bonus'] as $updateId => $bonus) {
				$coverage->$updateMethodName($updateId, $bonus);
			}
		}
	}

	programsCoverage::updateIndex($id);

	redirect("admin.php?mode=$mode&parent=$id");
}

redirect("admin.php?mode=$mode&parent=$parent&creat_mode=edit&id=$id");

?>
