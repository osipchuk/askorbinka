<?php
$model = new map_addresses();
$tablename = $model->get_table_name();

$address_types_array = array('s'=>'Сервисный центр', 'd'=>'Регионалное отделение');

$parent_factory = new parent_select_factory();
$select_printer = $parent_factory->get_select($model);

$top_form = new top_form_printer($select_printer);
echo $top_form->add_parameter('mode', $mode)->put();
$parent = $select_printer->get_value();
$data = $model->get_all_of_external_parent($parent);


if ($creat_mode == 'add' or ($creat_mode == 'edit' and $id and
		$line = mysql_line("select `type`, `tel`, `map_x`, `map_y`, `map_zoom`".mysql_langs_select(array('address'))." from `$tablename` where `id`='$id'")))
{
 $type_putter = new select_simple_printer($address_types_array);
 $type_putter->set_name('type')->set_value($line[0]);

 $google_maps = new google_maps();

 if (empty($line[2]) or empty($line[3]))
 {
  $parent_model_name = $model->external_model();
  $parent_model = new $parent_model_name();
  $city = $parent_model->get_item("`id`='".(int)$parent."'");
  
  try {
   $default_coords = $google_maps->get_address_coords($city['caption']);
   $line[2] = $default_coords->x;
   $line[3] = $default_coords->y;
  }
  catch (Exception $e)
  {
   $line[2] = 0;
   $line[3] = 0;
  }
 }


 $google_maps->add_marker($line[2], $line[3], '', null, TRUE);
 $map = $google_maps->show_coords_map($line[2], $line[3], isset($line[4]) ? $line[4] : 11);


 echo put_main_form($line,
	 array_merge(
		 array(
			 'Тип'=>array($type_putter->put()),
			 'Номер телефона'=>array(null,'text','tel',1),
			 'Кординаты объекта (x)'=>array(null,'text','map_x',1,null,'150px',null,' data-marker-x=""'),
			 'Кординаты объекта (y)'=>array(null,'text','map_y',1,null, '150px',null,' data-marker-y=""'),
			 'Масштаб карты'=>array(null,'text','map_zoom',1, isset($line[4]) ? $line[4] : 10, '150px')
	  
		 ),
		 main_form_fields_array($lang_array, array('Адрес'=>array('text','address')),
			 array('address_'.$prime_lang=>array(1))),
	  array(
		  'Карта проезда'=>array($map)
	  )
	 )
	 , "&mode=$mode&parent=$parent");
}


print_button('Добавить',"location.href='admin.php?mode=$mode&creat_mode=add&parent=$parent'");
print_table_header(array('Адрес','Телефон','Тип','Операции'),array(0,200,200,100));
$data = $model->get_all_of_external_parent($parent);

foreach ($data as $index => $line)
{
 echo "<tr".tr_class($index).">
        <td>{$line['address']}</td>
        <td>{$line['tel']}</td>
        <td style='text-align:center'>{$address_types_array[$line['type']]}</td>
        <td style='text-align:center'>".put_edit_buttons($line['id'], 'адрес', "&mode=$mode&parent=$parent")."</td>
       </tr>";
}
print_table_bottom();
?>
