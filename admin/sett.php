<?php
$line=@filter_html(mysql_line_assoc("select * from `settings`"));

echo "<form name='main' onsubmit='return test_input()' enctype='multipart/form-data' action='sett_edit.php' method='post'>
      <table class='table2' cellpadding='0' cellspacing='0' border='0' style='margin-bottom:10px;'>
       <tr>
        <th width='45%'>E-mail</th>
        <td><input id='r1' type='email' name='email' value='{$line['email']}' style='width:95%;'></td>
       </tr>
       <tr>
        <th>Отправитель писем (подпись и email)</th>
        <td>
        <input id='r3' type='text' name='sender_title' value='{$line['sender_title']}' style='width:46%; margin-right: 3%;'><input id='r2' type='text' name='sender' value='{$line['sender']}' style='width:46%;'>
        </td>
       </tr>
       <tr>
        <th>E-mail отдела логистики</th>
        <td><input type='email' name='logistic_department_email' value='{$line['logistic_department_email']}' style='width:95%;' required></td>
       </tr>
       
       <tr>
        <th>Название сайта (default)</th>
        <td><input type='text' name='title' value='{$line['title']}' style='width:95%;'></td>
       </tr>
       <tr>
        <th>META Description (default)</th>
        <td><input type='text' name='description' value='{$line['description']}' style='width:95%;'></td>
       </tr>
       <tr>
        <th>META Keywords (default)</th>
        <td><textarea name='keywords' style='width:95%; height:80px;'>{$line['keywords']}</textarea></td>
       </tr>
       <tr>
        <th>Google analytics</th>
        <td><textarea name='analytics' style='width:95%; height:80px;'>{$line['analytics']}</textarea></td>
       </tr>
       <tr>
        <th>Видео-инструкция для профи (video id)</th>
        <td><input type='text' name='pro_instruction_video_id' value='{$line['pro_instruction_video_id']}' style='width:95%;' required></td>
       </tr>";

foreach ($social_links as $social => $value)
{
 echo "<tr>
        <th>{$social}-профиль</th>
        <td><input type='text' name='$social' value='".htmlspecialchars($value, ENT_QUOTES)."' style='width:95%;'></td>
       </tr>";
}

echo "<tr>
        <th>Получатель (для счет-фактуры)</th>
        <td><textarea name='seller' style='width:95%; height:80px;'>{$line['seller']}</textarea></td>
       </tr>
      <tr>
        <th>Фон слайдера</th>
        <td><textarea name='slider_background' style='width:95%; height:80px;'>{$line['slider_background']}</textarea></td>
       </tr>
        
      <tr>
       <th colspan='2'>SMTP (в случае необходимости)</th>
      </tr>
      <tr>
        <th>Сервер, порт</th>
        <td>
         <input type='text' name='smtp_host' value='".htmlspecialchars($line['smtp_host'], ENT_QUOTES)."' style='width:300px;' autocomplete='off'>
         <input type='text' name='smtp_port' value='{$line['smtp_port']}' style='width:100px;' autocomplete='off'>
        </td>
      </tr>
      <tr>
        <th>Логин</th>
        <td><input type='text' name='smtp_username' value='".htmlspecialchars($line['smtp_username'], ENT_QUOTES)."' style='width:95%;'></td>
      </tr>
      <tr>
        <th>Изменить пароль</th>
        <td><input type='password' name='smtp_pass' value='' style='width:95%;'></td>
      </tr>
      
      </table>
      <input type='submit' value='Сохранить' class='sh_button'>
      </form>";

?>
