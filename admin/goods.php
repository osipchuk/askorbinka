<?php
$model = new goods();
$tablename = $model->get_table_name();

$parent_factory = new parent_select_factory();
$select_printer = $parent_factory->get_select($model, FALSE);

$top_form = new top_form_printer($select_printer);
echo $top_form->add_parameter('mode', $mode)->put();
$parent = $select_printer->get_value();

$goods = $model->get_all_of_external_parent($parent, null, 999999, true, true);


if ($creat_mode=='add' or ($creat_mode=='edit' and $id and
                           $line = mysql_line("select `external_parent`, `brand`, `hidden`, `rate`, `articul`, `code`, `price`, `action_price`, `code_discount`,
                                                      `code_bonuses`, `avail`, `best_sell`, `disable_reviews`, `video_review`, `static`".
                                              mysql_langs_select(array('caption','category','title','description','keywords','ship','pay','back','warranty'))."
                                               from `$tablename` where `id`='$id'")))
{
 if (empty($line[0])) $line[0] = $parent;
 $parent_putter = new select_tree_model_printer(new categories());
 
 $brands_model = new brands();
 $brand_putter = new select_simple_printer($brands_model->get_all());


 $analogs_array = array();
 $analogs = $model->get_analogs($id);
 foreach ($analogs as $analog) $analogs_array[] = $analog['id'];
 
 $add_analog_html = "<tr>
                      <td>
                       <select name='new_analog[]'><option value='0'>[Не добавлять]</option>";
 foreach ($goods as $analog)
 {
  if (!in_array($analog['id'], $analogs_array) and $analog['id'] != $id) $add_analog_html .= "<option value='{$analog['id']}'>{$analog['category']} {$analog['caption']}</option>";
 }
 
 $add_analog_html .= " </select>
                      </td>
                      <td style='text-align:center;'>-</td>
                     </tr>";
 
 $analogs_str = "<script>
                 var add_analog_html = '".safe($add_analog_html)."';
                </script>
                <table class='table_2' cellpadding='0' cellspacing='0' border='0' style='width:100%;' id='analogs-table'>
                <thead>
                 <tr>
                  <th>Товар</th>
                  <th style='width:100px;'>Операции</th>
                 </tr>
                </thead>
                <tbody>".$add_analog_html;

 foreach ($analogs as $analog)
 {
  $analogs_str .= "<tr>
                    <td>{$analog['caption']}</td>
                    <td style='text-align:center;'><input type='checkbox' name='delete_analog[]' value='{$analog['id']}' title='Удалить'></a></td>
                   </tr>";
 }
  

 $analogs_str .= "</tbody>
                 </table>";
 
 
 $params_str = "<table class='table_2' cellpadding='0' cellspacing='0' border='0' style='width:100%;' id='good-params-table'>
                <tr>
                 <th style='width: 30%;'>Параметр</th>
                 <th>Значение</th>
                </tr>";
 
 $categories_model = new categories();
 if (!empty($line[0]))
 {
  if ($creat_mode=='edit') $params = $model->get_params($id);
  else
  {
   $params = $categories_model->get_category_params($parent);
  }
 }
 else $params = array();
 
 
 foreach ($params as $param)
 {
  $name = 'param_'.$param['id'];
  
  $params_str .= "<tr>
                   <td>{$param['caption']}</td>
                   <td>";
  
  switch ($param['type'])
  {
   case 'char':
	$params_str .= "<input type='text' name='$name' value='".htmlspecialchars($param['value'])."' style='width: 95%;'>";
	break;
   
   case 'bool':
	$params_str .= "<input type='checkbox' name='$name' value='1'".($param['value'] ? ' checked' : '').">";
	break;
   
   case 'image':
	foreach ($param['images'] as $image)
	{
	 $image_id = 'param_'.$pimage['id'];
	 
	 $params_str .= "<div style='margin-bottom: 10px;'>
                      <label style='display: inline-block; text-align: center;' for='{$image_id}'>
		               <img src='/admin/image.php?file=project_images/{$image['image']}&amp;width=200&amp;height=200'>
	                   <div><input id='{$image_id}' type='radio' name='{$name}' value='{$image['id']}'".($param['value'] == $image['id'] ? ' checked' : '')."></div>
		              </label>
	                 </div>";
	}
	break;
  }
  
  $params_str .= "</td></tr>";
 }
 $params_str .= '</table>';
 
 $news_and_actions_goods_model = new news_and_actions_goods();
 $actions = $news_and_actions_goods_model->get_good_actions($id);
 $data = $news_and_actions_goods_model->get_not_good_actions($id);

 $add_action_html = "<tr>
                      <td>
                       <select name='new_action[]'><option value='0'>[Не добавлять]</option>";
 foreach ($data as $action)
 {
  $add_action_html .= "<option value='{$action['id']}'>{$action['category']} {$action['caption']}</option>";
 }


 $add_action_html .= " </select>
                      </td>
                      <td style='text-align:center;'>-</td>
                     </tr>";

 $actions_str = "<script>
                 var add_action_html = '".safe($add_action_html)."';
                </script>
                <table class='table_2' cellpadding='0' cellspacing='0' border='0' style='width:100%;' id='actions-table'>
                <thead>
                 <tr>
                  <th>Акция</th>
                  <th style='width:100px;'>Операции</th>
                 </tr>
                </thead>
                <tbody>".$add_action_html;

 foreach ($actions as $action)
 {
  $actions_str .= "<tr>
                    <td>{$action['caption']}</td>
                    <td style='text-align:center;'><input type='checkbox' name='delete_action[]' value='{$action['id']}' title='Удалить'></a></td>
                   </tr>";
 }


 $actions_str .= "</tbody>
                 </table>";
    
    
 $appendCategoriesPrinter = new type_head_multi_printer(new categories());
 $appendCategoriesPrinter->set_name('append_categories')->set_value($model->get_good_append_categories($id))->set_not_null(false);
    
    
    
    $filters_model = new category_filters();
    $filters_str = "<table class='table_2' cellpadding='0' cellspacing='0' border='0' style='width:100%;' id='good-filters-table'>
                    <tr>
                     <th style='width: 30%;'>Фильтр</th>
                     <th>Значение</th>
                    </tr>";

    if (!empty($line[0])) {
        $filters = $categories_model->get_category_filters($line[0]);
    } else {
        $filters = array();
    }

    foreach ($filters as $filter) {
        $filters_str .= "<tr>
                           <td>{$filter['caption']}</td>
                           <td>";
        $filter_values = $filters_model->getFilterItemsWithGoodValues($filter['id'], $id);
        foreach ($filter_values as $filter_value) {
            $checkbox = "<input type='checkbox' name='filter_{$filter_value['id']}' value='1'".($filter_value['value'] ? ' checked' : '')."> {$filter_value['caption']}";
            
            switch ($filter['type']) {
                case 'n':
                    $filters_str .= "<div><label>$checkbox</label></div>";
                    break;
                
                case 'i':
                    $filters_str .= "<div style='margin-bottom: 10px;'>
                                      <label style='display: inline-block; text-align: center;'>
                                       <img src='/admin/image.php?file=project_images/{$filter_value['image']}&amp;width=200&amp;height=200'>
                                       <div>$checkbox</div>
                                      </label>
                                     </div>";
                    break;
            }
            $filters_str .= "";
        }
        $filters_str .= "</td></tr>";
    }
    $filters_str .= '</table>';
    
    
    $videoModel = new good_videos();
    $videoViews = new type_head_printer($videoModel);
    $videoViews->set_name('video_review')->set_value($line[13])->set_not_null(false);
 
 
 echo put_main_form($line,
        array_merge(
                   array('Размещение'=>array($parent_putter->set_name('external_parent')->set_not_null(FALSE)->set_null_caption('Не задана')->set_value($line[0] ? $line[0] : $parent)->put()),
                         'Бренд'=>array($brand_putter->set_name('brand')->set_not_null(FALSE)->set_null_caption('Не задан')->set_value($line[1])->put()),
                         'Скрывать'=>array(null,'checkbox','hidden'),
                         'Приоритет'=>array(null,'text','rate',0,null,'150px'),
                         'Артикул'=>array(null,'text','articul',0,null,'150px'),
                         'Код'=>array(null,'text','code',0,null,'150px'),
                         'Цена'=>array(null,'text','price',0,null,'150px'),
                         'Акционная цена'=>array(null,'text','action_price',0,null,'150px'),
                         'Скида по сертификату (%)'=>array(null,'text','code_discount',0,null,'150px'),
                         'Количество начисляемых бонусов (%)'=>array(null,'text','code_bonuses',0,null,'150px'),
                         'На складе'=>array(null,'checkbox','avail'),
                         'Бестселлер'=>array(null,'checkbox','best_sell'),
                         'Отключить отзывы'=>array(null,'checkbox','disable_reviews'),
                         'Привязать готовый видеообзор'=>array($videoViews->put()),
                         'Статический адрес'=>array(null,'text','static',1,null,null,null,'readonly')),
                    main_form_fields_array($lang_array, array('Название'=>array('text','caption'), 'Название категории'=>array('text','category'), 'Title'=>array('text','title'),
                                                              'Description'=>array('text','description'), 'Keywords'=>array('text','keywords'),
                                                              'Условия доставки'=>array('text','ship'), 'Способы оплаты'=>array('text','pay'),
							                                  'Возврат'=>array('text','back'), 'Гарантия'=>array('text','warranty')),
                                          array('caption_'.$prime_lang=>array(1,null,null,null,"onkeyup=\"make_static('$mode','$id', $(this).parents('form').find('[name=category_{$prime_lang}]').val() + '-' + $(this).parents('form').find('[name=caption_{$prime_lang}]').val(), $(this).parents('form').find('[name=external_parent]').val()); \""),
											    'category_'.$prime_lang=>array(1,null,null,null,"onkeyup=\"make_static('$mode','$id', $(this).parents('form').find('[name=category_{$prime_lang}]').val() + '-' + $(this).parents('form').find('[name=caption_{$prime_lang}]').val(), $(this).parents('form').find('[name=external_parent]').val()); \""))
					),
                    project_images($mode, $id),
		            array(
					 'Аналоги'=>array($analogs_str),
					 'Параметры'=>array($params_str),
					 'Участие в акциях'=>array($actions_str),
                     'Участие в программах лояльности' => array(programsCoverageOnItemPage($model, $id)),
                     'Дополнительные категории' => array($appendCategoriesPrinter->put()),
                     'Фильтры' => array($filters_str),
					)
                       ), "&mode=$mode&parent=$parent", lang_selector());
}

print_button('Добавить',"location.href='admin.php?mode=$mode&creat_mode=add&parent=$parent'");
print_table_header(array('Изображение','Товар','Скрывать','На складе','Цена','Бестселлер ','Приоритет','Операции'),array(120,0,100,100,100,100,100,100));

foreach ($goods as $index => $line)
{
 echo "<tr".tr_class($index).">
        <td style='text-align:center;'>".imageexist($line['image'],100,200)."</td>
        <td>
         <div>{$line['category']} <strong>{$line['caption']}</strong></div>
         <div>{$line['brand_caption']}</div>
        </td>
        <td style='text-align:center'>" . strip_tags($bool_captions[$line['hidden']], true) . "</td>
        <td style='text-align:center'>{$bool_captions[$line['avail']]}</td>
        <td style='text-align:center'>".price_format($line['price'])."</td>
        <td style='text-align:center'>{$bool_captions[$line['best_sell']]}</td>
        <td style='text-align:center'>{$line['rate']}</td>
        <td style='text-align:center'>".put_edit_buttons($line['id'],'товар',"&mode=$mode&parent=$parent")."</td>
       </tr>";
}
print_table_bottom();
?>
