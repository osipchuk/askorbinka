 function check_static(s_value,s_mode,id,father)
 {
  var req;
  var result;
  if (window.XMLHttpRequest) req = new XMLHttpRequest();
  else if (window.ActiveXObject) {
      try {
          req = new ActiveXObject('Msxml2.XMLHTTP');
      } catch (e){}
      try {
      req = new ActiveXObject('Microsoft.XMLHTTP');
      } catch (e){}
  }
 
  if (req) {
      req.onreadystatechange = function() {
          if (req.readyState == 4 && req.status == 200)
           {
            if (req.responseText=='ok')
             {
              document.getElementById('main_submit').disabled=false;
              document.getElementById('main_submit').style.color='#5e5e5e';
              //alert(req.responseText);
             }
            else
             {
              document.getElementById('main_submit').disabled=true;
              document.getElementById('main_submit').style.color='#8F8F8F';
              //alert(req.responseText);
             }
           }
      };
      
      if (s_value!='' && s_mode!='')
       {
        req.open("POST", 'check_static_ajax.php', true);
        req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        req.send("s_value="+s_value+"&s_mode="+s_mode+"&id="+id+"&father="+father);
       }
  }
  else alert("Браузер не поддерживает AJAX");
 }
