<?php
require_once 'header.php';

if (!$mode) die('modes error');
$parent = get('gi', 'parent');
if ($parent <= 0) die('parent error');

/** @var category_filters $model */
$model = new $mode();
$tablename = $model->get_table_name();

if ($creat_mode == 'add' or ($creat_mode == 'edit' and $id)) {
    $set = mysql_langs_set(['caption']) .
        fill_query([
            'i' => ['rate'],
            'q' => ['type'],
        ]);

    if ($id = standart_edit($creat_mode, $tablename, $set, ", `external_parent`='$parent'", $id)) {

        $values_printer = new files_table_printer($model->getFilterItems($id));
        if (get('pq', 'type') == 'i') {
            $values_printer->add_file('image', 'Изображение', TRUE);
        }
        $values_printer->set_has_caption(true)->update($id, 'category_filter_values');
    }

    redirect("admin.php?mode=$mode&parent=$parent&creat_mode=edit&id=$id");
} elseif ($creat_mode == 'delete' and $id) {
    $deleter = new category_filters_deleter($model);
    $deleter->delete($id);
} elseif ($creat_mode == 'delete_filters_from_cat') {
    $categories_model = new categories();
    mysql_update($categories_model->get_table_name(), "`filters_from_cat`=null", "`id`='$parent'");
} elseif ($creat_mode == 'add_filters_from_cat') {
    $categories_model = new categories();
    $filters_from_cat = get('pi', 'filters_from_cat');
    if ($filters_from_cat != $parent) {
        mysql_update($categories_model->get_table_name(), "`filters_from_cat`='$filters_from_cat'", "`id`='$parent'");
    }
}

redirect("admin.php?mode=$mode&parent=$parent");

?>
