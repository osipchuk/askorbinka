<?php
/**
 * @package default
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 24.03.15
 */

require_once 'header.php';

$client = (new clients())->get_item("`id`='" . get('pi', 'client') . "'");
if (!empty($client)) {
    $result = [
        'status' => 'ok',
        'client' => $client,
    ];
} else {
    $result = [
        'status' => 'error',
        'message' => 'Неизвестная ошибка',
    ];
}

echo json($result);
