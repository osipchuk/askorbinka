<?php
function programsCoverageOnItemPage(a_model $model, $itemId)
{
 $programsModel = new programs();
 $programsModel->ignore_page_num(true);
 $mainModelClass = get_class($model);
 
 switch ($mainModelClass)
 {
  case 'brands':
   $itemColumn = 'brand';
   $itemCaption = 'Бренд';
   break;
  
  case 'categories':
   $itemColumn = 'category';
   $itemCaption = 'Категория';
   break;
  
  case 'goods':
   $itemColumn = 'good';
   $itemCaption = 'Товар';
   break;
  
  default:
   $itemColumn = '';
   $itemCaption = '';
 }
 
 $dataTable = constant('programsCoverage::' . strtoupper($mainModelClass . '_DATA_TABLE'));
 if (empty($dataTable)) return '-';
 
 $itemId = (int) $itemId;
 $delete_available_data = $programsModel->index_by_field(
     $programsModel->get_all("`{$programsModel->get_table_name()}`.`id` in (select `program` from `$dataTable` where `$itemColumn`='$itemId')"));
 
 if ($mainModelClass == 'goods')
 {
  $data = $programsModel->get_all("`{$programsModel->get_table_name()}`.`id` in (select `program` from `" . programsCoverageIndex::TABLE_NAME . "` where `$itemColumn`='$itemId')");
 }
 else
 {
  $data = $delete_available_data;
 }
 
 
 if (empty($data)) return $itemCaption . ' не участвует в программах лояльности';

 $result = '<table class="table2" cellpadding="0" cellspacing="0" style="width: 100%;">
             <tr>
             <th>Прорамма лояльности</th>
             <th style="width: 100px;">Операции</th>
             </tr>';
 
 foreach ($data as $line)
 {
  $result .= "<tr>
               <td><a href='admin.php?mode=programs&creat_mode=edit&id={$line['id']}' title='Посмотреть' target='_blank'>{$line['caption']}</a></td>
               <td style='text-align: center;'>" .
                (array_key_exists($line['id'], $delete_available_data) ? put_edit_buttons($itemId, 'запись', '&mode=item&delete_item=' . $line['id'] . '&delete_coverage=' . $mainModelClass, 'programs', true) : '-') .
               "</td>
              </tr>";
 }
 
 $result .= '</table>';
 
 return $result;
}

function youtube_video_preview($youtube_id, $image = '')
{
 if (empty($youtube_id)) return null;
 if (!empty($image))
 {
  return '/admin/image.php?file=project_images/' . $image . '&amp;width=480&amp;height=360';
 }
 
 return 'http://img.youtube.com/vi/'.$youtube_id.'/0.jpg';
}

function put_youtube_video($youtube_id, $width = 560, $height = 315)
{
 if (!empty($youtube_id)) $result = '<iframe width="'.$width.'" height="'.$height.'" src="//www.youtube.com/embed/'.$youtube_id.'" frameborder="0" allowfullscreen></iframe>';
 else $result = '';

 return $result;
}

/**
 * Генерирует 
 * @param a_model $model
 * @param $id
 * @return string
 */
function catalog_links_checkboxes(a_model $model, $id)
{
 $links_model = new catalog_links();
 $link_categories_class = $links_model->external_model();
 /** @var a_model $link_categories_model */
 $link_categories_model = new $link_categories_class;
 $checked = $links_model->get_ids_links_of_item($model, $id);
 
 $categories = $link_categories_model->get_all("`{$link_categories_model->get_table_name()}`.`id` in (select `external_parent` from `{$links_model->get_table_name()}`)");
 if (empty($categories)) return '-';
 
 $result = '';
 foreach ($categories as $category)
 {
  $result .= '<div style="clear: both; padding: 10px 0 5px; font-weight: bold;">'.$category['caption'].'</div>
              <ul class="checkboxes">';
  foreach ($links_model->get_all_of_external_parent($category['id']) as $link)
  {
   $result .= '<li><label><input type="checkbox" name="catalog_links_'.$link['id'].'"'.(in_array($link['id'], $checked) ? ' checked' : '').'>'.$link['caption'].'</label></li>';
  }
  
  $result .= '</ul>';
 }
 
 return $result;
}



function script_messages($words_array)
{
 global $words;
 
 $result='<script>
           var ';
 foreach ($words_array as $word_key) $result.=$word_key.' = "'.htmlspecialchars($words[$word_key]).'"'.($word_key==end($words_array) ? ';' : ', ');
 $result.='</script>';
 
 return $result;
}

function social_share($subject='', $item_type='catalog')
{
 global $words;
 global $modes_array;
 global $base;
 
 $page_url=$base.mb_substr(url(implode('/', $modes_array)), 1);
 
 $result='<nav class="social">
           <h4>'.$words[$item_type.'_social_share_h4'].'</h4>
           <ul>';
 
 $social_array=array('vk'=>array('http://vkontakte.ru/share.php?url='.$page_url.'&title='.$subject, 1),
                     'fb'=>array('http://www.facebook.com/sharer.php?u='.$page_url.'&t='.$subject, 1),
                     'tw'=>array('https://twitter.com/intent/tweet?url='.$page_url.'&text='.$subject, 1));
 
 foreach ($social_array as $class => $social_item)
 {
  $result.='<li class="'.$class.'"><a href="'.$social_item[0].'"'.(!$social_item[1] ? 'target="_blank"' : 'onclick="return !window.open(this.href, \'\', \'toolbar=no,location=no,left=50%,top=50%,directories=no,status=yes,menubar=no,scrollbars=no,resizable=yes,width=600,height=400\');"').' rel="nofollow"></a></li>';
 }
 
 $result.='</ul>
          </nav>';
 
 return $result;
}

function put_dl_pair($dt='', $dd='')
{
 if ($dt and $dd) $result='<dt>'.$dt.'</dt><dd>'.$dd.'</dd>'; else $result='';
 
 return $result;
}

function alphabetic_date($time, $show_year=false)
{
 $timestamp=strtotime($time);
 global $words;
 
 return date("d", $timestamp).' '.$words->_w('month_', date("n", $timestamp), '_1').($show_year ? ' '.date("Y", $timestamp) : '');
}

function page_caption($caption)
{
 global $words;
 
 return '<a href="#">'.$words['h1_start_href'].'</a><span>/</span>'.$caption;
}

function put_banners($banners, $block)
{
 $result='';
 if ($banners[$block]) foreach ($banners[$block] as $banner) $result.=put_banner($banner);
 
 return $result;
}



function get_manual_image_size($image, $new_width, $new_height, $cut=0)
{
 if ($imagesize=getimagesize($image))
 {
  if ($new_width and $new_height)
  {
   $width=$imagesize[0];
   $height=$imagesize[1];
   
   $ratio=$width/$height;
   $new_ratio=$new_width/$new_height;
    
   if ($cut)
   {
    $src_x=0;
    $src_y=0;



    if ($new_ratio>$ratio)
    {
     $original_height=$height;
     $height=$new_height/($new_width/$width);
     $src_x=0;
     switch($cut)
     {
      case 1: $src_y=0; break;
      case 2: $src_y=($original_height-$height)/2; break;
      case 3: $src_y=$original_height-$height; break;
     }
    }
    else
    {
     $original_width=$width;
     $width=$new_width/($new_height/$height);
     $src_y=0;
     switch($cut)
     {
      case 1: $src_x=0; break;
      case 2: $src_x=($original_width-$width)/2; break;
      case 3: $src_x=$original_width-$width; break;
     }
    }

   }
   else
   {
    $src_x=0;
    $src_y=0;

    if ($ratio<$new_ratio)
    {
     $new_width=round($new_height*$ratio);
     if (!$new_width) $new_width=1;
    }
    elseif ($ratio>$new_ratio)
    {
     $new_height=round($new_width/$ratio);
     if (!$new_height) $new_height=1;
    }
   }
   $result=array($new_width, $new_height);
  }
  else $result=$imagesize;
 }
 
 return $result;
}


function make_url($url)
{
 if ($url and strpos($url,'http')===false) $url='http://'.$url;
 if ($url) return $url; else return null;
}




function get_page_num($modes_array)
{
 if (preg_match("/^page-([\d]+)$/",end($modes_array),$page)) $page=$page[1]; else $page=0;
 return $page;
}




function print_title_description_keywords()
 {
  $line=filter_html(mysql_line("select `title`, `description`, `keywords` from `settings`"));
  if (!mysql_error())
   {
    global $modes_array;
    global $lang;
    global $words;
    global $main_nav;
    global $item;
    global $text;
    global $service;
    
    /*
    if ($item) $description_keywords=array_slice($item, -3);
    
    if (!$description_keywords[2])
    {
     if ($text) $description_keywords[2]=$item[1].($service ? ' - '.$words['main_nav_services'] : '').' - '.$line[0];
     if (in_array($modes_array[0], array('info','gallery')))
     {
      $description_keywords[2]= ($item ? $item[1].' - ' : '').$words['main_nav_'.$modes_array[0]].' - '.$line[0];
     }
     else $description_keywords[2]=($words['main_nav_'.$modes_array[0]] ? $words['main_nav_'.$modes_array[0]].' - ' : ($words[$modes_array[0].'_caption'] ? $words[$modes_array[0].'_caption'].' - ' : '')).$line[0];
    }
    */
    
    if (!($description=$description_keywords[0])) $description=$line[1];
    if (!($keywords=$description_keywords[1])) $keywords=$line[2];
    if (!($title=$description_keywords[2])) $title=($description_keywords[3] ? $description_keywords[3].' - ' : '').$line[0];
    
    
    if ($page=get_page_num($modes_array)) $title=$words['page_num'].' '.($page+1).' - '.$title;
    
    if ($title) echo "<title>$title</title>";
    if ($description) echo "<meta name=\"Description\" content=\"$description\">";
    if ($keywords) echo "<meta name=\"Keywords\" content=\"$keywords\">";
    return true;
   }
  else return false;
 }

function print_caption($caption, $h = 1)
 {
  if ($caption)
  {
   $result='<div class="page-header"><h'.$h.'>'.$caption.'</h'.$h.'></div>';
  }
  else $result='';
  
  return $result;
 }

function print_pages($current_page,$pages_count,$href,$chpu=true)
{
 function put_href($i, $current_page, $href, $chpu )
 {
  //return '<a href="'.$href.($i ? ($chpu ? '/page-'.$i : "&page=$i") : '').'"'.($i==$current_page ? ' class="current"' : '').'>'.($i+1).'</a>';
  $i=round($i);
  return '<a href="'.$href.($chpu ? ($i ? '/page-'.$i : ($href ? '' : '/')) : "&page=$i").'"'.($i==$current_page ? ' class="current"' : '').'>'.($i+1).'</a>';
 }
 
 if ($pages_count>1)
 {
  $result='<div style="clear:both;"></div><nav class="pages">';
  
  if ($pages_count<12)
  {
   for ($i=0;$i<$pages_count;$i++) $result.=put_href($i, $current_page, $href, $chpu);
  }
  else
  {
   for ($i=0;$i<5;$i++) $result.=put_href($i, $current_page, $href, $chpu);
   
   if ($pages_count-$current_page>4)
   {
    $i=$current_page-2;
    if ($i<5) $i=5; else $result.='<span>...</span>';
    $to=$current_page+3;
    if ($to>$pages_count) $to=$pages_count;
    for ($i;$i<$to;$i++) $result.=put_href($i, $current_page, $href, $chpu);
   }
   
   
   $from=$pages_count-5;
   if ($i>$from) $from=$i; else $result.='<span>...</span>';
   for ($i=$from;$i<$pages_count;$i++) $result.=put_href($i, $current_page, $href, $chpu);
  }
  
  
  $result.='</nav>';
 }
 else $result='';

 return $result;
}

function put_flash_object($dir,$file,$onclick='')
{
 $flesh_size=getimagesize($dir.$file);
 if ($onclick) $onclick=' onclick="'.$onclick.'"';
 
 return "<object type=\"application/x-shockwave-flash\" data=\"{$dir}{$file}\" width=\"{$flesh_size[0]}\" height=\"{$flesh_size[1]}\"$onclick>
                   <param name=\"movie\" value=\"{$dir}{$file}\" />
                   <param name=\"quality\" value=\"high\" />
                   <param name=\"menu\" value=\"true\" />
                   <param name=\"wmode\" value=\"opaque\" />
                   <param name=\"flashvars\" value=\"shop_id=101\">
                   No flash
                  </object>";
}
 
function put_banner($id,$admin=false)
 {
  if (!$admin) $dir='/admin/project_images/'; else $dir='project_images/';
  $result='';
  $banner_type=mysql_line("select `type`, `block_id` from `banners` where `id`='$id'");
  switch($banner_type[0])
   {
    case 'c':
     $banner_line=mysql_line("select `code` from `banners_code` where `banner_id`='$id'");
     $result=htmlspecialchars_decode($banner_line[0]);
     break;
    
    case 'p':
     $banner_line=filter_html(mysql_line("select `caption`, `href`, `image` from `banners_pict` where `banner_id`='$id'"));
     if ($banner_line[2] and ($ext=strtolower(file_extension($banner_line[2])))!='swf') 
      {
       //if ($banner_type[1]!=4) $width=" width='195'"; else $width='';
       $width='';
       $result="<img src=\"{$dir}{$banner_line[2]}\"$width title=\"{$banner_line[0]}\">";
      }
     elseif ($ext=='swf')
      {
        $result='<div style="cursor:pointer;">'.put_flash_object($dir,$banner_line[2]).'</div>';
      }
     if ($ext!='swf' and $href=makeurl($banner_line[1]) and $href!='#') $result='<a href="'.$href.'" target="blank">'.$result.'</a>';
     break;
    }
    
  return $result;
 }

function getLastNumber($filename,&$filenamewithotlasnum)
 {
  $i=strlen($filename)-1;
  $result='';
  while((int)($filename[$i]) or ($filename[$i])=='0') $result=$filename[$i--].$result;
  
  $filenamewithotlasnum=substr($filename,0,$i+1);
  
  return (int)$result;
 }

function ukr_date()
{
 switch (date("n"))
 {
  case '1': $month='січня'; break;
  case '2': $month='лютого'; break;
  case '3': $month='березня'; break;
  case '4': $month='квітня'; break;
  case '5': $month='травня'; break;
  case '6': $month='червня'; break;
  case '7': $month='липня'; break;
  case '8': $month='серпня'; break;
  case '9': $month='вересня'; break;
  case '10': $month='жовтня'; break;
  case '11': $month='листопада'; break;
  case '12': $month='грудня'; break;
 }
 return date("j").' '.$month.' '.date("Y").' р.';
}

function write_sum($price,$currency)
{
 $price=explode('.',$price);
 $str=strrev((string)$price[0]);
 for($i=0;$i<strlen($str);$i++)
 {
  switch ($i)
  {
   case '0':
    switch ($str[$i])
    {
     case '0': $r=''; break;
     case '1': $r=''; break;
     case '2': $r=''; break;
     case '3': $r=''; break;
     case '4': $r=''; break;
     case '5': $r=''; break;
     case '6': $r=''; break;
     case '7': $r=''; break;
     case '8': $r=''; break;
     case '9': $r=''; break;
    }
  }
 }

 return ''; 'Одна тисяча сімсот грн. 00 коп.';
}


function num2str($inn, $currency='UAH', $stripkop=false) {
 //echo $currency;
 //$currency='UAH';
 $nol = 'нуль';
 $str[100]= array('','сто','двісті','триста','чотириста','п\'ятсот','шістсот', 'сімсот', 'вісімсот','дев\'ятсот');
 $str[11] = array('','десять','одинадцять','дванадцать','тринадцать', 'чотирнадцять','пя\'тнадцять','шістнадцять','сімнадцять', 'вісімнадцять','дев\'ятнадцять','двадцять');
 $str[10] = array('','десять','двадцять','тридцять','сорок','п\'ятдесят', 'шістдесят','сімдесят','вісімдесят','дев\'яносто');
 $sex = array(
  array('','один','дві','три','чотири','п\'ять','шість','сім', 'вісім','дев\'ять'),// m FIXED!
  array('','одна','дві','три','чотире','п\'ять','шість','сім', 'вісім','дев\'ять') // f
 );
 $forms = array(
  //array('копійка', 'копійки', 'копійок', 1), // 10^-2
  array('UAH'=>array('копійка', 'копійки', 'копійок'),'USD'=>array('цент','центи','центів'),'EUR'=>array('євро цент','євро центи','євро центів'), 1),
  //array('рубль', 'рубля', 'рублей',  0), // 10^ 0
  //forms[1]
  array('UAH'=>array('гривня','гривні','гривень'),'USD'=>array('долар США','долари США','доларів США'),'EUR'=>array('євро','євро','євро'), 0),
  array('тисяча', 'тисячі', 'тисяч', 1), // 10^ 3
  array('міліон', 'міліона', 'міліонів',  0), // 10^ 6
  array('міліард', 'міліарда', 'міліардів',  0), // 10^ 9
  array('триліон', 'триліона', 'триліонів',  0), // 10^12
 );
 $out = $tmp = array();
 // Поехали!
 $tmp = explode('.', str_replace(',','.', $inn));
 $rub = number_format($tmp[ 0], 0,'','-');
 if ($rub== 0) $out[] = $nol;
 // нормализация копеек
 $kop = isset($tmp[1]) ? substr(str_pad($tmp[1], 2, '0', STR_PAD_RIGHT), 0,2) : '00';
 $segments = explode('-', $rub);
 $offset = sizeof($segments);
 if ((int)$rub== 0) { // если 0 рублей
  $o[] = $nol;
  $o[] = morph( 0, $forms[1][$currency][0],$forms[1][$currency][1],$forms[1][$currency][2]);
  //$o[] = morph( 0, $forms[1][0],$forms[1][1],$forms[1][2]);
 }
 else {
  foreach ($segments as $k=>$lev) {
   $sexi= (int) $forms[$offset][3]; // определяем род
   $ri = (int) $lev; // текущий сегмент
   if ($ri== 0 && $offset>1) {// если сегмент==0 & не последний уровень(там Units)
    $offset--;
    continue;
   }
   // нормализация
   $ri = str_pad($ri, 3, '0', STR_PAD_LEFT);
   // получаем циферки для анализа
   $r1 = (int)substr($ri, 0,1); //первая цифра
   $r2 = (int)substr($ri,1,1); //вторая
   $r3 = (int)substr($ri,2,1); //третья
   $r22= (int)$r2.$r3; //вторая и третья
   // разгребаем порядки
   if ($ri>99) $o[] = $str[100][$r1]; // Сотни
   if ($r22>20) {// >20
    $o[] = $str[10][$r2];
    $o[] = $sex[ $sexi ][$r3];
   }
   else { // <=20
    if ($r22>9) $o[] = $str[11][$r22-9]; // 10-20
    elseif($r22> 0) $o[] = $sex[ $sexi ][$r3]; // 1-9
   }
   // Рубли
   if ($offset==1)
    $o[] = morph($ri, $forms[$offset][$currency][ 0],$forms[$offset][$currency][1],$forms[$offset][$currency][2]);
   else $o[] = morph($ri, $forms[$offset][ 0],$forms[$offset][1],$forms[$offset][2]);
   $offset--;
  }
 }
 // Копейки
 if (!$stripkop) {
  $o[] = $kop;
  $o[] = morph($kop,$forms[ 0][$currency][ 0],$forms[ 0][$currency][1],$forms[ 0][$currency][2]);
 }
 return preg_replace("/\s{2,}/",' ',implode(' ',$o));
}

/**
 * Склоняем словоформу
 */
function morph($n, $f1, $f2, $f5) {
 $n = abs($n) % 100;
 $n1= $n % 10;
 if ($n>10 && $n<20) return $f5;
 if ($n1>1 && $n1<5) return $f2;
 if ($n1==1) return $f1;
 return $f5;
}


function delete_query_image($tablename, $where, $fileid='image', $mysql_column='image', $dir='project_images')
{
 if (get('pi','delete_image_'.$fileid))
 {
  delete_image($tablename, $where, true, $mysql_column, $dir.'/');
 }
}

function copy_image($image_url, $dir='project_images')
{
 $ext=file_extension($image_url);
 $file_name=md5($image_url);

 while (file_exists("$dir/$file_name.$ext")) $file_name=md5($file_name);

 if (@copy($image_url, $result=$dir.'/'.$file_name.'.'.$ext))
 {
  chmod($result, 0666);
  return $file_name.'.'.$ext;
 }
 else return '';
}
?>