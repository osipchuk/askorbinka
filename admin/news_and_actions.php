<?php
/** @var a_model $model */
$model = new $mode();
$tablename = $model->get_table_name();

$parent_factory = new parent_select_factory();
$select_printer = $parent_factory->get_select($model, false);

$top_form = new top_form_printer($select_printer);
echo $top_form->add_parameter('mode', $mode)->put();
$parent = $select_printer->get_value();
$data = $model->get_all_of_external_parent($parent);


if ($creat_mode=='add' or ($creat_mode=='edit' and $id and
    $line = mysql_line("select `external_parent`, `date_begin`, `date_end`, `discount`, `action`, `new`, `image`, `image_label`, `image_label_small`, `double_label`, `url`, `static`".mysql_langs_select(array('caption','label_text','title','description','keywords','preview','text'))."
                                              from `$tablename` where `id`='$id'")))
{
 $parent_putter = new select_tree_model_printer(new categories());
 
 echo put_main_form($line,
     array_merge(
         array('Размещение'=>array($parent_putter->set_name('external_parent')->set_value($line[0] ? $line[0] : $parent)->put()),
               'Дата начала'=>array(null,'date','date_begin',1),
               'Дата окончания'=>array(null,'date','date_end',1),
		       'Акция'=>array(null,'checkbox','action'),
		       'Новинка'=>array(null,'checkbox','new'),
               'Изображение'=>array(null,'image[200][300]','image'),
               'Изображение для лейблы'=>array(null,'image[100][100]','image_label'),
               'Маленькое изображение для лейблы'=>array(null,'image[50][50]','image_label_small'),
			   'Лейбл в две строки'=>array(null,'checkbox','double_label'),
               'Url'=>array(null,'text','url'),
		  
               'Статический адрес'=>array(null,'text','static',1,null,null,null,'readonly')),
         main_form_fields_array($lang_array, array('Заголовок'=>array('text','caption'), 'Текст на лейбле'=>array('text','label_text'), 'Title'=>array('text','title'),
                 'Description'=>array('text','description'), 'Keywords'=>array('text','keywords'),
			     'Превью'=>array('tinymce','preview'), 'Текст'=>array('tinymce','text') ),
             array('caption_'.$prime_lang=>array(1,null,null,null,"onkeyup=\"make_static('$mode','$id', this.value); \"")))
	  
     ), "&mode=$mode&parent=$parent", lang_selector());

 require_once 'images.php';
}


print_button('Добавить',"location.href='admin.php?mode=$mode&creat_mode=add&parent=$parent'");
print_table_header(array('Изображение','Заголовок/текст','Даты проведения','Операции'),array(120,0,200,100,100));

foreach ($data as $index => $line)
{
 echo "<tr".tr_class($index).">
        <td style='text-align:center;'>".imageexist($line['image'],100,200)."</td>
        <td>
         <div class='article_caption'>{$line['caption']}</div>
         <div class='article_text'>{$line['preview']}</div>
        </td>
        <td style='text-align:center'>{$line['date_begin']} - {$line['date_end']}</td>
        <td style='text-align:center'>".put_edit_buttons($line['id'],'новость',"&mode=$mode&parent=$parent")."</td>
       </tr>";
}
print_table_bottom();
?>
