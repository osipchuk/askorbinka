<?php
/** @var a_model $model */
$model = new $mode();
$tablename = $model->get_table_name();

$parent_factory = new parent_select_factory();
$select_printer = $parent_factory->get_select($model, false);

$top_form = new top_form_printer($select_printer);
echo $top_form->add_parameter('mode', $mode)->put();
$parent = $select_printer->get_value();
$data = $model->get_all_of_external_parent($parent);


if ($creat_mode=='add')
{
 $parent_putter = new select_tree_model_printer(new categories());
 
 echo put_main_form([],
     array_merge(
         array('Изменить на'=>array(null,'text','value',1,null,'150px'),
			   'Комментарий' => array(null, 'text', 'comment')
		 )
	  
     ), "&mode=$mode&parent=$parent");
}


print_button('Изменить баланс',"location.href='admin.php?mode=$mode&creat_mode=add&parent=$parent'");
print_table_header(array('ID транзакции','Дата','Сумма','Бонусный счет после применения операции','Комментарий','ID заказа'),array(200,200,0,0,0,200));

foreach ($data as $index => $line)
{
 echo "<tr" . tr_class($index) . " style='color: " . ($line['value'] > 0 ? '#0a0' : '#a00') . ";''>
        <td style='text-align:center;'>{$line['id']}</td>
        <td style='text-align:center;'>" . normal_date($line['add_time'], true) . "</td>
        <td style='text-align:center;'>{$line['value']}</td>
        <td style='text-align:center;'>{$line['ballance_after']}</td>
        <td style='text-align:center;'>" . ($line['comment'] ?: '-') . "</td>
        <td style='text-align:center;'>" . 
	     ($line['order_id'] ? "<a href='admin.php?mode=orders&creat_mode=edit&id={$line['order_id']}' target='_blank'>{$line['order_id']}</a>" : '-') . 
	   "</td>
       </tr>";
}
print_table_bottom();
?>
