<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 24.04.14
 */

$structure = new structure();
$table_name = $structure->get_table_name();

/*
$select_putter = new top_select_tree_model_printer($structure);
echo $select_putter->add_parameter('mode', $mode)->set_name('parent')->put();
$parent = $select_putter->get_value();
if (empty($parent)) die('parent error');
*/

if ($creat_mode=='add' or ($creat_mode=='edit' and $id and
        $line = mysql_line("select `parent`, `module`, `rate`, `hidden`, `contacts`, `start`, `block_class`, `static`".mysql_langs_select(array('caption','title','description','keywords','text'))." from `$table_name` where `id`='$id'")))
{
 $parent_putter = new select_tree_model_printer($structure);
 $module_putter = new select_simple_printer($standard_modules + ($creat_mode == 'edit' ? $extended_modules : array()));
 if ($creat_mode == 'edit') $parent_putter->add_ignored_item($id);
 
 
 echo put_main_form($line,
     array_merge(
         array('Размещение'=>array($parent_putter->set_name('parent')->set_value(get('gi','parent', $line[0]))->set_not_null(FALSE)->put()),
               'Модуль'=>array($module_putter->set_name('module')->set_disabled($creat_mode == 'edit')->set_value($line[1])->put()),
               'Приоритет'=>array(null,'text','rate',0,null,'150px'),
               'Скрывать в главном меню'=>array(null,'checkbox','hidden'),
               'Контакты'=>array(null,'checkbox','contacts'),
               'Стартовая страница'=>array(null,'checkbox','start'),
               'Класс основного блока'=>array(null,'text','block_class'),
               'Статический адрес'=>array(null,'text','static',1,null,null,null,'readonly')),
         main_form_fields_array($lang_array, array('Заголовок'=>array('text','caption'), 'Title'=>array('text','title'),'Description'=>array('text','description'),
                 'Keywords'=>array('text','keywords'), 'Текст'=>array('tinymce','text')),
             array('caption_'.$prime_lang=>array(1,null,null,null,"onkeyup=\"make_static('$mode','$id',this.value,document.main.parent.value); \"")))
     ), "&mode=$mode", lang_selector());

 require_once 'images.php';
}



print_button('Добавить',"location.href='admin.php?mode=$mode&creat_mode=add'");
$finder = new site_structure_ext_modules_finder();
$main_putter = new site_structure_tree_model_printer($structure);
echo $main_putter->set_ext_modules_finder($finder)->set_base_delete_href("{$mode}_edit.php?mode=$mode&creat_mode=delete")->put();

?> 