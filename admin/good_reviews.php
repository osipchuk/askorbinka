<?php
/** @var a_model $model */
$model = new $mode();
$tablename = $model->get_table_name();

$filter_array = array(0=>'Все', -1=>'Необработанные', 1=>'Обработанные');
$select_printer = new select_simple_printer($filter_array);
$select_printer->set_name('filter');
if (get('gi','filter')) $select_printer->set_value(get('gi','filter'));
$top_form = new top_form_printer($select_printer);
echo $top_form->add_parameter('mode',$mode)->put();

$filter = $select_printer->get_value();
$data = $model->get_all(!empty($filter) ? ($filter == 1 ? "`$tablename`.`submited`='1' or (`reply` is not null and `reply`<>'')" : "`$tablename`.`submited`='0' and (`reply` is null or `reply`='')") : '');
echo mysql_error();

if ($creat_mode=='edit' and $id and
    $line = mysql_line("select `external_parent`, `client`, `date`, `submited`, `rating`, `text`, `reply` from `$tablename` where `id`='$id'"))
{
 try {
  $parent_factory = new parent_select_factory();
  $parent_putter = $parent_factory->get_select($model, FALSE);
  $parent_select = $parent_putter->set_name('external_parent')->set_value($line[0] ? $line[0] : $parent)->put();
 }
 catch (Exception $e)
 {
  $parent_select = '-';
 }
 
 
 $clients_model = new clients();
 $clients_putter = new select_simple_printer($clients_model->get_all());
 
 echo put_main_form($line,
     array_merge(
         array(
			 'Товар'=>array($parent_select),
			 'Автор'=>array($clients_putter->set_name('client')->set_value($line[1] ? $line[1] : NULL)->put()),
			 'Дата и время'=>array(null,'text','date',1,null,'150px'),
			 'Подтвержден'=>array(null,'checkbox','submited'),
			 'Оценка'=>array(null,'number','rating',null,null,'150px'),
			 'Отзыв'=>array(null,'textarea','text',1),
			 'Ответ администратора'=>array(null,'tinymce','reply')
		 )
		 
     ), "&mode=$mode&parent=$parent&filter=$filter", '', "good_reviews_edit.php");

 require_once 'images.php';
}


print_table_header(array('Товар/Автор/Отзыв','Оценка товара','Ответ','Дата','Подтвержден','Операции'),array(0,100,0,100,100,100,100));

foreach ($data as $index => $line)
{
 echo "<tr".tr_class($index).">
        <td>
         <div>{$line['good_caption']}</div>
         <div class='article_caption'>".(!empty($line['author_fio']) ?  $line['author_fio'] : 'Пользователь удален')."</div>
         <div class='article_text'>".put_user_content($line['text'])."</div>
        </td>
        <td style='text-align:center'>{$line['rating']}"."</td>
        <td>".put_content($line['reply'])."</td>
        <td style='text-align:center'>".normal_date($line['date'])."</td>
        <td style='text-align:center'>{$bool_captions[$line['submited']]}</td>
        <td style='text-align:center'>".put_edit_buttons($line['id'],'запись',"&mode=$mode&parent=$parent&filter=$filter",'good_reviews')."</td>
       </tr>";
}
print_table_bottom();
?>
