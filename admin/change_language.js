function change_lang(lang)
 {
  for (var key in lang_array)
   {
    if(typeof(lang_array[key]) != 'function')
     {
      var tr = $('.' + lang_array[key] + '_tr');
      if (lang_array[key] == lang)
       {
        tr.show();
        $('#' + lang_array[key] + '_button').addClass('active');
       }
      else
       {
        tr.hide();
        $('#' + lang_array[key] + '_button').removeClass('active');
       }
     }
   }
 }