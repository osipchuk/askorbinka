<?php
require_once 'header.php';

if (!$mode) die('modes error');
$parent = get('gi','parent');

$model = new goods();
$categories_model = new categories();
$tablename = $model->get_table_name();

if ($creat_mode=='add' or ($creat_mode=='edit' and $id))
{
 $set = mysql_langs_set(array('caption','category','title','description','keywords','ship','pay','back','warranty')).
        fill_query(array(
         'i'=>array('rate', 'external_parent','brand','avail','best_sell','disable_reviews', 'hidden', 'code_bonuses'),
         'q'=>array('static','articul','code'),
		 'p'=>array('price','action_price', 'code_discount')
        ));
    
 $video_review = get('pi', 'video_review');
 if (empty($video_review)) $video_review = 'null';
 $set .= ", `video_review`= $video_review";
 
 if ($creat_mode == 'edit')
 {
  $line = $model->get_item("`{$model->get_table_name()}`.`id`='$id'");
  if (empty($line)) die('error');
 }
 else
 {
  $line = ['action_price' => 0];
 }
 
 if (get('pp', 'action_price') != $line['action_price']) $set .= ", `manual_action`='1'";

 if ($id=standart_edit($creat_mode, $tablename, $set, '', $id))
 {
  project_images_query('goods', $id);
  
  if (is_array($_POST['new_analog'])) foreach ($_POST['new_analog'] as $analog) if ($analog != 0) mysql_insert('good_analogs', "`good`='$id', `analog`='".(int)$analog."'");
  if (is_array($_POST['delete_analog'])) foreach ($_POST['delete_analog'] as $analog) mysql_delete('good_analogs', "`good`='$id' and `analog`='".(int)$analog."'");

  if (is_array($_POST['new_action'])) foreach ($_POST['new_action'] as $action) if ($action != 0) mysql_insert('news_and_actions_goods', "`good`='$id', `external_parent`='".(int)$action."'");
  
  if (is_array($_POST['delete_action'])) foreach ($_POST['delete_action'] as $action) mysql_delete('news_and_actions_goods', "`good`='$id' and `external_parent`='".(int)$action."'");

  $external_parent = get('pi','external_parent');
  if (!empty($external_parent))
  {
   $params = $model->get_params($id);
   $filters = $categories_model->get_category_filters($external_parent);
  }
  else
  {
   $params = array();
   $filters = array();
  }

  foreach ($params as $param)
  {
   $value = get('ps','param_'.$param['id']);
   
   if ($param['value'] != $value)
   {
	switch ($param['type'])
	{
	 case 'bool':
	  $value = (int)$value;
	  break;
	 
	 case 'char':
	  $value = "'".safe($value)."'";
	  break;
	 
	 case 'image':
	  $value = empty($value) ? NULL : (int)$value;
	  break;
	 
	 default:
	  throw new Exception('param type error');
	  break;
	}

	$set = "`external_parent`='$id', `param`='{$param['id']}', `value_{$param['type']}`= $value";
	
	if (is_null($param['value'])) mysql_insert('good_params', $set); else mysql_update('good_params', $set, "`external_parent`='$id' and `param`='{$param['id']}'");
   }
  }

  foreach ($_POST['append_categories_add'] as $addId)
  {
   mysql_insert($model->append_categories_table_name(), "`good`='$id', `category`='" . (int) $addId ."'");
  }

  foreach ($_POST['append_categories_delete'] as $deleteId)
  {
   mysql_delete($model->append_categories_table_name(), "`good`='$id' and `category`='" . (int) $deleteId ."'");
  }
  
  
  $filters_model = new category_filters();
  foreach ($filters as $filter)
  {
   $items = $filters_model->getFilterItemsWithGoodValues($filter['id'], $id);
   foreach ($items as $item)
   {
    $checked = get('pi', 'filter_' . $item['id']);
    
    if ($checked and !$item['value']) mysql_insert('good_filters', "`external_parent`='$id', `value`='{$item['id']}'");
    elseif (!$checked and $item['value']) mysql_delete('good_filters', "`external_parent`='$id' and `value`='{$item['id']}'");
   }
  }

  programsCoverage::updateAllProgramsIndex();
 }
}

if ($creat_mode=='delete' and $id)
{
 $deleter = new good_deleter($model);
 $deleter->delete($id);
 redirect("admin.php?mode=$mode&parent=$parent");
}

redirect("admin.php?mode=$mode&parent=$parent&creat_mode=edit&id=$id");

?>
