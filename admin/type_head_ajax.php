<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 15.12.14
 */

require_once 'header.php';

$modelName = get('ps', 'model');
if (!class_exists($modelName)) die('model error');
$model = new $modelName;

switch ($modelName) {
    case 'clients':
        $printer = new clients_type_head_printer($model);
        break;

    case 'goods':
        $printer = new goods_type_head_printer($model);
        break;
    
    default:
        $printer = new type_head_printer($model);
        break;
}

echo $printer->setQuery(get('ps', 'query'))->putItems();
