<?php
require_once 'header.php';

if (!$mode) die('mode error');
$tablename = $mode;

if ($creat_mode == 'add') {
    try {
        $client = get('pi', 'client');
        if (empty($client)) {
            $profile_form = new profile_form();
            $profile_form->set_fields($_POST)->send();
            $client = $profile_form->get_field('account_id');
        }
        
        $cart = new cart();
        $cart->clear();
        $items = explode(',', $_POST['goods_add']);
        //@todo: make ability to add more than one of items to the cart
        foreach ($items as $good_id) {
            $cart->add_item($good_id, count($items));
        }

        $account = (new clients())->get_item("`id`='$client'");
        $model = (new order_processor())->set_account($account)->set_fields($_POST)->set_cart($cart);
        $model->send();
        $id = $model->get_field('order_id');
        
        $cart->clear();
        
    } catch (Exception $e) {
        die($e->getMessage());
    }
}

if ($creat_mode=='edit' and $id)
{
 $status = get('pi','status');
 
// $line = mysql_line("select `id`, `client`, `add_time`, `status`, `name`, `surname`, `tel`, `email`, `pay`, `ship`, `city`, `address`, `note`, `pay_with_bonuses`, `other_receiver`,
// `receiver_name`, `receiver_surname`, `receiver_tel`, `receiver_email`
 
 $set = fill_query(array(
         'q'=>array('name','surname','tel','email','address','pay','note', 'city', 'note', 'receiver_name', 'receiver_surname', 'receiver_tel', 'receiver_email'),
         'i'=>array('ship','pay_with_bonuses','other_receiver')
        ));

 if ($id=standart_edit($creat_mode, $tablename, $set, '', $id)) {
	 if ($status == 1 or $status == -1) {
		 $order_processor = new order_processor();
		 if ($order_processor->load_from_db($id))
		 {
			 try {
				 $status == 1 ? $order_processor->submit_payments() : $order_processor->cancelOrder();
			 }
			 catch (Exception $e) {
				 die($e->getMessage());
			 }
		 }
	 }
	 else mysql_update($tablename, "`status`='$status'", "`id`='$id'");
 }
}

if ($creat_mode=='delete' and $id)
{
 mysql_delete($tablename, "`id`='$id'");
 redirect("admin.php?mode=$mode");
}

if ($creat_mode == 'send_to_logistic_department' and $id) {
    $model = new order_to_logistic_department_form();
    $model->add_send_email(applicationHelper::getInstance()->get_global('global_settings')['logistic_department_email']);
    $model->load_from_db($id);
    $model->send();
    
    mysql_update($tablename, "`sent_to_logistic` = now()", "`id`='$id'");
}

redirect("admin.php?mode=$mode&creat_mode=edit&id=$id");

?>
