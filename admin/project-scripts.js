$(function() {
 $(document).on('change','#analogs-table select', function() {
  var add = true;
  $('#analogs-table select').each(function() { if ($(this).val() == '0') add = false; });
  if (add) $('#analogs-table tbody').prepend(add_analog_html);
 });

 $(document).on('change','#actions-table select', function() {
  var add = true;
  $('#actions-table select').each(function() { if ($(this).val() == '0') add = false; });
  if (add) $('#actions-table tbody').prepend(add_action_html);
 });

    $('[data-new-order-add-client]').on('click','[data-type-head="item"]', function() {
        var client = $(this).data('type-head-value'),
            form = $(this).parents('form');
        
        $.post('get_client_info.php', {'client': client}, function(data) {
            var result = $.parseJSON(data);
            
            if (result.status == 'ok') {
                
                var fields = ['name', 'surname', 'tel', 'email', 'city', 'address'];
                for (var key in fields) {
                    var field = fields[key];
                    form.find('input[name="' + field + '"]').val(result.client[field]);
                }
                
            } else {
                alert(result.message);
            }
        });
    });
    
});