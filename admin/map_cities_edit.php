<?php
require_once 'header.php';

if (!$mode) die('mode error');

$model = new map_cities();
$tablename = $model->get_table_name();

if ($creat_mode=='add' or ($creat_mode=='edit' and $id))
{
 $set = mysql_langs_set(array('caption')).
	 fill_query(array(
		 'p'=>array('cx','cy','r','font_size'),
	     'q'=>array('region','transform','align'),
	  	 'i'=>array('nearest_city')
	 ));

 $id=standart_edit($creat_mode, $tablename, $set, '', $id);
}

if ($creat_mode=='delete' and $id)
{
 $deleter = new item_deleter($model);
 $deleter->delete($id);
 
 redirect("admin.php?mode=$mode");
}

redirect("admin.php?mode=$mode&creat_mode=edit&id=$id");

?>
