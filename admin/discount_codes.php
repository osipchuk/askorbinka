<?php
/** @var discount_codes $model */
$model = new $mode();
$tablename = $model->get_table_name();


$external_parent = new clients();
$select = $external_parent->get_all("`type`='pr'");

$parent_putter = new select_simple_printer($select);
$top_form = new top_form_printer($parent_putter->set_name('parent')->set_value(get('gi','parent')));
echo $top_form->add_parameter('mode', $mode)->put();
$parent = $parent_putter->get_value();

$words = applicationHelper::getInstance()->getTranslateResolver();

$data = $model->getCodesStatistic($parent, false);
print_table_header(
	array('Покупатель','Дата выдачи кода','Код','Купленные товары','Стоимость','Дата покупки','Начислено бонусов','Использован'),
	array(0,0,0,0,0,0,0,0,0)
);

/*
 * {foreach from=$line['goods item="good"}
		 <p>{$good}</p>
	   {/foreach}
 */

foreach ($data as $index => $line)
{
 echo "<tr".tr_class($index).">
        <td style='text-align:center;'>{$line['fio']}</td>
	    <td style='text-align:center;'>" . normal_date($line['add_time'], true) . "</td>
	    <td style='text-align:center;'>{$line['code']}</td>
	    <td>";
 foreach ($line['goods'] as $good) {
	 echo "<div>$good</div>";
 }
 echo "</td>
	    <td style='text-align:center;'>" . price_format($line['order_price'], true) . " {$words->currency}</td>
	    <td style='text-align:center;'>" . ($line['order_date'] != 0 ? normal_date($line['order_date'], true) : '-') . "</td>
	    <td style='text-align:center;'>{$line['bonuses']}</td>
	    <td style='text-align:center;'>{$bool_captions[$line['used']]}</td>
       </tr>";
}
print_table_bottom();
?>
