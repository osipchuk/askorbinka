<?php
$model = new good_tabs();
$goods_model = new goods();
$tablename = $model->get_table_name();

$parent_factory = new parent_select_factory();
$select_printer = $parent_factory->get_select($goods_model, FALSE)->set_name('category');
if (get('gi','category')) $select_printer->set_value(get('gi','category'));
$category = $select_printer->get_value();
$categories_form = new top_form_printer($select_printer);
echo $categories_form->add_parameter('mode', $mode)->set_form_name('categories_form')->put();


$goods = $goods_model->get_all_of_external_parent($category);
if (empty($goods)) die('В этой категории нет товаров');
$select_printer = new select_simple_printer($goods);
$select_printer->set_name('parent');
if (get('gi','parent')) $select_printer->set_value(get('gi','parent'));
$top_form = new top_form_printer($select_printer);
echo $top_form->add_parameter('mode', $mode)->add_parameter('category',$category)->put();
$parent = $select_printer->get_value();

$data = $model->get_all_of_external_parent($parent);


if ($creat_mode == 'add' or ($creat_mode == 'edit' and $id and
		$line = mysql_line("select `rate`".mysql_langs_select(array('caption','text'))." from `$tablename` where `id`='$id'")))
{
 $files_printer = new files_table_printer($model->get_files($id));
 $tech_printer = new files_table_printer($model->get_tech_images($id));
 
 echo put_main_form($line,
	 array_merge(array(
			 'Приоритет'=>array(null,'text','rate',null,null,'150px')),
		 main_form_fields_array($lang_array, array('Заголовок'=>array('text','caption'), 'Текст'=>array('tinymce','text'))),
	  array(
	   'Файлы'=>array($files_printer->add_file('file')->set_has_caption(TRUE)->put()),
	   'Технологии'=>array($tech_printer->set_has_caption(TRUE)->set_name('technologies')->add_file('image_small', 'Миниатюра', TRUE)->add_file('image', 'Изображение', TRUE)->put())
	  )
	 ), "&mode=$mode&parent=$parent&category=$category", lang_selector());
 
 require_once 'images.php';
}
 


print_button('Добавить', "location.href='admin.php?mode=$mode&creat_mode=add&parent=$parent&category=$category'");
print_table_header(array('Название/содержимое','Приоритет','Операции'),array(0,100,100));

foreach ($data as $index => $line)
{
 echo "<tr".tr_class($index).">
        <td>
         <div class='article_caption'>{$line['caption']}</div>
         <div class='article_text'>".put_content($line['text'])."</div>
        </td>
        <td style='text-align:center;'>{$line['rate']}</td>
        <td style='text-align:center;'>".put_edit_buttons($line['id'], 'вкладку', "&mode=$mode&parent=$parent&category=$category")."</td>
       </tr>";
}
print_table_bottom();
?>