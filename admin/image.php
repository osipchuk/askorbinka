<?php
header('Expires: '.gmdate('D, d M Y H:i:s',time()+86400).' GMT');
$cache_dir='cache';
$cache_time=86400*15;
$videos_array=array('flv','avi','mpg','mpeg','3gp');

function get_string($name,$default='')
{
 return array_key_exists($name,$_GET) ? strval($_GET[$name]) : $default;
}

function get_integer($name,$default=0)
{
 return array_key_exists($name,$_GET) ? intval($_GET[$name]) : $default;
}

function check_range($value,$min,$max)
{
 return $value<$min ? $min : $value>$max ? $max : $value;
}

function file_extension($file)
{
 $file=explode('.',basename($file));
 return count($file)>1 ? array_pop($file) : '';
}

$image=get_string('file');
$new_width=check_range(get_integer('width'),0,2048);
$new_height=check_range(get_integer('height'),0,2048);
$background=get_string('background');
$format=get_string('format',  file_extension($image));
if (!in_array($format,array('gif','jpeg','png'))) $format='jpeg';
$quality=check_range(get_integer('quality',-1),-1,100);
$cut=check_range(get_integer('cut'), 0, 3);
$grey=check_range(get_integer('grey'), 0, 1);

if (is_readable($image) and $file_size=filesize($image) and $file_time=filemtime($image))
 $cache_file=$cache_dir.'/'.md5($image.' '.$file_size.' '.$file_time.' '.$new_width.' '.$new_height.' '.$background.' '.$format.' '.$quality.' '.$cut.' '.$grey).'.'.$format;
else
{
 $file_size=0;
 $file_time=0;
 $cache_file='';
}

if ($cache_file and !file_exists($cache_file) and is_writable($cache_dir) and (
        (in_array($ext=strtolower(file_extension($image)),$videos_array) and $movie = new ffmpeg_movie($image) and $image=$movie->getFrame(round($movie->getFrameCount()/2)) and $src = $image->toGDImage())
        or $size=@getimagesize($image)))
{
 if (in_array($ext,$videos_array))
 {
  $width=$movie->getFrameWidth();
  $height=$movie->getFrameHeight();
  $type=2;
 }
 else
 {
  $width=$size[0];
  $height=$size[1];
  $type=$size[2];
 }
 
 if ($width>=1 and $height>=1 and $type>=1 and $type<=3)
 {
  if (!in_array($ext,$videos_array))
  switch ($type)
  {
   case 1: $src=@imagecreatefromgif($image); break;
   case 2: $src=@imagecreatefromjpeg($image); break;
   case 3: $src=@imagecreatefrompng($image); break;
   default: $src=false;
  }
  
  if ($src)
  {
   if ($grey)
   {
	imagefilter($src, IMG_FILTER_GRAYSCALE);
	imagefilter($src, IMG_FILTER_BRIGHTNESS, 80);
   }
   
   if ($new_width and $new_height)
   {
    $ratio=$width/$height;
    $new_ratio=$new_width/$new_height;
    
    if ($cut)
    {
     $src_x=0;
     $src_y=0;
     
     
     
     if ($new_ratio>$ratio)
     {
      $original_height=$height;
      $height=$new_height/($new_width/$width);
      $src_x=0;
      switch($cut)
      {
       case 1: $src_y=0; break;
       case 2: $src_y=($original_height-$height)/2; break;
       case 3: $src_y=$original_height-$height; break;
      }
     }
     else
     {
      $original_width=$width;
      $width=$new_width/($new_height/$height);
      $src_y=0;
      switch($cut)
      {
       case 1: $src_x=0; break;
       case 2: $src_x=($original_width-$width)/2; break;
       case 3: $src_x=$original_width-$width; break;
      }
     }
     
    }
    else
    {
     $src_x=0;
     $src_y=0;
     
     
     
     if ($ratio<$new_ratio)
     {
      $new_width=round($new_height*$ratio);
      if (!$new_width) $new_width=1;
     }
     elseif ($ratio>$new_ratio)
     {
      $new_height=round($new_width/$ratio);
      if (!$new_height) $new_height=1;
     }
     
    }
    
    if ($dest=@imagecreatetruecolor($new_width,$new_height))
    {
     if ($background)
     {
      $background=hexdec($background);
      $color=imagecolorallocate($dest,($background&255<<16)>>16,($background&255<<8)>>8,$background&255);
      imagefilledrectangle($dest,0,0,$new_width-1,$new_height-1,$color);
      imagecolordeallocate($dest,$color);
     }
     
     if ($format=='png' )
     {
      imageAlphaBlending($dest, false);
      imageSaveAlpha($dest, true);
     }
     
     
     
     imagecopyresampled($dest,$src,0,0,$src_x,$src_y,$new_width,$new_height,$width,$height);
     imagedestroy($src);
    }
   }
   else $dest=$src;
   if ($dest)
   {
    switch($format)
    {
     case 'gif':
      if (!imagegif($dest,$cache_file)) $cache_file='';
     break;
     case 'jpeg':
      if (!($quality<0 ? imagejpeg($dest,$cache_file,100) : imagejpeg($dest,$cache_file,$quality))) $cache_file='';
     break;
     case 'png':
      if (!imagepng($dest,$cache_file)) $cache_file='';
     break;
    }
    imagedestroy($dest);
    if ($cache_file) chmod($cache_file,0666);
   }
  }
 }
}

if ($cache_file and is_readable($cache_file))
{
 header('Content-type: image/'.$format);
 readfile($cache_file);
 touch($cache_file);
}

if (rand(1,50)==1 and $dir=opendir($cache_dir))
{
 $time=time()-$cache_time;
 while ($item=readdir($dir))
  if (is_file($file=$cache_dir.'/'.$item) and filemtime($file)<$time) unlink($file);
 closedir($dir);
}
?>
