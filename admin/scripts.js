//Scrips 2.0 with ajax check of the static value


Array.prototype.inArray = function (value) {
    var i;
    for (i=0; i < this.length; i++) {
        if (this[i] === value) {
            return true;
        }
    }
    return false;
};

function file_basename(filename)
 {
  base='';
  i=filename.length;
  while((ch=filename.charAt(i))!='/' && ch!='\\' && i>0)
   {
    base=ch+base;
    i--;
   }
  return base.toLowerCase();
 }
  

function file_extension(filename)
 {
  ext='';
  i=filename.length;
  while((ch=filename.charAt(i))!='.' && i>0)
   {
    ext=ch+ext;
    i--;
   }
  return ext.toLowerCase();
 }
 
 
function test_input(prefix)
 {
  ok=true;
  i=1;
  
  if (prefix==undefined) prefix=''; else prefix+='_';
  
  while (f=document.getElementById(prefix+'r'+i))
   {
    if (typeof window['tinyMCE']=='object' && tinyMCE.get(prefix+'r'+i)) val=tinyMCE.get(prefix+'r'+i).getContent(); else val=document.getElementById(prefix+'r'+i).value;
    if (val=='') ok=false;
    i++;
   }
  if (ok==false) alert("Вам нужно заполнить все обязательные поля!");
  return ok;
  return false;
 }

 
function test_filename(filename,a_ext,files)
 {
  if (!(a_ext.inArray(file_extension(filename))))
   {
    alert('Ви не можете завантажувати файли даного типу!');
    return false;
   }
  else
   {
    if (files.inArray(file_basename(filename)))
     {
      if (confirm('Файл з таким іменем вже завантажено. Перейменувати?')) return true; else return false;
     }
    else return true;
   }
 }
 
function insert_email(id)
 {
  obj=document.getElementById('i_'+id);
  if (obj.value=='0')
   {
    obj.value='1';
    document.getElementById('emails').value+=document.getElementById('email_'+id).title+'; ';
   }
  else
   {
    alert('Ви вже додали у список даного користувача');
   }
 }
 
function tinymceonchange()
 {
  if (document.delivery) document.delivery.text.value=tinyMCE.get('r2').getContent();
 }
 
function change_banner_type(type)
 {
  if (type=='c') hide='p'; else hide='c';
  i=1;
  while (show_el=document.getElementById(type+'_'+i++)) show_el.style.display='';
  i=1;
  while (hide_el=document.getElementById(hide+'_'+i++)) hide_el.style.display='none';
 }

 
function mousePageXY(e)
{
  var x = 0, y = 0;

  if (!e) e = window.event;

  if (e.pageX || e.pageY)
  {
    x = e.pageX;
    y = e.pageY;
  }
  else if (e.clientX || e.clientY)
  {
    x = e.clientX + (document.documentElement.scrollLeft || document.body.scrollLeft) - document.documentElement.clientLeft;
    y = e.clientY + (document.documentElement.scrollTop || document.body.scrollTop) - document.documentElement.clientTop;
  }

  return {"x":x, "y":y};
}


function insert_text(element, text)
{
 element.focus();
 if (document.selection)
 {
  var s = document.selection.createRange();
  s.text = text;
  s.select();
 }
 else if (typeof element.selectionStart == "number" && typeof element.selectionEnd == "number")
 {
  var start = element.selectionStart;
  var end = element.selectionEnd;
  element.value = element.value.substr(0, start) + text + element.value.substr(end);
  element.setSelectionRange(start += text.length, start);
 }
 else element.value += text;
}

function insert_html(html)
{
 tinyMCE.execCommand('mceInsertRawHTML',false, html);
}

function image_template(path, title)
{
 var href = prompt("Ссылка:");
 return (href ? "<a href=\"" + href + "\" target=\"_blank\">" : "") + "<img src=\"" + path + "\" border=\"0\" title=\"" + title + "\" />" + (href ? "</a>" : "");
}

function flash_template(path,flashvars)
{
 var width = prompt("Ширина flash:");
 var height = prompt("Высота flash:");
 
 if (flashvars=='undefined') flashvars='';
 
 width = width && !isNaN(width = parseInt(width)) ? " width = \"" + width + "\"" : '';
 height = height && !isNaN(height = parseInt(height)) ? " height = \"" + height + "\"" : '';
 return "<object classid=\"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000\" codebase=\"http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0\"" + width + height + ">\n" +
        " <param name=\"movie\" value=\"" + path + "\" />\n" +
        " <param name=\"quality\" value=\"high\" />\n" +
        " <param name=\"allowfullscreen\" value=\"true\" />\n" +
        " <param name=\"wmode\" value=\"transparent\" />\n" +
        " <param name=\"flashvars\" value=\"" + flashvars +"\" />\n" +
        " <embed src=\"" + path + "\" quality=\"high\" pluginspage=\"http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash\" type=\"application/x-shockwave-flash\"" + width + height + "></embed>\n" +
        " <embed flashvars=\"" + flashvars +"\" />\n" +
        "</object>";
}

function file_template(path, title)
{
 return "<a href=\"" + path + "\" target=\"_blank\">" + title + "</a>";
}

function insert_file(path, title)
{
 var file = path.split("/").pop();
 var file_name = file.split(".");
 var file_ext = (file_name.length > 1) ? file_name.pop() : "";
 file_name = file_name.join(".");
 if (!title) title = file;
 var template;
 switch(file_ext)
 {
  case "gif":
  case "jpe":
  case "jpeg":
  case "jpg":
  case "png": template = image_template(path, title); break;
  case "swf": template = flash_template(path); break;
  default: template = file_template(path, title);
 }
 insert_html(template);
}

function insert_file_ex(path, title, element)
{
 var file = path.split("/").pop();
 var file_name = file.split(".");
 var file_ext = (file_name.length > 1) ? file_name.pop() : "";
 file_name = file_name.join(".");
 if (!title) title = file;
 var template;
 switch(file_ext)
 {
  case "gif":
  case "jpe":
  case "jpeg":
  case "jpg":
  case "png": template = image_template(path, title); break;
  case "swf": template = flash_template(path); break;
  default: template = file_template(path, title);
 }
 insert_text(element, template);
}
/*
function showline(row)
 {
  if (row<=10) document.getElementById('row'+row).style.display='';
 }*/
 
 
function make_static(mode,id,normal_field_value,father,staticobject)
{
 normal_words = Array('А','а','Б','б','В','в','Г','г','Ґ','ґ','Д','д','Е','е','Є','є','Ж','ж','З','з','И','и','І','і','Ї','ї','Й','й','К','к','Л','л','М','м','Н','н','О','о','П','п','Р','р','С','с','Т','т','У','у','Ф','ф','Х','х','Ц','ц','Ч','ч','Ш','ш','Щ','щ','Ю','ю','Я','я','Ь','ь','Ы','ы',' ','Ъъ','-','Э','э');
 translate_words = Array('a','a','b','b','v','v','gh','gh','g','g','d','d','e','e','je','je','zh','zh','z','z','i','i','i','i','ji','ji','j','j','k','k','l','l','m','m','n','n','o','o','p','p','r','r','s','s','t','t','u','u','f','f','kh','kh','c','c','ch','ch','sh','sh','shh','shh','ju','ju','ja','ja','j','j','y','y','-','','-','e','e');
 result='';

 for(i=0;i<normal_field_value.length;i++)
 {
//    if ((word_index=(normal_words.indexOf(normal_field_value[i])))!=-1)
  if ((word_index=(jQuery.inArray(normal_field_value[i], normal_words)))!=-1)
   result+=translate_words[word_index];
  else
  if ((code=normal_field_value.charCodeAt(i)) && ((code>=48 && code<=57) || (code>=65 && code <=90) || (code>=97 && code<=122))) result+=normal_field_value.charAt(i).toLowerCase();
 }

 result = result.replace(/[-]+/g, '-');

 if (staticobject==undefined) staticobject=document.main.static;
 staticobject.value=result;

 if (result && mode) check_static(staticobject.value,mode,id,father);
}

function setCookie(name, value, expires, path, domain, secure) {
      document.cookie = name + "=" + escape(value) +
        ((expires) ? "; expires=" + expires : "") +
        ((path) ? "; path=" + path : "") +
        ((domain) ? "; domain=" + domain : "") +
        ((secure) ? "; secure" : "");
}

function getCookie(name) {
	var cookie = " " + document.cookie;
	var search = " " + name + "=";
	var setStr = null;
	var offset = 0;
	var end = 0;
	if (cookie.length > 0) {
		offset = cookie.indexOf(search);
		if (offset != -1) {
			offset += search.length;
			end = cookie.indexOf(";", offset)
			if (end == -1) {
				end = cookie.length;
			}
			setStr = unescape(cookie.substring(offset, end));
		}
	}
	return(setStr);
}


function menu(id)
 {
  if (document.getElementById('td_'+id).style.display=='')
   {
    //Jan 01 2020 00:00:00
    setCookie('visible_'+id, 0, "Wed, 01 Jan 2020 00:00:00 +0200");
    document.getElementById('td_'+id).style.display='none';
   }
  else
   {
    setCookie('visible_'+id, 1, "Wed, 01 Jan 2020 00:00:00 +0200");
    document.getElementById('td_'+id).style.display='';
   }
 }

function delete_content_image(id, href)
{
 if (confirm("Вы действительно хотите удалить изображение?"))
 {
  $.post('images_ajax.php', {'creat_mode': 'delete_image', 'id': id},
   function(data) {
    if (data == 'ok') href.parent().hide('slow'); else alert('Ошибка.');
   });
 }
}

$(function() {
 $(document).on('change','#project_images input[type="file"]', function() {
  var add = true;
  $('#project_images input[type="file"]').each(function() { if ($(this).val() == '') add = false; });
  if (add && ($('#project_images tbody tr').length < max_photos || max_photos == 0)) $('#project_images tbody').prepend(add_photo_html);
 });
});

function toggle_html_constructions(href)
{
 $('#html-constructions-block').slideToggle(200, function() {
  setCookie('html_constructions_block_visible', $(this).is(':visible') ? 1 : 0, "Wed, 01 Jan 2020 00:00:00 +0200");
  href.find('span').html($(this).is(':visible') ? 'Скрыть' : 'Показать');
 });
}

function insert_html_construction(id)
{
 insert_html(html_constructions[id]);
}

function file_selected(table, add_html)
{
 var add = true;
 
 table.find('tbody tr').each(function() {
  var all = $(this).find('input[type="file"]').length, empty = 0;
  
  $(this).find('input[type="file"]').each(function() { if ($(this).val() == '') empty++; });
  
  if (all > 0 && empty == all) add = false;
 });
 
 if (add) table.find('tbody').prepend(add_html);
}

function text_selected(table, add_html)
{
 var add = true;

 table.find('tbody tr').each(function() {
  var all = $(this).find('input[type="text"]').length, empty = 0;

  $(this).find('input[type="text"]').each(function() { if ($(this).val() == '') empty++; });

  if (all > 0 && empty == all) add = false;
 });

 if (add) table.find('tbody').prepend(add_html);
}

