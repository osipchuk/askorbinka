<?php
require_once 'header.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title>Admin - <?php echo $title; ?></title>
  <link href="style.css" rel="stylesheet" type="text/css" />
  <link href="/js/file_uploader/fileuploader.css" rel="stylesheet" type="text/css" />
  <link href="/js/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
  <meta http-equiv="Content-Type" content="text/html; charset=urf-8">
  <script type="text/javascript" src="../js/jquery-1.10.2.min.js"></script>
  <script type='text/javascript' src='scripts.js'></script>
  <script type='text/javascript' src='project-scripts.js'></script>
  <script type='text/javascript' src='change_language.js'></script>
  <script type='text/javascript' src='check_static_ajax.js'></script>
  <script type='text/javascript' src='type-head.js'></script>
  <script type="text/javascript" src="../js/file_uploader/fileuploader.js"></script>
  <script type="text/javascript" src="../js/datepicker/datepicker.js" charset="utf-8"></script>
  
   <?php
    echo '<script type="text/javascript">
           var lang_array = '.json($lang_array).';
          </script> ';
    
    $selector='';
    foreach ($lang_array as $lang_) $selector.=($lang_!=$prime_lang ? ($selector ? ', ' : '').'.'.$lang_.'_tr' : '') ;
    if ($selector) echo '<style> '.$selector.' {display:none;} </style>';
   ?>
  
  <?php
   require_once 'tiny_mce.php';
  ?>
</head>
<body>
<table width='100%'>
<tr valign='top'>
 <td width='100'>
  <a href="/" target="_blank" tabindex="-1" style="display: block; width: 160px; height: 100px; margin: 0 auto 15px; background: url('/img/admin-logo.png') 50% 0 no-repeat;"></a>
  <?php
  include('admin_menu.php');
  ?>
 </td>
 <td align='center'>
<?php
$creat_mode=get('gs','creat_mode');
$id=get('gi','id');


$new_orders=mysql_line("select count(*) from `orders` where `status`='0'");
$reviews_count = (new reviews())->getCountOfNew();
$good_reviews_count = (new good_reviews())->getCountOfNew();

echo '<div style="width:95%; padding:10px 1%; margin:0 0 10px 0; text-align:right; font-weight:bold; background:#B9B9B9;">
       <a href="admin.php?mode=good_reviews" style="margin-right: 20px;">Новых отзывов о товарах: ' . $good_reviews_count . '</a>
       <a href="admin.php?mode=reviews" style="margin-right: 20px;">Новых отзывов о магазине: ' . $reviews_count . '</a>
       <a href="admin.php?mode=orders&show=0">Новых заказов: '.$new_orders[0].'</a>
      </div>';

if ($mode != 'logout' and file_exists($file = $mode.'.php')) $page = $file;
else
{
 switch ($mode)
 {
  case 'temp':
   $page=$mode.'.php'; break;
  
  case 'reviews':
   $page = 'good_reviews.php'; break;
  
  case 'category_videos':
  case 'brand_videos':
  case 'good_videos':
   $page = 'videos.php'; break;
     
  case 'articles':
  case 'news':
   $page = 'news.php'; break;

  case 'logout': redirect('logout.php'); break;

  default: echo "<b>Пожалуйста, виберите нужный Вам раздел для администрирования.</b>";
 }
}

if ($page) require_once $page;

//echo 'Идут работы с БД, извините за временные неудобства.';
?>
 </td>
</table>


</body>

</html>
