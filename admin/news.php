<?php
/** @var news $model */
$model = new $mode();
$tablename = $model->get_table_name();

$parent_factory = new parent_select_factory();
$select_printer = $parent_factory->get_select($model);

$top_form = new top_form_printer($select_printer);
echo $top_form->add_parameter('mode', $mode)->put();
$parent = $select_printer->get_value();
$data = $model->get_all_of_external_parent($parent);


if ($creat_mode=='add' or ($creat_mode=='edit' and $id and
    $line = mysql_line("select `external_parent`, `date`, `image`, `langs`, `static`".mysql_langs_select(array('caption','title','description','keywords','preview','text'))."
                                              from `$tablename` where `id`='$id'")))
{
 $parent_putter = $parent_factory->get_select($model)->set_not_null(FALSE)->set_name('external_parent')->set_value(array_key_exists(0, $line) ? $line[0] : $parent);
 
 if (empty($line)) $line[3] = ';'.implode(';', $lang_array).';';
 $langs_str = '<ul class="checkboxes">';
 foreach ($lang_array as $_lang)
  $langs_str .= "<li style='width: 30%;'><label><input type='checkbox' name='lang_{$_lang}'".(strpos($line[3], ';'.$_lang.';') !== FALSE ? ' checked' : '')."> {$_lang}</label></li>";
 $langs_str .= '</ul>';
 
 
 echo put_main_form($line,
     array_merge(
         array('Размещение'=>array($parent_putter->put()),
               'Дата'=>array(null,'date','date',1),
               'Изображение'=>array(imagedelexist($line[2],"{$mode}_edit.php?mode=$mode&creat_mode=delete_image&id=$id",200,300)),
               'Языковые версии'=>array($langs_str),
               'Статический адрес'=>array(null,'text','static',1,null,null,null,'readonly')),
         main_form_fields_array($lang_array, array('Заголовок'=>array('text','caption'), 'Title'=>array('text','title'),
                 'Description'=>array('text','description'), 'Keywords'=>array('text','keywords'),
			 	 'Превью'=>array('tinymce','preview'), 'Текст'=>array('tinymce','text')),
             array('caption_'.$prime_lang=>array(1,null,null,null,"onkeyup=\"make_static('$mode','$id', this.value); \""))),
	  array('Темы'=>array(data_checkboxes_field('subjects', 'subjects', 'caption_'.$prime_lang, $model->subjects_table_name(), 'a_item', $id, 'subject')),
	        'Ссылки на каталог'=>array(catalog_links_checkboxes($model, $id)))
     ), "&mode=$mode&parent=$parent", lang_selector(), 'news_edit.php');

 require_once 'images.php';
}


print_button('Добавить',"location.href='admin.php?mode=$mode&creat_mode=add&parent=$parent'");
print_table_header(array('Изображение','Заголовок/текст','Языковые версии','Дата','Операции'),array(120,0,200,100,100));

foreach ($data as $index => $line)
{
 echo "<tr".tr_class($index).">
        <td style='text-align:center;'>".imageexist($line['image'],100,200)."</td>
        <td>
         <div class='article_caption'>{$line['caption']}</div>
         <div class='article_text'>{$line['preview']}</div>
        </td>
        <td style='text-align:center'>{$line['date']}"."</td>
        <td style='text-align:center'>{$line['langs']}"."</td>
        <td style='text-align:center'>".put_edit_buttons($line['id'],'элемент',"&mode=$mode&parent=$parent", 'news')."</td>
       </tr>";
}
print_table_bottom();
?>
