<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 24.04.14
 */

$model = new categories();
$table_name = $model->get_table_name();

$parent_factory = new parent_select_factory();
$select_printer = $parent_factory->get_select($model);

$top_form = new top_form_printer($select_printer);
echo $top_form->add_parameter('mode', $mode)->put();
$parent = $select_printer->get_value();
$model->set_external_parent($parent);


if ($creat_mode=='add' or ($creat_mode=='edit' and $id and
        $line = mysql_line("select `parent`, `hidden`, `rate`, `1c_id`, `image`, `image_subnav`, `image_filter`, `image_filter_active`, `static`".mysql_langs_select(array('caption','title','description','keywords','text'))." from `$table_name` where `id`='$id'")))
{
 $parent_putter = new select_tree_model_printer($model);
 
 echo put_main_form($line,
     array_merge(
         array('Размещение'=>array($parent_putter->set_name('parent')->set_value($line[0])->set_not_null(FALSE)->put()),
               'Скрывать'=>array(null,'checkbox','hidden'),
               'Приоритет'=>array(null,'text','rate',0,null,'150px'),
			   '1C ID (для импорта)'=>array(null,'text','1c_id',0,null,'150px'),
			   'Иконка для главного меню'=>array(null, 'image[50][50]', 'image'),
			   'Изображение для подменю'=>array(null, 'image[300][200]', 'image_subnav'),
			   'Иконка для фильтров'=>array(null, 'image[50][50]', 'image_filter'),
			   'Иконка для фильтров (активная)'=>array(null, 'image[50][50]', 'image_filter_active',null,null,'#18ccd8'),
               'Статический адрес'=>array(null,'text','static',1,null,null,null,'readonly')),
         main_form_fields_array($lang_array, array('Заголовок'=>array('text','caption'), 'Title'=>array('text','title'),'Description'=>array('text','description'),
                 'Keywords'=>array('text','keywords'), 'Текст'=>array('tinymce','text')),
             array('caption_'.$prime_lang=>array(1,null,null,null,"onkeyup=\"make_static('$mode','$id',this.value,document.main.parent.value); \""))),
             array('Участие в программах лояльности' => array(programsCoverageOnItemPage($model, $id)))
     ), "&mode=$mode&parent=$parent", lang_selector());

 require_once 'images.php';
}



print_button('Добавить',"location.href='admin.php?mode=$mode&creat_mode=add&parent=$parent'");
$main_putter = new categories_tree_model_printer($model);
echo $main_putter->set_base_delete_href("{$mode}_edit.php?mode=$mode&creat_mode=delete&parent=$parent")->put();

?> 