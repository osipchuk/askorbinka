<?php
/** @var category_filters $model */
$model = new $mode();
$tablename = $model->get_table_name();

$parent_factory = new parent_select_factory();
$select_printer = $parent_factory->get_select($model, false);

$top_form = new top_form_printer($select_printer);
echo $top_form->add_parameter('mode', $mode)->put();
$parent = $select_printer->get_value();
$data = $model->get_all_of_external_parent($parent);

$categories_model = new categories();
$category = $categories_model->get_item("`id`='$parent'");
if (empty($category)) {
    die('category error');
}


if ($creat_mode == 'add' or ($creat_mode == 'edit' and $id and
        $line = mysql_line("select `rate`, `type`" . mysql_langs_select(['caption']) . " from `$tablename` where `id`='$id'"))) {
    $type_printer = new select_simple_printer(['n' => 'Обычный', 'i' => 'Изображения']);
    
    if ($creat_mode == 'edit') {
        $values_printer = new files_table_printer($model->getFilterItems($id));
        if ($line[1] == 'i') {
            $values_printer->add_file('image', 'Изображение', TRUE);
        }

        $values = [
            'Варианты' => [
                $values_printer->set_has_caption(TRUE)->put()
            ]
        ];
    } else {
        $values = [];
    }

    echo put_main_form($line,
        array_merge(
            [
                'Приоритет' => [null, 'text', 'rate', 0, null, '150px'],
                'Тип' => [$type_printer->set_name('type')->set_value($line[1])->put()]
            ],
            main_form_fields_array($lang_array, ['Название' => ['text', 'caption']],
                ['caption_' . $prime_lang => [1]]),
            $values
        ), "&mode=$mode&parent=$parent");
} elseif ($creat_mode == 'add_filters_from_cat') {
    $printer = new select_tree_model_printer($categories_model);
    $printer->set_name('filters_from_cat');

    echo "<form action='{$mode}_edit.php?mode=$mode&creat_mode=$creat_mode&parent=$parent' method='post'>
           <table class='table2' cellpadding='0' cellspacing='0' border='0' style='margin-bottom: 10px;'>
             <tr>
             <td style='width: 15%;'>Категория</td>
             <td>{$printer->put()}</td>
            </tr>
            </table>
            <input type='submit' class='sh_button' id='main_submit' value='Сохранить'>
            </form>";
}

if ($category['filters_from_cat']) {
    $parent = $category['filters_from_cat'];
    $cat_caption = '';
    $cat = ['parent' => $parent];
    while (!empty($cat)) {
        $cat = $categories_model->get_item("`{$categories_model->get_table_name()}`.`id`='{$cat['parent']}'");
        if (!empty($cat['caption'])) {
            $cat_caption = $cat['caption'] . (!empty($cat_caption) ? ' - ' . $cat_caption : '');
        }
    }
    
    echo "<div>
           Фильтры для данной категории копируются с категории <a href='admin.php?mode=$mode&parent=$parent'>$cat_caption</a>
           <a href='#' onclick='if (confirm(\"Вы действительно хотите удалить привязку фильтров категории?\")) location.href=\"{$mode}_edit.php?mode=$mode&creat_mode=delete_filters_from_cat&parent={$category['id']}\";'><img src='images/delete.gif' title='Удалить привязку'></a>
          </div>";
    $data = $model->get_all_of_external_parent($parent);
} elseif (empty($data)) {
    print_button('Добавить привязку фильтров к категории',"location.href='admin.php?mode=$mode&creat_mode=add_filters_from_cat&parent=$parent'");
}

print_button('Добавить', "location.href='admin.php?mode=$mode&creat_mode=add&parent=$parent'");
print_table_header(['Фильтр', 'Приоритет', 'Операции'], [0, 100, 100]);

foreach ($data as $index => $line) {
    echo "<tr" . tr_class($index) . ">
        <td>{$line['caption']}</td>
        <td style='text-align:center'>{$line['rate']}</td>
        <td style='text-align:center'>" . put_edit_buttons($line['id'], 'фильтр', "&mode=$mode&parent=$parent") . "</td>
       </tr>";
}
print_table_bottom();
?>
