<?php
$model = new brands();
$tablename = $model->get_table_name();

$parent_factory = new parent_select_factory();
$select_printer = $parent_factory->get_select($model);

$top_form = new top_form_printer($select_printer);
echo $top_form->add_parameter('mode', $mode)->put();
$parent = $select_printer->get_value();
$data = $model->get_all_of_external_parent($parent);


if ($creat_mode=='add' or ($creat_mode=='edit' and $id and
    $line = mysql_line("select `external_parent`, `country`, `rate`, `image`, `image_large`, `url`, `static`".mysql_langs_select(array('caption','good_category','title','description','keywords','text'))."
                                              from `$tablename` where `id`='$id'")))
{
 $parent_putter = $parent_factory->get_select($model)->set_not_null(FALSE)->set_name('external_parent')->set_value(isset($line[0]) ? $line[0] : $parent);
 $countries_model = new countries();
 $country_putter = new select_simple_printer($countries_model->get_all());
 $country_putter->set_name('country')->set_not_null(FALSE)->set_null_caption('Не задана')->set_value($line[1]);

 
 $links_data = $model->get_links($id);
 $add_link = "<tr><td>";
 foreach ($lang_array as $lang_)
  $add_link .= "<div style='margin-bottom:10px;'>
                 <div>$lang_:</div>
				 <div><input type='text' name='new_link_caption_{$lang_}[]' style='width:95%;'></div>
	            </div>";
 
 $add_link .= "</td><td>";

 foreach ($lang_array as $lang_)
  $add_link .= "<div style='margin-bottom:10px;'>
                 <div>$lang_:</div>
				 <div><input type='text' name='new_link_url_{$lang_}[]' style='width:95%;'></div>
	            </div>";
 
 $add_link .= "</td>
  <td style='text-align: center;'><input type='text' name='new_link_rate[]' style='width:100px;'></td>
  <td style='text-align: center;'>-</td>
 </tr>";
 
 $links_str = "<script> var add_url_html = \"".safe($add_link)."\"; </script>
           <table class='table_2' cellpadding='0' cellspacing='0' border='0' style='width:100%;' id='urls'>
            <thead>
            <tr>
             <th colspan='4'><a href='#' onclick='$(\"#urls tbody\").prepend(add_url_html); return false;'>Добавить</a></th>
            </tr>
            <tr>
             <th>Название</th>
             <th>Ссылка</th>
             <th style='width:100px;'>Приоритет</th>
             <th style='width:100px;'>Удалить</th>
            </tr>
            </thead>
            <tbody>";

 if ($id)
 {
  foreach ($links_data as $link_line)
  {
   $links_str .= "<tr><td>";
   foreach ($lang_array as $lang_)
	$links_str .= "<div style='margin-bottom:10px;'>
                 <div>$lang_:</div>
				 <div><input type='text' name='link_{$link_line['id']}_caption_{$lang_}' style='width:95%;' value='".htmlspecialchars($link_line['caption_'.$lang_], ENT_QUOTES)."'></div>
	            </div>";

   $links_str .= "</td><td>";

   foreach ($lang_array as $lang_)
	$links_str .= "<div style='margin-bottom:10px;'>
                 <div>$lang_:</div>
				 <div><input type='text' name='link_{$link_line['id']}_url_{$lang_}' style='width:95%;' value='".htmlspecialchars($link_line['url_'.$lang_], ENT_QUOTES)."'></div>
	            </div>";

   $links_str .= "</td>
     <td style='text-align: center;'><input type='text' name='link_{$link_line['id']}_rate' style='width:100px;' value='{$link_line['rate']}'></td>
     <td style='text-align: center;'><input type='checkbox' name='delete_link_{$link_line['id']}' value='1' title='Удалить'></td>
    </tr>";
  }
 }

 $links_str.='</tbody></table>';


 echo put_main_form($line,
     array_merge(
         array('Размещение'=>array($parent_putter->put()),
			   'Страна'=>array($country_putter->put()),
			   'Приоритет'=>array(null,'text','rate',null,null,'150px'),
               'Логотип'=>array(imagedelexist($line[3],"{$mode}_edit.php?mode=$mode&creat_mode=delete_image&id=$id",200,300)),
               'Изображение'=>array(imagedelexist($line[4],"{$mode}_edit.php?mode=$mode&creat_mode=delete_image&id=$id",200,300,null,null,'project_images','image_large')),
			   'Официальный сайт'=>array(null,'text','url'),
               'Статический адрес'=>array(null,'text','static',1,null,null,null,'readonly')),
         main_form_fields_array($lang_array, array('Заголовок'=>array('text','caption'), 'Категория товаров'=>array('text','good_category'), 'Title'=>array('text','title'),
                 'Description'=>array('text','description'), 'Keywords'=>array('text','keywords'),
                 'Текст'=>array('tinymce','text')),
             array('caption_'.$prime_lang=>array(1,null,null,null,"onkeyup=\"make_static('$mode','$id', this.value); \""))),
	  array(
	   'Ссылки'=>array($links_str), 
	   'Ссылки на каталог'=>array(catalog_links_checkboxes($model, $id)),
       'Участие в программах лояльности' => array(programsCoverageOnItemPage($model, $id)),
	  )
     ), "&mode=$mode&parent=$parent", lang_selector());

 require_once 'images.php';
}


print_button('Добавить',"location.href='admin.php?mode=$mode&creat_mode=add&parent=$parent'");
print_table_header(array('Изображение','Заголовок/текст','Приоритет','Операции'),array(120,0,100,100));

foreach ($data as $index => $line)
{
 echo "<tr".tr_class($index).">
        <td style='text-align:center;'>".imageexist($line['image'],100,200)."</td>
        <td>
         {$line['url']}
         <div class='article_caption'>{$line['caption']}</div>
         <div class='article_text'>".truncate_text(strip_tags($line['text']), 200)."</div>
        </td>
        <td style='text-align:center'>{$line['rate']}"."</td>
        <td style='text-align:center'>".put_edit_buttons($line['id'],'бренд',"&mode=$mode&parent=$parent")."</td>
       </tr>";
}
print_table_bottom();
?>
