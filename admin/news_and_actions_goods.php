<?php
/** @var a_model $model */
$model = new $mode();
$tablename = $model->get_table_name();

$parent_factory = new parent_select_factory();
$select_printer = $parent_factory->get_select($model, false);

$top_form = new top_form_printer($select_printer);
echo $top_form->add_parameter('mode', $mode)->put();
$parent = $select_printer->get_value();
$data = $model->get_all_of_external_parent($parent);


print_table_header(array('Изображение','Название товара','Даты проведения','Операции'),array(120,0,200,100,100));

foreach ($data as $index => $line)
{
 echo "<tr".tr_class($index).">
        <td style='text-align:center;'>".imageexist($line['image'],100,200)."</td>
        <td>{$line['good_caption']}</td>
        <td style='text-align:center'>{$line['date_begin']} - {$line['date_end']}</td>
        <td style='text-align:center'>".put_edit_buttons($line['id'],'товар',"&mode=$mode&parent=$parent", '', true)."</td>
       </tr>";
}
print_table_bottom();
?>
