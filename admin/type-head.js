/**
 * Created by Nick on 15.12.14.
 */

function postQuery(block) {
	$.post('type_head_ajax.php', {
		'model': block.data('type-head-model'),
		'query': block.find('[data-type-head="caption"]').val()
	}, function(data) {
		var dropdown = block.find('[data-type-head="dropdown"]');
		dropdown.html(data);
		
		var value = '';
		if (dropdown.find('[data-type-head="item"]').length == 1) {
			value = dropdown.find('[data-type-head="item"]').data('data-type-head-value');
		}

		block.find('[data-type-head="value"]').val(value);
	});
}

function selectTypeHead(block, value, caption) {
	block.find('[data-type-head="value"]').val(value);
	block.find('[data-type-head="caption"]').val(caption);
  block.find('[name="goods_add"]').val(value);
}

function selectTypeHeadMulti(block, value, caption) {
	var selectedBlock = block.find('[data-type-head="selected-values"]'),
	    item = selectedBlock.find('input[value="' + value + '"]');
	
	if (!item.length) {
		selectedBlock.find('ul').append(
		 '<li><input type="hidden" name="' + block.data('type-head-name') + '_add[]" value="' + value + '">' +
		 '<label><input type="checkbox" name="' + block.data('type-head-name') + '_delete[]" value="' + value + '" title="Удалить">' + caption + '</label></li>'
		);
	}
}

function selectTypeHeadMultiWithBonuses(block, value, caption) {
	var selectedBlock = block.find('[data-type-head="selected-values"]'),
	 item = selectedBlock.find('input[value="' + value + '"]');

	if (!item.length) {
		selectedBlock.find('ul').append(
		 '<li><input type="hidden" name="' + block.data('type-head-name') + '_add[]" value="' + value + '">' +
		 '<input type="text" name="' + block.data('type-head-name') + '_bonus[' + value + ']" value="0"> ' +
		 '<label><input type="checkbox" name="' + block.data('type-head-name') + '_delete[]" value="' + value + '" title="Удалить">' + caption + '</label> ' +
		 '</li>'
		);
	}
}

function selectTypeHeadMultiWithCounts(block, value, caption) {
    var selectedBlock = block.find('[data-type-head="selected-values"]'),
     item = selectedBlock.find('input[value="' + value + '"]');

    if (!item.length) {
        selectedBlock.find('ul').append(
         '<li><input type="hidden" name="' + block.data('type-head-name') + '_add[]" value="' + value + '">' +
         '<input type="text" name="' + block.data('type-head-name') + '_count[' + value + ']" value="1" title="Количество элементов"> ' +
         '<label><input type="checkbox" name="' + block.data('type-head-name') + '_delete[]" value="' + value + '" title="Удалить">' + caption + '</label> ' +
         '</li>'
        );
    }
}


$(function() {
	$('[data-type-head="caption"]').keyup(function() {
		postQuery($(this).parents('[data-type-head="block"]'));
	});
	
	$('body').on('click', '[data-type-head="dropdown"] [data-type-head="item"]', function(e) {
		e.preventDefault();
		
		var block = $(this).parents('[data-type-head="block"]'),
		    caption = $(this).html(),
		    value = $(this).data('type-head-value');
		
		if (value == '') value = caption;
		
		switch (block.data('type-head-type')) {
			case 'multi':
				selectTypeHeadMulti(block, value, caption);
				break;
			
			case 'multi-with-bonuses':
				selectTypeHeadMultiWithBonuses(block, value, caption);
				break;

            case 'multi-with-counts':
                selectTypeHeadMultiWithCounts(block, value, caption);
                break;

			default:
			    selectTypeHead(block, value, caption);
				break;
		}

	});
});