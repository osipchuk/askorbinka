<!-- tinyMCE -->
<script type="text/javascript" src="/admin/tiny_mce/tinymce.min.js"></script>
<script type="text/javascript">

 tinymce.init({
  selector: ".mceEdvanced",
  theme: "modern",
  language: "ru",
  body_class: "content",
  relative_urls: false,
  extended_valid_elements: "span[*]",
  plugins: [
   "advlist autolink link image lists charmap print preview hr anchor pagebreak",
   "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
   "save table contextmenu directionality emoticons template paste textcolor"
  ],
  menubar: false,
  toolbar1: "fullpage | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
  toolbar2: "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image media code | inserttime preview | forecolor backcolor",
  toolbar3: "table | hr removeformat | subscript superscript | charmap emoticons | print fullscreen | spellchecker | visualchars visualblocks nonbreaking",
  toolbar_items_size: 'small',
  paste_as_text: true,
  fontsize_formats: '10px 11px 12px 14px 18px 24px 30px 36px 48px 60px 72px',
  content_css: "/bootstrap/css/bootstrap.css,/css/style.css,/admin/tinymce.css",
  style_formats_merge: true,
  style_formats: [
   {
    title: "Выравнивание изображений", items: [
        {title: "По левому краю", icon: "alignleft", selector: 'img', classes: 'left-image'},
        {title: "По центру", icon: "aligncenter", selector: 'img', classes: 'center-image'},
        {title: "По правому краю", icon: "alignright", selector: 'img', classes: 'right-image'}
    ]
  }]
 });
</script>
<!-- /tinyMCE -->