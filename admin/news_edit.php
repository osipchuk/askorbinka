<?php
require_once 'header.php';

if (!$mode) die('mode error');
$parent = get('gq', 'parent');

/** @var news $model */
$model = new $mode();
$tablename = $model->get_table_name();

if ($creat_mode=='add' or ($creat_mode=='edit' and $id))
{
 $langs = ';';
 foreach ($lang_array as $_lang) if (get('ps', 'lang_'.$_lang)) $langs .= $_lang.';';
 
 $set = mysql_langs_set(array('caption','title','text','description','keywords','preview')).
        fill_query(array(
         'd'=>array('date'),
         'q'=>array('static'),
         'i'=>array('external_parent')
        )).query_add().", `langs`='$langs'";

 if ($id=standart_edit($creat_mode, $tablename, $set, '', $id))
 {
  data_checkboxes_update('subjects', 'subjects', 'caption_'.$prime_lang, $model->subjects_table_name(), 'a_item', $id, 'subject');
  data_checkboxes_update('catalog_links', 'catalog_links', 'caption_'.$prime_lang, get_class($model) . '_catalog_links', 'external_parent', $id, 'link');
 }
}

if ($creat_mode == 'delete_image' and $id) delete_image($tablename, "`id`='$id'");

if ($creat_mode=='delete' and $id)
{
 $deleter = new item_deleter($model);
 $deleter->add_file('image')->delete($id);
 redirect("admin.php?mode=$mode&parent=$parent");
}

redirect("admin.php?mode=$mode&parent=$parent&creat_mode=edit&id=$id");

?>
