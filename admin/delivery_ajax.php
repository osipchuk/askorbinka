<?php
require_once 'header.php';
$mode = get('ps','mode', get('gs','mode'));
$dir = 'delivery_images/';

switch ($mode)
{
 case 'search_emails':
  if ($category = get('pi','category'))
  {
   $query = get('pq','query');
   $data = mysql_data_assoc("select `delivery_emails`.* from `delivery_emails` join `delivery_categories_email` on `delivery_emails`.`id`=`delivery_categories_email`.`email`
                             where `delivery_categories_email`.`category`='$category' and `delivery_emails`.`submited`='1'".
                            (!empty($query) ? " and (`delivery_emails`.`email` like '%$query%' or `delivery_emails`.`fio` like '%$query%')" : '').
                            "order by `id` desc");
   
   if (empty($data)) echo '-';
   else
   {
    foreach ($data as $line)
    {
     $caption = $line['email'];
     if (!empty($line['fio'])) $caption = htmlspecialchars("{$line['fio']} <{$caption}>");
     
     echo "<div><a href='#{$line['email']}'>$caption</a></div>";
    }
   }
  }
  break;
 
 case 'delete_image':
  if ($file=get('ps','file') and strpos($file, '/') === false and unlink($dir.'/'.$file)) echo 'ok'; else echo 'error';
  break;
 
 case 'upload_image':
  require_once '../classes/upload_classes.php';
  
  $uploader = new qqFileUploader(array("jpg", "jpeg", "png", "gif", "flv"), 2 * 1024 * 1024);
  $result = $uploader->handleUpload($dir);

  echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
  break;
}
?>