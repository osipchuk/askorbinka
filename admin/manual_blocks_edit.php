<?php
require_once 'header.php';

if (!$mode) die('mode error');
/** @var a_model $model */
$model = new $mode();
$tablename = $model->get_table_name();

if ($creat_mode=='add' or ($creat_mode=='edit' and $id))
{
 $set = "`langs`='".languages_checkboxes_set()."'".mysql_langs_set(array('caption','text')).
     fill_query(array(
         'q'=>array('href'),
         'i'=>array('rate')
     )).query_add();
 
 if ($id = standart_edit($creat_mode, $tablename, $set, '', $id))
 {
  delete_query_image($tablename, "`id`='$id'", 'image_small', 'image_small');
 }
}

if ($creat_mode=='delete' and $id)
{
 $deleter = new item_deleter($model);
 $deleter->add_file('image')->delete($id);
 redirect("admin.php?mode=$mode");
}

if ($creat_mode=='delete_image' and $id) delete_image($tablename, "`id`='$id'");

redirect("admin.php?mode=$mode&creat_mode=edit&id=$id");
?>
