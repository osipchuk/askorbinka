<?php
require_once 'header.php';

if (!$mode) die('mode error');
$parent = get('gq', 'parent');
$model = new categories();
$tablename = $model->get_table_name();

if ($creat_mode=='add' or ($creat_mode=='edit' and $id))
{
 if ($id) {
  $item_before_edit = $model->get_item("`{$model->get_table_name()}`.`id`='$id'");
 }
 
 $set = mysql_langs_set(array('caption','title','text','description','keywords')).
        fill_query(array(
         'i'=>array('rate','parent','1c_id', 'hidden'),
         'q'=>array('static')
        )).query_add().query_add('image_filter', 'image_filter').query_add('image_filter_active', 'image_filter_active').query_add('image_subnav', 'image_subnav');
 
 if ($id=standart_edit($creat_mode, $tablename, $set, ", `external_parent`='$parent'", $id))
 {
  delete_query_image($tablename, "`id`='$id'", 'image_filter', 'image_filter');
  delete_query_image($tablename, "`id`='$id'", 'image_filter_active', 'image_filter_active');
  delete_query_image($tablename, "`id`='$id'", 'image_subnav', 'image_subnav');
  
  $hidden = get('pi', 'hidden');
  if (isset($item_before_edit['hidden']) and $hidden != $item_before_edit['hidden'])
  {
   $item = $model->find_item_in_structure_by_id($model->get_structure(true), $id);
   if (isset($item['sub']))
   {
    $subitems = $model->get_all_subcategories_id($item['sub']);
    mysql_update($tablename, "`hidden`='$hidden'", "`id` in (" . implode(', ', $subitems) . ")");
   }
  }
 }
}

if ($creat_mode=='delete' and $id and $item = $model->get_structure_item($id))
{
 $deleter = new category_deleter($model);
 $deleter->delete($item);
 
 redirect("admin.php?mode=$mode&parent=$parent");
}

redirect("admin.php?mode=$mode&creat_mode=edit&id=$id&parent=$parent");

?>
