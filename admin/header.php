<?php
header("Content-Type: text/html; charset=utf-8");
ini_set('display_errors', 0);
session_start();
require_once 'tools.php';
test_auth();
$main_db = db_connect();

$creat_mode=get('gs','creat_mode');
$id=get('gi','id');
$mode=get('gs','mode');
require_once 'global_settings.php';
$page = NULL;

require_once $classes_dir.'autoloader.php';
autoloader::attach($autoload_classes_dir);


 $menu=array('Общие настройки'=>array('administrators'=>'Администраторы','sett'=>'Настройки','files'=>'Управление файлами','translation'=>'Переводы',
                                      'html_constructions'=>'Конструкции HTML','mail_templates'=>'Шаблоны писем','structure'=>'Структура сайта',
                                      'countries'=>'Список стран', 'slider'=>'Слайдер изображений','manual_blocks'=>'Блоки, заданные вручную','subjects'=>'Темы новостей / статтей',
                                      'reviews'=>'Отзывы'),
             'Стандартные модули'=>array('news'=>'Новости','brands'=>'Бренды', 'articles'=>'Статьи'),
  			 'Ссылка на каталог (для брендов и новостей)'=>array('catalog_link_categories'=>'Категории', 'catalog_links'=>'Ссылки'),
             'Каталог товаров'=>array('categories'=>'Категории', 'category_params'=>'Характеристики категорий', 'category_filters' => 'Фильтры категорий',
				                      'goods'=>'Товары', 'good_tabs'=>'Характеристики товаров', 'good_reviews'=>'Отзывы о товарах',
				                      'orders'=>'Заказы', 'prices'=>'Импорт прайс листов', 'ships'=>'Способы доставки', 'payment_texts'=>'Способы оплаты',
				                      'catalog_page_links'=>'Ссылки на странице товара'),
             'Новинки и акции'=>array('news_and_actions'=>'Новинки и акции', 'news_and_actions_goods'=>'Акционные товары'),
             'Программы лояльности' => array('programs'=>'Список программ', 'program_settings'=>'Настройки программ'),
  			 'Видео'=>array('video_categories'=>'Категории', 'good_videos'=>'Видеообзоры товаров', 'brand_videos'=>'О брендах', 'category_videos'=>'Полезно знать', 'videos'=>'Как заказать товар'),
             'Учетные записи пользователей'=>array('clients'=>'Учетные записи', 'discount_codes' => 'Скидочные коды', 'client_bonuses' => 'Операции с бонусами',
				                                   'bonus_transactions' => 'Заявки на вывод бонусов'),
	         'Рассылка новостей'=>array('delivery_categories'=>'Категории','delivery_emails'=>'Список адресов', 'delivery'=>'Создать рассылку','delivery_mails'=>'Отправленные'),
             'Дистрибьюция и сервис'=>array('map_cities'=>'Список городов', 'map_addresses'=>'Список адресов'),
             'Выход'=>array('logout'=>'Выход'));
?>
