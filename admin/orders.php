<?php
$tablename = $mode;
$order_processor = new order_processor();
$cart_ = new cart();
$clients_model = new clients(); 
$words = applicationHelper::getTranslateResolver();
$ships = new ships();

if ($creat_mode == 'add' or ($creat_mode == 'edit' and $id and
	$line = mysql_line("select `id`, `sent_to_logistic`, `discount_code_id`, `client`, `add_time`, `status`, `name`, `surname`, `tel`,
                               `email`, `pay`, `ship`, `city`, `address`, `note`, `pay_with_bonuses`, `bonuses_applied`, `other_receiver`,
                               `receiver_name`, `receiver_surname`, `receiver_tel`, `receiver_email`, `sent_to_logistic`
                        from `$tablename` where `id`='$id'")))
{
    if ($id) {
        $cart_->load_from_db($id);
    }
 
 
 $pays_array = array();
 foreach ($pay_systems as $system) $pays_array[$system] = $words->_w('pay_system_', $system);
 $pay_select = new select_simple_printer($pays_array);
 $ship_select = new select_simple_printer($ships->get_all());

 if (!empty($line[2])) {
	 $discountCodeModel = new discount_codes();
	 $discountCode = $discountCodeModel->get_item("`{$discountCodeModel->get_table_name()}`.`id`='{$line[2]}'")['code'];
 } else {
	 $discountCode = '-';
 }
	
 if (!empty($line[3])) $account = $clients_model->get_item("`id`='{$line[3]}'"); else $account = NULL;
    
    if (!empty($id)) {
        $client = $clients_model->get_client_admin_url($line[3]);
        $goods =
            $cart_->format_table('table2') .
            "<div style='margin: 10px;'><a href='{$mode}_edit.php?mode=$mode&creat_mode=send_to_logistic_department&id=$id'>Отправить в отдел логистики &raquo;</a></div>";
        
    } else {
        $setClientPrinter = new type_head_printer($clients_model);
        $setClientPrinter->set_not_null(false)->set_name('client');
        
        $goodsPrinter = new type_head_multi_printer_with_counts(new goods());
        $goodsPrinter->set_name('goods')->set_not_null(false);
        
        $client = '<div data-new-order-add-client>' . $setClientPrinter->put() . '</div>';
        $goods = $goodsPrinter->put();
    }
 
 
 echo put_main_form($line,
     array_merge(
         array('ID заказа'=>array($line[0] ?: '-'),
               'Отправлен в отдел логистики' => [$line[1] ? normal_date($line[1], true) : '-'],
			   'Скидочнй сертификат' => [$discountCode],
               'Клиент'=>array($client),
               'Дата заказа'=>array(normal_date($line[4], TRUE)),
               'Статус'=>array($id ? form_select('status', $line[5], $payment_statuses) : 'Новый заказ'),
               'Имя'=>array($account ? $account['name'].'&nbsp;' : null,'text','name',1),
               'Фамилия'=>array($account ? $account['surname'].'&nbsp;' : null,'text','surname',1),
               'Номер телефона'=>array($account ? $account['tel'].'&nbsp;' : null,'tel','tel',1),
               'Email'=>array($account ? $account['email'].'&nbsp;' : null,'email','email'),
               'Способ оплаты'=>array($pay_select->set_name('pay')->set_value($line[10])->put()),
               'Способ доставки'=>array($ship_select->set_name('ship')->set_value($line[11])->put()),
			   'Город'=>array($account ? $account['city'].'&nbsp;' : null,'text','city',1),
			   'Адрес доставки'=>array(null,'text','address'),
			   'Примечание'=>array(null,'textarea','note',null, str_replace('<br />', '', $line[14]) ),
			   'Оплата бонусами'=>array(null,'checkbox','pay_with_bonuses'),
			   'Бонусов потрачено'=>array($id ? $line[16] : '-'),
			 
			   'Получатель третье лицо'=>array(null,'checkbox','other_receiver'),
			   'Имя получателя'=>array(null,'text','receiver_name'),
			   'Фамилия получателя'=>array(null,'text','receiver_surname'),
			   'Номер телефона получателя'=>array(null,'tel','receiver_tel'),
			   'Email получателя'=>array(null,'email','receiver_email'),
               
               'Заказанные товары'=>array($goods)
         )
     )
     , "&mode=$mode");
}


print_button('Добавить закза', "location.href='admin.php?mode=$mode&creat_mode=add'");
print_table_header(array('ID','Клиент','Дата заказа','Ф.И.О.', 'Email','Количество товаров','Сумма зказа','Статус','Операции'), array(130,0,120,0,0,100,100,160,100));
$data = $order_processor->get_orders();

foreach ($data as $index => $line)
{
 $cart_->load_from_db($line['id']);
 $summary = $cart_->get_summary();
 
 echo "<tr".tr_class($index).">
        <td style='text-align: center;'>{$line['id']}</td>
        <td>{$clients_model->get_client_admin_url($line['client'])}</td>
        <td>".normal_date($line['add_time'], TRUE)."</td>
        <td>{$clients_model->account_fio($line)}</td>
        <td>{$line['email']}</td>
        <td style='text-align: center;'>{$summary['count']}</td>
        <td style='text-align: center;'>{$summary['price']} {$words->_('currency')}</td>
        <td style='text-align: center;'>{$payment_statuses[$line['status']]}</td>
        <td style='text-align: center;'>".put_edit_buttons($line['id'], 'заказ', "&mode=$mode")."</td>
       </tr>";
}

print_table_bottom();

?> 