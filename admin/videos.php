<?php
/** @var videos $model */
$model = new $mode();
$tablename = $model->get_table_name();
$external_parent_key = $model->external_parent_key();


if (is_null($external_parent_key))
{
 $parent_putter = null;
}
else
{
 $external_parent_class = $model->external_model();
 /** @var a_model $external_parent */
 $external_parent = new $external_parent_class();
 $select = $external_parent->get_all();
 
 $parent_putter = new select_simple_printer($select);
 $parent_putter->set_name($external_parent_key);
}

if (!is_null($parent_putter))
{
 $top_parent_putter = clone $parent_putter;
 $top_form = new top_form_printer($top_parent_putter->set_name('parent')->set_value(get('gi','parent')));
 echo $top_form->add_parameter('mode', $mode)->put();
 $parent = $top_parent_putter->get_value();
}


if ($creat_mode == 'add' or ($creat_mode == 'edit' and $id and
        $line = mysql_line("select ".(!is_null($external_parent_key) ? "`$external_parent_key`" : 'null').", `rate`, `video_id`, `image`".mysql_langs_select(array('caption'))." from `$tablename` where `id`='$id'")))
{
 $preview = put_youtube_video($line[1]);
 
 echo put_main_form($line,
	 array_merge(array(
			 'Размещение'=>array(!is_null($parent_putter) ? $parent_putter->set_value($line[0] ? : $parent)->put() : '-'),
			 'Приоритет'=>array(null,'text','rate',null,null,'150px'),
			 'Ссылка на youtube-видео'=>array(null,'text','video_id',1,($line[2] ? 'http://www.youtube.com/watch?v='.$line[2] : '')),
             'Превью видео' => array(null, 'image[300][225]', 'image')
         ),
		 main_form_fields_array($lang_array, array('Заголовок'=>array('text','caption') )),
		 array(
			 'Превью'=>array($preview ? $preview : '-')
		 )
	 ), "&mode=$mode&parent=$parent", '', 'videos_edit.php');
}
 


print_button('Добавить', "location.href='admin.php?mode=$mode&creat_mode=add&parent=$parent'");
print_table_header(array('Видео','Название','Приоритет','Операции'),array(320,0,100,100));


$data = $model->get_all_of_external_parent($parent);
foreach ($data as $index => $line)
{
 $image = youtube_video_preview($line['video_id'], $line['image']);
 
 
 echo "<tr".tr_class($index).">
        <td style='text-align:center;'>".($image ? "<img src='$image' style='width: 300px; height: auto;'>" : '')."</td>
        <td>
         <div class='article_caption'>{$line['caption']}</div>
        </td>
        <td style='text-align:center;'>{$line['rate']}</td>
        <td style='text-align:center;'>".put_edit_buttons($line['id'], 'видео', "&mode=$mode&parent=$parent", 'videos')."</td>
       </tr>";
}
print_table_bottom();
?>
