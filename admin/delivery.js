/**
 * Created by Nick on 15.01.14.
 */
function search_email()
{
 $.post('delivery_ajax.php', {'mode': 'search_emails', 'category': category, 'query': $('#email_search').val()},
  function(data) {
   $('#email_search_results').html(data);
  });
}

function insert_email(email)
{
 email = ';' + email.replace('#', '') + ';';
 var selected_emails = $('#recievers').val();
 
 if (selected_emails.indexOf(email) == -1) $('#recievers').val(selected_emails + email + ' ');
 
 return false;
}

function insert_all_emails()
{
 $('#email_search_results').find('a').each(function() { insert_email($(this).attr('href')); });
}

function delete_delivery_image(filename, href)
{
 if (confirm("Вы действительно хотите удалить изображение?"))
 {
  $.post('delivery_ajax.php', {'mode': 'delete_image', 'file': filename},
      function(data) {
       if (data == 'ok') href.parent().hide('slow'); else alert('Ошибка.');
      });
 }
}


$(function() {
 search_email();
 
 $('#email_search').keyup(function() { search_email() });

 $('#email_search_results').on('click', 'a', function() { return insert_email($(this).attr('href')); });
});