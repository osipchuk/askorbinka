<?php
function put_cat($category)
{
 return empty($category) ? '-' : $category;
}

$tablename = 'delivery_mails';

$query = "select `$tablename`.*, `delivery_categories`.`caption_$prime_lang` as `category_caption`
          from `$tablename` left join `delivery_categories` on `$tablename`.`category`=`delivery_categories`.`id`";

if ($id=get('gi','show') and $line = mysql_line_assoc($query." where `$tablename`.`id`='$id'"))
{
 echo "<table class='table2' cellpadding='0' cellspacing='0' border='0' style='margin-bottom:10px;'>
        <tr>
         <th colspan='2'>Информация о рассылке #$id</td>
        </tr>
        <tr>
         <td width='15%'>Категория</td>
         <td>".put_cat($line['category_caption'])."</td>
        </tr>
        <tr>
         <td>Отправитель</td>
         <td>".htmlspecialchars($line['sender'])."</td>
        </tr>
        <tr>
         <td>Дата и время рассылки</td>
         <td>".normal_date($line['send_time'], true)."</td>
        </tr>
        <tr>
         <td>Тема</td>
         <td>{$line['subject']}</td>
        </tr>
        <tr>
         <td>Текст</td>
         <td>{$line['text']}</td>
        </tr>
        <tr>
         <td>Список получателей</td>
         <td>{$line['recievers']}</td>
        </tr>
       </table>";
}

print_table_header(array('Категория','Отправитель','Список получателей','Тема','Текст сообщение','Операции'),array(150,150,150,100,0,100));
$data = mysql_data_assoc($query." order by `$tablename`.`id` desc");
echo mysql_error();

foreach ($data as $index => $line)
{
 echo "<tr".tr_class($index).">
        <td style='text-align:center;'>".put_cat($line['category_caption'])."</td>
        <td>".htmlspecialchars($line['sender'])."</td>
        <td>".truncate_text($line['recievers'],200)."</td>
        <td>{$line['subject']}</td>
        <td>".truncate_text(strip_tags($line['text']),300)."</td>
        <td style='text-align:center;'>
         <a href='admin.php?mode=$mode&show={$line['id']}' title='Детальнее'><img src='images/detailed.png' border='0'></a>
         <a href='#' onclick='if (confirm(\"Вы действительно хотите удалить информацию о рассылке?\")) location.href=\"delivery_edit.php?&creat_mode=delete&id={$line['id']}&mode=$mode\"' title='Удалить'><img src='images/delete.gif' border='0'></a>
        </td>
       </tr>";
}
print_table_bottom();

?> 