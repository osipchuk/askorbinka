<?php
/** @var programs $model */
$model = new programs();
$tablename = $model->get_table_name();

$select_printer = (new select_simple_printer($model->get_all()))->set_name('parent');
$value = get('gi','parent');
if ($value > 0) $select_printer->set_value($value);

$top_form = new top_form_printer($select_printer);
echo $top_form->add_parameter('mode', $mode)->put();
$parent = $select_printer->get_value();
$data = $model->get_all_of_external_parent($parent);


$line = $model->get_item("`$tablename`.`id`='$parent'");
if (empty($line)) {
	die('Program error');
}

echo "<form name='main' enctype='multipart/form-data' action='programs_edit.php?creat_mode=update_settings&mode=$mode&id=$parent' method='post' style='margin-bottom:10px'>
       <table class='table2' cellpadding='0' cellspacing='0' border='0' style='margin-bottom:10px'>
       <tr>
        <th colspan='2'>Базовые настройки</th>
       </tr>";

switch ($line['type']) {
	case 1:
	case 2:
	case 3:
	case 4:
		echo "<tr>
			   <td>" . ($line['type'] == 4 ? 'Минимальное количество акционных товаров в корзине' : 'Минимальна сумма (грн)') . " <sup style='color: #f00;'>*</sup></td>
		       <td><input type='number' name='param_n' value='{$line['param_n']}' style='width: 150px;' required></td>
			  </tr>";
		break;
}

echo "<tr>
       <th colspan='2'>Бонусы</th>
      </tr>
      <tr>
	   <td width='30%'>{$model->getBonusTypes($line['bonus_type'])}</td>
	   <td>";

switch ($line['bonus_type']) {
	case 1:
		echo "<input type='number' name='base_bonus' value='{$line['base_bonus']}' style='width: 150px;' required> (базовый процент для всех товаров программы)";
		break;
	
	case 2:
	case 5:
		echo "<input type='number' name='discount' value='{$line['discount']}' style='width: 150px;' required>";
		break;
	
	case 3:
		$printer = new type_head_printer($model->getGoodsModel());
		echo $printer->set_name('free_good')->set_value($line['free_good'])->setQuery($printer->get_value_caption())->put();
		break;

	case 4:
		echo "+";
		break;
}

echo "</td>
	  </tr>";

if ($line['type'] == 3) {
	echo "<tr>
<td>Бонусы уже начислены</td>
<td><input type='checkbox' name='bonuses_applied' value='1'" . ($line['bonuses_applied'] ? ' checked' : '') . "></td>
</tr>";
}

echo "<tr>
       <th colspan='2'>Действие акции</th>
	  </tr>
       <tr>
	   <td>Все товары</td>
	   <td>" . strip_tags($bool_captions[$line['all_goods']]) ."</td>
      </tr>";

if ($line['bonus_type'] == 1 or !$line['all_goods']) {
	$coverage = new programsCoverage($parent);
	
	if ($line['bonus_type'] == 1) {
		$printerClass = 'type_head_multi_printer_with_bonuses';
	} else {
		$printerClass = 'type_head_multi_printer';
	}
	
	$addBrandPrinter = new $printerClass(new brands());
	$addGoodPrinter = new $printerClass(new goods());
	$addCategoriesPrinter = new $printerClass(new categories());

	if ($line['bonus_type'] == 1) {
		$addBrandPrinter->setBonuses(mysql_data_assoc("select * from `" . $coverage::BRANDS_DATA_TABLE . "` where `program`='$parent'"), 'brand');
		$addGoodPrinter->setBonuses(mysql_data_assoc("select * from `" . $coverage::GOODS_DATA_TABLE . "` where `program`='$parent'"), 'good');
		$addCategoriesPrinter->setBonuses(mysql_data_assoc("select * from `" . $coverage::CATEGORIES_DATA_TABLE . "` where `program`='$parent'"), 'category');
	}
	
	echo "<tr>
		   <td>Бренды</td>
		   <td>" . $addBrandPrinter->set_name('cover_brand')->set_value(array_keys($coverage->getBrands()))->set_not_null(false)->put() . "</td>
		  </tr>
		  <tr>
		   <td>Категории</td>
		   <td>" . $addCategoriesPrinter->set_name('cover_category')->set_value(array_keys($coverage->getCategories()))->set_not_null(false)->put() . "</td>
		  </tr>
		  <tr>
		   <td>Товары</td>
		   <td>" . $addGoodPrinter->set_name('cover_good')->set_value(array_keys($coverage->getGoods()))->set_not_null(false)->put() . "</td>
		  </tr>
		  <tr>
		   <td>Полная сводка товаров</td>
		   <td>";
	
	foreach ($coverage->getAllGoods(false) as $line) {
		echo "<div><a href='admin.php?mode=goods&creat_mode=edit&id={$line['id']}' target='_blank' title='Посмотреть товар'>{$line['category']} {$line['caption']}</a></div>";
	}
	
	echo  "</td>
		  </tr>";
}


echo " </table>
       <input type='submit' class='sh_button' id='main_submit' value='Сохранить'>
      </form>";