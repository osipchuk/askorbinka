<?php
require_once 'header.php';

if (!$mode) die('mode error');

$model = new good_reviews();
$tablename = $model->get_table_name();

if ($creat_mode=='add' or ($creat_mode=='edit' and $id))
{
 $set = fill_query(array(
         'i'=>array('client', 'rating', 'submited'),
	     'q'=>array('date', 'text', 'reply')
        )).query_add();

 $external_parent = get('pi','external_parent');
 if ($external_parent <= 0) $external_parent = 'null';
 $set .= ", `external_parent`=$external_parent";
 if (!empty($_POST['reply'])) $set .= ", `reply_date` = now()";

 $id=standart_edit($creat_mode, $tablename, $set, '', $id);
}


if ($creat_mode=='delete' and $id)
{
 $deleter = new item_deleter($model);
 $deleter->delete($id);
 redirect("admin.php?mode=$mode");
}

redirect("admin.php?mode=$mode&creat_mode=edit&id=$id");

?>
