<?php
require_once 'header.php';
if (!$mode) die('mode error');

$dir = 'prices';

$model = new import_files();
$model->set_save_files_dir($dir);
$tablename = $model->get_table_name();

if ($creat_mode=='add' and !empty($_FILES['file']['name']))
{
 $set = fill_query(array(
         'q'=>array('caption'),
     )).", `filename`='".safe($_FILES['file']['name'])."'";
 
 if ($dir = $model->get_save_files_dir()) $set .= query_add('file', 'file', $dir);
 
 if ($id = standart_edit($creat_mode, $tablename, $set, '', $id))
 {
  try {
   $importer = new price_importer();
   $importer->set_lang($prime_lang);
   $model->set_importer($importer);
   
   $count = $model->import($_FILES['file']);
   
   mysql_update($tablename, "`updates_count`='$count'", "`id`='$id'");
  }
  catch (Exception $e)
  {
   die($e->getMessage());
  }
 }
}
elseif ($creat_mode=='delete' and $id)
{
 $deleter = new item_deleter($model);
 $deleter->add_file('file', $dir)->delete($id);
 redirect("admin.php?mode=$mode");
}

redirect("admin.php?mode=$mode");
?>