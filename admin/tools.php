<?php
require_once 'settings.php';
require_once 'extended_functions.php';


function safe($value)
{
 return is_string($value) ? str_replace(array("\\","\x00","\n","\r","'","\"","\x1a"),
  array("\\\\","\\x00","\\n","\\r","\\'","\\\"","\\x1a"),$value) : $value;
}

function safe_array($data)
{
 foreach ($data as $key=>$value)
  if (is_string($value))
   $data[$key]=safe($value);
 return $data;
}


function safe_file_name($file)
{
 return basename(str_replace(array("\0","\n"),'',$file));
}

function utf2ansi($s)
{
 return $s ? iconv('UTF-8','CP1251',$s) : '';
}

function ansi2utf($s)
{
 return $s ? iconv('CP1251','UTF-8',$s) : '';
}

function get($type,$name,$result=null)
{
 if ($type and $name)
 {
  switch ($type{0})
  {
   case 'g': $data=$_GET; break;
   case 'p': $data=$_POST; break;
   case 'c': $data=$_COOKIE; break;
   case 'r': $data=$_REQUEST; break;
   case 's': $data=$_SESSION; break;
   default: $data=null;
  }
  if ($data)
   if (strlen($type)>1)
    {
     if (!array_key_exists($name, $data)) $data[$name] = NULL;
     
     if (get_magic_quotes_gpc())
     {
      if (!is_array($data[$name])) $data[$name]=stripslashes($data[$name]);
//      Дописать для массива
     }
     switch ($type{1})
      {
       case 'i': $result=intval($data[$name] ? $data[$name] : $result); break;
       case 'f': $result=floatval($data[$name]); break;
       case 's': $result=strval($data[$name]); break;
       case 'q': $result=safe($data[$name]); break;
       case 'u': $result=utf2ansi($data[$name]); break;
       case 'U': $result=safe(utf2ansi($data[$name])); break;
       case 'a': $result=is_array($data[$name]) ? $data[$name] : array($data[$name]); break;
       case 'A': $result=safe_array(is_array($data[$name]) ? $data[$name] : array($data[$name])); break;
       case 'p': $result=floatval(str_replace(array(',',' '),array('.',''),get('ps',$name,0))); break;
       case 'd': $result=safe($data[$name] ? $data[$name] : '0000-00-00'); break;
       default: $result=$data[$name];
      }
     }
   else $result=$data[$name];
 }
 return $result;
}

function filter_html($line)
{
 foreach ($line as $key=>$value)
  $line[$key]=htmlspecialchars($value,ENT_QUOTES);
 return $line;
}

function db_connect()
 {
  global $dbsettings;
  $db = @mysql_connect($dbsettings['server'],$dbsettings['username'],$dbsettings['pass']);
  mysql_select_db($dbsettings['dbname']);
  mysql_query('set NAMES utf8');
  if (mysql_error()) die('Ошибка поключения к базе данных');
  return $db;
 }

function mysql_line($query)
 {
  if ($query and $data=mysql_query($query) and $line=mysql_fetch_row($data))
   return $line;
  else return false;
 }

function mysql_line_assoc($query)
{
 if ($query and $data = mysql_query($query) and $line = mysql_fetch_assoc($data)) return $line; else return null;
}

function mysql_data($query)
 {
  $result=array();
  if ($query and $data=mysql_query($query))
   while ($line=mysql_fetch_row($data)) array_push($result,$line);
  return $result;
 }

function mysql_data_assoc($query, $db = null) {
 $result=array();
 if ($query and (($db and $data = mysql_query($query, $db)) or (!$db and $data = mysql_query($query))))
  while ($line=mysql_fetch_assoc($data)) array_push($result,$line);
 return $result;
}
 
function mysql_array($query)
{
 $result=array();
 $data=mysql_data($query);
 foreach ($data as $line) $result[]=$line[0];
 
 return $result;
}

function mysql_keys_array($query)
{
 $result=array();
 $data=mysql_data($query);
 foreach ($data as $line) $result[$line[0]]=array_slice($line, 1);
 
 return $result;
}
 
function mysql_delete($table, $where, $files=array())
{
 if ($table and $where)
 {
  $default_dir='project_images';
  
  foreach ($files as $field => $dir)
  {
   if (!$field and $dir) { $field=$dir; $dir=$default_dir; }
   $images=mysql_data("select `{$field}` from `{$table}` where $where");//fixed_delete
   foreach ($images as $image) if ($image[0]) unlink("{$dir}/{$image[0]}");
  }
  
  mysql_query("delete from `{$table}` where $where");
  return mysql_error();
 }
 else return 'No variables';
}

function mysql_insert($table, $set)
{
 if ($table and $set)
 {
  global $main_db;
  
  mysql_query("insert into `{$table}` set $set", $main_db);
  return mysql_insert_id($main_db);
 }
 else return false;
}

function mysql_update($table, $set, $where)
{
 if ($table and $set and $where)
 {
  mysql_query($query="update `{$table}` set $set where $where");
  return mysql_error();
 }
 else return 'No variables';
}

function standart_edit($creat_mode, $table, $set, $insert_add='', $id=0)
{
 if (!empty($id)) delete_query_image($table, "`id`='$id'");

 if (mb_substr($set, 0, 1, 'utf8') == ',') $set = mb_substr($set, 1, mb_strlen($set, 'utf8'), 'utf8');
 if ($table and $set)
 {
  if ($creat_mode=='add') $result=mysql_insert($table, $set.$insert_add);
   elseif ($creat_mode=='edit' and $id)
   {
    mysql_update($table, $set, "`id`='$id'");
    $result=$id;
   }
   else $result=0;
 }
 else $result=0;
 
 return $result;
}


function str2lower($str)
{
 return str_replace(array('А','Б','В','Г','Ґ','Д','Е','Є','Ж','З','И','І','Ї','Й','К','Л','М','Н','О','П','Р','С','Т','У','Ф','Х','Ц','Ч','Ш','Щ','Ю','Я','Ь','Ы','Ъ','Э','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'),
                    array('а','б','в','г','г','д','е','є','ж','з','и','і','ї','й','к','л','м','н','о','п','р','с','т','у','ф','х','ц','ч','ш','щ','ю','я','ь','ы','ъ','э','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'), $str);
}

function str2upper($str)
{
 return str_replace(array('а','б','в','г','г','д','е','є','ж','з','и','і','ї','й','к','л','м','н','о','п','р','с','т','у','ф','х','ц','ч','ш','щ','ю','я','ь','ы','ъ','э','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'),
                    array('А','Б','В','Г','Ґ','Д','Е','Є','Ж','З','И','І','Ї','Й','К','Л','М','Н','О','П','Р','С','Т','У','Ф','Х','Ц','Ч','Ш','Щ','Ю','Я','Ь','Ы','Ъ','Э','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'), $str);
}

function put_back_href($href='')
{
 global $words;
 if ($href) $href='href="'.$href.'"'; else $href='href="javascript:history.go(-1);" mce_href="javascript:history.go(-1)"';
 return "<footer class=\"back-div\"><a {$href}>{$words->_('back_href', '&laquo; назад')}</a></footer>";
}

function put_user_content($content, $strip_tags=false)
{
 if (!$strip_tags) $replace=array('<br>','<br>','<b>','</b>','<i>','</i>','<u>','</u>','<a href="$1" target="_blank">','</a>','<div class="quote">','</div>','<img src="$1" />','<span style="color:$1">','</span>','<span style="font-size:$1%">','</span>','<del>','</del>'); else $replace=''; 
 $result=preg_replace(array("/".chr(10).chr(13)."/","/\n/",'/\[b\]/','/\[\/b\]/','/\[i\]/','/\[\/i\]/','/\[u\]/','/\[\/u\]/','/\[url=([^\]]+)\]/','/\[\/url\]/','/\[quote\]/','/\[\/quote\]/','/\[img\]([^\]]+)\[\/img\]/','/\[color=([^]]+)\]/', '/\[\/color\]/','/\[size=([^]]+)\]/', '/\[\/size\]/','/\[del\]/','/\[\/del\]/'), $replace, $content);

 if (strpos($result, '<img src')!==false)
 {
  preg_match_all("/<img src=\"([^\"]+)\"/", $result, $images);
  
  foreach ($images[1] as $image)
  {
   if ($imagesize=@getimagesize($image) and $imagesize[0]>495) $result=preg_replace("/<img src=\"".str_replace('/', "\/", $image)."\"/", "<img src=\"$image\" width=\"100%\"", $result);
  }
 }
 return $result;
}


function normal_date($strtime='',$show_time=false)
 {
  if ($strtime) $timestamp=strtotime($strtime); else $timestamp=time();
  if ($show_time) $add_time=' H:i'; else $add_time='';
  return date("d.m.Y$add_time",$timestamp);
 }


function test_auth($to_all=false, $mode='')
 {
  if ($mode=='manager')
  {
   if ($manager=get('sq','manager') and mysql_line("select `id` from `m_users` where `id`='$manager'")) return $manager; else redirect('index.php?page='.$page_url);
  }
  else
  {
   global $project_name;
   if ($login=get('sq',$project_name.'_login'))
   {
    if (!($mode=get('gs','mode')))
    {
     if (preg_match("/\/([^\/_]+)_/", $_SERVER['SCRIPT_NAME'], $mode)) $mode=$mode[1];
    }

    if (!is_array($_SESSION[$project_name.'_moduls'])) redirect('logout.php');

    if ($to_all or in_array('all', $_SESSION[$project_name.'_moduls']) or in_array($mode, $_SESSION[$project_name.'_moduls'])) return $login; else die('Ошибка доступа');
   }
   else redirect('index.php');
  }
 }

function rewrite_text_file($text,$path)
{
 if ($file=@fopen($path,'w'))
 {
  fwrite($file,$text);
  fclose($file);
  return true;
 } else return false;
}

function redirect($path, $time=0, $messgae = NULL)
 {
  header("Content-Type: text/html; charset=utf-8");
  echo "<META HTTP-EQUIV='Refresh' CONTENT='$time; URL=$path'>";
  
  if (is_null($messgae)) $messgae = "Сейчас вы будете переадресованы на $path";
  die($messgae);
 }

function image_resize($infile,$outfile,$neww,$newh,$quality)
 {
  $imagesize=getimagesize($infile);
  
  switch ($imagesize[2])
  {
   case 1: $im=@imagecreatefromgif($infile); break;
   case 2: $im=imagecreatefromjpeg($infile); break;
   case 3: $im=@imagecreatefrompng($infile); break;
   default: $im=false;
  }
  
  if ($im)
  {
   $k1=$neww/imagesx($im);
   $k2=$newh/imagesy($im);
   if ($k1>$k2) $k=$k2; else $k=$k1;

   $w=imagesx($im)*$k;
   $h=imagesy($im)*$k;

   $new=imagecreatetruecolor($w,$h);
   imagecopyresampled($new,$im,0,0,0,0,$w,$h,imagesx($im),imagesy($im));
   imagejpeg($new,$outfile,$quality);
   imagedestroy($im);
   imagedestroy($new);
  }
 }


function file_extension($file)
{
 $file=explode('.',basename($file));
 return count($file)>1 ? array_pop($file) : '';
}

function truncate_text($s,$n,$tail='...')
{
 if (strlen($s)>$n)
 {
  $i=$n-1;
  while ($i>=0 and $s{$i}<>' ') $i--;
  if ($i>=0) $s=mb_substr($s,0,$i+1, 'utf-8').$tail;
  else $s='';
 }
 return $s;
}


 function upload_file($id,$path='',$new_name=false,$new_extension=false,$extensions=false,$size_limit=false)
{
 $result=false;

 global $accepted_ext;
 if (!$extensions) $extensions=implode(',',$accepted_ext);
 
 if (array_key_exists($id,$_FILES) and !$_FILES[$id]['error'] and is_uploaded_file($_FILES[$id]['tmp_name']))
 {
  $file=explode('.',safe_file_name($_FILES[$id]['name']));
  $extension=count($file)>1 ? array_pop($file) : '';
  $name=implode('.',$file);
  if ((!$extensions or in_array(strtolower($extension),explode(',',$extensions))) and (!$size_limit or $_FILES[$id]['size']<=$size_limit))
  {
   if ($new_name)
    switch ($new_name)
    {
     case '/l': $name=strtolower($name); break;
     case '/u': $name=strtoupper($name); break;
     default: $name=$new_name;
    }
   if ($new_extension)
    switch ($new_extension)
    {
     case '/l': $extension=strtolower($extension); break;
     case '/u': $extension=strtoupper($extension); break;
     default: $extension=$new_extension;
    }
   if ($filename=$name.($extension ? '.'.$extension : ''))
   {
    if ($path and $path{strlen($path)-1}!='/') $path.='/';
    $path.=$filename;
    if (file_exists($path)) unlink($path);
    if (copy($_FILES[$id]['tmp_name'],$path))
    {
     chmod($path,0666);
     $result=$filename;
    }
   }
  }
 }
 return $result;
}

function select_file_name($id, $dir, &$ext, $file_name = null)
 {
  if (is_null($file_name)) $file_name = $_FILES[$id]['name'];

  $ext=file_extension($file_name);
  $file_name=md5($file_name);

  while (file_exists("$dir/$file_name.$ext")) $file_name=md5($file_name);

  return $file_name;
 }

function upload_image($image_url, $dir='admin/project_images/')
{
 $ext=file_extension($image_url);
 $file_name=md5($image_url);

 while (file_exists("{$dir}{$file_name}.{$ext}")) $file_name=md5($file_name);

 if (copy($image_url, $result=$dir.'/'.$file_name.'.'.$ext))
 {
  chmod($result, 0666);
  return $file_name.'.'.$ext;
 }
 else return false;
}

function makeurl($url)
 {
  if ($url)
   {
    if (substr($url,0,7)!='http://' and substr($url,0,8)!='https://' and substr($url,0,9)!='index.php') $url='http://'.$url;
   }
  else $url='#';  
  return $url;
 }
 
function print_table_header($columns=array(), $width=array(), $style='')
 {
  if ($style) $style=" style=\"$style\"";
  echo "<table class=\"table2\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\"$style>
         <tr>";
  foreach ($columns as $index => $column)
   {
    if ($width[$index]) $column_width=" width=\"{$width[$index]}\""; else $column_width='';
    echo "<th{$column_width}>{$column}</th>";
   }
  echo "</tr>";
 }
 
function print_table_bottom()
 {
  echo '</table>';
 }
 
function tr_class($index)
 {
  if (($index%2)==1) $class=" class='even_tr'"; else $class='';
  return $class;
 }
 
function print_button($caption, $onclick='', $style='')
 {
  if ($onclick) $button_onclick=" onclick=\"$onclick\"";  else $button_onclick='';
  if ($style) $button_style=" style='$style'"; else $button_style='';
  echo "<div id=\"button_div\"><input class=\"sh_button\" type=\"button\" value=\"$caption\"{$button_onclick}{$button_style}></div>";
 }

/**
 * Отправка почты
 * @param array|string $to
 * @param string $subject
 * @param string $text
 * @param string|null $sender_title
 * @param string|null $sender_email
 * @param array $attachments
 * @return bool
 */
function send_mail($to, $subject, $text, $sender_title = null, $sender_email = null, $attachments = array())
{
 global $classes_dir;
 require_once $classes_dir.'PHPMailer/PHPMailerAutoload.php';
 
 if (empty($sender_title) or empty($sender_email))
 {
  $settings = mysql_line_assoc("select * from `settings`");
  if (empty($sender_title)) $sender_title = $settings['sender_title'];
  if (empty($sender_email)) $sender_email = $settings['sender'];
 }
 
 if (!is_array($to)) $to = (array)$to;
 
 $text = '<!DOCTYPE html>
          <html>
           <head>
            <style type="text/css" media="all">
             p { margin: 0 0 5px; }
             table { width: 100%; border-collapse: collapse; }
             table td, table th { padding: 3px 5px; border: 1px solid #333; }
            </style>
           </head>
           <body>'.$text.'</body>
          </html>';
 
 $mail = new PHPMailer;
 
 if (!empty($settings['smtp_host']) and !empty($settings['smtp_port']) and !empty($settings['smtp_username']) and !empty($settings['smtp_pass']))
 {
  $mail->isSMTP();
  $mail->SMTPAuth   = true;
  $mail->Host       = $settings['smtp_host'];
  $mail->Port       = $settings['smtp_port'];
  $mail->Username   = $settings['smtp_username'];
  $mail->Password   = $settings['smtp_pass'];
  $mail->Timeout = 90;
 } 
 else $mail->isMail();
 
 $mail->isHTML(true);
 $mail->CharSet = 'utf-8';
 $mail->From = $sender_email;
 $mail->FromName = $sender_title;
 $mail->ReturnPath = $sender_email;
 $mail->Subject = $subject;
 $mail->Body = $text;
 
 foreach ($to as $email) $mail->addAddress($email);
 
 foreach ($attachments as $attachment)
 {
  if (!empty($attachment))
  {
   if (is_array($attachment)) $mail->addAttachment($attachment['file'], $attachment['filename']); else $mail->addAttachment($attachment);
  }
 }
 
 return $mail->send();
}

function json($x)
{
 if (is_null($x)) return 'null';
 if (is_bool($x)) return $x ? 'true' : 'false';
 if (is_int($x) or is_float($x)) return $x;
 if (is_string($x)) return '"'.str_replace(
  array("\\", "\n", "\t", "\r", "\b", "\f", "\""),
  array("\\\\", "\\n", "\\t", "\\r", "\\b", "\\f", "\\\""),$x).'"';
 if (is_array($x))
  if (count($x))
  {
   $values=array();
   if (array_keys($x)===range(0,count($x)-1))
   {
    foreach ($x as $value)
     array_push($values,json($value));
    return '['.implode(', ',$values).']';
   }
   else
   {
    foreach ($x as $key=>$value)
     array_push($values,json($key).': '.json($value));
    return '{'.implode(', ',$values).'}';
   }
  }
  else return '[]';
 else return '""';
}


function remove_extension($file)
{
 $file=explode('.',basename($file));
 if (count($file)>1) array_pop($file);
 return implode('.',$file);
}


function imageexist($filename,$width=0,$height='',$style='',$none='Отсутствует',$dir='project_images',$add='')
 {
  if (strpos(strtolower($_SERVER['PHP_SELF']),'admin/')) $imagephp='image.php';
   elseif (strpos(strtolower($_SERVER['PHP_SELF']),'clients/')) $imagephp='../admin/image.php';
    else $imagephp='admin/image.php';
  if ($filename)
   {
    if ($style) $style=" style='$style'"; else $style='';
    if ($add) $add=' '.$add;
    $result="<img src='$imagephp?file={$dir}/{$filename}&width=$width&height=$height'{$style}{$add}>";
   }
  else $result=$none;
  
  return $result;
 }
 
function imagedelexist($filename,$href = null,$width=0,$height='',$style='',$none='Отсутствует',$dir='project_images',$inputname='image')
 {
  if ($filename) $result='<div style="display: inline-block; text-align: center;">
                           <div>'.imageexist($filename,$width,$height,$style,$none,$dir).'</div>
                           <input type="checkbox" name="delete_image_'.$inputname.'" value="1" title="Удалить">
                          </div>';
   else
    {
     if ($style) $style=" style='$style'"; else $style='';
     $result="<div><div>Загрузите собственный файл:</div><input type='file' name='$inputname'$style></div>
              <div style='margin-top: 5px;'><div>Или добавьте удаленный файл по ссылке:</div><input type='url' name='{$inputname}_file_url' style='width: 95%;'></div>";
    }
  
  return $result;
 }
 
function check_s_creat_mode(&$creat_mode, &$id)
 {
  $creat_mode=get('gs','creat_mode');
  if ($creat_mode=='add' or ($creat_mode=='edit' and $id=get('gi','id'))) return true; else return false;
 }
 

function query_add($fileid='image',$mysql_column='image',$dir='project_images', $new_width=0, $new_height=0, $quality=0, &$filename='')
 {
  $file_url = get('ps', $fileid.'_file_url');
  if (!empty($file_url))
  {
   global $accepted_ext;
   
   if (in_array(file_extension($file_url), $accepted_ext) and $filename = upload_image($file_url, $dir)) $result = ", `$mysql_column` = '$filename'";
  }
  elseif ($_FILES[$fileid] and $filename=select_file_name($fileid,$dir,$ext) and $ext and upload_file($fileid,$dir,$filename,$ext,''))
  {
   $result=", `$mysql_column`='$filename.$ext'";
   $filename=$filename.'.'.$ext;
  }
  else
  {
   $result='';
   $filename='';
  }

  if (!empty($result) and $new_width and $new_height and $quality) image_resize($dir.'/'.$filename.'.'.$ext, $dir.'/'.$filename.'.'.$ext, $new_width, $new_height, $quality);
  
  return $result;
 }
 
function title_description_keywords(&$title, &$description, &$keywords)
 {
  $line=filter_html(mysql_line("select `title`, `description`, `keywords` from `settings`"));
  if (!mysql_error())
   {
    if ($line[0]) $title="<title>{$line[0]}</title>"; else $title='';
    if ($line[1]) $description="<meta name=\"Description\" content=\"{$line[1]}\">"; else $description='';
    if ($line[2]) $keywords="<meta name=\"Keywords\" content=\"{$line[2]}\">"; else $keywords='';
    return true;
   }
  else return false;
 }

 
function delete_content_images($q_mode)
 {
  $data=mysql_data("select `image` from `content_images` where `mode`='$q_mode'");
  if (strpos(strtolower($_SERVER['PHP_SELF']),'shop/')) $dir='../admin/'; else $dir='';
  foreach ($data as $line) unlink("{$dir}content_images/{$line[0]}");
  mysql_query("delete from `content_images` where `mode`='$q_mode'");
 }
 
function put_content($content)
{
 $result=str_replace('"image.php?file=admin/content_images/','admin/image.php?file=content_images/',str_replace('"content_images','admin/content_images',  str_replace('../image','image',$content)));
 
 if (!empty($result)) $result = '<div class="content">'.$result.'</div>';
 
 return $result;
}
 
function lang_selector()
 {
  global $lang_array;
  if (count($lang_array)>1)
  {
   $result="
   <div class='lang_selector'>
    <table cellpadding='3' cellspacing='3'>
     <tr>";

   foreach ($lang_array as $lang_value)
   {
    $result.="<td ".($lang_value==reset($lang_array) ? ' class="active"' : '')." id='{$lang_value}_button' onclick='change_lang(\"{$lang_value}\");'>".str2upper($lang_value)."</td>";
   }

   $result.="
     </tr>
    </table>
   </div>";
  }
  else $result='';
  
  return $result;
 }

function translit($str)
 {
  $normal_words = Array('А','а','Б','б','В','в','Г','г','Ґ','ґ','Д','д','Е','е','Є','є','Ж','ж','З','з','И','и','І','і','Ї','ї','Й','й','К','к','Л','л','М','м','Н','н','О','о','П','п','Р','р','С','с','Т','т','У','у','Ф','ф','Х','х','Ц','ц','Ч','ч','Ш','ш','Щ','щ','Ю','ю','Я','я','Ь','ь','Ы','ы',' ','Ъъ','-','Э','э');
  $translate_words = Array('a','a','b','b','b','v','gh','gh','g','g','d','d','e','e','je','je','zh','zh','z','z','i','i','i','i','ji','ji','j','j','k','k','l','l','m','m','n','n','o','o','p','p','r','r','s','s','t','t','u','u','f','f','kh','kh','c','c','ch','ch','sh','sh','shh','shh','ju','ju','ja','ja','j','j','y','y','-','','-','e','e');
  $result='';
  
  
  for($i=0;$i<mb_strlen($str,'utf8');$i++)
   {
    //echo $word_index=(array_search($str[$i],$normal_words)).' ';
    //echo ord($str[$i]).' ';
    $char=mb_substr($str, $i, 1, 'utf8');
   
    if (($word_index=(array_search($char, $normal_words))) or $word_index===0)
     $result.=$translate_words[$word_index];
    else
     if (($code=ord($char)) and (($code>=48 and $code<=57) or ($code>=65 and $code<=90) or ($code>=97 and $code<=122))) $result.=strtolower($char);
   }
  return $result;
 }
 

function put_main_form($line,$tr_array,$add_action_1='',$top_add='',$action_file='',$first_column_width='15%')
{
 global $mode;
 global $lang_array;
 global $prime_lang;
 $creat_mode=get('gs','creat_mode');
 $result = '';
 
 if ($creat_mode=='add' or ($creat_mode=='edit' and $id=get('gi','id')))
 {
  if ($creat_mode=='add')
  {
   $add_action='';
  }
  else
  {
   $line=filter_html($line);
   $add_action="&id=$id";
  }
  
  if (!($action_file)) $action_file=$mode.'_edit.php';
  
  $result="$top_add<form name='main' enctype='multipart/form-data' action='{$action_file}?creat_mode={$creat_mode}{$add_action}{$add_action_1}' method='post' style='margin-bottom:10px'>
            <table class='table2' cellpadding='0' cellspacing='0' border='0' style='margin-bottom:10px'>";
         
  
  $index=0;
  $required_index=1;
  $langs_index=array();
  
  foreach ($tr_array as $tr_caption => $tr_input_array)
  {
   if ($tr_input_array[0])
   {
    $td=$tr_input_array[0];
   }
   else
   {
    if (!($name=$tr_input_array[2])) $name='field_'.($index+1);
    if ($tr_input_array[3])
    {
     $id=" required";
     $tr_caption.='<sup style="color:#f00;">*</sup>';
    } else $id='';
    if (!($value=$tr_input_array[4])) $value=$line[$index];
    if (!($width=$tr_input_array[5])) $width='95%';
    if ($input_add=$tr_input_array[7]) $input_add=' '.$input_add;
    
    if ($type=$tr_input_array[1] and ($type=='textarea' or $type=='tinymce'))
    {
     if ($type=='tinymce') $class=" class='mceEdvanced'; style='width:95%; height:400px'"; else $class='';
     $td="<textarea$class name='$name'{$id}{$input_add}>$value</textarea>";
    }
    elseif ($type=='date')
    {
     $td="<input type='text' value='".($value==0 ? '' : $value)."' name='$name' style='width:150px'{$id}{$input_add}> <input type='button' style='background: url(\"../js/datepicker/datepicker.jpg\") no-repeat; width: 30px; height: 17px; border: 0px;' onclick='displayDatePicker(\"$name\", false, \"ymd\", \"-\");'>";
    }
    elseif ($type=='checkbox')
    {
     $td="<input type='checkbox' value='1' name='$name'".($value ? ' checked' : '')."{$id}{$input_add}>";//checked!!
    }
	elseif ($type == 'image' or preg_match("/^image\[([\d]+)\]\[([\d]+)\]$/", $type, $image_size))
	{
	 if ($type != 'image')
	 {
	  $width = $image_size[1];
	  $height = $image_size[2];
	 }
	 else
	 {
	  $width = 200;
	  $height = 200;
	 }
	 
	 $td = imagedelexist($value, '', $width, $height, '', '', 'project_images', $name);
	 if (isset($tr_input_array[5]) and !empty($tr_input_array[5]) and !empty($value)) $td = '<div style="display: inline-block; padding: 5px; background: '.$tr_input_array[5].';">'.$td.'</div>';
	}
    else $td="<input type='$type' name='$name' value='$value' style='width:{$width};'{$id}{$input_add}>";
   }
   
   if ($tr_add=$tr_input_array[6]) $tr_add=' '.$tr_add;
   
   
   foreach ($lang_array as $lang)
   {
    if (strpos($tr_caption, "($lang)")!==false) $tr_add.=" class='{$lang}_tr'";
   }
   
   $result.="<tr{$tr_add}>
              <td width='$first_column_width'>$tr_caption</td>
              <td>{$td}</td>
             </tr>";
   $index++;
  }
  
  $result.=" </table>
             <input type='submit' class='sh_button' id='main_submit' value='Сохранить'>
            </form>";
 }
 return $result;
}

function put_top_form($hidden_fields_array,$caption,$name,&$value,$data,$option_caption_divider=' - ',$replace_array='',$caption_start=1)
{
 global $mode;
 $hidden_fields='';
 foreach ($hidden_fields_array as $hidden_field) $hidden_fields.="<input type='hidden' name='".htmlspecialchars($hidden_field[0],ENT_QUOTES)."' value='".htmlspecialchars($hidden_field[1],ENT_QUOTES)."'>";
 
 $options='';
 $value=get('gi',$name,$data[0][0]);
 foreach ($data as $line)
 {
  $option_caption='';
  $count=count($line);
  for ($i=$caption_start;$i<$count;$i++)
  {
   if ($option_caption!=='') $option_caption.=$option_caption_divider;
   $option_caption.=$line[$i];
  }
 
  if ($line[0]==$value) $checked=' selected'; else $checked='';
  
  if ($replace_array) $option_caption=str_replace($replace_array[0], $replace_array[1], $option_caption);
  
  $options.="<option value='{$line[0]}'$checked>$option_caption</option>";
 }
 
 $result="<form name='top_form' action='admin.php?mode=$mode' method='get' style='margin-bottom:20px'>
           $hidden_fields
           <div style='text-align:left; padding:0px 0px 5px 5px; width:95%'>$caption</div>
           <select name='$name' style='width:95%' onchange='document.top_form.submit();'>$options</select>
          </form>";
 
 return $result;
}

function put_edit_buttons($id,$delete_caption='запись',$add='',$delete_script='',$hide_edit = false, $hide_delete = false)
{
 global $mode;
 if (!($delete_script)) $delete_script=$mode;
 
 $result = '';
 if (!$hide_edit) $result .= "<a href='admin.php?mode=$mode&creat_mode=edit&id=$id{$add}' title='Редактировать'><img src='images/edit.gif'></a> ";
 if (!$hide_delete) $result .= "<a href='#' onclick='if (confirm(\"Вы действительно хотите удалить ".htmlspecialchars($delete_caption,ENT_QUOTES)."?\")) location.href=\"{$delete_script}_edit.php?creat_mode=delete&id=$id{$add}\";'><img src='images/delete.gif' title='Удалить'></a> ";
 
 return $result;
}

function delete_image($tablename, $where, $delete_from_db=true, $field='image', $dir='project_images/')
{
 if ($image=mysql_line("select `{$field}` from `{$tablename}` where $where") and $image[0]) @unlink($dir.$image[0]);
 if ($delete_from_db) mysql_query("update `{$tablename}` set `{$field}`='' where $where");
 return mysql_error();
}

function mysql_langs_set($fields)
{
 global $lang_array;
 $set='';
 foreach ($lang_array as $lang)
 {
  foreach ($fields as $field) $set.=", `{$field}_{$lang}`='".get('pq', $field.'_'.$lang)."'";
 }
 
 return $set;
}

function mysql_langs_select($fields)
{
 global $lang_array;
 $select='';
 
 foreach ($lang_array as $lang)
 {
  foreach ($fields as $field) $select.=", `{$field}_{$lang}`";
 }
 
 return $select;
}

function put_404()
{
 header('HTTP/1.1 404 Not Found');
 require_once '404.php';
 die();
}

function price_format($price)
{
 return number_format($price, 0, '.', '');
}

function main_form_fields_array($langs_array, $input, $additions=array())
{
 $result=array();
 $count=count($input);
 $show_all_langs = FALSE;
 
 if ($count == 1)
 {
  $input_ = reset($input);
  if ($input_[0] != 'tinymce') $show_all_langs = TRUE;
 }
 
 foreach ($langs_array as $lang)
 {
  foreach ($input as $field_caption => $field_attr)
  {
   $result[$field_caption.' '.($show_all_langs ? '[' : '(').$lang.($show_all_langs ? ']' : ')')]=array_merge(array(null, $field_attr[0], $field_attr[1].'_'.$lang), array_key_exists($field_attr[1].'_'.$lang, $additions) ? $additions[$field_attr[1].'_'.$lang] : array()) ;
  }
 }
 
 return $result;
}

function url($url, $lang='', $fullUrl = false)
{
 if (mb_strpos($url, 'http://', null, 'utf-8') === 0 or mb_strpos($url, 'https://', null, 'utf-8') === 0) return $url;
 if ($url == 'home' or $url == '/home' or $url == 'start' or $url == '/start') $url = '';

 global $prime_lang;

 if (!$lang) {
  $appHelper = applicationHelper::getInstance();
  $lang = $appHelper->getLang();
 }

 $result = '';
 if ($lang != $prime_lang) $result = '/'.$lang;
 if ($result == '' and $url == '') $url = '/';

 $result = $result.((mb_strlen($url)==0 or mb_substr($url, 0, 1) == '/') ? '' : '/').$url;
 if ($fullUrl) {
	 global $base;
	 $result = mb_substr($base, 0, -1).$result;
 }
	
 return $result;
}

function get_modes(&$lang, &$page_num)
{
 $str = @reset(explode('?', $_SERVER['REQUEST_URI']));
 global $lang_array;
 
 if ($str=substr($str,1)) $array=explode('/',$str); else $array=array();
 
 if (count($array))
 {
  if (in_array($array[0], $lang_array)) { $lang=$array[0]; array_shift($array); }
  if (preg_match("/^page-([\d]+)$/", end($array), $page)) { $page_num = $page[1]; array_pop($array); }
 }
 else
 {
  global $prime_lang;
  $lang=$prime_lang;
 }
 
 $_SESSION['lang']=$lang;
 return $array;
}

function put_start_href($lang)
{
 global $prime_lang;
 return '/'.($lang!=$prime_lang ? $lang : '');
}

function put_lang_href($modes_array, $lang)
{
 global $prime_lang;
 if ($modes_array[0]=='start' and count($modes_array)==1) $modes_array=array();
 return ($lang==$prime_lang and count($modes_array)==0) ? '/' : url(implode('/', $modes_array), $lang);
}

function delete_project_images($mode, $id)
{
 mysql_delete('project_images', "`mode`='{$mode}__{$id}'", array('image'));
}

function project_image_select($mode, $id=0)
{
 return "(select `image` from `project_images` where `mode`=".($id ? "'{$mode}__{$id}'" : "concat('{$mode}__', `a_id`)")." order by `rate` desc limit 0, 1) as `image`";
}

function project_images_query($mode, $id)
{
 global $lang_array, $accepted_ext;
 $data=mysql_data("select `id` from `project_images` where `mode`='{$mode}__{$id}'");
 foreach ($data as $line)
 {
  if (get('pi','delete_project_image_'.$line[0])) mysql_delete('project_images', "`id`='{$line[0]}'", array('image'));
  else
  {
   $set="`rate`='".get('pi',"project_image_{$line[0]}_rate")."'";
   foreach ($lang_array as $lang) $set.=", `caption_$lang`='".get('pq',"project_image_{$line[0]}_caption_".$lang)."'";
   mysql_update('project_images', $set, "`id`='{$line[0]}'");
  }
 }
 
 if (!empty($_FILES['new_project_image_file']))
 {
  if (is_array($_FILES['new_project_image_file']['name']))
  {
   foreach ($_FILES['new_project_image_file']['name'] as $index => $file_name)
   {
    if ($image = select_file_name(null, 'project_images', $ext, $file_name) and !empty($ext) and in_array(strtolower($ext), $accepted_ext))
    {
     $path = 'project_images/'.$image.'.'.$ext;
     
     if (move_uploaded_file($_FILES['new_project_image_file']['tmp_name'][$index], $path))
     {
      chmod($path,0666);

      $set = "`mode`='{$mode}__{$id}', `image`='{$image}.{$ext}', `rate`='".(int)$_POST['new_project_image_rate'][$index]."'";
      foreach ($lang_array as $lang) $set.=", `caption_$lang`='".safe($_POST['new_project_image_caption_'.$lang][$index])."'";

      mysql_insert('project_images', $set);
     }
    }
   }
  }
  elseif ($query_add=query_add('new_project_image_file'))
  {
   $set="`mode`='{$mode}__{$id}', `rate`='".get('pi','new_project_image_rate')."'".$query_add;
   foreach ($lang_array as $lang) $set.=", `caption_$lang`='".get('pq','new_project_image_caption_'.$lang)."'";
   mysql_insert('project_images', $set);
  }
 }
}


function project_images($mode, $id, $max_photos=0)
{
 if ($mode)
 {
  global $lang_array;
  $data=mysql_data("select `id`, `image`, `rate`".mysql_langs_select(array('caption')) ." from `project_images` where `mode`='{$mode}__{$id}' order by `rate` desc");
  
  if ($max_photos == 0 or count($data) < $max_photos)
  {
   $add_photo = "<tr>
                 <td><input type='file' name='new_project_image_file[]'></td>
                 <td>";

   foreach ($lang_array as $lang) $add_photo .= "<div style='margin-bottom:10px;'>
                                                 <div>$lang:</div>
                                                 <div><input type='text' name='new_project_image_caption_{$lang}[]' style='width:95%;'></div>
                                                </div>";

   $add_photo .= "</td>
                 <td><input type='text' name='new_project_image_rate[]' style='width:100px;'></td>
                 <td style='text-align:center;'>-</td>
                </tr>";
  }
  else $add_photo = '';
  
  $result="<script>
            var add_photo_html = \"".safe($add_photo)."\", max_photos = {$max_photos};
           </script>
           <table class='table_2' cellpadding='0' cellspacing='0' border='0' style='width:100%;' id='project_images'>
            <thead>
            <tr>
             <th style='width:210px;'>Фото</th>
             <th>Названия</th>
             <th style='width:100px;'>Приоритет</th>
             <th style='width:100px;'>Удалить</th>
            </tr>
            </thead>
            <tbody>".$add_photo;
  
  if ($id)
  {
   foreach ($data as $line)
   {
    $result.="<tr>
               <td style='text-align:center;'>".imageexist($line[1], 200, 150)."</td>
               <td>";

    $index=0;
    foreach ($lang_array as $lang)
    {
     $result.="<div style='margin-bottom:10px;'>
                <div>$lang:</div>
                <div><input type='text' name='project_image_{$line[0]}_caption_$lang' style='width:95%;' value='".htmlspecialchars($line[$index+3], ENT_QUOTES)."'></div>
               </div>";
     $index++;
    }
    $result.=" </td>
               <td><input type='text' name='project_image_{$line[0]}_rate' style='width:100px;' value='{$line[2]}'></td>
               <td style='text-align:center;'><input type='checkbox' name='delete_project_image_{$line[0]}' value='1' title='Удалить'></td>
              </tr>";
   }
  }

  $result.='</tbody>
            </table>';
 }
 else $result='-';
 
 return array('Фото'=>array($result));
}

function meta_select_add($lang, $tablename='')
{
 if ($tablename) $tablename="`$tablename`.";
 return ", {$tablename}`description_$lang` as `description`, {$tablename}`keywords_$lang` as `keywords`, {$tablename}`title_$lang` as `title`";
}

function fill_query($variables)
{
 $set = '';
 foreach ($variables as $type => $array)
 {
  foreach ($array as $key)
  {
   $value = get('p'.$type, $key);
   if ($_POST[$key] == 'null') $value = 'null'; else {
    if ($type == 'i') $value = (int) $value;
    $value = "'{$value}'";
   }
   
   $set .= ", `$key`= $value";
  }
 }
 
 return $set;
}

function mysql_assoc_select($fields, $lang = null, $table_name = null)
{
 $result = '';
 if (!empty($fields))
 {
  if (is_null($lang))
  {
   global $prime_lang;
   $lang = $prime_lang;
  }
  
  if (!is_array($fields)) $fields = array($fields);
  
  foreach ($fields as $field) $result .= ", ".(!empty($table_name) ? "`$table_name`." : '')."`{$field}_{$lang}` as `{$field}`";
 }
 
 return $result;
}

/**
 * Генерує список чекбоксів на основі таблиці. Повертає рядок із html розміткою.
 * 
 * @param string $checkbox_name імена чекбоксів
 * @param string $caption_table таблиця, з якої потрібно вибирати список чекбоксів 
 * @param string $caption_field поле в таблиці із назвою чекбокса
 * @param string $data_table таблиця, в якій зберігаються дані про відмічені чекбокси
 * @param string $data_key_field поле, яке відповідає за вибраний елемент (id)
 * @param string $data_key_value значення вибраного елементу (id)
 * @param string $data_relation_key поле в таблиці, яке відповідає за зв'язок із таблицею, в якій містяться назви чекбоксів
 * @param string $order_by поля, за якими буде проведене сортування чекбоксів
 * @return string
 */
function data_checkboxes_field($checkbox_name, $caption_table, $caption_field, $data_table, $data_key_field, $data_key_value, $data_relation_key, $order_by = "`rate` desc")
{
 $result = '';
 $data = mysql_data_assoc("select `id`, `$caption_field` as `caption`, 
                                  (select `id` from `$data_table` where `$data_key_field`='$data_key_value' and `$data_relation_key`=`$caption_table`.`id`) as `checked`
                           from `$caption_table`".(!empty($order_by) ? " order by $order_by" : ''));
 
 foreach ($data as $line) $result .= "<li><label><input type='checkbox' name='{$checkbox_name}_{$line['id']}'".($line['checked'] ? ' checked' : '')."> {$line['caption']}</label></li>";
 
 return '<ul class="checkboxes">'.$result.'</ul>';
}


/**
 * Генерує список чекбоксів на основі таблиці. Повертає рядок із html розміткою.
 *
 * @param string $checkbox_name імена чекбоксів
 * @param string $caption_table таблиця, з якої потрібно вибирати список чекбоксів
 * @param string $caption_field поле в таблиці із назвою чекбокса. Залишено для збереження сумісності із сигнатурою попередньої функції
 * @param string $data_table таблиця, в якій зберігаються дані про відмічені чекбокси
 * @param string $data_key_field поле, яке відповідає за вибраний елемент (id)
 * @param string $data_key_value значення вибраного елементу (id)
 * @param string $data_relation_key поле в таблиці, яке відповідає за зв'язок із таблицею, в якій містяться назви чекбоксів
 * @return void
 */
function data_checkboxes_update($checkbox_name, $caption_table, $caption_field, $data_table, $data_key_field, $data_key_value, $data_relation_key)
{
 $data = mysql_data_assoc("select `id`, (select `id` from `$data_table` where `$data_key_field`='$data_key_value' and `$data_relation_key`=`$caption_table`.`id`) as `checked`
                           from `$caption_table`".(!empty($order_by) ? " order by $order_by" : ''));
 
 foreach ($data as $line)
 {
  $checked = get('ps',$checkbox_name.'_'.$line['id']);

  if (!$checked and $line['checked']) mysql_delete($data_table, "`id`='{$line['checked']}'");
  elseif ($checked and !$line['checked']) mysql_insert($data_table, "`$data_relation_key`='{$line['id']}', `$data_key_field`='$data_key_value'");
  
  echo mysql_error();
 }
}


/**
 * Генерирует select для формы
 * 
 * @param $select_name - параметр name селекта
 * @param $value - поточне значення
 * @param $data - масив, асоціативний масив або запит
 * @return string
 */
function form_select($select_name, $value, $data)
{
 if (is_string($data)) $data = mysql_data($data);
 
 if (is_array($data) and !empty($data))
 {
  $result = '';
  foreach ($data as $key => $line)
  {
   if (is_array($line) and count($line) > 1)
   {
    if (array_key_exists('value', $line)) $v = $line['value']; else $v = $line[0];
    if (array_key_exists('caption', $line)) $c = $line['caption']; else $c = $line[1];
   }
   else
   {
    $v = $key;
    if (is_array($line)) $c = reset($line); else $c = $line;
   }
   
   if (!empty($line)) $result .= "<option value='$v'".($v == $value ? ' selected' : '').">".strip_tags($c)."</option>";
  }
 }
 else return '-';
 
 return "<select name='$select_name' style='width: 250px;'>$result</select>";
}


function put_html_constructions()
{
 $model = new html_constructions();
 $data = $model->get_all();
 $json = array();
 $visibility = (bool)$_COOKIE['html_constructions_block_visible'];

 $result = '<ul class="checkboxes">';
 foreach ($data as $line)
 {
  $result .= "<li><a href='javascript:insert_html_construction({$line['id']});'>{$line['caption']}</a></li>";

  $json[$line['id']] = $line['html'];
 }
 $result .= '</ul>
             <div style="clear: both;"></div>
             <script type="text/javascript"> var html_constructions = '.json($json).'; </script>';


 return '<div class="html-constructions">
          <div><a href="javascript:void(0)" onclick="toggle_html_constructions($(this));"><span>'.(!$visibility ? 'Показать': 'Скрыть').'</span> HTML блоки для вставки</a></div>
          <div id="html-constructions-block"'.(!$visibility ? ' style="display: none;"' : '').'>'.$result.'</div>
         </div>';
}

/**
 * Возвращает HTML страницы
 * @param string $url
 * @return string
 */
function get_site_content($url)
{
 $ch = curl_init();
 curl_setopt($ch, CURLOPT_URL, $url);
 curl_setopt($ch,CURLOPT_RETURNTRANSFER, 1);
 curl_setopt($ch,CURLOPT_TIMEOUT, 10);
 curl_setopt($ch,CURLOPT_USERAGENT, 'Mozilla/5.0 (Ubuntu; X11; Linux i686; rv:8.0) Gecko/20100101 Firefox/8.0');
 curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

 $html=curl_exec($ch);
 curl_close($ch);
 return $html;
}

function rand_string($length)
{
 $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
 return substr(str_shuffle($chars),0,$length);
}

/**
 * Генерирует черкбоксы для выбора языковых версий
 * @param string $name
 * @param string|null $value
 * @return string
 */
function languages_checkboxes_ul($name = 'lang', $value = NULL)
{
 global $lang_array;

 if (is_null($value)) $value = ';'.implode(';', $lang_array).';';

 $result = '<ul class="checkboxes">';
 foreach ($lang_array as $lang)
  $result .= "<li style='width: 30%;'><label><input type='checkbox' name='{$name}_{$lang}'".(strpos($value, ';'.$lang.';') !== FALSE ? ' checked' : '')."> {$lang}</label></li>";
 $result .= '</ul>';

 return $result;
}

/**
 * Генерирует строку для передачи в запрос со значением из выбранных чекбосками языковых версий
 * @param string $name
 * @return string
 */
function languages_checkboxes_set($name = 'lang')
{
 global $lang_array;

 $result = ';';
 foreach ($lang_array as $_lang) if (get('ps', $name.'_'.$_lang)) $result .= $_lang.';';

 return $result;
}

?>