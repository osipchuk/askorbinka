<?php
/** @var programs $model */
$model = new $mode();
$tablename = $model->get_table_name();

$parent_factory = new parent_select_factory();
$select_printer = $parent_factory->get_select($model);

$top_form = new top_form_printer($select_printer);
echo $top_form->add_parameter('mode', $mode)->put();
$parent = $select_printer->get_value();
$data = $model->get_all_of_external_parent($parent);


if ($creat_mode=='add' or ($creat_mode=='edit' and $id and
    $line = mysql_line("select `external_parent`, `rate`, `date_begin`, `date_end`, `all_goods`, `type`, `bonus_type`, `image`, `good_image`, `static`
                        ".mysql_langs_select(array('caption','title','description','keywords','preview','text'))."
                                              from `$tablename` where `id`='$id'")))
{
 $parent_putter = $parent_factory->get_select($model)->set_not_null(FALSE)->set_name('external_parent')->set_value(array_key_exists(0, $line) ? $line[0] : $parent);
    
 echo put_main_form($line,
     array_merge(
         array('Размещение'=>array($parent_putter->put()),
               'Приоритет'=>array(null,'text','rate',0,null,'150px'),
               'Дата начала'=>array(null,'date','date_begin',1),
               'Дата окончания'=>array(null,'date','date_end',1),
               'Действует на все товары'=>array(null,'checkbox','all_goods'),
               'Тип программы'=>array((new select_simple_printer($model->getTypes()))->set_name('type')->set_value(isset($line) ? $line[5] : null)->put()),
               'Тип бонусов'=>array((new select_simple_printer($model->getBonusTypes()))->set_name('bonus_type')->set_value(isset($line) ? $line[6] : null)->put()),
               'Изображение'=>array(null,'image[200][300]','image'),
               'Изображение для страницы товара'=>array(null,'image[200][300]','good_image'),
               'Статический адрес'=>array(null,'text','static',1,null,null,null,'readonly')),
         main_form_fields_array($lang_array, array('Заголовок'=>array('text','caption'), 'Title'=>array('text','title'),
                 'Description'=>array('text','description'), 'Keywords'=>array('text','keywords'),
			     'Превью'=>array('tinymce','preview'), 'Текст'=>array('tinymce','text') ),
             array('caption_'.$prime_lang=>array(1,null,null,null,"onkeyup=\"make_static('$mode','$id', this.value); \"")))
	  
     ), "&mode=$mode&parent=$parent", lang_selector());

 require_once 'images.php';
}


print_button('Добавить',"location.href='admin.php?mode=$mode&creat_mode=add&parent=$parent'");
print_table_header(array('Изображение','Заголовок/текст','Тип','Даты проведения','Приоритет','Операции'),array(120,0,200,200,100,100));

foreach ($data as $index => $line)
{
 echo "<tr".tr_class($index).">
        <td style='text-align:center;'>".imageexist($line['image'],100,200)."</td>
        <td>
         <div class='article_caption'>{$line['caption']}</div>
         <div class='article_text'>{$line['preview']}</div>
        </td>
        <td style='text-align:center'>{$model->getTypes($line['type'])}</td>
        <td style='text-align:center'>{$line['date_begin']} - {$line['date_end']}</td>
        <td style='text-align:center'>{$line['rate']}</td>
        <td style='text-align:center'>".put_edit_buttons($line['id'],'программу',"&mode=$mode&parent=$parent")."</td>
       </tr>";
}
print_table_bottom();
?>
