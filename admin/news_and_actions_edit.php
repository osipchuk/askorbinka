<?php
require_once 'header.php';

if (!$mode) die('mode error');
$parent = get('gq', 'parent');

/** @var a_model $model */
$model = new $mode();
$tablename = $model->get_table_name();

if ($creat_mode=='add' or ($creat_mode=='edit' and $id))
{
 $set = mysql_langs_set(array('caption','title','text','description','keywords','label_text','preview')).
        fill_query(array(
         'd'=>array('date_begin', 'date_end'),
         'q'=>array('static', 'url'),
         'i'=>array('external_parent', 'action', 'new', 'double_label')
        )).query_add().query_add('image_label', 'image_label').query_add('image_label_small', 'image_label_small');

 if ($id=standart_edit($creat_mode, $tablename, $set, '', $id))
 {
  delete_query_image($tablename, "`id`='$id'", 'image_label', 'image_label');
  delete_query_image($tablename, "`id`='$id'", 'image_label_small', 'image_label_small');
 }
}

if ($creat_mode == 'delete_image' and $id) delete_image($tablename, "`id`='$id'");

if ($creat_mode=='delete' and $id)
{
 $deleter = new news_and_actions_deleter($model);
 $deleter->delete($id);
 redirect("admin.php?mode=$mode&parent=$parent");
}

redirect("admin.php?mode=$mode&parent=$parent&creat_mode=edit&id=$id");

?>
