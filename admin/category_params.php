<?php
$model = new category_params();
$tablename = $model->get_table_name();

$parent_factory = new parent_select_factory();
$select_printer = $parent_factory->get_select($model, FALSE);

$top_form = new top_form_printer($select_printer);
echo $top_form->add_parameter('mode', $mode)->put();
$parent = $select_printer->get_value();
$data = $model->get_all_of_external_parent($parent);

$categories_model = new categories();
$category = $categories_model->get_item("`id`='$parent'");
if (empty($category)) {
    die('category error');
}


if ($creat_mode=='add' or ($creat_mode=='edit' and $id and
                           $line = mysql_line("select `rate`, `type`".mysql_langs_select(array('caption'))." from `$tablename` where `id`='$id'")))
{
 $type_putter = new select_simple_printer($model::$types);
 
 if ($creat_mode == 'edit' and $line[1] == 'image')
 {
  $images_printer = new files_table_printer($model->get_param_images($id));
  
  $images = array('Изображения'=>array($images_printer->set_has_caption(TRUE)->add_file('image_small', 'Миниатюра', TRUE)->add_file('image', 'Изображение', TRUE)->put()));
 }
 else $images = array();
 
 echo put_main_form($line,
        array_merge(
                   array('Приоритет'=>array(null,'text','rate',0,null,'150px'),
					     'Тип'=>array($type_putter->set_disabled($creat_mode == 'edit')->set_name('type')->set_value(isset($line[1]) ? $line[1] : NULL)->put())
				   ),
                    main_form_fields_array($lang_array, array('Название'=>array('text','caption')),
                                          array('caption_'.$prime_lang=>array(1))),
		  			$images
                       ), "&mode=$mode&parent=$parent");
} elseif ($creat_mode == 'add_params_from_cat') {
    $printer = new select_tree_model_printer($categories_model);
    $printer->set_name('params_from_cat');
    
    echo "<form action='{$mode}_edit.php?mode=$mode&creat_mode=$creat_mode&parent=$parent' method='post'>
           <table class='table2' cellpadding='0' cellspacing='0' border='0' style='margin-bottom: 10px;'>
             <tr>
             <td style='width: 15%;'>Категория</td>
             <td>{$printer->put()}</td>
            </tr>
            </table>
            <input type='submit' class='sh_button' id='main_submit' value='Сохранить'>
            </form>";
}

if ($category['params_from_cat']) {
    $parent = $category['params_from_cat'];
    $cat_caption = '';
    $cat = ['parent' => $parent];
    while (!empty($cat)) {
        $cat = $categories_model->get_item("`{$categories_model->get_table_name()}`.`id`='{$cat['parent']}'");
        if (!empty($cat['caption'])) {
            $cat_caption = $cat['caption'] . (!empty($cat_caption) ? ' - ' . $cat_caption : '');
        }
    }
    
    echo "<div>
           Параметры для данной категории копируются с категории <a href='admin.php?mode=$mode&parent=$parent'>$cat_caption</a>
           <a href='#' onclick='if (confirm(\"Вы действительно хотите удалить привязку параметров категории?\")) location.href=\"{$mode}_edit.php?mode=$mode&creat_mode=delete_params_from_cat&parent={$category['id']}\";'><img src='images/delete.gif' title='Удалить привязку'></a>
          </div>";
    $data = $model->get_all_of_external_parent($parent);
} elseif (empty($data)) {
    print_button('Добавить привязку параметров к категории',"location.href='admin.php?mode=$mode&creat_mode=add_params_from_cat&parent=$parent'");
}

print_button('Добавить',"location.href='admin.php?mode=$mode&creat_mode=add&parent=$parent'");
print_table_header(array('Параметр','Тип','Приоритет','Операции'),array(0,200,100,100));

foreach ($data as $index => $line)
{
 echo "<tr".tr_class($index).">
        <td>{$line['caption']}</td>
        <td style='text-align:center'>{$model->type_caption($line['type'])}</td>
        <td style='text-align:center'>{$line['rate']}</td>
        <td style='text-align:center'>".put_edit_buttons($line['id'],'параметр',"&mode=$mode&parent=$parent")."</td>
       </tr>";
}
print_table_bottom();
?>
