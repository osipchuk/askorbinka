<?php
$global_settings = mysql_line_assoc("select * from `settings`");

$global_email=$global_settings['email'];
$title = $global_settings['title'];

$analytics=$global_settings['analytics'];

$social_links = array('vk'=>NULL, 'fb'=>NULL, 'od'=>NULL, 'gp'=>NULL, 'in'=>NULL, 'tw'=>NULL);
foreach ($social_links as $social => $value) $social_links[$social] = $global_settings[$social];

$register_social = array('vkontakte', 'facebook', 'googleplus', 'odnoklassniki', 'twitter', 'instagram');

$right_nav = array('brands', 'articles', 'news_and_actions', 'news', 'reviews', 'video_goods', 'programs', 'online_consultation', 'hot_line');

$sex_array = array('m'=>'мужской', 'w'=>'женский');


$pay_systems = array('card', 'liqpay', 'privat24', 'webmoney', 'terminal', 'check', 'pod', 'cash');
$payment_statuses = array(
    -1 => '<span style="color:#f00;">Отменен</span>',
    0 => 'Ожидает подтверждения',
    1 => '<span style="color:#050;">Подтверждена оплата</span>',
    3 => '<span style="color:#060;">Товар отправлен</span>',
    2 => '<span style="color:#070;">Успешен</span>',
    4 => '<span style="color:#050;">Подтвержден клиентом</span>',
    5 => 'Заказ по телефону',
    );

$image_types = array('jpg','jpeg','png','gif');

$classes_dir = __DIR__.'/../classes/';
$autoload_classes_dir = array($classes_dir, $classes_dir.'models/', $classes_dir.'models/form_models/', $classes_dir.'printers/', $classes_dir.'deleters/', $classes_dir.'exceptions/',
                              $classes_dir.'controllers/', $classes_dir.'importers_and_exporters/', $classes_dir.'payments/', $classes_dir.'amountModifiers/',
                              $classes_dir.'programs/');


$standard_modules = array('texts'=>'текстовая страница', 'news'=>'новости', 'brands'=>'бренды', 'articles'=>'статьи');
$extended_modules = array('html_constructions'=>'конструкции HTML', 'cron'=>'задания сron', 'auth'=>'авторизация', 'cart'=>'корзина', 'search'=>'результаты поиска',
	                      'categories'=>'каталог товаров', 'order'=>'оформление заказа', 'news_and_actions'=>'новинки и акции', 'reviews'=>'отзывы', 'profile'=>'личный кабинет пользователя',
                          'download_check'=>'Скачать счет-фактуру', 'videos'=>'Видео', 'video_goods'=>'Видео-обзор товаров', 'video_brands'=>'О брендах',
	                      'video_categories'=>'Полезно знать', 'video_simple'=>'Как заказать товар', 'brand_technologies'=>'технологии', 'discount_card' => 'Карточка скидки',
                          'bonuses_to_card_texts' => 'Вывод бонусов на карту', 'bonuses_to_goods_texts' => 'Обмен бонусов на товары', 'programs'=>'Программы лояльности');
/**
 * Модули, которые имеют подэленты, например статьи или новости 
 */
$items_modules = array('html_constructions'=>1, 'news'=>1, 'categories'=>'n', 'profile'=>1, 'brands'=>1, 'brand_technologies'=>1, 'news_and_actions'=>1, 'download_check'=>2,
					   'discount_card' => 1, 'programs' => 1, 'articles' => 1);

/**
 * Шаблоны, которые нужно отображать самостоятельно без обертки в контеннер
 */
$independentTemplates = array('default/404.tpl', 'default/ajax.tpl', 'default/html-construction-item.tpl', 'payment-check.tpl');

$client_account_types = array('cl'=>'Клиент', 'pr'=>'Профи');
$bonus_transaction_statuses = [
	-1 => '<span style="color: #c00;">Отменен</span>',
	'Новый',
	'<span style="color: #001bcc;">Отложен</span>',
	'<span style="color: #0b0;">Успешен</span>',
];

$files_dir = __DIR__.'/down_files/';
