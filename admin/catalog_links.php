<?php
/** @var a_model $model */
$model = new $mode();
$tablename = $model->get_table_name();

$parent_factory = new parent_select_factory();
$select_printer = $parent_factory->get_select($model);

$top_form = new top_form_printer($select_printer);
echo $top_form->add_parameter('mode', $mode)->put();
$parent = $select_printer->get_value();
$data = $model->get_all_of_external_parent($parent);

if ($creat_mode=='add' or ($creat_mode=='edit' and $id and
		$line = mysql_line("select `external_parent`, `rate`, `image`, `href`" . mysql_langs_select(array('caption')) . " from `$tablename` where `id`='$id'")))
{
 $parent_putter = $parent_factory->get_select($model)->set_not_null(FALSE)->set_name('external_parent')->set_value(array_key_exists(0, $line) ? $line[0] : $parent);
 
 echo put_main_form($line,
	 array_merge(array(
			 'Размещение' => array($parent_putter->put()),
			 'Приоритет' => array(null, 'text', 'rate', null, null, '150px'),
			 'Изображение' => array(null, 'image[100][100]', 'image'),
			 'Ссылка' => array(null, 'text', 'href', 1)),
		 main_form_fields_array($lang_array, array('Заголовок' => array('text', 'caption')))
	 ), "&mode=$mode&parent=$parent", lang_selector());
}


print_button('Добавить', "location.href='admin.php?mode=$mode&creat_mode=add&parent=$parent'");
print_table_header(array('Изображение','Название','Ссылка','Приоритет','Операции'),array(150,0,0,100,100));

foreach ($data as $index => $line)
{
 echo "<tr".tr_class($index).">
        <td style='text-align:center;'>".imageexist($line['image'], 100, 100)."</td>
        <td>
         <div class='article_caption'>{$line['caption']}</div>
         <div class='article_text'>".truncate_text(strip_tags($line['text']), 200)."</div>
        </td>
        <td>".(!empty($line['href']) ? $line['href'] : '-')."</td>
        <td style='text-align:center;'>{$line['rate']}</td>
        <td style='text-align:center;'>".put_edit_buttons($line['id'], 'ссылку', "&mode=$mode&parent=$parent")."</td>
       </tr>";
}
print_table_bottom();
?>
