<?php
require_once 'header.php';

$category = get('gi','category');
$parent = get('gi','parent');
if (!$mode) die('mode error');
if (empty($category)) die('category error');
if (empty($parent)) die('parent error');

$model = new good_tabs();
$tablename = $model->get_table_name();

if ($creat_mode=='add' or ($creat_mode=='edit' and $id))
{
 $set = mysql_langs_set(array('caption','text')).
     fill_query(array(
         'i'=>array('rate')
     )).query_add();
 
 if ($id = standart_edit($creat_mode, $tablename, $set, ", `external_parent`='$parent'", $id))
 {
  $printer = new files_table_printer($model->get_files($id));
  $printer->set_has_caption(TRUE)->add_file('file')->update($id, 'good_tab_files');
  
  $tech_printer = new files_table_printer($model->get_tech_images($id));
  $tech_printer->set_has_caption(TRUE)->set_name('technologies')->add_file('image_small', 'Миниатюра', 'TRUE')->add_file('image', 'Изображение', 'TRUE')->update($id, 'good_tab_tech_images');
 }
}

if ($creat_mode=='delete' and $id)
{
 $deleter = new tab_deleter($model);
 $deleter->delete($id);
 redirect("admin.php?mode=$mode&parent=$parent&category=$category");
}

if ($creat_mode=='delete_image' and $id) delete_image($tablename, "`id`='$id'");

redirect("admin.php?mode=$mode&creat_mode=edit&id=$id&parent=$parent&category=$category");
?>