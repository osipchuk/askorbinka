<?php
require_once 'header.php';

if (!$mode) die('mode error');
$parent = get('gq', 'parent');

$model = new brands();
$tablename = $model->get_table_name();

if ($creat_mode=='add' or ($creat_mode=='edit' and $id))
{
 $set = mysql_langs_set(array('caption','title','text','description','keywords','good_category')).
        fill_query(array(
         'q'=>array('static','url'),
         'i'=>array('external_parent','rate','country')
        )).query_add().query_add('image_large', 'image_large');

 if ($id=standart_edit($creat_mode, $tablename, $set, '', $id))
 {
  delete_query_image($tablename, "`id`='$id'", 'image_large', 'image_large');
  data_checkboxes_update('catalog_links', 'catalog_links', 'caption_'.$prime_lang, 'brands_catalog_links', 'external_parent', $id, 'link');

  $data = $model->get_links($id);
  foreach ($data as $line)
  {
   if (get('pi','delete_link_'.$line['id'])) mysql_delete($model->get_links_model()->get_table_name(), "`id`='{$line['id']}'");
   else
   {
	$set="`rate`='".get('pi',"link_{$line['id']}_rate")."'";
	foreach ($lang_array as $lang_) $set .= ", `caption_$lang_`='".get('pq',"link_{$line['id']}_caption_".$lang_)."', `url_$lang_`='".get('pq',"link_{$line['id']}_url_".$lang_)."'";
	mysql_update($model->get_links_model()->get_table_name(), $set, "`id`='{$line['id']}'");
   }
  }
  
  if (isset($_POST['new_link_rate']))
  {
   foreach ($_POST['new_link_rate'] as $index => $rate)
   {
	$set = "`rate`='".(int)$rate."', `external_parent`='{$id}'";
	
	foreach ($lang_array as $lang_) $set .= ", `caption_{$lang_}` = '".safe($_POST['new_link_caption_'.$lang_][$index])."'";
	foreach ($lang_array as $lang_) $set .= ", `url_{$lang_}` = '".safe($_POST['new_link_url_'.$lang_][$index])."'";
	
	mysql_insert($model->get_links_model()->get_table_name(), $set);
   }
  }
 }
}

if ($creat_mode == 'delete_image' and $id) delete_image($tablename, "`id`='$id'");

if ($creat_mode=='delete' and $id)
{
 $deleter = new brand_deleter($model);
 $deleter->delete($id);
 redirect("admin.php?mode=$mode&parent=$parent");
}

redirect("admin.php?mode=$mode&parent=$parent&creat_mode=edit&id=$id");

?>
