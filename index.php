<?php
session_start();
require_once 'admin/tools.php';
$main_db = db_connect();
require_once 'admin/global_settings.php';

require_once $classes_dir.'autoloader.php';
autoloader::attach($autoload_classes_dir);
require_once SMARTY_DIR.'Smarty.class.php';


$appHelper = applicationHelper::getInstance();
$app = applicationController::getInstance();
$words = translatesResolver::getInstance();


$app->displayTemplate();
?>