/**
 * Created by Nick on 04.03.14.
 */

function move_photo_to_cart(href)
{
 var good_id = href.data('add-to-cart'),
     image = $('[data-main-good-image="' + good_id + '"]');
 
 if (!image.length) image = href.parents('.item').find('[data-good-image="' + good_id + '"]');
 
 if (image.length)
 {
  $('body').append('<div class="move-to-cart-photo"><img src="' + image.attr('src') + '" alt=""></div>');
  var photo = $('.move-to-cart-photo'),
      cart = $('.main-nav .cart');
  
  photo.css({'left': image.offset().left + 'px', 'top': image.offset().top + 'px'});
  
  photo.animate({
   'left': (cart.offset().left + 40) + 'px',
   'top': (cart.offset().top + 10) + 'px',
   'width': '30px',
   'height': '30px',
   'opacity': .3
  }, 500, function() { photo.remove(); });
 }
}


function add_to_cart(href)
{
 var good = parseInt(href.attr('data-add-to-cart')),
     clear_cart_before = href.data('buy-one-good') ? 1 : 0;

 $.post('/ajax/cart_ajax/add_good', {'id': good, 'clear_cart_before': clear_cart_before},
  function(data)
  {
   var result = $.parseJSON(data);
   if (clear_cart_before) location.href = result.order_url;
   else
   {
	show_empty_cart_message(result.summary);
	$('[data-add-to-cart="' + good + '"]').addClass('active').html(cart_good_is_in_cart);
	move_photo_to_cart(href);
   }
  });
}

function change_items_count(item_id, count, height, size)
{
 if (item_id && count)
 {
  $.post('/ajax/cart_ajax/set_cart_items_count', {'item_id': item_id, 'count': count, 'height': height, 'size': size},
   function(data)
   {
    var result = $.parseJSON(data);
    if (result.status == 'ok') show_empty_cart_message(result.summary);
   });
 }
}

function delete_cart_item(item_id, href)
{
 if (item_id && confirm(cart_delete_item_confirm))
 {
  $.post('/ajax/cart_ajax/delete_item', {'item_id': item_id},
   function(data)
   {
    var result = $.parseJSON(data);
    if (result.status == 'ok')
    {
	 $('#cart-table tr[data-id="' + item_id + '"]').remove();
	 $('[data-add-to-cart="' + item_id + '"]').removeClass('active');
	 
	 if (href)
	 {
	  update_cart_counters(result.summary);
	  $('[data-add-to-cart="' + item_id + '"]').removeClass('active').html(cart_good_add_to_cart);
	 }
	 else show_empty_cart_message(result.summary);
    }
   });
 }
}

function clear_cart()
{
 if (confirm(cart_clear_confirm))
 {
  $.post('/ajax/cart_ajax/clear_cart', {},
   function(data)
   {
    var result = $.parseJSON(data);
    if (result.status == 'ok') show_empty_cart_message(result.summary); else alert(result.message);
   });
 }
}

function toggle_proccess_form()
{
 $('#process-form').slideToggle(200, function() {
  $('html, body').animate({ scrollTop: $("#process-form").offset().top }, 200);
 });
}

function update_cart_counters(summary)
{
 $('[data-cart-price]').each(function() { $(this).html(summary.price); });
 $('[data-cart-count]').each(function() { $(this).html(summary.count); });

 $('#cart-table tr[data-id]').each(function() {
  var price = Math.abs(parseFloat($(this).find('td[data-price]').html())),
   count = Math.abs(parseInt($(this).find('.change-count').val()));

  $(this).find('td[data-sum]').html(price * count);
 });
}

function show_empty_cart_message(summary, order)
{
 update_cart_counters(summary);
 if (summary.count == 0)
 {
  $('.cart-href').addClass('no-goods');
  
  if (!order)
  {
   $('.cart-section').html('<div class="content"><p>' + cart_no_goods + '</p></div>');
   $('html, body').animate({ scrollTop: 0 }, 200);
  }
 }
 else $('.cart-href').removeClass('no-goods')
}

function toggle_ship()
{
 if ($('#ship').length)
 {
  var ship = $('#ship').val(),
      ship_object;

  for (var i in ships) if (ships[i].id == ship) ship_object = ships[i];
  
  console.log(ship_object);
  
  if (ship_object.new_post == 1)
  {
   $('[data-proccess-form] [data-new-post-only]').show();
  }
  else
  {
   $('[data-proccess-form] [data-new-post-only]').hide(); 
  }
  
  if (ship_object.home_ship == 1) $('#process-form [data-ship="home"]').show();
  if (ship_object.company_ship == 1) $('#process-form [data-ship="company"]').show();
 }
}

function applyDiscountCodeToOrder() {
	var code = $('#discount-code'),
	 	form = code.parents('form');
	
	form.fadeTo(200, .5);
	
	$.post('/' + $('html').attr('lang') + '/ajax/cart_ajax/apply_discount_code', {'code': code.val()}, function(data) {
		form.fadeTo(200, 1);
		var result = $.parseJSON(data);
		if (result.status == 'ok') {
			$('#discount-info').html(result.discount_amount);
			$('#discount-code-id').val(result.code_id);
			$('[data-discount-info]').slideDown(200);
		} else {
			alert(result.message);
			$('#discount-code-id').val('');
			$('[data-discount-info]').slideUp(200);
		}
	});
}






$(function() {
 $(document).on('click', '[data-add-to-cart]', function(e) {
  e.preventDefault();
  if ($(this).hasClass('active')) delete_cart_item($(this).data('add-to-cart'), $(this)); else add_to_cart($(this));
  
 });


 $('#cart-table .change-count').change(function() {
   var tr = $(this).parents('tr');
   change_items_count(tr.attr('data-id'), $(this).val());
 });

 $('#cart-table .delete').click(function(e) {
  e.preventDefault();
  var tr = $(this).parents('tr[data-id]');
  delete_cart_item(tr.attr('data-id'));
 });
 
 $('[data-cart-clear]').click(function(e) {
  e.preventDefault();
  clear_cart();
 });

 $('[data-cart-process]').click(function(e) {
  e.preventDefault();
  toggle_proccess_form();
 });

 $('form[data-cart-form]').submit(function(e) {
  e.preventDefault();
  var form = $(this);

  form.fadeTo(200, .6);
  $.post(form.attr('action'), {'form': form.serialize()}, function(data) {
   var result = $.parseJSON(data);
   form.fadeTo(200, 1);

   switch (result.status)
   {
    case 'ok': $('[data-cart-form]').parent().html(result.message); $('html, body').animate({ scrollTop: 0 }, 200); show_empty_cart_message(result.summary, true); break;
    default: alert(result.message);
   }
  });

  return false;
 });
 
 $('[data-count-change]').click(function() {
  var input = $(this).parents('td').find('input.change-count'),
      tr = $(this).parents('tr'),
      count = parseInt(input.val());
  
  count += parseInt($(this).data('count-change'));
  
  if (count <= 0) count = 1;
  
  input.val(count);
  change_items_count(tr.attr('data-id'), count);
 });
 
 $('#ship').change(function() {
  toggle_ship();
 });

 toggle_ship();
	
	$('.goods-list .item[data-discount-make-href]').click(function(e) {
		if (!$(e.target).is('a')) {
			location.href = $(this).data('discount-make-href');
		}
	});
	
	$('[data-apply-discount-code]').click(function() {
		applyDiscountCodeToOrder();
	});

	$('#discount-code').keypress(function(e) {
		if (e.which == 13) {
			e.preventDefault();
			applyDiscountCodeToOrder();
		}
	});
});