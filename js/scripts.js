function get_hash()
{
 return location.hash.replace(/[#!]/gi, '');
}

function toggle_services_buttons(spoiler)
{
 spoiler.find('.item').each(function() {
  if ($(this).find('.collapse.in').length) $(this).addClass('active'); else $(this).removeClass('active');
 });
}

function set_spoilers_hash(spoiler)
{
 var static = spoiler.find('.item.active:last').attr('data-id');
 if (static == undefined) static = '';
 location.hash = '!' + static;
}

function has_li_subnav(li)
{
 if (li.children('ul').length) return true; else return false;
}


function register_response(result, form)
{
 form.fadeTo(200, 1);
 
 if (result.status == 'ok')
 {
  form.html(result.message);
 }
 else alert(result.message);
}




function auth_response(result, form)
{
 form.fadeTo(200, 1);

 if (result.status == 'ok') location.href = result.location; else alert(result.message);
}


function logout()
{
 $.post(logout_url, {}, function(data) {
  var result = $.parseJSON(data);
  
  switch (result.status)
  {
   case 'ok': location.href = location.href; break;
   default: alert(result.message);
  }
 });
}

function height_100_resize()
{
 if ($('.height-100').length > 0)
 {
  $('.height-100 .item').css('height', 'auto').removeClass('without-border');
  
  var prev_item = $('.height-100 .item'),
      max_height = prev_item.innerHeight(),
      items_count = 0;

  $('.height-100 .item').each(function() {
   if ($(this).innerHeight() > max_height) max_height = $(this).innerHeight();

   if ($(this).offset().top != prev_item.offset().top || $(this).parent().is(':last-child'))
   {
	if (items_count > 1 || $(this).parent().is(':last-child'))
	{
	 var offset_top = prev_item.offset().top;
	 $('.height-100 .item').each(function() { if ($(this).offset().top == offset_top) $(this).css('height', max_height + 'px'); });
	}
	
	max_height = $(this).innerHeight();
	items_count = 0;

	if (!$(this).parent().is(':last-child')) prev_item.addClass('without-border'); else $(this).addClass('without-border');
   }

   items_count++;
   prev_item = $(this);
  });
 }
}


function show_popup_message(message, block_id)
{
 var block = $('.popup-message[data-block-id="' + block_id + '"]');
 block.find('.message').html(message);
 block.css('top', ($(document).scrollTop() + 100) + 'px').fadeIn(200);
}

function hide_popup_message(block_id)
{
 $('.popup-message[data-block-id="' + block_id + '"]').fadeOut(200);
}

function close_comparison_table()
{
 $('.catalog-list-comparison').fadeOut(200, function() {
  $('.catalog-list').css('paddingTop', 0);
 });
}

function position_comparison_table()
{
 var comparisonBlock = $('.catalog-list-comparison'),
     scrollTop = $(document).scrollTop(),
     catalogBlock = $('.catalog .catalog-list');
 
 if ($('#popup-table-comparison:visible table tbody tr').length <= 0)
 {
  close_comparison_table();
  return false;
 }
 
 var lastTrHeight = $('#popup-table-comparison table tbody tr:last-child').height();
 
 if (scrollTop < ($('.catalog-list').offset().top + parseInt($('.catalog-list').css('paddingTop')) - lastTrHeight - parseInt(comparisonBlock.css('paddingBottom')) - parseInt(comparisonBlock.css('paddingBottom'))))
 {
  comparisonBlock.css('top', $('.catalog-list').offset().top + 'px');
  
  var paddingBefore = parseInt(catalogBlock.css('paddingTop'));
  catalogBlock.css('paddingTop', (comparisonBlock.innerHeight() + 10) + 'px');
  var paddingAfter = parseInt(catalogBlock.css('paddingTop'));
  
  if (paddingBefore < paddingAfter)
  {
   var scrollTo = $('.catalog-list').offset().top + parseInt($('.catalog-list').css('paddingTop')) - lastTrHeight - parseInt(comparisonBlock.css('paddingBottom')) - parseInt(comparisonBlock.css('paddingBottom'));
   $(document).scrollTop(scrollTo);
  }
 }
 else
 {
  comparisonBlock.css('top', (
   scrollTop -
   comparisonBlock.innerHeight() +
   lastTrHeight +
   parseInt(comparisonBlock.css('paddingBottom')) +
   parseInt(comparisonBlock.css('paddingTop')) -
   10
  ) + 'px');
 }
 
 return true;
}

function compare_change_good(href)
{
 var good_id = href.data('comparison'),
     mode = href.hasClass('active') ? 'remove' : 'add';
 $.post('/' + $('html').attr('lang') + '/ajax/comparison_ajax', {'mode': mode, 'good': good_id},
  function(data)
  {
   var result = $.parseJSON(data);
   
   if (result.status == 'ok')
   {
	$('[data-comparison="' + good_id + '"]').each(function() { mode == 'add' ? $(this).addClass('active') : $(this).removeClass('active'); });
	
	$('#popup-table-comparison').html(result.message);
    $('.catalog-list-comparison').fadeIn(200);
    position_comparison_table();
   }
   else alert(result.message);
  });
}

function position_right_nav()
{
 var dx = 0,
  	 nav = $('.right-nav'),
  	 navHeight = nav.height(),
     winHeight = $(window).height();
	 bodyHeight = $('body').height(),
	 scrollTop = $(window).scrollTop();
	
 if (winHeight > navHeight) {
	 dx = ($(window).height() - nav.height()) / 2;
 } else {
	 if (scrollTop >= (bodyHeight - winHeight)) {
		 dx = -200;
	 }
 }
	
	
 nav.css({'top': ($(this).scrollTop() + dx) + 'px'}, 200);
}




$(function() {
 $('form[data-ajax-form]').submit(function(e) {
  e.preventDefault();
  var form = $(this);
  
  form.fadeTo(200, .6);
  $.post(form.attr('action'), {'form': form.serialize()}, function(data) {
   var result = $.parseJSON(data),
	callback = form.attr('data-ajax-form-response'),
	fn = window[callback];


   if(typeof fn === 'function')
   {
	fn(result, form);
   }
   else
   {
	form.fadeTo(200, 1);

	switch (result.status)
	{
	 case 'ok': form.html(result.message); break;
	 default: alert(result.message);
	}
   }
  });

  return false;
 });


 $('#spoilers').on('hidden.bs.collapse', function () {
  toggle_services_buttons($(this));
 });

 $('#spoilers').on('shown.bs.collapse', function () {
  toggle_services_buttons($(this));
 });

 $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
  var hash = e.currentTarget.hash,
      scroll_top = $(window).scrollTop();
  location.hash = hash;
  
  $(window).scrollTop(scroll_top);
  
  //$('body, html').animate({'scrollTop': ($('a[data-toggle="tab"][href="' + hash + '"]').offset().top - 65) + 'px'}, 500);
 });

 
 var hash = location.hash;
 if (hash.length && $('a[data-toggle="tab"][href="' + hash + '"]').length)
 {
  var a = $('a[data-toggle="tab"][href="' + hash + '"]');
  a.click();
  
  $('body, html').animate({'scrollTop': (a.offset().top - 65) + 'px'}, 500);
 }
 

 
 $('[data-top-button]').click(function(e) {
  e.preventDefault();
  $('html, body').animate({'scrollTop': 0}, 200);
 });
 
 $('[data-logout]').click(function(e) { e.preventDefault(); logout(); });
 
 $('.catalog-list-comparison .close').click(function(e) {
  e.preventDefault();
  close_comparison_table();
 });
 
 $(document).on('click', '[data-comparison]', function(e) {
  e.preventDefault();
  compare_change_good($(this));
 });


 $(".goods-scroll").owlCarousel({
  autoPlay: 8000,
  stopOnHover: true,
  navigation: true,
  navigationText: false,
  pagination: false,
  lazyLoad: false,
  items: 5,
  scrollPerPage: true,
  slideSpeed: 800,
  itemsDesktop: [1199,4],
  itemsMobile: [479,1]
 });


 $('.popup-message [data-close]').click(function(e) {
  e.preventDefault();
  hide_popup_message($(this).parents('.popup-message').attr('data-block-id'));
 });
 
 height_100_resize();
 
 $(window).resize(function() {
  height_100_resize();
  position_right_nav();
  resize_profile_disabled_columns();
 });
 
 $('.rating.rating-editable .star').click(function() {
  var index = $(this).index();
  
  $(this).parents('form').find('#rating').val(index + 1);
  
  $(this).parent().find('.star').each(function() {
   if ($(this).index() <= index) $(this).addClass('active'); else $(this).removeClass('active');
  });
 });
 
 /*
 $('.categories-nav > .nav > .dropdown').hover(function() {
  $(this).siblings('.dropdown').removeClass('open'); 
  $(this).addClass('open');
 },
 function() {
  var dropdown = $(this);
  setTimeout(function() { if (!dropdown.is(':hover')) dropdown.removeClass('open'); }, 500);
  
 });
 */

 position_right_nav();
 
 $(document).scroll(function() {
  position_right_nav();
  position_comparison_table();
 });
 
 $('.news-and-actions-list .item').click(function() {
  var href = $(this).find('a[data-more]').attr('href');
  if (href) location.href = href;
 });
	
	$('[data-submit-buttton]').click(function(e) {
		e.preventDefault();
		$(this).parents('form').find('input[type="submit"]').click();
	});
	
 
 if ($('[data-scroll-content]').length) {
  var contentBlock = $('[data-scroll-content]'),
       scrollTop = contentBlock.offset().top;
  
  if (contentBlock.data('scroll-content') == 'without-animation') {
   $(document).scrollTop(scrollTop);
  } else {
   $("html, body").animate({ scrollTop: scrollTop }, 500);
  }
 }

 $('[data-phone-number]').mask(
  '+38N 00 0000000',
  {
      'translation': {
          'N': {pattern: '0'},
          'D': {pattern: /\d/},
          '9': {pattern: /\d/, optional: true},
          '#': {pattern: /\d/, recursive: true},
          'A': {pattern: /[a-zA-Z0-9]/},
          'S': {pattern: /[a-zA-Z]/}
      }
  });
});
