/**
 * Created by Nick on 01.10.14.
 */

function change_ship() {
	var ship_id = $('#ship').val(),
	 	count = ships.length,
	 	i = 0,
	 	text = '',
    $address = $('#address'),
	 	describe_block = $('[data-ship-describe]');
	if (ship_id === "4"){
    $address.attr('required', false);
  } else {
    $address.attr('required', true);
  }
	
	while (i < count && ships[i]['id'] != ship_id) i++;
	
	if (i < count) {
		text = ships[i].text;
	}
	
	if (text != null && text.length) {
		describe_block.html('<div class="content">' + text + '</div>').show();
	} else {
		describe_block.fadeOut(200);
	}
}



$(function() {
 $('.select-receiver .btn').click(function(e) {
  e.preventDefault();
  
  var other_receiver = $(this).data('other-receiver'),
      other_receiver_block = $('.other-receiver');
  
  $('#other-receiver').val(other_receiver);
  
  $(this).parents('.select-receiver').find('.btn').removeClass('active');
  $(this).addClass('active');
  
  if (other_receiver == 1) other_receiver_block.slideDown(200); else other_receiver_block.slideUp(200);
 });
	
	$('[data-ship]').change(function() { change_ship(); });
	
	change_ship();
});