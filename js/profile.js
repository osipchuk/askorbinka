function save_discount_code_response(result, form) {
	form.fadeTo(200, 1);
	var code_id = parseInt(result.code_id);

	if (result.status == 'ok' && code_id > 0) {
		$('#code-id').val(code_id);
		$('.column-right.disabled').removeClass('disabled');
		
		if (result.card_url != null) location.href = result.card_url;
	}

	alert(result.message);
}

function edit_profile_response(result, form) {
	form.fadeTo(200, 1);

	if (result.message != '') alert(result.message);
	if (result.status == 'ok') {
		location.href = location.href;
	}
}





function add_profile_disabled_columns() {
	$('.column.disabled').append('<div class="disabled-wrapper"></div>');
}

function resize_profile_disabled_columns() {
	$('.disabled-wrapper').each(function() {
		var parent = $(this).parent();
		$(this).innerWidth(parent.innerWidth());
		$(this).css({
			'marginLeft': (-parseInt(parent.css('paddingLeft'))) + 'px',
			'top': (parent.offset().top - parent.parents('.row').offset().top) + 'px'
		});
	});
}


function generate_code() {
	var words = '0123456789qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM',
		max_position = words.length - 1,
		length = 14,
	 	blocks = $('[data-discount-code]');
	
	blocks.html('').val('');
    $('#code-id').val('0');
	
	var generate = setInterval(function() {
		var value = blocks.val(),
		 	position = Math.floor(Math.random() * max_position);
		
		value += words.substring(position, position + 1);
		blocks.html(value).val(value);
		
		if (value.length >= length) {
			clearInterval(generate);
			disable_buttons();
		}
		
	}, 40);
}

function disable_buttons() {
	disabled = false;
	$('[data-generate-form] input, [data-generate-form] textarea').each(function() {
		if ($(this).attr('required') != undefined && $(this).val() == '') disabled = true;
	});

	if (disabled) {
		$('.column.column-right').addClass('disabled');
	} else {
		$('.column.column-right').removeClass('disabled');
	}
}



$(function() {
	$(document).on('click', '.disabled-wrapper', function() {
		var column = $(this).parents('.column'),
		 	index = column.hasClass('column-right') ? 3 : 2;
		alert(generate_code_disabled[index]);
	});

	$('[data-code-discount-form-change-visibility]').change(function() {
		var checked = $(this).is(':checked'),
		 block = $('[data-code-discount-form-visibility="' + $(this).data('code-discount-form-change-visibility') + '"]');

		if (checked) block.slideDown(200); else block.slideUp(200);
	});
	
	
	$('[data-generate-code-button]').click(function(e) {
		e.preventDefault();
		generate_code();
	});


	$('[data-generate-form] input, [data-generate-form] textarea').each(function() {
		$(this)[0].oninput = function() {
			disable_buttons();
		};
	});
	
	$('[data-send-controller]').click(function(e) {
		e.preventDefault();
		$('[data-generate-form] input[name="send_controller"]').val($(this).data('send-controller'));
		$('[data-generate-form] input[type="submit"]').click();
	});
	
	$('[data-show-all-statistic]').click(function(e) {
		e.preventDefault();
		
		$(this).parents('.sold-goods').find('.hidden').each(function() {
			$(this).removeClass('hidden');
			console.log($(this).html());
		});

		$(this).parents('.block-footer').remove();
	});
	
	if ($('#avatar-file-uploader').length) {
		var form = $('form.register');
		
		var uploader = new qq.FileUploader({
			element: document.getElementById("avatar-file-uploader"),
			action: avatar_upload_url,
			sizeLimit: 1048576,
			params: {},
			template : '<div class="qq-uploader"><div class="qq-upload-drop-area"><span>Перетяните файлы сюда.</span></div><a href="javascript:void(0);" class="btn btn-primary qq-upload-button">' + avatar_upload_button + '</a><ul class="qq-upload-list"></ul></div>',
			allowedExtensions: ["jpg", "jpeg", "gif", "png"],
			onSubmit: function(id, fileName) {
				form.fadeTo(200, .5);
			},
			onComplete: function(id, fileName, responseJSON) {
				if (responseJSON["status"] == 'ok') {
					$("#avatar-image").attr("src", "/admin/image.php?file=cache/" + responseJSON["filename"] + "&width=80&height=80");
					$("#avatar-filename").val(responseJSON["filename"]);
					
					form.submit();
				}
				form.fadeTo(200, 1);
			},
			onError: function(id, fileName, xhr) {
				form.fadeTo(200, 1);
			}
		});
	}
	

	add_profile_disabled_columns();
	resize_profile_disabled_columns();
	disable_buttons();

});