$(document).ready(function(){
 $(".show_photo").fancybox(
 {
  'titlePosition'	: 'inside',
  'transitionIn'	: 'elastic',
  'transitionOut'	: 'elastic',
  'showCloseButton' : true,
  'fitToView': true,
  'scrolling': 'no',
  'openEffect '	: 'elastic',
  'closeEffect'	: 'elastic',
  'nextEffect'	: 'elastic',
  'prevEffect'	: 'elastic',
   helpers:  {
    title : {
     type : 'inside'
    },
    overlay : {
     showEarly : false
    }
   }
 });

	$(".show_lg_photo").fancybox({
		'titlePosition'	: 'inside',
		'transitionIn'	: 'elastic',
		'transitionOut'	: 'elastic',
		'showCloseButton' : true,
		'fitToView': true,
		'scrolling': 'no',
		'openEffect '	: 'elastic',
		'closeEffect'	: 'elastic',
		'nextEffect'	: 'elastic',
		'prevEffect'	: 'elastic',
		helpers:  {
			title : {
				type : 'inside'
			},
			overlay : {
				showEarly : false
			},
			buttons: {}
		}
	});
 
 $(".various").fancybox({
  'titlePosition'	: 'inside',
  fitToView	: false,
  width		: '70%',
  height		: '70%',
  autoSize	: false,
  closeClick	: false,
  openEffect	: 'none',
  closeEffect	: 'none'
 });

});

