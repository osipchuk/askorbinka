/**
 * Created by Nick on 07.10.14.
 */

function submit_filters()
{
 var form = $('.catalog-filters');
 if (form.find('input[name="filter_brands[]"]:checked').length == 0) form.append('<input type="hidden" name="filter_brands[]" value="all">');
 form.submit();
}

function change_filter_location(change_param, value)
{
 var result = '?';
 
 for (var param in filter_params)
 {
  result += param + '=' + (change_param == param ? value : filter_params[param]) + '&';
 }
 
 location.href = location.pathname + result.substr(0, result.length - 1) + location.hash;
}



$(function() {

 $('form.catalog-filters input').change(function() {
  return submit_filters();
 });

 $('[data-filter-brand], [data-filter]').click(function(e) {
  e.preventDefault();
  var input = $(this).siblings('input');
  input.prop('checked', !input.prop('checked'));
  submit_filters();
 })
 
 $('[data-filter-all-brands]').click(function(e) {
  e.preventDefault();
  var form = $(this).parents('form');
  form.find('input[type="checkbox"]').prop('checked', false);
  submit_filters();
 });

 $("#filter-from").datepicker({
  maxDate: 0,
  onSelect: function (selectedDate) {
   $("#filter-to").datepicker("option", "minDate", selectedDate);
   change_filter_location('dateBegin', selectedDate);
  }
 });

 $("#filter-to").datepicker({
  maxDate: 0,
  minDate: $("#filter-to").datepicker( "getDate" ),
  onSelect: function (selectedDate) {
   $("#filter-from").datepicker("option", "maxDate", selectedDate);
   change_filter_location('dateEnd', selectedDate);
  }
 });

 $("#filter-from, #filter-to").datepicker( "option", {
  changeMonth: true,
  changeYear: true,
  dateFormat: "yy-mm-dd",
  beforeShowDay: function(date) {
   var date = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate(),
	   add_class = active_dates.indexOf(date) != -1 ? 'active-date' : '';
   
   return [true, add_class];
  }
 });
 

 
 $('.calendar .dropdown-menu').on({
  "click":function(e){
   e.stopPropagation();
  }
 });
 
 if (typeof filter_params != 'undefined')
 {
  if (filter_params.dateBegin != null) $("#filter-from").datepicker('setDate', filter_params.dateBegin);
  if (filter_params.dateEnd != null) $("#filter-to").datepicker('setDate', filter_params.dateEnd);
 }
 
 
 
 if (location.href.indexOf('filter_brands') != -1) $('html, body').animate({'scrollTop': $('.catalog-filters').offset().top - 20}, 200);

});