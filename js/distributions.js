$(document).ready(function() {
	
	var mhWidth = $('body').width();
	
	$('.page-services .map .region path').mouseenter(function() {
		$(this).css('fill', '#cacacb');
	});
	
	$('.page-services .map .region path').mouseleave(function() {
		$(this).css('fill', '#dfe0e0');
	});
	
	$('.page-services .map .city').mouseenter(function() {
		$('.region.' + $(this).data('region') + ' path').css('fill', '#cacacb');
	});
	
	$('.page-services .map .city').mouseleave(function() {
		$('.region.' + $(this).data('region') + ' path').css('fill', '#dfe0e0');
	});	
	
	$('.page-services .map .city').click(function() {
		loadServices($(this).data($(this).data('city') == undefined ? 'region' : 'city'));
	});

	$('.page-services .map .city[data-region="kiev"]').click();
	
	
	function loadDescription(cityName, isService, nearestCity) {
		var wrap = $('.page-services .description .service.' + (isService ? 'service-center' : 'regional-office') + ' .contacts');
		var key = isService ? 'service' : 'office';
	    var nearest_city_title = wrap.parents('.service').find('.type .title .nearest-city');
		wrap.find('.address .title').text(city[cityName][key].address);
		wrap.find('.tel .title').text(city[cityName][key].tel);
		wrap.find('.map').attr('href', city[cityName][key].map);
	 
	 if (nearestCity)
	 {
	  nearest_city_title.find('.nearest-city-name').html(city[cityName][key]['city_caption']);
	  nearest_city_title.show();
	 }
	 else
	 {
	  nearest_city_title.find('.nearest-city-name').html('');
	  nearest_city_title.hide();
	 }
	}
	
	function loadServices(cityName) {
		var desc = $('.page-services .description');
		desc.css('visibility', 'hidden');
		if (city[cityName] != undefined) {
			var realName = '';
			$('.map-wrapper .city').each(function() {
				if ($(this).data('city') == cityName || $(this).data('region') == cityName) {
					realName = $(this).find('text').text();

				    $('.region path').each(function() { $(this)[0].removeAttribute('class'); });
				    $('.region.' + $(this).data('region') + ' path')[0].setAttribute('class', 'selected');
					
					return false;
				}
			});
			if (realName != '') {
				desc.find('.city').text(realName);
				loadDescription(city[cityName].service.city == undefined ? cityName : city[cityName].service.city, true, city[cityName].service.city != undefined);
				loadDescription(city[cityName].office.city == undefined ? cityName : city[cityName].office.city, false, city[cityName].office.city != undefined);
				desc.css('visibility', 'visible');
			}			
		}
	}
	
	var mx = 730 / 478.022, my = 520 / 336.25, defaultCity = 'dnepropetrovsk';
	
	$('.map-wrapper .city').each(function() {
		var cityName = $(this).data($(this).data('city') == undefined ? 'region' : 'city');
		if (city[cityName] != undefined) {
			var realName = $(this).find('text').text();
			if (cityName == defaultCity) $('.dropdown-toggle .title').text(realName);
			$('.page-services .dropdown-menu').append(
				'<li role="presentation">' +
					'<a role="menuitem" tabindex="-1" href="javascript:void(0)" data-city="' + cityName + '">' + realName + '</a>' +
				'</li>'
			);
			var noOffice = city[cityName].office.city != undefined;
			var noService = city[cityName].service.city != undefined;
			var marker = $(this).find('circle');
			var x = Math.round(mx * marker.attr('cx'));
			var y = Math.round(my * marker.attr('cy')) + Math.round((300 - marker.attr('cy')) / 70);
			$('.map-wrapper').append(
				'<div class="services' + (noService ? ' no-service' : '') + (noOffice ? ' no-office' : (city[cityName].side == undefined ? '' : ' ' + city[cityName].side)) + 
				'" style="left: ' + x + 'px; top: ' + y + 'px;" data-region="' + $(this).data('region') + '"' + 
				($(this).data('city') == undefined ? '' : ' data-city="' + $(this).data('city') + '"') + '></div>'
			);
		}
	});
	
	$('.page-services .dropdown-menu a').click(function() {
		$('.page-services .dropdown-toggle .title').text($(this).text());
		loadServices($(this).data('city'));
	});
	
	$('.page-services .map-wrapper .services').mouseenter(function() {
		var region = $(this).data('region'), parentCity = $(this).data('city') == undefined ? false : $(this).data('city');
		$('.region.' + region + ' path').css('fill', '#cacacb');
		$('.map-wrapper .city').each(function() {
			var childCity = $(this).data('city') == undefined ? false : $(this).data('city');
			if ($(this).data('region') == region && parentCity == childCity) {
				$(this).find('text').css('fill', '#e3000f');
				return false;
			}
		});
	});
	
	$('.page-services .map-wrapper .services').mouseleave(function() {
		var region = $(this).data('region'), parentCity = $(this).data('city') == undefined ? false : $(this).data('city');
		$('.region.' + region + ' path').css('fill', '#dfe0e0');
		$('.map-wrapper .city').each(function() {
			var childCity = $(this).data('city') == undefined ? false : $(this).data('city');
			if ($(this).data('region') == region && parentCity == childCity) {
				$(this).find('text').css('fill', 'black');
				return false;
			}
		});
	});	
	
	$('.page-services .map-wrapper .services').click(function() {
		loadServices($(this).data($(this).data('city') == undefined ? 'region' : 'city'));
	});	
	
 /*
	if (mhWidth > 767 && mhWidth < 990 && $('#slider').html() != undefined) {
		var mhScale = 960 / parseInt($('.container').width());
		$('#slider').css('width', Math.round(1766 / mhScale) + 'px').css('margin-left', '-' + Math.round(883 / mhScale) + 'px');
		$('#header').css('height', Math.round(488 / mhScale) + 94 + 'px');		
	}
*/	
});